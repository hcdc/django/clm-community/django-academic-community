# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
"""testproject test URL Configuration.

This module contains the urls for testing the application with pytest.
"""
from django.contrib import admin
from django.urls import include, path

from academic_community.members import urls_apphook

urlpatterns = [
    # add the apphooks here to make sure they are registered in the urlconf
    path("accounts/", include("django.contrib.auth.urls")),
    path("members/", include("academic_community.members.urls")),
    path(
        "members/",
        include(
            (urls_apphook.urlpatterns, "member_area"), namespace="member_area"
        ),
        name="member_area",
    ),
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    path("admin/", admin.site.urls),
    path("institutions/", include("academic_community.institutions.urls")),
    path("topics/", include("testproject._topics_test_urls")),
    path("activities/", include("academic_community.activities.urls")),
    path("events/", include("academic_community.events.urls")),
    path("", include("academic_community.urls")),
]
