# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0
from __future__ import annotations

from typing import TYPE_CHECKING, Dict

if TYPE_CHECKING:
    import environ


def config(env: environ.Env) -> Dict:
    service_name = (
        env("DATABASE_SERVICE_NAME", default="postgres")
        .upper()
        .replace("-", "_")
    )
    engine = "django.db.backends.postgresql"
    name = env(f"{service_name}_DATABASE", default="django_db")
    return {
        "ENGINE": engine,
        "NAME": name,
        "USER": env(f"{service_name}_USER", default="django_user"),
        "PASSWORD": env(f"{service_name}_PASSWORD", default="changeme!"),
        "HOST": env(f"{service_name}_SERVICE_HOST", default="localhost"),
        "PORT": env(f"{service_name}_SERVICE_PORT", default="5432"),
    }
