"""Urls of the :mod:`topics` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import include, path, re_path

from academic_community.topics import views

app_name = "topics"

urlpatterns = [
    path("new/", views.TopicCreateView.as_view(), name="topic-create"),
    path(
        "export/",
        views.registry.get_export_list_view().as_view(),
        name="topic-rest-list",
    ),
    path("<slug>/", views.TopicDetailView.as_view(), name="topic-detail"),
    path(
        "<slug>/export/",
        views.registry.get_export_view().as_view(),
        name="topic-rest-detail",
    ),
    path(
        "<slug>/uploads/",
        include(views.TopicMaterialRelationViewSet().urls),
    ),
    path(
        "<slug>/channels/",
        include(views.TopicChannelRelationViewSet().urls),
    ),
    path(
        "<topic_slug>/signup/",
        views.TopicMembershipCreate.as_view(),
        name="topicmembership-request",
    ),
    path("<slug>/edit/", views.TopicUpdateView.as_view(), name="edit-topic"),
    re_path(
        r"(?P<pk>\d+)/edit/field/(?:(?P<language>en)/)?",
        views.TopicFieldUpdate.as_view(),
        name="edit-topic-field",
    ),
    path("<slug>/clone/", views.TopicCloneView.as_view(), name="clone-topic"),
    path(
        "<slug>/edit/members/",
        views.TopicMembershipFormsetView.as_view(),
        name="topicmembership-formset",
    ),
    path(
        "<slug>/edit/members/<pk>/approve/",
        views.TopicMembershipApproveView.as_view(),
        name="topicmembership-approve",
    ),
    path(
        "<slug>/history/",
        views.TopicRevisionList.as_view(),
        name="topic-history",
    ),
]
