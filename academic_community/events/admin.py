"""Admins for the events app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from aldryn_apphooks_config.admin import BaseAppHookConfig, ModelAppHookConfig
from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from reversion_compare.admin import CompareVersionAdmin

from academic_community.admin import ManagerAdminMixin
from academic_community.events import models


@admin.register(models.EventsConfig)
class EventsConfigAdmin(
    BaseAppHookConfig, GuardedModelAdmin, admin.ModelAdmin
):
    def get_config_fields(self):
        return ("single_event_mode",)

    pass


@admin.register(models.Event)
class EventAdmin(
    ManagerAdminMixin,
    GuardedModelAdmin,
    ModelAppHookConfig,
    CompareVersionAdmin,
):
    filter_horizontal = [
        "orga_team",
        "submission_groups",
        "registration_groups",
        "view_programme_groups",
        "event_view_groups",
        "view_connections_groups",
        "activities",
        "submission_licenses",
        "submission_upload_licenses",
    ]

    readonly_fields = [
        "orga_group",
        "submission_closed",
        "registration_closed",
    ]

    search_fields = [
        "name",
        "slug",
        "abstract",
        "description",
        "activities__name",
        "activities__abbreviation",
    ]
