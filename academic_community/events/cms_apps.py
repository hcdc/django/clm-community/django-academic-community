# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from aldryn_apphooks_config.app_base import CMSConfigApp
from cms.apphook_pool import apphook_pool
from django.urls import include, path
from django.utils.translation import gettext as _

from academic_community.events.cms_appconfig import EventsConfig


@apphook_pool.register
class EventsApphook(CMSConfigApp):
    app_name = "events"
    name = _("Events")

    app_config = EventsConfig

    def get_config(self, namespace):
        return super().get_config(namespace.split(":")[0])

    def get_urls(self, page=None, language=None, **kwargs):
        from academic_community.events.models import Event

        if page is not None and page.application_namespace:
            config: EventsConfig = self.get_config(page.application_namespace)
            if config.single_event_mode:
                event = Event.objects.filter(app_config=config).first()
                if event:
                    return [
                        path(
                            "",
                            include("academic_community.events.urls_single"),
                        )
                    ]
                else:
                    return []
        return ["academic_community.events.urls"]
