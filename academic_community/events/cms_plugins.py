# utility plugins for academic_community

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from typing import List, Optional

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib.auth.models import Group
from django.db.models import Q, QuerySet
from django.urls import reverse
from django.utils.translation import gettext as _
from guardian.shortcuts import get_anonymous_user, get_objects_for_user

from academic_community import utils
from academic_community.cms_plugins import (
    ListPublisher,
    PaginationPublisherMixin,
)
from academic_community.events import forms, models
from academic_community.members.models import CommunityMember
from academic_community.uploaded_material.models import License


@plugin_pool.register_plugin
class EventCardPublisher(CMSPluginBase):
    """A plugin to display content only if the user is authenticated."""

    model = models.EventPluginModel
    name = _("Event Card")
    module = _("Community Events")
    render_template = "events/components/event_card.html"
    cache = False

    fieldsets = [
        (
            None,
            {
                "fields": (
                    "event",
                    ("show", "show_abstract", "show_logo"),
                )
            },
        ),
        (
            _("Advanced settings"),
            {"classes": ("collapse",), "fields": ("card_class",)},
        ),
    ]

    autocomplete_fields = ["event"]

    def render(self, context, instance: models.EventPluginModel, placeholder):
        context.update(
            {
                "event": instance.event,
                "card_class": instance.card_class,
                "show": instance.show,
                "hide_description": not instance.show_abstract,
                "show_logo": instance.show_logo,
                "base_id": f"event-plugin-{instance.pk}-",
            }
        )
        return context


class EventListPublisherBase(PaginationPublisherMixin, ListPublisher):
    """A plugin to display material to the user."""

    module = _("Events")
    render_template = "events/components/event_listplugin.html"
    cache = False

    list_model = models.Event

    context_object_name = "events"

    def get_query(
        self, instance: models.AbstractEventListPluginModel
    ) -> Optional[Q]:
        """Get the query for the members."""
        query = None

        queries: List[Q] = []

        query = models.EventQueryset.get_query(
            past=instance.past_events,
            upcoming=instance.future_events,
            current=instance.ongoing_events,
        )
        if query:
            queries.append(query)

        if instance.app_config:
            queries.append(Q(app_config=instance.app_config))

        for q in queries:
            query = q if query is None else (query & q)  # type: ignore[operator]
        return query

    def filter_list_queryset(
        self,
        context,
        instance: models.AbstractEventListPluginModel,
        queryset: QuerySet[models.Event],
    ) -> QuerySet[models.Event]:
        """Get a queryset of the filtered events"""
        if (
            not instance.past_events
            and not instance.future_events
            and not instance.ongoing_events
        ):
            return models.Event.objects.none()
        query = self.get_query(instance)
        if query is not None:
            queryset = queryset.filter(query)
        if instance.check_permissions:
            user = context["request"].user
            if user.is_anonymous:
                user = get_anonymous_user()
            queryset = get_objects_for_user(user, "view_event", queryset)
        return queryset

    def render(
        self,
        context,
        instance: models.AbstractEventListPluginModel,  # type: ignore[override]
        placeholder,
    ):
        context = super().render(context, instance, placeholder)

        context.update(
            {
                "card_class": instance.card_class,
                "show": instance.show,
                "hide_description": not instance.show_abstract,
                "show_logo": instance.show_logo,
                "base_id": f"event-plugin-{instance.pk}-",
            }
        )
        return context


@plugin_pool.register_plugin
class EventListPublisher(EventListPublisherBase):
    """A plugin to display selected community events"""

    name = _("Selected Events")
    model = models.EventListPluginModel

    filter_horizontal = ["events"]

    def get_full_queryset(
        self, context, instance: models.EventListPluginModel
    ):
        """Get the activities to display"""
        if instance.show_all:
            return super().get_full_queryset(context, instance)
        else:
            return instance.events.all()


@plugin_pool.register_plugin
class EventRegistrationButtonPublisher(CMSPluginBase):
    """A plugin to display content only if the user is authenticated."""

    model = models.EventRegistrationButtonPluginModel
    name = _("Registration button")
    module = _("Community Events")
    render_template = "events/components/buttons/registration.html"

    fieldsets = [
        (
            None,
            {
                "fields": (
                    "event",
                    "display_button",
                )
            },
        ),
        (
            _("Advanced settings"),
            {"classes": ("collapse",), "fields": ("button_class",)},
        ),
    ]

    def render(
        self,
        context,
        instance: models.EventRegistrationButtonPluginModel,
        placeholder,
    ):
        context.update(
            {
                "event": instance.event,
                "button_class": instance.button_class,
                "display_button": instance.display_button,
            }
        )
        return context


@plugin_pool.register_plugin
class EventSubmissionButtonPublisher(EventRegistrationButtonPublisher):
    """A plugin to display content only if the user is authenticated."""

    model = models.EventSubmissionButtonPluginModel
    name = _("Submission button")
    render_template = "events/components/buttons/submission.html"


@plugin_pool.register_plugin
class CreateEventButtonPublisher(CMSPluginBase):
    """A plugin for rendering a modal button to create a new event."""

    model = models.CreateEventButtonPluginModel
    name = _("Button to add a new event")
    module = _("Community Events")

    render_template = "events/components/create_event_button_plugin.html"

    filter_horizontal = [
        "event_view_groups",
        "view_programme_groups",
        "view_connections_groups",
        "registration_groups",
        "submission_groups",
        "submission_licenses",
        "submission_upload_licenses",
        "orga_team",
        "activities",
    ]

    def render(
        self,
        context,
        instance: models.CreateEventButtonPluginModel,
        placeholder,
    ):
        """Render the context of the queryset."""
        context["instance"] = instance

        request = context["request"]
        user = request.user

        has_perm = user.has_perm("events.add_event") and utils.has_perm(
            user, "events.add_event_for_namespace", instance.app_config
        )

        if has_perm:
            initial = {
                "event_view_groups": instance.event_view_groups.all(),
                "view_programme_groups": instance.view_connections_groups.all(),
                "view_connections_groups": instance.view_connections_groups.all(),
                "registration_groups": instance.registration_groups.all(),
                "submission_groups": instance.submission_groups.all(),
                "submission_licenses": instance.submission_licenses.filter(
                    active=True
                ),
                "submission_upload_licenses": instance.submission_upload_licenses.filter(
                    active=True
                ),
                "activities": instance.activities.all(),
                "registration_detail_form": instance.registration_detail_form,
                "submission_for_activity": instance.submission_for_activity,
                "submission_for_session": instance.submission_for_session,
                "single_session_mode": instance.single_session_mode,
            }

            organizer_pks = list(
                instance.orga_team.values_list("pk", flat=True)
            )

            if hasattr(user, "communitymember"):
                organizer_pks.append(user.communitymember.pk)  # type: ignore

            if organizer_pks:
                initial["orga_team"] = CommunityMember.objects.filter(
                    pk__in=organizer_pks
                )
            context["form"] = forms.EventForm(
                initial=initial, prefix="event-form-%i" % instance.pk
            )

            context["target_url"] = reverse(
                instance.app_config.namespace + ":event-create"
            )

        return context

    def get_changeform_initial_data(self, request):
        """Get the intial data for the change form.

        This is the same as for the standard EventCreateView.
        """
        initial = super().get_changeform_initial_data(request)
        registered = utils.get_group_names("MEMBERS", "DEFAULT")
        all_groups = registered + utils.get_group_names("ANONYMOUS")
        registered_groups = Group.objects.filter(name__in=registered)
        with_anonymous = Group.objects.filter(name__in=all_groups)
        initial.update(
            {
                "event_view_groups": with_anonymous,
                "view_programme_groups": with_anonymous,
                "view_connections_groups": registered_groups,
                "registration_groups": registered_groups,
                "submission_groups": registered_groups,
                "submission_licenses": License.objects.filter(active=True),
                "submission_upload_licenses": License.objects.filter(
                    active=True
                ),
            }
        )
        return initial
