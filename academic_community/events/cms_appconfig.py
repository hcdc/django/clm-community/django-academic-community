# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App Config for the activities app
"""

from typing import Dict, Optional, Tuple

from aldryn_apphooks_config.models import AppHookConfig
from aldryn_apphooks_config.utils import setup_config
from app_data import AppDataForm
from django.db import models
from django.urls import reverse


class EventsConfig(AppHookConfig):
    """Apphook config for events."""

    class Meta:
        permissions = (
            (
                "add_event_for_namespace",
                "Can add a new event under the namespace",
            ),
        )

    single_event_mode = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=(
            "Enable this option if you plan to only serve one single event "
            "on this page. If this is not enabled, individual events will "
            "be available under their configured url slug below this page."
        ),
    )

    def fix_reverse_target(self, target: str) -> str:
        return "%s:%s" % (self.namespace, target)

    def fix_reverse_args(self, args: Tuple) -> Tuple:
        if self.single_event_mode and args:
            return args[1:]
        return args

    def fix_reverse_kwargs(self, kwargs: Dict) -> Dict:
        if self.single_event_mode and kwargs:
            kwargs.pop("event_slug", None)
        return kwargs

    def reverse(
        self,
        target,
        args: Optional[Tuple] = None,
        kwargs: Optional[Dict] = None,
    ):
        """Reverse to a specific view."""
        target = self.fix_reverse_target(target)
        if args:
            args = self.fix_reverse_args(args)
            return reverse(target, args=args)
        elif kwargs:
            self.fix_reverse_kwargs(kwargs)
            return reverse(target, kwargs=kwargs)
        else:
            return reverse(target)


class EventsConfigForm(AppDataForm):
    pass


setup_config(EventsConfigForm, EventsConfig)
