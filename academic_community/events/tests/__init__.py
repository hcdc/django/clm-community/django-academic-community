# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING

import pytest
import reversion

from academic_community.events.models import Event, EventsConfig
from academic_community.uploaded_material.models import License

if TYPE_CHECKING:
    from psycopg2.extras import DateTimeTZRange


@pytest.fixture
def event(db, current_range: DateTimeTZRange) -> Event:
    app_config = EventsConfig.objects.first()
    if not app_config:
        app_config = EventsConfig.objects.create(
            type="academic_community.events.cms_appconfig.EventsConfig",
            namespace="events",
            app_data={},
        )
    with reversion.create_revision():
        ret = Event.objects.get_or_create(
            name="Test Event",
            slug="test-event",
            time_range=current_range,
            single_session_mode=False,
            app_config=app_config,
        )[0]
        ret.submission_licenses.add(*License.objects.all_active())
    return ret
