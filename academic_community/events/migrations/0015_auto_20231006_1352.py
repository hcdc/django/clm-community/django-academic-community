# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.21 on 2023-10-06 11:52

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("events", "0014_auto_20230917_2244"),
    ]

    operations = [
        migrations.AddField(
            model_name="eventlistpluginmodel",
            name="show_all",
            field=models.BooleanField(
                default=False,
                help_text="Show all events, no matter what you selected above.",
            ),
        ),
        migrations.AlterField(
            model_name="eventlistpluginmodel",
            name="events",
            field=models.ManyToManyField(
                blank=True,
                help_text="The events to display.",
                to="events.Event",
            ),
        ),
    ]
