# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.7 on 2022-03-30 11:28

import django_reactive.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("events", "0005_auto_20220324_1532"),
    ]

    operations = [
        migrations.AddField(
            model_name="event",
            name="registration_detail_form",
            field=django_reactive.fields.ReactJSONSchemaField(
                default=dict,
                help_text="Create a form to ask for more details for a registration.",
                null=True,
            ),
        ),
    ]
