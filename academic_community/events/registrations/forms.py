# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django_reactive.widget import ReactJSONSchemaModelForm

from academic_community.events.models import Event
from academic_community.events.registrations import models

SCHEMA = {
    "type": "list",
    "items": {
        "type": "dict",
        "keys": {
            "label": {"type": "string"},
            "link": {"type": "string"},
            "new_tab": {"type": "boolean", "title": "Open in new tab"},
        },
    },
}


class RegistrationAdminForm(ReactJSONSchemaModelForm):
    """An admin form for registrations."""

    class Meta:
        model = models.Registration
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        event = self.get_initial_for_field(self.fields["event"], "event")

        if event is not None:
            if not isinstance(event, Event):
                event = Event.objects.get(pk=event)

            schema = event.registration_detail_schema

            if schema:
                field = self.fields["details"]
                field.schema = field.widget.schema = schema
                field.ui_schema = field.widget.ui_schema = {}


class RegistrationForm(RegistrationAdminForm):
    """A form for registration."""

    class Meta:
        model = models.Registration
        fields = ["member", "event", "details"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if getattr(self.instance, "member", None) or self.initial.get(
            "member"
        ):
            self.fields["member"].disabled = True
        self.fields["event"].disabled = True

        event = self.get_initial_for_field(self.fields["event"], "event")

        if event is not None:
            if not isinstance(event, Event):
                event = Event.objects.get(pk=event)

            if not event.registration_detail_schema:
                self.fields.pop("details")
        else:
            self.fields.pop("details")
