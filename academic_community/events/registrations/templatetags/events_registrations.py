"""Template tags to check registrations."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING, Optional

from django import template

if TYPE_CHECKING:
    from academic_community.events.models import Event
    from academic_community.events.registrations.models import Registration
    from academic_community.members.models import CommunityMember


register = template.Library()


@register.filter
def is_registered(
    member: CommunityMember, event: Event
) -> Optional[Registration]:
    """Test if the community member is registered for the event"""
    return member.event_registration.filter(event=event).first()
