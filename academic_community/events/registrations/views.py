# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from django.views import generic

from academic_community.events.registrations import forms, models
from academic_community.events.views import (
    EventContextMixin,
    EventPermissionMixin,
)
from academic_community.history.views import RevisionMixin
from academic_community.utils import PermissionRequiredMixin


class RegistrationCreateView(
    RevisionMixin,
    EventContextMixin,
    EventPermissionMixin,
    generic.edit.CreateView,
):
    """A view for registering to the assembly."""

    model = models.Registration

    form_class = forms.RegistrationForm

    permission_required = "events.change_event"

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        initial = kwargs.setdefault("initial", {})
        initial["event"] = self.event
        return kwargs


class SelfRegistrationCreateView(RegistrationCreateView):
    """A view where the user can register him or herself."""

    permission_required = "events.register_for_event"

    template_name_suffix = "_form_self"

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        if hasattr(self.request.user, "communitymember"):
            initial = kwargs.setdefault("initial", {})
            initial["member"] = self.request.user.communitymember
        return kwargs


class RegistrationListView(
    EventContextMixin,
    EventPermissionMixin,
    generic.ListView,
):
    """A view for registering to the assembly."""

    model = models.Registration

    permission_required = "events.change_event"

    def get_queryset(self):
        return self.event.registration_set.all().order_by(
            "member__last_name", "member__first_name"
        )


class RegistrationDeleteView(  # type: ignore[misc]
    EventContextMixin,
    RevisionMixin,
    PermissionRequiredMixin,
    generic.edit.DeleteView,
):
    """A view for registering to the assembly."""

    model = models.Registration

    def get_success_url(self) -> str:
        return self.event.app_config.reverse(
            "registrations:registration-create", args=(self.event.slug,)
        )

    permission_required = "registrations.delete_registration"
