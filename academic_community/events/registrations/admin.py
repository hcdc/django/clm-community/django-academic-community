# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from reversion.models import Version
from reversion_compare.admin import CompareVersionAdmin

from academic_community.admin import ManagerAdminMixin
from academic_community.events.registrations import forms, models


@admin.register(models.Registration)
class RegistrationAdmin(
    ManagerAdminMixin, GuardedModelAdmin, CompareVersionAdmin
):
    """An admin view for registrations."""

    form = forms.RegistrationAdminForm

    search_fields = [
        "member__first_name",
        "member__last_name",
        "member__email__email",
    ]

    list_display = [
        "member",
        "event",
        "member_email",
        "registration_date",
    ]

    list_filter = [
        "event",
    ]

    def member_email(self, obj):
        if obj.member.email:
            return obj.member.email.email
        else:
            return "--"

    member_email.short_description = "Email"  # type: ignore  # noqa: E501

    def registration_date(self, obj):
        version = Version.objects.get_for_object(obj).last()
        if version is not None:
            return version.revision.date_created.date().isoformat()
        else:
            return ""
