"""Urls of the :mod:`events` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import re_path

from academic_community.events.registrations import views

app_name = "registrations"

urlpatterns = [
    re_path(
        r"^register/?$",
        views.SelfRegistrationCreateView.as_view(),
        name="registration-create",
    ),
    re_path(
        r"^registrations/?$",
        views.RegistrationListView.as_view(),
        name="registration-list",
    ),
    re_path(
        r"^registrations/new/?$",
        views.RegistrationCreateView.as_view(),
        name="registration-create-admin",
    ),
    re_path(
        r"^registrations/(?P<pk>\d+)/?$",
        views.RegistrationDeleteView.as_view(),
        name="registration-delete",
    ),
]
