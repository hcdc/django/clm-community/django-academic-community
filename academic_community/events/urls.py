"""Urls of the :mod:`events` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import include, path

from academic_community.events import views

app_name = "events"

urlpatterns = [
    path("new/", views.EventCreateView.as_view(), name="event-create"),
    path("<event_slug>/", include("academic_community.events.urls_single")),
]
