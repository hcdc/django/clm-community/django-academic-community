"""Urls of the :mod:`events` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import include, path

from academic_community.events import views

urlpatterns = [
    path("", views.event_or_session_view, name="event-detail"),
    path(
        "notify",
        views.EventUserNotificationView.as_view(),
        name="event-notify",
    ),
    path("edit/", views.EventUpdateView.as_view(), name="edit-event"),
    path("", include("academic_community.events.programme.urls")),
    path("", include("academic_community.events.registrations.urls")),
]
