# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.core.management.base import BaseCommand

from academic_community.management.utils import log_progress


class Command(BaseCommand):
    """Django command to generate authors and affiliations.

    This command takes all the authors and academic organizations in the
    database and creates an author/affiliation for it.
    """

    help = """
        Generate authors and affiliations.

        This command takes all the authors and academic organizations in the
        database and creates an author/affiliation for it.
        """

    def add_arguments(self, parser):
        """Add connection arguments to the parser."""

        parser.add_argument(
            "-db",
            "--database",
            help=(
                "The Django database identifier (see settings.py), "
                "default: %(default)s"
            ),
            default="default",
        )

    def handle(self, *args, **options):
        """Migrate the database."""
        from academic_community.events.programme.models import (
            Affiliation,
            Author,
            PresentationType,
        )
        from academic_community.institutions.models import Institution
        from academic_community.members.models import CommunityMember

        database = options["database"]

        print("Creating authors")
        for member in log_progress(CommunityMember.objects.all()):
            Author.objects.using(database).create(
                first_name=member.first_name,
                last_name=member.last_name,
                orcid=member.orcid,
                member=member,
            )

        print("Creating affiliations")
        for organization in log_progress(Institution.objects.all()):
            name = organization.name
            if organization.city:
                country = organization.city.country
            else:
                country = None
            Affiliation.objects.using(database).create(
                name=name,
                organization=organization,
                country=country,
            )

        PresentationType.objects.using(database).create(name="Poster")
        PresentationType.objects.using(database).create(name="Talk")
