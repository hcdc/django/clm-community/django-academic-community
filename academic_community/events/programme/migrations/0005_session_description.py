# Generated by Django 3.1.3 on 2021-09-17 13:42

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import djangocms_text_ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("programme", "0004_auto_20210824_1607"),
    ]

    operations = [
        migrations.AddField(
            model_name="session",
            name="description",
            field=djangocms_text_ckeditor.fields.HTMLField(
                blank=True,
                help_text="Optional longer description of this session, agenda, etc.. Will be displayed on the detail page.",
                null=True,
            ),
        ),
    ]
