# Generated by Django 3.1.3 on 2021-06-21 23:12

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import colorfield.fields
import django.db.models.deletion
import djangocms_text_ckeditor.fields
from django.db import migrations, models

import academic_community.history.models


class Migration(migrations.Migration):
    dependencies = [
        ("members", "0005_auto_20210528_1324"),
        ("programme", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="MeetingRoom",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        help_text="Name of the meeting room",
                        max_length=100,
                        unique=True,
                    ),
                ),
                (
                    "url",
                    models.URLField(
                        help_text="URL to connect to the meeting room",
                        max_length=400,
                    ),
                ),
                (
                    "description",
                    djangocms_text_ckeditor.fields.HTMLField(
                        help_text="Optional description of the meeting room"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Session",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "start",
                    models.DateTimeField(
                        blank=True,
                        help_text="Start time of the session",
                        null=True,
                    ),
                ),
                (
                    "duration",
                    models.DurationField(
                        blank=True,
                        help_text="Duration of the session",
                        null=True,
                    ),
                ),
                (
                    "title",
                    models.CharField(
                        help_text="Title (name) of the session", max_length=200
                    ),
                ),
                (
                    "abstract",
                    djangocms_text_ckeditor.fields.HTMLField(
                        blank=True,
                        help_text="What is this session all about?",
                        max_length=4000,
                        null=True,
                    ),
                ),
                (
                    "conveners",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Conveners for the session.",
                        to="members.CommunityMember",
                    ),
                ),
                (
                    "meeting_rooms",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Meeting rooms that are used within this session.",
                        to="programme.MeetingRoom",
                    ),
                ),
            ],
            options={
                "ordering": ["start"],
                "permissions": (
                    (
                        "schedule_session",
                        "Can set time and date for a session",
                    ),
                    (
                        "schedule_slots",
                        "Can set time and date for the session slots",
                    ),
                ),
            },
            bases=(  # type: ignore[arg-type]
                academic_community.history.models.RevisionMixin,
                models.Model,
            ),
        ),
        migrations.AlterModelOptions(
            name="contribution",
            options={
                "ordering": ["start"],
                "permissions": (
                    ("schedule_slot", "Can set time and date for the slot"),
                    ("accept_contribution", "Can accept the contribution"),
                    ("withdraw_contribution", "Can withdraw the contribution"),
                    (
                        "change_activity",
                        "Can change the activity of the contribution",
                    ),
                ),
            },
        ),
        migrations.AddField(
            model_name="contribution",
            name="duration",
            field=models.DurationField(
                blank=True, help_text="Duration of the session", null=True
            ),
        ),
        migrations.AddField(
            model_name="contribution",
            name="start",
            field=models.DateTimeField(
                blank=True, help_text="Start time of the session", null=True
            ),
        ),
        migrations.AddField(
            model_name="presentationtype",
            name="color",
            field=colorfield.fields.ColorField(
                default="#3788d8",
                help_text="Color for this presentation type in the calendar.",
                max_length=18,
            ),
        ),
        migrations.AddField(
            model_name="presentationtype",
            name="font_color",
            field=colorfield.fields.ColorField(
                default="#fff",
                help_text="Color for this presentation type in the calendar.",
                max_length=18,
            ),
        ),
        migrations.AddField(
            model_name="presentationtype",
            name="for_contributions",
            field=models.BooleanField(
                default=True,
                help_text="Can this presentation type be used for submitted contributions?",
            ),
        ),
        migrations.AddField(
            model_name="presentationtype",
            name="for_sessions",
            field=models.BooleanField(
                default=False,
                help_text="Can this presentation type be used for sessions?",
            ),
        ),
        migrations.AlterField(
            model_name="author",
            name="contributions",
            field=models.ManyToManyField(
                blank=True,
                help_text="Contributions for this author",
                related_name="contributing_author",
                through="programme.ContributingAuthor",
                to="programme.Contribution",
            ),
        ),
        migrations.AlterField(
            model_name="contribution",
            name="presentation_type",
            field=models.ForeignKey(
                blank=True,
                help_text="Type of the contribution. You can also leave this open and it will be decided at a later point.",
                limit_choices_to={"for_contributions": True},
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="programme.presentationtype",
            ),
        ),
        migrations.CreateModel(
            name="Slot",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "start",
                    models.DateTimeField(
                        blank=True,
                        help_text="Start time of the session",
                        null=True,
                    ),
                ),
                (
                    "duration",
                    models.DurationField(
                        blank=True,
                        help_text="Duration of the session",
                        null=True,
                    ),
                ),
                (
                    "title",
                    models.CharField(
                        help_text="Title of the contribution", max_length=200
                    ),
                ),
                (
                    "comment",
                    models.TextField(
                        blank=True,
                        help_text="Any other comments to the Organizing Committee/Working Group Leader.",
                        max_length=1000,
                        null=True,
                        verbose_name="Other comments",
                    ),
                ),
                (
                    "abstract",
                    djangocms_text_ckeditor.fields.HTMLField(
                        blank=True,
                        help_text="Please provide an abstract (max. 4000 characters)",
                        max_length=4000,
                        null=True,
                    ),
                ),
                (
                    "organizing_members",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Responsible members for this item.",
                        to="members.CommunityMember",
                    ),
                ),
                (
                    "presentation_type",
                    models.ForeignKey(
                        blank=True,
                        help_text="Type of the contribution.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="programme.presentationtype",
                    ),
                ),
                (
                    "session",
                    models.ForeignKey(
                        help_text="Parent session that contains this slot.",
                        on_delete=django.db.models.deletion.CASCADE,
                        to="programme.session",
                    ),
                ),
            ],
            options={
                "ordering": ["start"],
            },
            bases=(  # type: ignore[arg-type]
                academic_community.history.models.RevisionMixin,
                models.Model,
            ),
        ),
        migrations.AddField(
            model_name="session",
            name="presentation_type",
            field=models.ForeignKey(
                blank=True,
                help_text="Type of the contribution. You can also leave this open and it will be decided at a later point.",
                limit_choices_to={"for_sessions": True},
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="programme.presentationtype",
            ),
        ),
        migrations.AddField(
            model_name="contribution",
            name="session",
            field=models.ForeignKey(
                blank=True,
                help_text="Parent session that contains this slot.",
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="programme.session",
            ),
        ),
    ]
