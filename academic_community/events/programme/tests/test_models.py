"""Test file for the programme models."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import reversion

from . import affiliation, author  # noqa: F401


@pytest.mark.parametrize(
    "attr,value",
    [
        ("first_name", "Test name"),
        ("last_name", "Test name"),
        ("orcid", "123"),
    ],
)
def test_author_change(member, author, attr, value):  # noqa: F811
    """ "Test changing the name of a member."""
    with reversion.create_revision():
        setattr(member, attr, value)
        member.save()
    author.refresh_from_db()
    assert getattr(author, attr) == getattr(member, attr)


def test_affiliation_change(affiliation, institution):  # noqa: F811
    """ "Test changing the name of a member."""
    with reversion.create_revision():
        institution.name = "Test name"
        institution.save()
    affiliation.refresh_from_db()
    assert affiliation.name == institution.name
