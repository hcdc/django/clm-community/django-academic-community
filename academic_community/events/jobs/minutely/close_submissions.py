"""Send pending emails to the users.

Different from pending notifications, pending emails are supposed to be sent
immediately.
"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import reversion
from django.utils.timezone import now
from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "Close submissions for events outside of the submission range."

    def execute(self):
        from academic_community.events import models

        qs = models.Event.objects.filter(
            submission_range__endswith__lte=now(), submission_closed=False
        )

        events = list(qs)

        with reversion.create_revision():
            qs.update(submission_closed=True)
            for event in events:
                event.submission_closed = True
                event.update_submission_permissions()
                reversion.add_to_revision(event)
