"""Send pending emails to the users.

Different from pending notifications, pending emails are supposed to be sent
immediately.
"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import reversion
from django.utils.timezone import now
from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "Close registrations for events outside of the registration range."

    def execute(self):
        from academic_community.events import models

        qs = models.Event.objects.filter(
            registration_range__endswith__lte=now(), registration_closed=False
        )

        events = list(qs)

        with reversion.create_revision():
            qs.update(registration_closed=True)
            for event in events:
                event.registration_closed = True
                event.update_registration_permissions()
                reversion.add_to_revision(event)
