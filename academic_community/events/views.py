"""Views of the events."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING, Any, Dict

from aldryn_apphooks_config.utils import get_app_instance
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import Group
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property
from django.views import generic

from academic_community.events import forms, models
from academic_community.faqs.views import FAQContextMixin
from academic_community.history.views import RevisionMixin
from academic_community.members.models import CommunityMember
from academic_community.mixins import AppConfigMixin, NextMixin
from academic_community.notifications.views import (
    CreateOutgoingNotificationViewBase,
)
from academic_community.uploaded_material.models import License
from academic_community.utils import (
    PermissionRequiredMixin,
    get_group_names,
    has_perm,
)
from academic_community.views import register_object_page_content_renderer

if TYPE_CHECKING:
    from academic_community.events.programme.models import Session
    from academic_community.models import ObjectPageContent


class EventPermissionMixin(AppConfigMixin, PermissionRequiredMixin):
    """A mixin for permissions to get the event."""

    permission_required = "events.view_event"

    @cached_property
    def permission_object(self) -> models.Event:
        if self.config.single_event_mode:
            return get_object_or_404(models.Event, app_config=self.config)
        else:
            return get_object_or_404(
                models.Event,
                app_config=self.config,
                slug=self.kwargs["event_slug"],
            )


class EventAppConfigMixin(AppConfigMixin):
    """A mixin for getting the event from the namespace."""

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.namespace(self.namespace.split(":")[0])

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        if self.config.single_event_mode:
            return get_object_or_404(models.Event, app_config=self.config)
        else:
            return super().get_object(queryset)


class EventContextMixin(AppConfigMixin):
    """Mixin for getting the event in the template."""

    @cached_property
    def event(self) -> models.Event:
        if self.config.single_event_mode:
            return get_object_or_404(models.Event, app_config=self.config)
        else:
            return get_object_or_404(
                models.Event,
                app_config=self.config,
                slug=self.kwargs["event_slug"],  # type: ignore[attr-defined]
            )

    def get_queryset(self):
        return super().get_queryset().filter(event=self.event)  # type: ignore

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)  # type: ignore
        context["event"] = self.event
        return context


def event_or_session_view(request, event_slug=None):
    """Render an event or session depending on the event"""

    namespace, config = get_app_instance(request)

    if config.single_event_mode:
        event: models.Event = get_object_or_404(
            models.Event.objects, app_config=config
        )
    else:
        event = get_object_or_404(
            models.Event.objects, app_config=config, slug=event_slug
        )

    if event.single_session_mode:
        from academic_community.events.programme.views import SessionDetailView

        session: Session = event.session_set.first()
        return SessionDetailView.as_view()(
            request, event_slug=event_slug, pk=session.pk  # type: ignore
        )
    else:
        return EventDetailView.as_view()(request, event_slug=event.slug)


class EventDetailView(
    EventAppConfigMixin, PermissionRequiredMixin, generic.DetailView
):
    """Detail view for community events."""

    permission_required = "events.view_event"

    model = models.Event

    slug_url_kwarg = "event_slug"

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        content = (
            self.get_object().page.get_content()
            or self.get_object().page.get_draft_content()
        )
        context["content"] = content
        return context


def render_event_content(request, objectpagecontent: ObjectPageContent):
    event: models.Event = objectpagecontent.page.content_object  # type: ignore
    return EventDetailFrontendEditing.as_view()(
        request,
        event=event,
        slug=event.slug,
        content=objectpagecontent.pk,
    )


register_object_page_content_renderer(models.Event, render_event_content)


class EventDetailFrontendEditing(EventDetailView):
    """A detail view dedicated to the frontend editing."""

    template_name_suffix = "_detail_cms"

    def get_object(self, queryset=None):
        if "event" in self.kwargs:
            ret = self.kwargs["event"]
            if ret.app_config:
                self.request.current_app = ret.app_config.namespace
        else:
            ret = super().get_object(queryset)
        self.request.toolbar.set_object(ret)
        return ret

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        if "content" in self.kwargs:
            context["content"] = self.kwargs["content"]
        else:
            context["content"] = self.get_object().page.get_content()
        return context


class EventUpdateView(
    EventAppConfigMixin,
    FAQContextMixin,
    PermissionRequiredMixin,
    RevisionMixin,
    NextMixin,
    generic.edit.UpdateView,
):
    """Update view for community events."""

    permission_required = "events.change_event"

    model = models.Event

    form_class = forms.EventUpdateForm

    slug_url_kwarg = "event_slug"


class EventCreateView(
    EventAppConfigMixin,
    FAQContextMixin,
    UserPassesTestMixin,
    RevisionMixin,
    NextMixin,
    generic.edit.CreateView,
):
    """A view to create a new event."""

    model = models.Event

    form_class = forms.EventForm

    slug_url_kwarg = "event_slug"

    def test_func(self):
        user = self.request.user
        return user.has_perm("events.add_event") and has_perm(
            user, "events.add_event_for_namespace", self.config
        )

    def get_prefix(self):
        """Get the form prefix"""
        # This is reimplemented because the CreateEventButton-Plugin wants
        # to give it's own prefix
        if self.request.method == "POST":
            form_prefix = self.request.POST.get("form_prefix")
            if form_prefix:
                return form_prefix
        return super().get_prefix()

    def get_initial(self):
        initial = super().get_initial()
        registered = get_group_names("MEMBERS", "DEFAULT")
        all_groups = registered + get_group_names("ANONYMOUS")
        registered_groups = Group.objects.filter(name__in=registered)
        with_anonymous = Group.objects.filter(name__in=all_groups)
        initial.update(
            {
                "event_view_groups": with_anonymous,
                "view_programme_groups": with_anonymous,
                "view_connections_groups": registered_groups,
                "registration_groups": registered_groups,
                "submission_groups": registered_groups,
                "submission_licenses": License.objects.filter(active=True),
                "submission_upload_licenses": License.objects.filter(
                    active=True
                ),
            }
        )
        if hasattr(self.request.user, "communitymember"):
            initial["orga_team"] = CommunityMember.objects.filter(
                pk=self.request.user.communitymember.pk  # type: ignore
            )
        return initial

    def form_valid(self, form):
        form.instance.app_config = self.config
        return super().form_valid(form)


class EventUserNotificationView(
    EventPermissionMixin,
    CreateOutgoingNotificationViewBase,
):
    """Notification view for people that participate at the event."""

    permission_required = "events.change_event"

    template_name = "events/event_notification_form.html"

    def get_user_queryset(self):
        event = self.permission_object
        qs = super().get_user_queryset()
        query = (
            Q(groups=event.orga_group)
            | Q(communitymember__event_registration__event=event)
            | Q(
                communitymember__author__contribution_map__contribution__event=event
            )
            | Q(communitymember__session__event=event)
        )
        return qs.filter(query).distinct()

    def get_filterset(self, **kwargs):
        event = self.permission_object
        kwargs["field_querysets"] = {
            "presentation_type": event.presentationtype_set.filter(
                for_contributions=True
            ),
            "session": event.session_set.all(),
        }
        kwargs["event"] = event
        return super().get_filterset(**kwargs)

    def get_form(self, *args, **kwargs):
        ret = super().get_form(*args, **kwargs)
        if self.filterset.is_valid():
            self.filters["event"] = self.permission_object
        return ret

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        event = self.permission_object
        context["event"] = event
        return context
