"""Middleware objects for Django."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.utils.deprecation import MiddlewareMixin


class HideToolbarMiddleware(MiddlewareMixin):
    """Simple middleware to hide the CMS toolbar by default."""

    def process_request(self, request):
        toolbar = request.toolbar
        if (
            not toolbar.structure_mode_active
            and not toolbar.edit_mode_active
            and not toolbar.preview_mode_active
        ):
            toolbar.show_toolbar = False
