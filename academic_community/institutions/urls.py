"""Urls of the :mod:`institutions` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import path

from academic_community.institutions import views

app_name = "institutions"

urlpatterns = [
    path(
        "new/",
        views.InstitutionCreateView.as_view(),
        name="institution-create",
    ),
    path(
        "<slug>/",
        views.InstitutionDetailView.as_view(),
        name="institution-detail",
    ),
    path(
        "<slug>/history/",
        views.InstitutionRevisionList.as_view(),
        name="institution-history",
    ),
    path(
        "<slug>/edit/",
        views.InstitutionUpdate.as_view(),
        name="edit-institution",
    ),
    path(
        "<inst_slug>/departments/new/",
        views.DepartmentCreateView.as_view(),
        name="department-create",
    ),
    path(
        "<inst_slug>/departments/<pk>/",
        views.DepartmentDetailView.as_view(),
        name="department-detail",
    ),
    path(
        "<inst_slug>/departments/<pk>/edit/",
        views.DepartmentUpdate.as_view(),
        name="edit-department",
    ),
    path(
        "<inst_slug>/departments/<pk>/history/",
        views.DepartmentRevisionList.as_view(),
        name="department-history",
    ),
    path(
        "<inst_slug>/departments/<dept_pk>/units/new/",
        views.UnitCreateView.as_view(),
        name="unit-create",
    ),
    path(
        "<inst_slug>/departments/<dept_pk>/units/<pk>/",
        views.UnitDetailView.as_view(),
        name="unit-detail",
    ),
    path(
        "<inst_slug>/departments/<dept_pk>/units/<pk>/edit/",
        views.UnitUpdate.as_view(),
        name="edit-unit",
    ),
    path(
        "<inst_slug>/departments/<dept_pk>/units/<pk>/history/",
        views.UnitRevisionList.as_view(),
        name="unit-history",
    ),
]
