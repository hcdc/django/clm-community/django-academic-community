# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Plugins for the institutions app."""
from __future__ import annotations

from typing import TYPE_CHECKING, Optional

from cms.plugin_pool import plugin_pool
from django.db.models import Q
from django.utils.translation import gettext as _

from academic_community.cms_plugins import (
    ActiveFiltersPublisher,
    FilterButtonPublisher,
    FilterListPublisher,
    ListPublisher,
    PaginationPublisherMixin,
)
from academic_community.institutions import filters, models

if TYPE_CHECKING:
    from django.db.models import QuerySet


@plugin_pool.register_plugin
class InstitutionActiveFiltersPublisher(ActiveFiltersPublisher):
    """A plugin to display the applied filters for academic organizations."""

    module = _("Institutions")
    cache = False

    name = _("Display active filters for institutions")

    filterset_class = filters.InstitutionFilterSet

    list_model = models.Institution


@plugin_pool.register_plugin
class InstitutionFilterButtonPublisher(FilterButtonPublisher):
    """A plugin to display the applied filters for academic organizations."""

    module = _("Institutions")
    cache = False

    name = _("Filter button for institutions")

    filterset_class = filters.InstitutionFilterSet

    list_model = models.Institution


class AcademicOrganizationListPublisherBase(
    PaginationPublisherMixin, ListPublisher
):
    """A plugin to display academic organizations."""

    module = _("Institutions")
    render_template = "institutions/components/organization_listplugin.html"
    cache = False

    list_model = models.AcademicOrganization

    context_object_name = "organizations"

    def get_query(
        self,
        instance: models.AbstractAcademicOrganizationListPluginModel,
    ) -> Optional[Q]:
        """Get the query for the members."""
        return None

    def filter_list_queryset(
        self,
        context,
        instance: models.AbstractAcademicOrganizationListPluginModel,
        queryset: QuerySet[models.AcademicOrganization],
    ) -> QuerySet[models.AcademicOrganization]:
        """Get a queryset of the filtered members"""
        queryset = super().filter_list_queryset(context, instance, queryset)
        query = self.get_query(instance)
        if query is not None:
            queryset = queryset.filter(query)
        return queryset

    def render(
        self,
        context,
        instance: models.AbstractAcademicOrganizationListPluginModel,  # type: ignore[override]
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        context["show_logo"] = instance.show_institution_logos
        context["base_id"] = f"institution-plugin-{instance.pk}-"
        queryset = context["object_list"]
        if instance.show_parent_organizations:
            context["top_organization"] = "institution"
        else:
            if any(queryset.values_list("institution", flat=True)):
                context["top_organization"] = "institution"
            elif any(queryset.values_list("department", flat=True)):
                context["top_organization"] = "department"
            else:
                context["top_organization"] = "unit"
        if instance.former_institutions and instance.active_institutions:
            context["active_status"] = "all"
        elif instance.former_institutions:
            context["active_status"] = "inactive"
        elif instance.active_institutions:
            context["active_status"] = "active"
        context["render_children"] = instance.show_child_organizations
        context["render_all_children"] = instance.show_all_child_organizations
        return context


@plugin_pool.register_plugin
class AcademicOrganizationListPublisher(AcademicOrganizationListPublisherBase):
    """A plugin to display selected organizations"""

    name = _("Selected Organizations")
    model = models.AcademicOrganizationListPluginModel

    filter_horizontal = ["organizations"]

    def get_full_queryset(
        self, context, instance: models.AcademicOrganizationListPluginModel
    ) -> QuerySet:
        if instance.show_all:
            return super().get_full_queryset(context, instance)
        else:
            return instance.organizations.all()


@plugin_pool.register_plugin
class InstitutionListPublisher(  # type: ignore[misc]
    AcademicOrganizationListPublisher, FilterListPublisher
):
    """A plugin to display selected institutions."""

    name = _("Selected Institutions")

    model = models.InstitutionListPluginModel

    render_template = "institutions/components/institution_listplugin.html"

    list_model = models.Institution

    list_ordering = "start_date"

    filterset_class = filters.InstitutionFilterSet
