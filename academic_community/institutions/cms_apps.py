# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import gettext as _


@apphook_pool.register
class InstitutionsApphook(CMSApp):
    app_name = "institutions"
    name = _("Organizations")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["academic_community.institutions.urls"]
