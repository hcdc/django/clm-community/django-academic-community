# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from typing import Dict, List, Set, Tuple

from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool
from cms.models import CMSPlugin, Page, Placeholder
from cms.models.fields import PlaceholderRelationField
from cms.models.managers import WithUserMixin
from cms.utils.placeholder import get_placeholder_from_slot
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.utils.functional import cached_property
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from djangocms_frontend.contrib.link.models import Link
from djangocms_frontend.fields import AttributesField

from academic_community import utils


class AbstractPaginationPluginModelMixin(models.Model):
    """A plugin model that supports pagination."""

    class Meta:
        abstract = True

    paginate_by = models.PositiveIntegerField(  # type: ignore[var-annotated]
        default=30,
        null=True,
        blank=True,
        help_text=(
            "Limit the amount of items that are shown and include a "
            "pagination bar if necessary."
        ),
    )

    pagination_param = models.CharField(  # type: ignore[var-annotated]
        max_length=50,
        null=True,
        blank=True,
        help_text=(
            "The GET param that is used to select the page of the model. If "
            "empty, we will use a unique one for this instance."
        ),
    )

    show_count = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text="Display the total number of items that are available",
    )


class BaseFilterPluginModelMixin(models.Model):
    """A mixin model to display active filters."""

    class Meta:
        abstract = True

    filter_query_prefix = models.SlugField(
        null=True,
        blank=True,
        help_text=(
            "The suffix for the filter parameter in the GET request. If you "
            "have multiple plugins with filter options on a page, make sure, "
            "to give them unique suffixes."
        ),
    )

    strict_filters = models.BooleanField(
        default=True,
        help_text="Return no data at all if invalid filter parameters are "
        "provided in the URL.",
    )


class AlphabetFilterButtonsPluginModel(CMSPlugin):
    """A mixin to filter for attributes that start with a certain character."""

    filter_name = models.SlugField(  # type: ignore[var-annotated]
        max_length=50,
        help_text=(
            "The name of the filter in the requests GET parameter (including "
            "eventual filter query prefix). A `__istartswith` will be "
            "appended."
        ),
    )

    verbose_field_name = models.CharField(  # type: ignore[var-annotated]
        null=True,
        blank=True,
        max_length=50,
        help_text="An optional display name for the filter in the popup.",
    )


class FilterButtonPluginModelMixin(models.Model):
    """A mixin model to display a modal button for filters."""

    class Meta:
        abstract = True

    filter_button_text = models.CharField(
        null=True,
        blank=True,
        max_length=50,
        help_text="The text to show on the filter button.",
    )

    filter_button_attributes = AttributesField(
        help_text="Additional attributes for the filter button",
        blank=True,
    )


class FilterButtonPluginModel(  # type: ignore[django-manager-missing]
    BaseFilterPluginModelMixin, FilterButtonPluginModelMixin, CMSPlugin
):
    """A plugin model for a modal button for filters."""


class ActiveFiltersPluginModelMixin(models.Model):
    """A mixin model to display active filters."""

    class Meta:
        abstract = True

    show_filters_in_card = models.BooleanField(
        default=True, help_text="Render a card around the badges."
    )


class AbstractExportListButtonPluginModelMixin(  # type: ignore[misc]
    BaseFilterPluginModelMixin,
    AbstractPaginationPluginModelMixin,
    models.Model,
):
    """A mixin for creating buttons to export selected data."""

    class Meta:
        abstract = True

    button_text = models.CharField(  # type: ignore[var-annotated]
        null=True,
        blank=True,
        max_length=50,
        help_text="The text to show on the button.",
    )

    button_attributes = AttributesField(  # type: ignore[var-annotated]
        help_text="Additional attributes for the button",
        blank=True,
        verbose_name="Button attributes",
        default={"class": "btn btn-primary btn-round btn-download"},
    )


class ActiveFiltersPluginModel(  # type: ignore[django-manager-missing]
    BaseFilterPluginModelMixin, ActiveFiltersPluginModelMixin, CMSPlugin
):
    """A mixin model to display active filters."""


class FilterPluginModelMixin(
    BaseFilterPluginModelMixin,
    FilterButtonPluginModelMixin,
    ActiveFiltersPluginModelMixin,
    models.Model,
):
    """A cms plugin model to show filters for a list."""

    class Meta:
        abstract = True

    enable_filters = models.BooleanField(
        default=False,
        help_text="Enable the option to filter the resulting list.",
    )

    show_filter_button = models.BooleanField(
        default=True,
        help_text=(
            "Show the button to open the filtering menu (when filters are "
            "enabled)"
        ),
    )

    show_active_filters = models.BooleanField(
        default=True,
        help_text="Show the active filters (when filters are enabled)",
    )


class UserGroupBasedContent(CMSPlugin):
    """A plugin to show content to a specific user/group only."""

    is_superuser = models.BooleanField(  # type: ignore[var-annotated]
        default=True, help_text="Show the content to super users."
    )

    is_anonymous = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Display this button to users that are not logged in.",
    )

    is_authenticated = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Display this button to users that are logged in.",
    )

    is_member = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Display this button to logged-in community members.",
    )

    is_staff = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=(
            "Display this button to logged-in staff users (includes "
            "community members)."
        ),
    )

    is_manager = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Display this button to logged-in community managers.",
    )

    users = models.ManyToManyField(  # type: ignore[var-annotated]
        settings.AUTH_USER_MODEL,
        help_text="Explicit users that should see this content.",
        blank=True,
    )

    exclude_users = models.ManyToManyField(  # type: ignore[var-annotated]
        settings.AUTH_USER_MODEL,
        help_text=(
            "Users that are explicitly excluded from viewing the content."
        ),
        related_name="usergroupbasedcontent_excluded",
        blank=True,
    )

    groups = models.ManyToManyField(  # type: ignore[var-annotated]
        Group,
        help_text="Groups whose members should see this content.",
        blank=True,
    )

    exclude_groups = models.ManyToManyField(  # type: ignore[var-annotated]
        Group,
        help_text=(
            "Groups whose members are explicitly excluded from viewing the "
            "content."
        ),
        related_name="usergroupbasedcontent_excluded",
        blank=True,
    )

    def copy_relations(self, oldinstance):
        self.users.clear()
        self.users.add(*oldinstance.users.all())
        self.groups.clear()
        self.groups.add(*oldinstance.groups.all())
        self.exclude_users.clear()
        self.exclude_users.add(*oldinstance.exclude_users.all())
        self.exclude_groups.clear()
        self.exclude_groups.add(*oldinstance.exclude_groups.all())


class ProfileButton(CMSPlugin):
    """A button displaying the user profile."""

    classes = models.CharField(  # type: ignore[var-annotated]
        null=True,
        blank=True,
        help_text="CSS classes for the button",
        max_length=200,
    )


class DropdownHeader(CMSPlugin):
    """A text for a dropdown."""

    text = models.CharField(  # type: ignore[var-annotated]
        help_text="The text to display", max_length=200
    )


class InternalLink(Link):  # type: ignore[django-manager-missing]
    """Bootsrap5-like link with internal link arguments.

    This link reimplements the Link-model of djangocms-frontend and adds an
    additional url_name field that is resolved to the internal URL.
    """

    class Meta:
        proxy = True

    def get_link(self):
        if getattr(self, "internal_url_name", None):
            try:
                if getattr(self, "internal_url_args", None):
                    return reverse(
                        self.internal_url_name, args=self.internal_url_args
                    )
                else:
                    ret = reverse(self.internal_url_name)
            except NoReverseMatch:
                ret = ""
        else:
            ret = super().get_link()
        if getattr(self, "get_params", None) or getattr(
            self, "add_next", None
        ):
            ret += "?"
        if getattr(self, "get_params", None):
            ret += self.get_params
        return ret


class MenuButtonPluginModel(InternalLink):  # type: ignore[django-manager-missing]
    """A model for the MenuButtonPlugin."""

    class Meta:
        proxy = True

    def attributes_as_str(
        self, attributes: Dict, additional_classes: Set = set()
    ) -> str:
        classes = set(attributes.get("class", "").split())
        classes.update(additional_classes)
        if classes:
            attributes["class"] = " ".join(classes)
        parts = (
            f'{item}="{conditional_escape(value)}"' if value else f"{item}"
            for item, value in attributes.items()
        )
        attributes_string = " ".join(parts)
        return mark_safe(" " + attributes_string) if attributes_string else ""

    def get_dropdown_button_attributes(self) -> str:
        return self.attributes_as_str(
            self.config.get("dropdown_button_attributes", {})
        )

    def get_dropdown_attributes(self) -> str:
        return self.attributes_as_str(
            self.config.get("dropdown_attributes", {})
        )


@extension_pool.register
class PageStylesExtension(PageExtension):
    """Styling options for a CMS page."""

    class BootstrapContainerClasses(models.TextChoices):
        default = "container", "Default (.container)"
        md = "container-md", r"100% on mobile phones (.container-md)"
        lg = "container-lg", r"100% on tables (.container-lg)"
        xl = "container-xl", r"100% on wide screens (.container-xl)"
        xxl = "container-xxl", r"100% on extra wide screens (.container-xxl)"
        fluid = "container-fluid", r"Allways 100% (.container-fluid)"

    container_class = models.CharField(  # type: ignore[var-annotated]
        max_length=20,
        choices=BootstrapContainerClasses.choices,
        help_text=(
            "Select a class for the main container of the page. See the "
            "bootstrap5 docs for for information on the container classes."
        ),
        default="container",
    )

    hide_title = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Hide the headline of the page with the title.",
    )


@extension_pool.register
class PageMenuExtension(PageExtension):
    """An extension for custom menu options."""

    menu_get_params = models.CharField(  # type: ignore[var-annotated]
        max_length=400,
        help_text=(
            "Any additional query parameters that you want to append to the "
            "Link after a <i>?</i> sign."
        ),
        verbose_name=_("Query Parameters for menus"),
        blank=True,
        null=True,
    )

    is_menu_breakpoint = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=(
            "When selected, each navigation menu will stop at this page and "
            "the children will not be displayed. You may want to select this "
            "option if you aim to render the menu with the children of the "
            "page on the page itself."
        ),
    )


class SectionPluginModel(CMSPlugin):
    """A plugin to render a section."""

    class HeadingType(models.TextChoices):
        """Choices for the heading type"""

        h2 = ("h2", "Second level (h2)")
        h3 = ("h3", "Third level (h3)")
        h4 = ("h4", "Fourth level (h4)")
        h5 = ("h5", "Fifth level (h5)")
        h6 = ("h6", "Sixth level (h6)")

    name = models.CharField(  # type: ignore[var-annotated]
        max_length=400, help_text="The header for the section"
    )

    heading_type = models.CharField(  # type: ignore[var-annotated]
        max_length=2, choices=HeadingType.choices, default=HeadingType.h2
    )

    show_permalink = models.BooleanField(  # type: ignore[var-annotated]
        default=True, help_text="Show a small link symbol next to the heading"
    )

    def __str__(self):
        return self.name


def get_reverse_ids() -> List[Tuple[str, str]]:
    """Get the choices of reverse ids to display."""
    return [
        (p.reverse_id, str(p))
        for p in Page.objects.filter(reverse_id__isnull=False)
    ]


class MenuPluginModel(CMSPlugin):
    """A plugin model to render the page menu."""

    from_level = models.IntegerField(  # type: ignore[var-annotated]
        default=0,
        help_text="From which level shall the navigation be rendered?",
    )
    to_level = models.IntegerField(  # type: ignore[var-annotated]
        default=100, help_text="At which level should the naviation stop?"
    )
    extra_inactive = models.IntegerField(  # type: ignore[var-annotated]
        default=100,
        help_text=(
            "How many levels of navigation shall be displayed  if a node is "
            "not a direct ancestor or descendant of the current active node?"
        ),
    )
    extra_active = models.IntegerField(  # type: ignore[var-annotated]
        default=100,
        help_text=(
            "How many levels of descendants of the currently active page "
            "shall be displayed"
        ),
    )

    root_id = models.ForeignKey(  # type: ignore[var-annotated]
        Page,
        null=True,
        blank=True,
        help_text="A reverse ID of a CMS page where to start from.",
        on_delete=models.SET_NULL,
        limit_choices_to={"reverse_id__isnull": False},
    )

    expand_to_level = models.PositiveIntegerField(  # type: ignore[var-annotated]
        null=True, help_text="Expand items in the menu up to a certain level."
    )


class ObjectPage(models.Model):  # type: ignore[django-manager-missing]
    """A model to associate CMS content with a database object."""

    class Meta:
        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    def get_content(self, show_draft_content=False):
        if show_draft_content:
            return self.get_draft_content()
        try:
            return self._content_cache
        except AttributeError:
            qs = self.contents.all()
            qs = qs.prefetch_related("placeholders")

            self._content_cache = qs.first()
            return self._content_cache

    def get_draft_content(self):
        try:
            return self._draft_content_cache
        except AttributeError:
            qs = self.contents(manager="admin_manager").latest_content()
            qs = qs.prefetch_related("placeholders")

            self._draft_content_cache = qs.first()
            return self._draft_content_cache

    def has_change_permission(self, user):
        """Test if the user can change the object page."""
        perm = utils.get_model_perm(self.content_object, "change")
        return utils.has_perm(user, perm, self.content_object)

    def __str__(self):
        return f"CMS Page of {self.content_object}"


class ObjectPageContentManager(WithUserMixin, models.Manager):
    """Adds with_user syntax to ActivityContent w/o using versioning"""

    pass


class ObjectPageContent(models.Model):  # type: ignore[django-manager-missing]
    """CMS content for an activity."""

    objects = ObjectPageContentManager()  # type: ignore[django-manager-missing]

    class Meta:
        verbose_name = _("%(working_group)s content") % {
            "working_group": _("Working group")
        }
        verbose_name_plural = _("%(working_group)s contents") % {
            "working_group": _("Working group")
        }

    page = models.ForeignKey(
        ObjectPage,
        on_delete=models.CASCADE,
        related_name="contents",
    )

    placeholders = PlaceholderRelationField()

    @cached_property
    def action_buttons_placeholder(self) -> Placeholder:
        return get_placeholder_from_slot(self.placeholders, "action_buttons")

    @cached_property
    def content_placeholder(self) -> Placeholder:
        return get_placeholder_from_slot(self.placeholders, "content")

    @cached_property
    def sidebar_placeholder(self) -> Placeholder:
        return get_placeholder_from_slot(self.placeholders, "sidebar")

    @cached_property
    def page_banners_placeholder(self) -> Placeholder:
        return get_placeholder_from_slot(self.placeholders, "page_banners")

    def get_placeholders(self) -> List[Placeholder]:
        return [
            self.content_placeholder,
            self.sidebar_placeholder,
            self.sidebar_placeholder,
            self.page_banners_placeholder,
        ]

    def get_template(self):
        return "cms_components/object_page_content.html"

    @cached_property
    def state(self) -> str:
        return self.versions.values_list("state", flat=True).first()  # type: ignore[attr-defined]

    def has_placeholder_change_permission(self, user):
        return self.page.has_change_permission(user)

    def __str__(self):
        try:
            return f"ObjectPageContent ({self.pk}, {self.state}): {self.page.content_object}"
        except ObjectPageContent.page.RelatedObjectDoesNotExist:
            return super().__str__()


def copy_object_page_content(original_content: ObjectPageContent):
    """Copy the content object and deepcopy its placeholders and plugins

    This is needed for versioning integration.
    """
    # Copy content object
    content_fields = {
        field.name: getattr(original_content, field.name)
        for field in ObjectPageContent._meta.fields
        # don't copy primary key because we're creating a new obj
        if ObjectPageContent._meta.pk.name != field.name  # type: ignore[union-attr]
    }
    new_content = ObjectPageContent.objects.create(**content_fields)

    # Copy placeholders
    new_placeholders = []
    for placeholder in original_content.placeholders.all():
        placeholder_fields = {
            field.name: getattr(placeholder, field.name)
            for field in Placeholder._meta.fields
            # don't copy primary key because we're creating a new obj
            # and handle the source field later
            if field.name not in [Placeholder._meta.pk.name, "source"]
        }
        if placeholder.source:
            placeholder_fields["source"] = new_content
        new_placeholder = Placeholder.objects.create(**placeholder_fields)
        # Copy plugins
        placeholder.copy_plugins(new_placeholder)
        new_placeholders.append(new_placeholder)
    new_content.placeholders.add(*new_placeholders)

    return new_content
