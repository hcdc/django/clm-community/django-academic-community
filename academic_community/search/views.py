# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from typing import Any, Dict, Tuple

from cms.models import Page, PagePermission
from django.contrib.postgres.search import (
    SearchQuery,
    SearchRank,
    SearchVector,
)
from django.db.models import Exists, OuterRef, Q, QuerySet
from django.shortcuts import render
from django.views import generic
from guardian.shortcuts import get_objects_for_user

from academic_community.activities.models import (
    Activity,
    ActivityMaterialRelation,
)
from academic_community.events.models import Event
from academic_community.events.programme.models import (
    Contribution,
    ContributionMaterialRelation,
    Session,
    SessionMaterialRelation,
)
from academic_community.faqs.models import FAQ
from academic_community.institutions.models import AcademicOrganization
from academic_community.members.models import CommunityMember
from academic_community.mixins import MemberOnlyMixin
from academic_community.topics.models import Topic, TopicMaterialRelation


class SearchView(MemberOnlyMixin, generic.View):
    """A view to search the database"""

    def get(self, request):
        query = request.GET.get("q")
        if not query:
            return render(request, "search/search.html", {})
        context = {"query": query}

        # activities
        context["activity_list"] = self.query_activities(query)

        context["activitymaterial_list"] = self.query_activitymaterial(query)

        context["topicmaterial_list"] = self.query_topicmaterial(query)

        context["sessionmaterial_list"] = self.query_sessionmaterial(query)

        context["contributionmaterial_list"] = self.query_contributionmaterial(
            query
        )

        # topics
        topics = self.query_topics(query)
        context["active_topic_list"] = topics.filter(
            end_date__isnull=True
        ).distinct()
        context["finished_topic_list"] = topics.filter(
            end_date__isnull=False
        ).distinct()

        # institutions
        context["organization_list"] = self.query_organizations(query)

        # members
        (
            context["active_communitymember_list"],
            context["former_communitymember_list"],
            context["non_communitymember_list"],
        ) = self.query_members(query)

        context["faq_list"] = self.query_faq(query)

        # events
        context["event_list"] = self.query_events(query)

        context["session_list"] = self.query_sessions(query)

        context["contribution_list"] = self.query_contributions(query)

        context["page_list"] = self.query_pages(query)

        context["view"] = self

        return render(request, "search/search.html", context)

    def query_activities(self, query: str) -> QuerySet[Activity]:
        return Activity.objects.filter(
            Q(name__icontains=query)
            | Q(leaders__first_name__icontains=query)
            | Q(leaders__last_name__icontains=query)
            | Q(leaders__email__email__icontains=query)
            | Q(abbreviation__istartswith=query)
            | Q(abstract__icontains=query)
            | Q(category__name__icontains=query)
        ).distinct()

    def query_activitymaterial(
        self, query: str
    ) -> QuerySet[ActivityMaterialRelation]:
        return ActivityMaterialRelation.objects.filter(
            material__name__icontains=query
        )

    def query_topicmaterial(
        self, query: str
    ) -> QuerySet[TopicMaterialRelation]:
        return TopicMaterialRelation.objects.filter(
            material__name__icontains=query
        )

    def query_sessionmaterial(
        self, query: str
    ) -> QuerySet[SessionMaterialRelation]:
        return SessionMaterialRelation.objects.filter(
            material__name__icontains=query
        )

    def query_contributionmaterial(
        self, query: str
    ) -> QuerySet[ContributionMaterialRelation]:
        return ContributionMaterialRelation.objects.filter(
            material__name__icontains=query
        )

    def query_topics(self, query: str) -> QuerySet[Topic]:
        vector = (
            SearchVector("name", "id_name", weight="A")
            + SearchVector("description", weight="B")
            + SearchVector(
                "leader__first_name", "leader__last_name", weight="B"
            )
            + SearchVector(
                "lead_organization__institution__abbreviation",
                "lead_organization__department__abbreviation",
                "lead_organization__department__parent_institution__abbreviation",
                "lead_organization__unit__abbreviation",
                "lead_organization__unit__parent_department__abbreviation",
                "lead_organization__unit__parent_department__parent_institution__abbreviation",
                weight="C",
            )
            + SearchVector(
                "lead_organization__name",
                "lead_organization__unit__parent_department__name",
                "lead_organization__unit__parent_department__parent_institution__name",
                "lead_organization__department__parent_institution__name",
                weight="C",
            )
        )
        topics = (
            get_objects_for_user(self.request.user, "view_topic", Topic)
            .annotate(rank=SearchRank(vector, SearchQuery(query)))
            .filter(rank__gte=0.1)
            .order_by("rank")
        )
        return topics

    def query_organizations(
        self, query: str
    ) -> QuerySet[AcademicOrganization]:
        return AcademicOrganization.objects.filter(
            Q(name__icontains=query)
            | Q(name__icontains=query)
            | Q(members__first_name__icontains=query)
            | Q(members__last_name__icontains=query)
            | Q(institution__abbreviation__icontains=query)
            | Q(department__abbreviation__icontains=query)
            | Q(unit__abbreviation__icontains=query)
        ).distinct()

    def query_members(
        self, query: str
    ) -> Tuple[
        QuerySet[CommunityMember],
        QuerySet[CommunityMember],
        QuerySet[CommunityMember],
    ]:
        members_query = (
            Q(first_name__icontains=query)
            | Q(last_name__icontains=query)
            | Q(membership__name__icontains=query)
            | Q(membership__institution__abbreviation__icontains=query)
            | Q(membership__department__abbreviation__icontains=query)
            | Q(membership__unit__abbreviation__icontains=query)
            | Q(
                membership__department__parent_institution__name__icontains=query
            )
            | Q(
                membership__department__parent_institution__abbreviation__icontains=query
            )
            | Q(membership__unit__parent_department__name__icontains=query)
            | Q(
                membership__unit__parent_department__abbreviation__icontains=query
            )
            | Q(
                membership__unit__parent_department__parent_institution__name__icontains=query
            )
            | Q(
                membership__unit__parent_department__parent_institution__abbreviation__icontains=query
            )
        )
        members = get_objects_for_user(
            self.request.user, "view_communitymember", CommunityMember
        )
        active_communitymember_list = members.filter(
            Q(is_member=True) & Q(end_date__isnull=True) & members_query
        ).distinct()
        former_communitymember_list = members.filter(
            Q(is_member=True) & Q(end_date__isnull=False) & members_query
        ).distinct()
        non_communitymember_list = members.filter(
            Q(is_member=False) & members_query
        ).distinct()
        return (
            active_communitymember_list,
            former_communitymember_list,
            non_communitymember_list,
        )

    def query_faq(self, query: str) -> QuerySet[FAQ]:
        return (
            get_objects_for_user(self.request.user, "view_faq", FAQ)
            .filter(
                Q(question__icontains=query)
                | Q(plain_text_answer__icontains=query)
                | Q(categories__name__icontains=query)
                | Q(categories__description__icontains=query)
            )
            .distinct()
        )

    def query_events(self, query: str) -> QuerySet[Event]:
        return (
            get_objects_for_user(self.request.user, "view_event", Event)
            .filter(
                Q(name__icontains=query)
                | Q(slug__icontains=query)
                | Q(abstract__icontains=query)
                | Q(description__icontains=query)
            )
            .distinct()
        )

    def query_sessions(self, query: str) -> QuerySet[Session]:
        return (
            get_objects_for_user(self.request.user, "view_session", Session)
            .filter(
                Q(title__icontains=query)
                | Q(abstract__icontains=query)
                | Q(description__icontains=query)
                | Q(conveners__first_name__icontains=query)
                | Q(conveners__last_name__icontains=query)
                | Q(contribution__title__icontains=query)
                | Q(contribution__abstract__icontains=query)
                | Q(
                    contribution__contributingauthor__affiliation__name__icontains=query
                )
            )
            .distinct()
        )

    def query_contributions(self, query: str) -> QuerySet[Contribution]:
        return (
            get_objects_for_user(
                self.request.user, "view_contributition", Contribution
            )
            .filter(
                Q(title__icontains=query)
                | Q(abstract__icontains=query)
                | Q(contributingauthor__affiliation__name__icontains=query)
                | Q(contributingauthor__author__first_name__icontains=query)
                | Q(contributingauthor__author__last_name__icontains=query)
            )
            .distinct()
        )

    def query_pages(self, query: str) -> QuerySet[Page]:
        # we could use Page.objects.search here, but this does not do a ranking
        from cms.constants import GRANT_ALL_PERMISSIONS
        from cms.utils.page_permissions import get_view_id_list
        from django.contrib.sites.shortcuts import get_current_site
        from djangocms_text_ckeditor.models import Text

        request = self.request

        site = get_current_site(request)
        ids = get_view_id_list(request.user, site)

        related_query_name = Text.cmsplugin_ptr.field.related_query_name()
        vector = SearchVector(
            "pagecontent_set__title",
            "pagecontent_set__page_title",
            "pagecontent_set__menu_title",
            "pagecontent_set__meta_description",
            "urls__slug",
            weight="A",
        ) + SearchVector(
            "pagecontent_set__placeholders__cmsplugin__%s__body"
            % related_query_name,
            weight="B",
        )

        filter_kws: Dict[str, Any] = dict(rank__gte=0.1)

        if ids != GRANT_ALL_PERMISSIONS:
            # query pages without view permissions
            subquery = PagePermission.objects.filter(
                can_view=True, page__pk=OuterRef("pk")
            )
            public_ids = (
                Page.objects.annotate(has_view_restrictions=Exists(subquery))
                .filter(has_view_restrictions=False)
                .values_list("id", flat=True)
            )
            filter_kws["pk__in"] = list(ids) + list(public_ids)

        pages = (
            Page.objects.annotate(rank=SearchRank(vector, SearchQuery(query)))
            .filter(**filter_kws)
            .order_by("-rank")
            .distinct()
        )
        return pages
