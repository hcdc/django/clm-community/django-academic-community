# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the academic community package.

pytest-lazy-fixture_ is unmaintained and not compatible with pytest 8.0.
Therefore this module implements the package as suggested in
https://github.com/TvoroG/pytest-lazy-fixture/issues/65#issuecomment-1914581161

.. _pytest-lazy-fixture: https://pypi.org/project/pytest-lazy-fixture/
"""

import dataclasses
from typing import Mapping, cast

import pytest


@dataclasses.dataclass
class LazyFixture:
    """Lazy fixture dataclass."""

    name: str


def lazy_fixture(name: str) -> LazyFixture:
    """Mark a fixture as lazy."""
    return LazyFixture(name)


def is_lazy_fixture(value: object) -> bool:
    """Check whether a value is a lazy fixture."""
    return isinstance(value, LazyFixture)


def _resolve_lazy_fixture(
    __val: object, request: pytest.FixtureRequest
) -> object:
    """Lazy fixture resolver.

    Args:
        __val (object): fixture value object.
        request (pytest.FixtureRequest): pytest fixture request object.

    Returns:
        object: resolved fixture value.
    """
    if isinstance(__val, (list, tuple)):
        return tuple(_resolve_lazy_fixture(v, request) for v in __val)
    if isinstance(__val, Mapping):
        return {k: _resolve_lazy_fixture(v, request) for k, v in __val.items()}
    if not is_lazy_fixture(__val):
        return __val
    lazy_obj = cast(LazyFixture, __val)
    return request.getfixturevalue(lazy_obj.name)
