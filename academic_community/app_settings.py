# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
# SPDX-License-Identifier: EUPL-1.2

"""App settings
------------

This module defines the settings options for the
``django-academic-community`` app.
"""


from __future__ import annotations

from django.conf import settings  # noqa: F401

#: Base route for the websocket routing of the
#: ``django-academic-community`` app
#:
#: .. setting:: ACADEMIC_COMMUNITY_WEBSOCKET_URL_ROUTE
ACADEMIC_COMMUNITY_WEBSOCKET_URL_ROUTE = getattr(
    settings, "ACADEMIC_COMMUNITY_WEBSOCKET_URL_ROUTE", "ws/"
)


#: CMS page id for the page where community members or anonymous users can
#: contact the community managers
#:
#: .. setting:: CONTACT_PAGE_ID
CONTACT_PAGE_ID = getattr(settings, "CONTACT_PAGE_ID", "contact")


#: URL for the imprint (used in the Footer) but not automatically updated!
#:
#: .. setting:: IMPRINT_URL
IMPRINT_URL = getattr(
    settings,
    "IMPRINT_URL",
    "https://www.hereon.de/innovation_transfer/communication_media/imprint/index.php.en",
)


#: URL for the data protection (used in the Footer) but not automatically updated!
#:
#: .. setting:: DATA_PROTECTION_URL
DATA_PROTECTION_URL = getattr(
    settings,
    "DATA_PROTECTION_URL",
    "https://www.hereon.de/innovation_transfer/communication_media/imprint/privacy_policy/index.php.en",
)


#: URL for the accessibility (required by german law). Used in the Footer but not automatically updated!
#:
#: .. setting:: ACCESSIBILITY_URL
ACCESSIBILITY_URL = getattr(
    settings,
    "ACCESSIBILITY_URL",
    "https://www.hereon.de/innovation_transfer/communication_media/imprint/barrierefreiheit/index.php.en",
)


#: The path to the latex template we use with the drf-django package (if
#: installed) for exporting topics, etc. as Latex or PDF
#:
#: .. setting:: PANDOC_LATEX_TEMPLATE
PANDOC_LATEX_TEMPLATE = getattr(settings, "PANDOC_LATEX_TEMPLATE", None)


#: Renderers to be used for exporting single topics, contributions, etc.
#:
#: .. setting:: EXPORT_SINGLE_RENDERER
EXPORT_SINGLE_RENDERER = getattr(
    settings,
    "EXPORT_SINGLE_RENDERER",
    (
        "rest_framework.renderers.JSONRenderer",
        "rest_framework.renderers.BrowsableAPIRenderer",
    ),
)


#: Renderers to be used for exporting lists of topics, etc.
#:
#: .. setting:: EXPORT_LIST_RENDERER
EXPORT_LIST_RENDERER = getattr(
    settings, "EXPORT_LIST_RENDERER", EXPORT_SINGLE_RENDERER
)
