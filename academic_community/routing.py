# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
# SPDX-License-Identifier: EUPL-1.2

"""
Websocket routing configuration for the django-academic-community app.

It exposes the `websocket_urlpatterns` list, a list of url patterns to be used
for deployment.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/routing.html
"""

from typing import Any, List

from django.urls import path

import academic_community.channels.routing as chats_routing
import academic_community.notifications.routing as notifications_routing
from academic_community import app_settings
from channels.routing import URLRouter

websocket_urlpatterns: List[Any] = [
    path(
        app_settings.ACADEMIC_COMMUNITY_WEBSOCKET_URL_ROUTE,
        URLRouter(
            notifications_routing.websocket_urlpatterns
            + chats_routing.websocket_urlpatterns
        ),
    )
]
