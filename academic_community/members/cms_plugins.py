# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Plugins for the members app."""
from __future__ import annotations

from typing import TYPE_CHECKING, List, Optional

from cms.plugin_pool import plugin_pool
from django.db.models import Q
from django.utils.translation import gettext as _
from guardian.shortcuts import get_anonymous_user, get_objects_for_user

from academic_community.cms_plugins import (
    ActiveFiltersPublisher,
    FilterButtonPublisher,
    FilterListPublisher,
    PaginationPublisherMixin,
)
from academic_community.members import filters, models

if TYPE_CHECKING:
    from django.db.models import QuerySet


@plugin_pool.register_plugin
class CommunityMemberActiveFiltersPublisher(ActiveFiltersPublisher):
    """A plugin to display the applied filters for activities."""

    module = _("Community members")
    cache = False

    name = _("Display active filters for %(community_members)s") % {
        "community_members": _("Community members")
    }

    filterset_class = filters.CommunityMemberFilterSet

    list_model = models.CommunityMember


@plugin_pool.register_plugin
class CommunityMemberFilterButtonPublisher(FilterButtonPublisher):
    """A plugin to display the applied filters for activities."""

    module = _("Community members")
    cache = False

    name = _("Filter button for %(community_members)s") % {
        "community_members": _("Community members")
    }

    filterset_class = filters.CommunityMemberFilterSet

    list_model = models.CommunityMember


class CommunityMembersListPublisherBase(
    PaginationPublisherMixin, FilterListPublisher
):
    """A plugin to display material to the user."""

    module = _("Community members")
    render_template = "members/components/communitymember_listplugin.html"
    cache = False

    filterset_class = filters.CommunityMemberFilterSet

    list_model = models.CommunityMember

    context_object_name = "members"

    def get_query(
        self, instance: models.AbstractCommunityMemberListPluginModel
    ) -> Optional[Q]:
        """Get the query for the members."""
        query = None

        queries: List[Q] = []

        if instance.members_only and instance.former_members:
            queries.append(Q(is_member=True) | Q(end_date__isnull=False))
        elif instance.members_only:
            queries.append(Q(is_member=True))
        elif not instance.former_members:
            queries.append(Q(end_date__isnull=True))
        if instance.app_config:
            queries.append(Q(app_config=instance.app_config))

        for q in queries:
            query = q if query is None else (query & q)  # type: ignore[operator]

        return query

    def filter_list_queryset(
        self,
        context,
        instance: models.AbstractCommunityMemberListPluginModel,  # type: ignore[override]
        queryset: QuerySet[models.CommunityMember],
    ) -> QuerySet[models.CommunityMember]:
        queryset = super().filter_list_queryset(context, instance, queryset)
        query = self.get_query(instance)
        if query is not None:
            queryset = queryset.filter(query)
        if instance.check_permissions:
            user = context["request"].user
            if user.is_anonymous:
                user = get_anonymous_user()
            queryset = get_objects_for_user(
                user, "view_communitymember", queryset
            )
        return queryset

    def render(
        self,
        context,
        instance: models.AbstractCommunityMemberListPluginModel,  # type: ignore[override]
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        context["show_logo"] = instance.show_institution_logos
        context["show_institutions"] = instance.show_institutions
        context["base_id"] = f"member-plugin-{instance.pk}-"
        return context


@plugin_pool.register_plugin
class CommunityMembersListPublisher(CommunityMembersListPublisherBase):
    """A plugin to display selected community members"""

    name = _("Selected Members")
    model = models.CommunityMemberListPluginModel

    filter_horizontal = ["members"]

    def get_full_queryset(
        self, context, instance: models.CommunityMemberListPluginModel
    ) -> QuerySet[models.CommunityMember]:
        """Get the activities to display"""
        if instance.show_all:
            return super().get_full_queryset(context, instance)
        else:
            return self.filter_list_queryset(
                context, instance, instance.members.all()
            )
