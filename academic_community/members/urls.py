"""Urls of the :mod:`members` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.conf import settings
from django.urls import path, re_path
from django.views.decorators.cache import never_cache

from academic_community.members import views

app_name = "members"

urlpatterns = [
    re_path(
        r"member/(?P<pk>\d+)/?$",
        views.RedirectMemberView.as_view(),
        name="members",
    ),
    path("profile/", views.ProfileView.as_view(), name="profile"),
    path(
        "profile/edit/", views.EditProfileView.as_view(), name="edit-profile"
    ),
    path(
        "profile/edit/emails/",
        views.ProfileEmailUpdateView.as_view(),
        name="edit-profile-emails",
    ),
    path(
        "verify/",
        views.SendVerificationEmailView.as_view(),
        name="send-verification-mail",
    ),
    path(
        "verify/<uuid:pk>/",
        # this view should never be cached as it can also be modified by
        # anonymous users
        never_cache(views.VerifyEmailView.as_view()),
        name="verify-email",
    ),
]


if getattr(settings, "COMMUNITY_MEMBER_REGISTRATION", True):
    urlpatterns.extend(
        [
            path(
                "profile/become-a-member/",
                views.SelfRequestMemberStatusView.as_view(),
                name="self-become-member",
            ),
            # we need to add these views here to make sure, communitymembers
            # can always approve other members. Otherwise they might be blocked
            # through CMS permissions
            re_path(
                r"member/(?P<pk>\d+)/approve/?$",
                views.GrantMemberStatusView.as_view(),
                name="approve-member",
            ),
        ]
    )
