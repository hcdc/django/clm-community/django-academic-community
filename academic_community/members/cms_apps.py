# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from aldryn_apphooks_config.app_base import CMSConfigApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import gettext as _

from academic_community.members.cms_appconfig import MembersConfig


@apphook_pool.register
class MembersApphook(CMSConfigApp):
    app_name = "members"
    name = _("Members")
    app_config = MembersConfig

    def get_urls(self, page=None, language=None, **kwargs):
        return ["academic_community.members.urls_apphook"]
