# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from aldryn_apphooks_config.admin import BaseAppHookConfig, ModelAppHookConfig
from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from reversion_compare.admin import CompareVersionAdmin

from academic_community import utils
from academic_community.activities.models import Activity
from academic_community.admin import ManagerAdminMixin
from academic_community.institutions.forms import AcademicMembershipForm
from academic_community.institutions.models import AcademicMembership
from academic_community.members import models

# Register your models here.
from academic_community.topics.admin import TopicMembershipInline


class ActivityInline(admin.TabularInline):
    model = Activity


class AcademicMembershipInline(admin.TabularInline):
    form = AcademicMembershipForm
    model = AcademicMembership

    class Media:
        js = (
            "https://code.jquery.com/jquery-3.6.0.min.js",  # jquery
            "js/institution_query.js",
        )


class EmailInline(admin.TabularInline):
    model = models.Email


@admin.register(models.MembersConfig)
class MembersConfigAdmin(BaseAppHookConfig, admin.ModelAdmin):
    pass


@admin.register(models.CommunityMember)
class CommunityMemberAdmin(
    ManagerAdminMixin,
    GuardedModelAdmin,
    ModelAppHookConfig,
    CompareVersionAdmin,
):
    """Administration class for the :model:`topics.Topic` model."""

    inlines = [
        EmailInline,
        AcademicMembershipInline,
        TopicMembershipInline,
    ]

    search_fields = ["first_name", "last_name", "email__email"]

    filter_horizontal = [
        "activities",
        "user_view_profile_permission",
        "group_view_profile_permission",
        "user_contact_view_permission",
        "group_contact_view_permission",
    ]

    list_display = [
        "first_name",
        "last_name",
        "email",
        "start_date",
        "end_date",
    ]

    list_filter = [
        "is_member",
        "reviewed",
        "start_date",
        "end_date",
    ]

    def get_changeform_initial_data(self, request):
        ret = super().get_changeform_initial_data(request)
        if "group_view_profile_permission" not in ret:
            ret["group_view_profile_permission"] = utils.get_groups("MEMBERS")
        if "group_contact_view_permission" not in ret:
            ret["group_contact_view_permission"] = utils.get_groups("MEMBERS")
        return ret

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            email_qs = models.Email.objects.filter(member=obj)
            form.base_fields["email"].queryset = email_qs
            form.base_fields["contact_email"].queryset = email_qs

        return form


@admin.register(models.Email)
class EmailAdmin(ManagerAdminMixin, GuardedModelAdmin, CompareVersionAdmin):
    search_fields = ["email", "member__first_name", "member__last_name"]

    list_display = ["email", "member", "is_verified", "is_primary"]

    list_filter = ["is_verified"]

    def is_primary(self, obj: models.Email) -> bool:
        return getattr(obj, "primary_member", None) is not None
