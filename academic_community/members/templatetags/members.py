"""Template tags to display a community member.."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import Dict, Sequence

from classytags.arguments import Argument, MultiKeywordArgument
from classytags.core import Options
from classytags.helpers import InclusionTag, Tag
from django import template
from django.template.loader import render_to_string
from django.utils.translation import gettext as _

from academic_community.members.models import CommunityMember

register = template.Library()


@register.tag
class MemberRows(Tag):
    """Display a member and it's affiliations."""

    name = "member_rows"

    options = Options(
        Argument("communitymembers"),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def render_tag(
        self,
        context,
        communitymembers: Sequence[CommunityMember],
        template_context: Dict = {},
    ) -> str:
        context = context.flatten()
        context.update(template_context)
        context.setdefault("show_logo", False)

        rows = []
        for member in communitymembers:
            if not isinstance(member, CommunityMember):
                if not hasattr(member, "communitymember"):
                    continue
                member = member.communitymember
            context["communitymember"] = member
            rows.append(
                render_to_string(
                    "members/components/communitymember_row.html", context
                )
            )
        return "\n".join(rows)


@register.tag
class MemberRow(InclusionTag):
    """A row of a single member."""

    name = "member_row"
    push_context = True

    template = "members/components/communitymember_row.html"

    options = Options(
        Argument("communitymember"),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def get_context(
        self,
        context,
        communitymember: CommunityMember,
        template_context: Dict = {},
    ) -> Dict:
        template_context["communitymember"] = communitymember
        template_context.setdefault("show_logo", False)
        return template_context


@register.tag
class MemberCard(InclusionTag):
    """Render the card of a member (without affiliation)."""

    name = "member_card"
    push_context = True

    template = "members/components/communitymember_card.html"

    options = Options(
        Argument("communitymember"),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def get_context(
        self,
        context,
        communitymember: CommunityMember,
        template_context: Dict = {},
    ) -> Dict:
        template_context["communitymember"] = communitymember
        return template_context


@register.tag
class MemberCards(Tag):
    """Display a member and it's affiliations."""

    name = "member_cards"

    options = Options(
        Argument("communitymembers"),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def render_tag(
        self,
        context,
        communitymembers: Sequence[CommunityMember],
        template_context: Dict = {},
    ) -> str:
        context = context.flatten()
        context.update(template_context)

        cards = []
        for member in communitymembers:
            if not isinstance(member, CommunityMember):
                if not hasattr(member, "communitymember"):
                    continue
                member = member.communitymember
            context["communitymember"] = member
            cards.append(
                render_to_string(
                    "members/components/communitymember_card.html", context
                )
            )
        return "\n".join(cards)


@register.simple_tag
def member_roles(communitymember: CommunityMember) -> str:
    """Get a text that displays the role of the member in the community."""
    roles = []
    if communitymember.user and communitymember.user.is_manager:  # type: ignore  # noqa: E501
        roles += ["community manager"]
    if communitymember.activity_leader.count():
        roles += [_("working group leader")]
    if communitymember.organization_contact.count():
        roles += ["organization contact"]
    if communitymember.is_member:
        roles += [
            ("former" if communitymember.end_date else "active")
            + " community member"
        ]
    if not communitymember.is_member:
        roles += ["external"]
    return ", ".join(roles)
