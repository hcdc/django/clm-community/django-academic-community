"""Urls of the :mod:`members` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.conf import settings
from django.urls import path, re_path

from academic_community.members import views

urlpatterns = [
    re_path(
        r"(?P<pk>\d+)/?$",
        views.CommunityMemberDetail.as_view(),
        name="communitymember-detail",
    ),
    path(
        "<int:pk>/edit/",
        views.CommunityMemberUpdate.as_view(),
        name="edit-communitymember",
    ),
    path(
        "<int:pk>/edit/emails/",
        views.CommunityMemberEmailsUpdateView.as_view(),
        name="edit-communitymember-emails",
    ),
    path(
        "<int:pk>/history/",
        views.CommunityMemberRevisionList.as_view(),
        name="communitymember-history",
    ),
    path(
        "<int:pk>/end-topics/<int:membership_pk>/",
        views.EndOrAssignTopicsView.as_view(),
        name="end-or-assign-topics",
    ),
]


if getattr(settings, "COMMUNITY_MEMBER_REGISTRATION", True):
    urlpatterns.extend(
        [
            path(
                "<int:pk>/become-a-member/",
                views.RequestMemberStatusView.as_view(),
                name="become-member",
            ),
            path(
                "<int:pk>/become-a-member/approve/",
                views.GrantMemberStatusAppConfigView.as_view(),
                name="approve-member",
            ),
        ]
    )
