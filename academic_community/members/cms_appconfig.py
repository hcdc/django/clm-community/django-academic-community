# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App Config for the activities app
"""


from aldryn_apphooks_config.models import AppHookConfig
from aldryn_apphooks_config.utils import setup_config
from app_data import AppDataForm


class MembersConfig(AppHookConfig):
    pass


class MembersConfigForm(AppDataForm):
    pass


setup_config(MembersConfigForm, MembersConfig)
