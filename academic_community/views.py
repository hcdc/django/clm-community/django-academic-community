"""Views and mixins that are commonly used throughout the apps."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING, Any, Callable, Dict, Type

from django.http import HttpResponseBadRequest
from django.urls import reverse_lazy
from django.views import generic
from django_e2ee.models import MasterKeySecret
from django_filters.views import FilterView as BaseFilterView
from guardian.mixins import LoginRequiredMixin

from academic_community import models
from academic_community.mixins import NextMixin

if TYPE_CHECKING:
    from django.http import HttpRequest


class FilterView(BaseFilterView):
    """Reimplemented FilterView to default to ``_list`` templates."""

    template_name_suffix = "_list"


class ServiceWorkerView(generic.TemplateView):
    """A view to render the webmanifest"""

    template_name = "base_components/serviceworker.js"

    content_type = "application/javascript"


class ManifestView(NextMixin, generic.TemplateView):
    """A view to render the webmanifest"""

    template_name = "base_components/manifest.webmanifest"

    content_type = "application/json"


class E2EEStatusView(LoginRequiredMixin, generic.TemplateView):
    """A view to display the E2EE status for the user."""

    template_name = "academic_community/e2ee_status.html"


class MasterKeySecretDeleteView(  # type: ignore
    NextMixin, LoginRequiredMixin, generic.edit.DeleteView
):
    """A view to delete the secret of a master key."""

    model = MasterKeySecret

    success_url = reverse_lazy("e2ee-status")

    slug_field: str = "uuid"
    slug_url_kwarg: str = "uuid"

    def can_delete_secret(self):
        """Test if the secret can be deleted.

        We cannot delete it if there is only one left."""
        return self.request.user.master_key.masterkeysecret_set.count() > 1

    def get_queryset(self):
        if not hasattr(self.request.user, "master_key"):
            return MasterKeySecret.objects.none()
        else:
            return self.request.user.master_key.masterkeysecret_set.all()

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            can_delete_secret=self.can_delete_secret(), **kwargs
        )

    def post(self, request, **kwargs):
        if self.can_delete_secret():
            return super().post(request, **kwargs)
        else:
            return HttpResponseBadRequest()


def render_object_page_content(
    request, objectpagecontent: models.ObjectPageContent
):
    return renderers[objectpagecontent.page.content_object.__class__](
        request, objectpagecontent
    )


def register_object_page_content_renderer(
    cls_: Type, func: Callable[[HttpRequest, models.ObjectPageContent], Any]
):
    renderers[cls_] = func


renderers: Dict[
    Type, Callable[[HttpRequest, models.ObjectPageContent], Any]
] = {}
