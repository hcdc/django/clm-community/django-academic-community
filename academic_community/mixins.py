"""Mixins of the academic-community."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

import datetime as dt
from pathlib import Path
from typing import TYPE_CHECKING, Dict, List

from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from guardian.mixins import PermissionListMixin
from rest_framework import generics

from academic_community import utils
from academic_community.context_processors import get_default_context
from academic_community.decorators import manager_only, member_only
from academic_community.uploaded_material.models import License

if TYPE_CHECKING:
    from django.contrib.auth.models import User

    from academic_community.utils import (
        PermissionCheckBaseInlineFormSet,
        PermissionCheckBaseModelFormSet,
        PermissionCheckFormMixin,
    )


class AppConfigMixin:
    """
    This mixin must be attached to any class-based views used which implements AppHookConfig.

    It provides:
    * current namespace in self.namespace
    * namespace configuration in self.config
    * current application in the `current_app` context variable

    Notes
    -----
    This is a reimplementation of the original
    :class:`aldryn_apphooks_config.mixins.AppConfigMixin` class that uses
    the :func:`academic_community.utils.get_app_instance` function to account
    for CMS pages that are below an apphook.
    """

    def dispatch(self, request, *args, **kwargs):
        self.namespace, self.config = utils.get_app_instance(request)
        request.current_app = self.namespace
        return super().dispatch(request, *args, **kwargs)

    def render_to_response(self, context, **response_kwargs):
        if "current_app" in response_kwargs:  # pragma: no cover
            response_kwargs["current_app"] = self.namespace
        return super().render_to_response(context, **response_kwargs)


class MemberOnlyMixin:
    """A member only mixin for use with class based views.

    This Class is a light wrapper around the `member_only` decorator and hence
    function parameters are just attributes defined on the class.

    Due to parent class order traversal this mixin must be added as the left
    most mixin of a view.

    The mixin has exactly the same flow as `member_only` decorator:

        If the user isn't logged in, redirect to ``settings.LOGIN_URL``,
        passing the current absolute path in the query string. Example:
        ``/accounts/login/?next=/polls/3/``.

        If the user is a member, execute the view normally. The view code is
        free to assume the user being a member.

    Notes
    -----
    This mixin is inspired by the :class:`guardian.mixins.MemberOnlyMixin`
    """

    redirect_field_name = REDIRECT_FIELD_NAME
    login_url = settings.LOGIN_URL

    def dispatch(self, request, *args, **kwargs):
        return member_only(
            redirect_field_name=self.redirect_field_name,
            login_url=self.login_url,
        )(super().dispatch)(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["license"] = License.objects.internal_default()
        return context


class ManagerOnlyMixin:
    """A manager only mixin for use with class based views.

    This Class is a light wrapper around the `manager_only` decorator and hence
    function parameters are just attributes defined on the class.

    Due to parent class order traversal this mixin must be added as the left
    most mixin of a view.

    The mixin has exactly the same flow as `manager_only` decorator:

        If the user isn't logged in, redirect to ``settings.LOGIN_URL``,
        passing the current absolute path in the query string. Example:
        ``/accounts/login/?next=/polls/3/``.

        If the user is a member, execute the view normally. The view code is
        free to assume the user being a member.

    Notes
    -----
    This mixin is inspired by the :class:`guardian.mixins.MemberOnlyMixin`
    """

    redirect_field_name = REDIRECT_FIELD_NAME
    login_url = settings.LOGIN_URL

    def dispatch(self, request, *args, **kwargs):
        return manager_only(
            redirect_field_name=self.redirect_field_name,
            login_url=self.login_url,
        )(super().dispatch)(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["license"] = License.objects.internal_default()
        return context


class PermissionCheckViewMixin:
    """A mixin class for CreateViews that check the permissions of the user.py

    Requires that the :attr:`form_class` is a subclass of the
    :class:`PermissionCheckFormMixin`.
    """

    def get_form(self, *args, **kwargs):
        form: PermissionCheckFormMixin = super().get_form(*args, **kwargs)  # type: ignore
        user: User = self.request.user  # type: ignore
        form.update_from_user(user)
        return form


class PermissionCheckModelFormWithInlinesMixin:
    def construct_inlines(self) -> List[PermissionCheckBaseInlineFormSet]:
        ret: List[PermissionCheckBaseInlineFormSet] = super().construct_inlines()  # type: ignore
        user: User = self.request.user  # type: ignore
        for inline in ret:
            inline.update_from_user(user)
        return ret


class PermissionCheckModelFormSetViewMixin:
    def construct_formset(self) -> PermissionCheckBaseModelFormSet:
        ret: PermissionCheckBaseModelFormSet = super().construct_formset()  # type: ignore
        user: User = self.request.user  # type: ignore
        ret.update_from_user(user)
        return ret


class NextMixin:
    """A mixin to take the next GET parameter into account."""

    def get_success_url(self):
        if self.request.GET.get("next"):
            return self.request.GET["next"]
        return super().get_success_url()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.GET.get("next"):
            context["next_url"] = self.request.GET["next"]
        return context


class PermissionRequiredCompatibleListMixin(PermissionListMixin):
    """PermissionListMixin that can be used with PermissionRequiredMixin."""

    list_permission_required: str

    def get_get_objects_for_user_kwargs(self, queryset):
        ret = super().get_get_objects_for_user_kwargs(queryset)
        ret["perms"] = self.list_permission_required
        return ret


class ExportRetrieveAPIView(generics.RetrieveAPIView):
    """A base class for retrieving objects via the API.

    This class implements several defaults for the drf-pandoc package to
    export content in various formats.
    """

    renderer_classes = utils.get_export_single_renderer_classes()

    pandoc_title = "Exported data"

    def get_pandoc_title(self, data: Dict):
        """Get the title for the document."""
        return self.pandoc_title

    def get_pandoc_metadata(self, format, data):
        """Get the metadata for the export."""

        data = data[0]

        default_context = get_default_context(self.request)

        ret = {"title": self.get_pandoc_title(data)}

        license = None
        if "license" in data:
            license: License = data["license"]
        elif "license" in default_context:
            license: License = default_context["license"]
        if license:
            ret["license"] = {
                "url": license.url,
                "identifier": license.identifier,
                "name": license.name,
            }
        return ret

    def get_pandoc_latex_template(self) -> Path:
        """Get the path to the latex template"""
        return utils.get_pandoc_latex_template()

    def get_pandoc_extra_args(self, format, tmpdir, data):
        """Get extra arguments for the pandoc conversion."""
        from django.contrib.staticfiles.finders import FileSystemFinder

        data = data[0]

        default_context = get_default_context(self.request)
        finder = FileSystemFinder()
        ret = [
            "-V",
            "community_name=" + default_context["community_name"],
            "-V",
            "graphics=true",
            "-V",
            "logo_path=" + finder.find("images/logo.png"),
            "-V",
            "geometry:margin=1in",
            "-V",
            "documentclass=article",
            "--top-level-division=section",
        ]
        for fname in utils.get_pandoc_file("./").glob("*.lua"):
            ret.extend(["--lua-filter", str(fname)])
        if default_context["community_abbreviation"]:
            ret += [
                "-V",
                "community_abbreviation="
                + default_context["community_abbreviation"],
            ]
        if format in ["latex", "pdf"]:
            template_file = self.get_pandoc_latex_template()
            ret += [
                "--pdf-engine=xelatex",
                "--template",
                str(template_file),
            ]
        return ret


class ExportListAPIView(generics.ListAPIView):
    """A base class for listing objects via the API.

    This class implements several defaults for the drf-pandoc package to
    export content in various formats.
    """

    renderer_classes = utils.get_export_list_renderer_classes()

    pandoc_title = "Exported data"

    def get_context(self, format: str, data: Dict):
        if format in ["latex", "pdf"]:
            return {"heading_level": "h1"}
        else:
            return {}

    def get_pandoc_title(self, data: List[Dict]):
        """Get the title for the document."""
        return self.pandoc_title

    def get_pandoc_metadata(self, format, data):
        """Get the metadata for the export."""
        default_context = get_default_context(self.request)
        full_path = f"{self.request.path}?{self.request.GET.urlencode()}"
        ret = {
            "date": dt.date.today(),
            "title": self.get_pandoc_title(data),
            "metadata": [
                {
                    "date_code": "creationDate",
                    "label": "Exported",
                    "value": dt.date.today(),
                    "is_date": True,
                }
            ],
            "links": [
                {
                    "url": default_context["index_url"],
                    "title": default_context["community_name"],
                },
                {
                    "url": default_context["root_url"] + full_path,
                    "title": "View on site",
                },
            ],
            "site_name": default_context["site_name"],
        }

        license = None
        if "license" in data:
            license: License = data["license"]
        elif "license" in default_context:
            license: License = default_context["license"]
        if license:
            ret["license"] = {
                "url": license.url,
                "identifier": license.identifier,
                "name": license.name,
            }
        return ret

    def add_pagination_metadata(
        self, data: List[Dict], metadata: Dict, object_name: str
    ):
        if hasattr(self, "paginator"):
            if self.paginator.page.paginator.page_range.stop > 2:
                pagination_info = {
                    "count": self.paginator.page.paginator.count,
                    "results": len(data),
                    "next": self.paginator.get_next_link(),
                    "previous": self.paginator.get_previous_link(),
                }
                metadata["metadata"].append(
                    {
                        "label": "Warning",
                        "value": "Only showing %(results)s of %(count)s results"
                        % pagination_info,
                    }
                )
                if pagination_info["previous"]:
                    metadata["links"].append(
                        {
                            "url": pagination_info["previous"],
                            "title": "Previous " + object_name,
                        }
                    )
                if pagination_info["next"]:
                    metadata["links"].append(
                        {
                            "url": pagination_info["next"],
                            "title": "Next " + object_name,
                        }
                    )

    def get_pandoc_latex_template(self) -> Path:
        """Get the path to the latex template"""
        return utils.get_pandoc_latex_template()

    def get_pandoc_extra_args(self, format, tmpdir, data):
        """Get extra arguments for the pandoc conversion."""
        from django.contrib.staticfiles.finders import FileSystemFinder

        default_context = get_default_context(self.request)
        finder = FileSystemFinder()
        ret = [
            "-V",
            "community_name=" + default_context["community_name"],
            "-V",
            "graphics=true",
            "-V",
            "logo_path=" + finder.find("images/logo.png"),
            "-V",
            "toc=true",
            "-V",
            "geometry:margin=1in",
            "-V",
            "documentclass=report",
            "--top-level-division=section",
        ]
        for fname in utils.get_pandoc_file("./").glob("*.lua"):
            ret.extend(["--lua-filter", str(fname)])
        if default_context["community_abbreviation"]:
            ret += [
                "-V",
                "community_abbreviation="
                + default_context["community_abbreviation"],
            ]
        if format in ["latex", "pdf"]:
            template_file = self.get_pandoc_latex_template()
            ret += [
                "--pdf-engine=xelatex",
                "--template",
                str(template_file),
            ]
        return ret
