"""Url config of the uploaded_material app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import private_storage.urls
from django.urls import include, path, re_path

from academic_community.uploaded_material import views

urlpatterns = [
    path("material/", include(views.UserMaterialViewSet().urls)),
    path(
        "community_material/",
        include(views.CommunityMaterialViewSet().urls),
    ),
    re_path(
        r"media/(?P<model_name>\w+)/(?P<pk>\d+)/(?P<filename>.*)$",
        views.RedirectMaterialDownloadView.as_view(),
    ),
    re_path(
        r"media/(?P<model_name>\w+)/(?P<uuid>%s)/(?P<filename>.*)$"
        % views.uuid_regex,
        views.MaterialDownloadView.as_view(),
    ),
    path("", include(private_storage.urls)),
]
