"""Plugins for uploaded material"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING, Any, Dict

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.translation import gettext as _
from guardian.shortcuts import get_objects_for_user

from academic_community.cms_plugins import (
    ListPublisher,
    PaginationPublisherMixin,
)
from academic_community.templatetags.community_utils import url_for_next
from academic_community.uploaded_material import models
from academic_community.utils import concat_classes

if TYPE_CHECKING:
    from django.db.models import QuerySet


class MaterialListPublisher(PaginationPublisherMixin, ListPublisher):
    """A plugin to display material to the user."""

    model = models.CommunityMaterialListPluginModel
    name = _("Material List")
    module = _("Uploaded material")
    render_template = "uploaded_material/components/material_listplugin.html"
    cache = False

    filter_horizontal = ["keywords"]

    list_model = models.Material

    list_ordering = "-last_modification_date"

    def get_filter_kws(
        self,
        context,
        instance: models.MaterialListPluginBaseModel,
    ) -> Dict[str, Any]:
        """Get the keywords to filter the queryset."""
        filter_kws: Dict[str, Any] = {}
        if instance.category:
            filter_kws["category"] = instance.category
        if instance.license:
            filter_kws["license"] = instance.license
        if not instance.all_keywords_required:
            keywords = instance.keywords.values_list("pk", flat=True)
            if keywords:
                filter_kws["keywords__pk__in"] = keywords
        return filter_kws

    def filter_list_queryset(
        self,
        context,
        instance: models.MaterialListPluginBaseModel,
        queryset: QuerySet[models.Material],
    ) -> QuerySet[models.Material]:
        """Filter the queryset based on the instance."""
        queryset = get_objects_for_user(
            context["request"].user, "view_material", queryset
        )
        filter_kws = self.get_filter_kws(context, instance)
        if filter_kws:
            queryset = queryset.filter(**filter_kws)
        if instance.all_keywords_required:
            keywords = instance.keywords.values_list("pk", flat=True)
            if keywords:
                for pk in keywords:
                    queryset = queryset.filter(keywords__pk=pk)
        return queryset

    def render(
        self,
        context,
        instance: models.MaterialListPluginBaseModel,  # type: ignore[override]
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        qs = context["material_list"].prefetch_related(
            "activitymaterialrelation_set",
            "channelmaterialrelation_set",
            "sessionmaterialrelation_set",
            "topicmaterialrelation_set",
        )
        context["material_list"] = context["object_list"] = qs
        context["show_links"] = instance.show_links

        return context


@plugin_pool.register_plugin
class CommunityMaterialListPublisher(MaterialListPublisher):
    """A plugin to display community material to the user."""

    model = models.CommunityMaterialListPluginModel
    name = _("Community Material List")

    def render(self, *args, **kwargs):
        context = super().render(*args, **kwargs)
        context["material_list_url"] = reverse("material-list")
        return context


class AddMaterialButtonPublisher(CMSPluginBase):
    """A publisher to generate a button to add new material."""

    model = models.AddMaterialButtonBasePluginModel
    name = _("Add Material Button")
    module = _("Uploaded material")
    render_template = (
        "uploaded_material/components/add_material_button_plugin.html"
    )

    filter_horizontal = ["keywords"]

    def can_render_button(
        self, context, instance: models.AddMaterialButtonBasePluginModel
    ) -> bool:
        """Check if the user can add material."""
        return True

    def get_create_url(
        self, context, instance: models.AddMaterialButtonBasePluginModel
    ) -> str:
        """Get the URL to create material."""
        return ""

    def get_url_params(
        self, context, instance: models.AddMaterialButtonBasePluginModel
    ) -> Dict[str, Any]:
        """Get the parameters for the URL."""
        ret = {"next": url_for_next(context)}
        if instance.category:
            ret["category"] = instance.category.pk
        keywords = instance.keywords.values_list("pk", flat=True)
        if keywords:
            ret["keywords"] = list(keywords)
        if instance.license:
            ret["license"] = instance.license.pk
        return ret

    def render(
        self,
        context,
        instance: models.AddMaterialButtonBasePluginModel,
        placeholder,
    ):
        can_render = self.can_render_button(context, instance)
        if not can_render:
            return {"hide_add_button": True}
        else:
            uri = self.get_create_url(context, instance)
            params = self.get_url_params(context, instance)
            enc_params = urlencode(params, doseq=True)
            classes = instance.attributes.get("class")
            if not classes:
                instance.attributes["class"] = concat_classes(
                    ["btn", "btn-primary", "btn-create"]
                    + [instance.attributes.get("extra_classes")]
                )
            else:
                instance.attributes["class"] = concat_classes(["btn", classes])
            instance.attributes.pop("extra_classes", None)
            return {
                "material_add_url": uri + "?" + enc_params,
                "button_text": instance.button_text or "",
                "instance": instance,
            }


@plugin_pool.register_plugin
class AddCommunityMaterialButtonPublisher(AddMaterialButtonPublisher):
    """A publisher to generate a button to add new material."""

    model = models.AddCommunityMaterialButtonPluginModel
    name = _("Add Community Material Button")

    filter_horizontal = AddMaterialButtonPublisher.filter_horizontal + [
        "group_view_permission",
        "group_change_permission",
        "user_view_permission",
        "user_change_permission",
    ]

    def can_render_button(
        self, context, instance: models.AddMaterialButtonBasePluginModel
    ) -> bool:
        return models.Material.has_add_permission(context["request"].user)

    def get_create_url(
        self, context, instance: models.AddMaterialButtonBasePluginModel
    ) -> str:
        return models.Material.get_create_url_from_kwargs()

    def get_url_params(
        self, context, instance: models.AddMaterialButtonBasePluginModel
    ) -> Dict[str, Any]:
        ret = super().get_url_params(context, instance)
        group_view_permission = instance.group_view_permission.values_list(
            "pk", flat=True
        )
        if group_view_permission:
            ret["group_view_permission"] = list(group_view_permission)
        group_change_permission = instance.group_change_permission.values_list(
            "pk", flat=True
        )
        if group_change_permission:
            ret["group_change_permission"] = list(group_change_permission)
        user_view_permission = instance.user_view_permission.values_list(
            "pk", flat=True
        )
        if user_view_permission:
            ret["user_view_permission"] = list(user_view_permission)
        user_change_permission = instance.user_change_permission.values_list(
            "pk", flat=True
        )
        if user_change_permission:
            ret["user_change_permission"] = list(user_change_permission)
        return ret
