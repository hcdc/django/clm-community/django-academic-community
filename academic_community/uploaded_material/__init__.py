"""App for uploading material."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations


def allow_members(private_file):
    u = private_file.request.user
    return not u.is_anonymous and (u.is_superuser or u.is_staff or u.is_member)
