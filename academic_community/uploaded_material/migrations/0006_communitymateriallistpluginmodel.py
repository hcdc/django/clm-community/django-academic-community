# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.15 on 2022-09-07 16:20

import django.db.models.deletion
import djangocms_frontend.fields
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("cms", "0022_auto_20180620_1551"),
        ("auth", "0012_alter_user_first_name_max_length"),
        ("uploaded_material", "0005_communitymaterial"),
    ]

    operations = [
        migrations.CreateModel(
            name="CommunityMaterialListPluginModel",
            fields=[
                (
                    "cmsplugin_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        related_name="uploaded_material_communitymateriallistpluginmodel",
                        serialize=False,
                        to="cms.cmsplugin",
                    ),
                ),
                (
                    "category",
                    models.ForeignKey(
                        blank=True,
                        help_text="Select a category that you want to filter for.",
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="uploaded_material.materialcategory",
                    ),
                ),
                (
                    "keywords",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Select keywords that you want to filter for.",
                        to="uploaded_material.MaterialKeyword",
                    ),
                ),
                (
                    "license",
                    models.ForeignKey(
                        blank=True,
                        help_text="Select a license that you want to filter for?",
                        limit_choices_to={"active": True},
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="uploaded_material.license",
                    ),
                ),
                (
                    "all_keywords_required",
                    models.BooleanField(
                        default=False,
                        help_text="Show only material that has all of the mentioned keywords.",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("cms.cmsplugin",),
        ),
        migrations.CreateModel(
            name="AddCommunityMaterialButtonPluginModel",
            fields=[
                (
                    "cmsplugin_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        related_name="uploaded_material_addcommunitymaterialbuttonpluginmodel",
                        serialize=False,
                        to="cms.cmsplugin",
                    ),
                ),
                (
                    "button_text",
                    models.CharField(
                        blank=True,
                        default="Add material",
                        help_text="Display text on the button",
                        max_length=100,
                        null=True,
                    ),
                ),
                (
                    "category",
                    models.ForeignKey(
                        blank=True,
                        help_text="Select a category that you want to filter for.",
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="uploaded_material.materialcategory",
                    ),
                ),
                (
                    "group_change_permission",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Groups with write permission (this does not include read permission!)",
                        related_name="addcommunitymaterialpluginmodel_write",
                        to="auth.Group",
                    ),
                ),
                (
                    "group_view_permission",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Groups with read permission",
                        related_name="addcommunitymaterialpluginmodel_read",
                        to="auth.Group",
                    ),
                ),
                (
                    "keywords",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Select keywords that you want to filter for.",
                        to="uploaded_material.MaterialKeyword",
                    ),
                ),
                (
                    "license",
                    models.ForeignKey(
                        blank=True,
                        help_text="Select a license that you want to filter for?",
                        limit_choices_to={"active": True},
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="uploaded_material.license",
                    ),
                ),
                (
                    "attributes",
                    djangocms_frontend.fields.AttributesField(
                        blank=True, default=dict, verbose_name="Attributes"
                    ),
                ),
                (
                    "user_change_permission",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Users with explicit write permission (this does not include read permission!)",
                        related_name="addcommunitymaterialpluginmodel_write",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "user_view_permission",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Users with explicit read permission",
                        related_name="addcommunitymaterialpluginmodel_read",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("cms.cmsplugin",),
        ),
    ]
