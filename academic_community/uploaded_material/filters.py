"""Filter sets for the uploaded material views."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING, List

import django_filters
from django.utils.translation import gettext as _

from academic_community.activities.models import Activity
from academic_community.filters import ActiveFilterSet
from academic_community.uploaded_material import models

if TYPE_CHECKING:
    from django.db.models import QuerySet


class MaterialFilterSet(ActiveFilterSet):
    """A filterset for uploaded material."""

    class Meta:
        model = models.Material
        fields = {
            "name": ["icontains"],
            "category": ["exact"],
            "last_modification_date": ["range", "gte", "lte"],
            "content": ["icontains"],
            "date_created": ["range", "gte", "lte"],
            "license": ["exact"],
            "file_size": ["range", "gte", "lte"],
            "keywords": ["exact"],
            "description": ["icontains"],
            "group_view_permission": ["exact"],
        }

    o = django_filters.OrderingFilter(
        label="Order by",
        fields=(
            ("name", "name"),
            ("date_created", "date_created"),
            ("last_modification_date", "last_modification_date"),
            ("file_size", "file_size"),
        ),
        field_labels={
            "name": "Material name",
            "date_created": "Creation date",
            "last_modification_date": "Last modification date",
            "file_size": "File size of the upload",
        },
    )

    date_from_to_model_fieds = ActiveFilterSet.date_from_to_model_fieds + [
        "date_created"
    ]

    date_created = django_filters.DateRangeFilter()

    last_modification_date = django_filters.DateRangeFilter()

    external_url = django_filters.BooleanFilter(
        method="filter_external", label="Is hosted externally"
    )

    activity = django_filters.ModelChoiceFilter(
        field_name="name",
        label=_("Working Group"),
        method="filter_for_activity",
        queryset=Activity.objects.all(),
    )

    def get_template_for_active_filter(self, key: str) -> List[str]:
        if key == "keywords":
            return ["uploaded_material/components/badges/materialkeyword.html"]
        else:
            return super().get_template_for_active_filter(key)

    def filter_external(
        self, queryset: QuerySet[models.Material], name: str, value: bool
    ) -> QuerySet[models.Material]:
        """Filter material that is hosted at an external location."""
        if value is None:
            return queryset
        elif value:
            return queryset.filter(external_url__isnull=False)
        else:
            return queryset.filter(external_url__isnull=True)

    def filter_for_activity(self, queryset, name, value):
        if value is None:
            return queryset
        return queryset.filter(activitymaterialrelation__activity=value)

    @property
    def form(self):
        form = super().form
        if "keywords" in form.fields:
            form.fields[
                "keywords"
            ].queryset = models.MaterialKeyword.objects.filter(
                pk__in=self.queryset.values_list("keywords", flat=True)
            )
        return form
