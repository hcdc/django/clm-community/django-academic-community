"""Admin interface for uploaded material."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.contrib import admin
from django.db.models.deletion import Collector, ProtectedError
from django.utils.safestring import mark_safe
from guardian.admin import GuardedModelAdmin
from reversion_compare.admin import CompareVersionAdmin

from academic_community.admin import ManagerAdminMixin
from academic_community.uploaded_material import models


@admin.register(models.License)
class LicenseAdmin(ManagerAdminMixin, admin.ModelAdmin):
    """Admin for licenses."""

    search_fields = ["name", "identifier"]

    list_display = [
        "name",
        "identifier",
        "active",
        "in_use",
        "public_default",
        "internal_default",
        "website",
    ]

    list_editable = ["active", "public_default", "internal_default"]

    list_filter = ["active"]

    def website(self, obj: models.License) -> str:
        """Get the link to the license."""
        if not obj.url:
            return ""
        else:
            return mark_safe(f"<a href='{obj.url}'>License text</a>")

    def in_use(self, obj: models.License) -> bool:
        """Test if a license is in use."""
        if obj.public_default or obj.internal_default:
            return True
        collector = Collector(using="default")
        try:
            collector.collect([obj])
        except ProtectedError:
            return True
        else:
            return False


@admin.register(models.MaterialCategory)
class MaterialCategoryAdmin(ManagerAdminMixin, admin.ModelAdmin):
    """Admin for the MaterialCategory."""

    search_fields = ["name"]

    list_display = ["name", "uploaded_material_count"]

    def uploaded_material_count(self, obj: models.MaterialCategory) -> int:
        """Get the number of uploaded material for the given category."""
        return obj.material_set.count()


@admin.register(models.Material)
class MaterialAdmin(ManagerAdminMixin, GuardedModelAdmin, CompareVersionAdmin):
    """An admin for material"""

    search_fields = ["name__icontains", "description__icontains", "uuid"]

    list_display = ["name", "date_created", "user"]

    list_filter = ["license", "keywords", "date_created"]
