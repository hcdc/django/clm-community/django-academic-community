# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

import hashlib
import json
import re
from functools import partial
from itertools import chain, islice
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    Iterable,
    List,
    Optional,
    Sequence,
    Union,
    cast,
)
from urllib.parse import parse_qs, parse_qsl, quote, urlparse, urlunparse

from bs4 import BeautifulSoup, Comment, NavigableString
from classytags.arguments import (
    Argument,
    MultiKeywordArgument,
    MultiValueArgument,
)
from classytags.core import Options
from classytags.helpers import InclusionTag, Tag
from cms.models import PageContent
from cms.toolbar.utils import get_object_edit_url, get_object_preview_url
from django import template, urls
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.humanize.templatetags.humanize import naturalday
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Model, QuerySet
from django.utils.http import urlencode
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.utils.timezone import now
from djangocms_versioning.constants import DRAFT
from guardian.shortcuts import get_objects_for_user
from menus.templatetags.menu_tags import ShowMenu

from academic_community import utils
from academic_community.members.models import CommunityMember

if TYPE_CHECKING:
    from cms.models import Page
    from django.contrib.auth.models import Group, User
    from django.forms import Form
    from djangocms_text_ckeditor.models import Text


register = template.Library()


@register.inclusion_tag("base_components/pwa.html", takes_context=True)
def pwa_head(context):
    # Pass all PWA_* settings into the template
    context = context.flatten()
    context.setdefault(
        "pwa_splash_screens", getattr(settings, "PWA_APP_SPLASH_SCREEN", [])
    )
    return context


@register.filter
def as_list_items(html, classes=""):
    """Split an HTML document and wrap it's children in to li-tags."""
    soup = BeautifulSoup(html, "html.parser")
    # remove comments
    for elem in soup(
        text=lambda text: isinstance(text, (Comment, NavigableString)),
        recursive=False,
    ):
        elem.extract()
    # wrap non-empty elements with li-tags
    for elem in soup:
        if not getattr(elem, "is_empty_element", False):
            new_tag = soup.new_tag("li", attrs={"class": classes})
            elem.wrap(new_tag)
        else:
            elem.extract()
    return soup.prettify()


@register.filter
def as_mb(bytes: Optional[int]) -> str:
    """Transform bytes into mega bytes"""
    if bytes is None:
        return "unknown"
    return "%1.1f MB" % (bytes / 1048576,)


@register.filter(is_safe=True)
def trailing_slash(url: str):
    """Add a trailing slash to a url (unless there is one already)."""
    url = str(url)
    return url if url.endswith("/") else (url + "/")


@register.filter
def order_by(queryset: QuerySet, field: str):
    return queryset.order_by(field)


@register.simple_tag(takes_context=True)
def page_edit_url(context, editable_object=None) -> str:
    if editable_object is None:
        content = None
        page = context["request"].current_page
        qs = PageContent.admin_manager.filter(page=page)
        for content in chain(qs.latest_content(), qs.current_content()):
            if content.versions.first().state == DRAFT:
                editable_object = content
                break

        if editable_object is None and content is not None:
            return get_object_preview_url(content) + "?toolbar_on"
    else:
        content = editable_object
    return get_object_edit_url(content) if content else ""


@register.filter(is_safe=True)
def js(obj):
    """Serialize a python object to JSON"""
    return mark_safe(json.dumps(obj, cls=DjangoJSONEncoder))


@register.filter
def md5(value: str) -> str:
    return hashlib.md5(value.encode()).hexdigest()


@register.filter
def str_add(value: Any, arg: Any):
    """Append the string representations of two objects."""
    return str(value) + str(arg)


@register.simple_tag
def replace(input: str, old: str, new: str):
    """Replace a value in a string"""
    return input.replace(str(old), str(new))


@register.filter
def ifthenelse(value: Any, arg_if_else: str) -> Any:
    """Conditional choice rendering of arguments.

    The first character of the input argument must be the separator between the
    strings to use. E.g. something like::

        {{ True|ifthenelse:",true,false" }}
    """
    sep = arg_if_else[0]
    arg_if, arg_else = arg_if_else[1:].split(sep)
    return arg_if if value else arg_else


@register.filter
def braced(value: str) -> str:
    """Surround a string with curved braces.

    The output will be equivalent to::

        {% templatetag openbrace %}{{ value }}{% templatetag closebrace %}
    """
    return "{" + str(value) + "}"


@register.filter
def ifthen(value: Any, arg_if: Any) -> Any:
    """Conditional choice rendering of arguments.

    If `value` evaluates to ``True``, select `arg_if`, else return an empty
    string
    """
    return arg_if if value else ""


@register.filter
def op_or(cond1, cond2) -> bool:
    return cond1 or cond2


@register.filter
def op_and(cond1, cond2) -> bool:
    return cond1 and cond2


@register.filter
def ifnotthen(value: Any, arg_if: Any) -> Any:
    """Conditional choice rendering of arguments.

    If `value` evaluates to ``True``, select `arg_if`, else return an empty
    string
    """
    return arg_if if not value else ""


@register.filter
def invert(value: bool) -> bool:
    """Convenience filter to invert a boolean expression."""
    return not bool(value)


@register.filter
def subtract(value, arg):
    return value - arg


@register.filter
def equals(value: Any, arg: Any) -> Any:
    """Test if two elements are the same with a filter.

    This may be used to within a filter chain, such as
    ``1|equals:2|ifthen:"equal"``
    """
    return value == arg


@register.filter
def combine(value: Any, arg: Any) -> Any:
    """Append a value to a list."""
    return [value] + [arg]


@register.filter
def as_list(value: Any) -> List:
    """Transform a value into a list."""
    return [value]


@register.filter
def append(value: List, arg: Any):
    """Append a new value to an existing list."""
    return value + [arg]


def tex_escape(text: str) -> str:
    """Escape latex special characters in string

    Parameters
    ----------
    text : str
        The text to escape

    Returns
    -------
    str
        A safe latex text
    """
    if not text:
        return text
    conv = {
        "&": r"\&",
        "%": r"\%",
        "$": r"\$",
        "#": r"\#",
        "_": r"\_",
        "{": r"\{",
        "}": r"\}",
        "~": r"\textasciitilde{}",
        "^": r"\^{}",
        "\\": r"\textbackslash{}",
        "<": r"\textless{}",
        ">": r"\textgreater{}",
    }
    regex = re.compile(
        "|".join(
            re.escape(str(key))
            for key in sorted(conv.keys(), key=lambda item: -len(item))
        )
    )
    return regex.sub(lambda match: conv[match.group()], text)


@register.filter
def values_list(
    objects: Union[QuerySet, List[Union[Dict, Model]]], attr: str
) -> List[Any]:
    def _get(obj, attr):
        return (
            get_item(obj, attr)
            if isinstance(obj, dict)
            else get_attr(obj, attr)
        )

    if isinstance(objects, QuerySet):  # type: ignore[misc]
        return list(objects.values_list(attr, flat=True))
    else:
        ret = []
        splitted = attr.split(".", 1)
        if len(splitted) == 1:
            return [_get(obj, attr) for obj in objects]
        else:
            for item in objects:
                base, remainder = splitted
                ret.extend(values_list([_get(item, base)], remainder))
        return ret


@register.filter
def unique_values_list(
    objects: Union[QuerySet, List[Union[Dict, Model]]], attr: str
) -> List[Any]:
    return list(utils.unique_everseen(values_list(objects, attr)))


@register.simple_tag
def get_admin_url_name(model, what="change"):
    if isinstance(model, ContentType):
        app_label = model.app_label
        name = model.model
    else:
        app_label = model._meta.app_label
        name = model._meta.model_name

    if name == "academicorganization":
        name = model.organization._meta.model_name
    return f"admin:{app_label}_{name}_{what}"


@register.filter
def object_id(model) -> str:
    """Generate an ID for the use in a template."""
    app_label = model._meta.app_label
    name = model._meta.model_name
    return f"{app_label}-{name}-{model.id}"


@register.filter
def model_name(model) -> str:
    """Get the name of a model class."""
    return model._meta.model_name


@register.filter
def verbose_model_name(model) -> str:
    """Get the name of a model class."""
    return " ".join(map(str.capitalize, model._meta.verbose_name.split()))


@register.filter
def verbose_model_name_plural(model) -> str:
    """Get the name of a model class."""
    return " ".join(
        map(str.capitalize, model._meta.verbose_name_plural.split())
    )


@register.simple_tag
def membership_description(membership, object, role=None) -> str:
    """Describe a membership for an object."""
    start_date = naturalday(membership.start_date)
    end_date = naturalday(membership.end_date)
    if start_date and end_date:
        return (
            f"{role or 'Member in'} {object} since {start_date}, "
            f"ended {end_date}"
        )
    elif start_date:
        return f"{role or 'Member in'} {object} since {start_date}"
    elif end_date:
        return f"{role or 'Membership in'} {object} ended {end_date}"
    else:
        return f"{role or 'Member in'} {object}"


@register.simple_tag(takes_context=True)
def filter_for_user(
    context, perm: Union[List[str], str], queryset: QuerySet
) -> QuerySet:
    """Filter a queryset for a user.

    This assumes that the queryset has a get_for_user method that takes a user
    as argument.
    """
    return get_objects_for_user(context.get("user"), perm, queryset)


@register.filter
def filter_page_texts(page: Page, query: str) -> QuerySet[Text]:
    """Get all text elements for a page that contain a keyword"""
    from cms.models.contentmodels import EmptyPageContent
    from django.contrib.contenttypes.models import ContentType
    from djangocms_text_ckeditor.models import Text

    content = page.get_content_obj()
    if isinstance(content, EmptyPageContent):
        return Text.objects.none()
    ct = ContentType.objects.get_for_model(content)

    return Text.objects.filter(
        placeholder__content_type=ct,
        placeholder__object_id=content.id,
        body__icontains=query,
    )


@register.filter
def filter_true(seq: Sequence) -> List:
    return list(filter(None, seq))


@register.filter
def replace_with_pk(data: Dict[str, Any]) -> Dict[str, Any]:
    """A filter to replace all objects with their pk.

    Useful if you, for instance, want to change JSON serialize a cleaned_data
    of a form.
    """
    try:
        data = data.copy()
    except AttributeError:
        return data
    for key, val in data.items():
        if isinstance(val, Model):
            data[key] = val.pk
        elif isinstance(val, QuerySet):  # type: ignore[misc]
            data[key] = list(val.values_list("pk", flat=True))
        else:
            try:
                iter(val)
            except (TypeError, ValueError):
                pass
            else:
                val = list(val)
                if val and isinstance(val[0], Model):
                    data[key] = [v.pk for v in val]
    return data


@register.simple_tag
def errored_fields(form: Form) -> str:
    """Get the fields with errors."""
    fields: List[str] = []
    for field in form:
        if field.errors:
            fields.append(field.name)
    return mark_safe(", ".join(map("<i>{}<i>".format, fields)))


@register.simple_tag(takes_context=True)
def url_for_next(context):
    try:
        context = context.flatten()
    except AttributeError:  # already a dict
        pass
    if "request" in context:
        request = context["request"]
    else:
        request = context["view"].request
    parts = urlparse(request.path)
    url = urlunparse(
        [
            parts.scheme,
            parts.netloc,
            parts.path,
            parts.params,
            request.GET.urlencode(),
            parts.fragment,
        ]
    )
    return quote(url)


@register.simple_tag
def get_initials(user: User) -> str:
    """Return the initials of the user.

    This tag takes the first_name and last_name of the user and returns the
    initials. If there is not first_name or last_name, we take the first two
    letters of the username.
    """
    first = user.first_name
    last = user.last_name
    if first and last:
        return first[:1] + last[:1]
    else:
        return user.username[:2]


@register.simple_tag(takes_context=True)
def remove_param(context, key, path: Optional[str]) -> str:
    """Remove a param from a url."""
    context = context.flatten()
    if path is None:
        parts = urlparse(context["request"].path)
    else:
        parts = urlparse(path)
    params = parse_qs(context["request"].GET.urlencode())
    params.pop(key, None)
    url = urlunparse(
        [
            parts.scheme,
            parts.netloc,
            parts.path,
            parts.params,
            urlencode(params, doseq=True),
            parts.fragment,
        ]
    )
    return url


@register.tag
class CombineUrlParamsTag(Tag):
    """A tag to combine multiple url params into one."""

    name = "combine_url_params"

    options = Options(MultiValueArgument("url_params", required=True))

    def render_tag(self, context, url_params: List) -> str:
        """Combine multiple url params into one query string."""
        combined_params = list(
            chain.from_iterable(map(parse_qsl, filter(None, url_params)))
        )
        if combined_params:
            return urlencode(combined_params, doseq=True)  # type: ignore
        else:
            return ""


@register.simple_tag(takes_context=True)
def namespace_url(context, view_name, *args, **kwargs):
    """
    Returns an absolute URL matching named view with its parameters and the
    provided application instance namespace.

    If no namespace is passed as a kwarg (or it is "" or None), this templatetag
    will look into the request object for the app_config's namespace. If there
    is still no namespace found, this tag will act like the normal {% url ... %}
    template tag.

    Normally, this tag will return whatever is returned by the ultimate call to
    reverse, which also means it will raise NoReverseMatch if reverse() cannot
    find a match. This behaviour can be override by suppling a 'default' kwarg
    with the value of what should be returned when no match is found.

    Notes
    -----
    This is a reimplemented tag from the original aldryn_apphooks_config
    implementation to account for the case when there are pages below an
    apphook
    """

    namespace = kwargs.pop("namespace", None)

    if not namespace:
        namespace, __ = utils.get_app_instance(context["request"])

    if namespace:
        namespace += ":"

    reverse = partial(urls.reverse, "{:s}{:s}".format(namespace, view_name))

    # We're explicitly NOT happy to just re-raise the exception, as that may
    # adversely affect stack traces.
    if "default" not in kwargs:
        if kwargs:
            return reverse(kwargs=kwargs)
        elif args:
            return reverse(args=args)
        else:
            return reverse()

    default = kwargs.pop("default", None)
    try:
        if kwargs:
            return reverse(kwargs=kwargs)
        elif args:
            return reverse(args=args)
        else:
            return reverse()
    except urls.NoReverseMatch:
        return default


@register.filter
def highlight(text: Any, keyword: str) -> str:
    """Highlight text in an HTML snipped

    Parameters
    ----------
    text : str
        The HTML formatted text that shall be searched
    keyword : str
        The text to highlight

    Returns
    -------
    str
        The highlighted text
    """
    soup = BeautifulSoup(str(text), "html.parser")
    for n in soup.find_all(string=re.compile(keyword, flags=re.I)):
        new_text = BeautifulSoup(
            highlight_in_text(n.text, keyword), "html.parser"
        )
        n.replaceWith(new_text)
    return mark_safe(soup.prettify())


@register.simple_tag(takes_context=True)
def render_text(context, instance: Text) -> str:
    """Render the text for a text plugin."""
    from djangocms_text_ckeditor.utils import plugin_tags_to_user_html

    return mark_safe(plugin_tags_to_user_html(instance.body, context))


@register.filter
def link_to(text, url):
    """Get the ancestors of a page."""
    return mark_safe("<a href='%s'>%s</a>" % (url, text))


@register.filter
def highlight_in_text(text: str, keyword: str) -> str:
    """Highlight text in an HTML snipped

    Parameters
    ----------
    text : str
        The HTML formatted text that shall be searched
    keyword : str
        The text to highlight

    Returns
    -------
    str
        The highlighted text
    """
    text = str(text)
    text = re.sub(
        r"(?i)(%s)" % keyword, r"<mark class='marker-yellow'>\1</mark>", text
    )
    return mark_safe(text)


@register.simple_tag(takes_context=True)
def add_multiple_to_filters(
    context, key, vals, remove: bool = False, path: Optional[str] = None
) -> str:
    ret = path or ""
    params: Optional[Dict] = None
    for i, val in enumerate(vals):
        ret = add_to_filters(
            context,
            key,
            val,
            (remove and i == 0) or remove,
            ret or None,
            params,
        )

        remove = False
        params = parse_qs(urlparse(ret).query)
    return ret


@register.simple_tag
def add_to_query(url, key, val) -> str:
    """Add a new query parameter to a url."""
    parts = urlparse(url)
    query = parts.query
    if query:
        query += "&"
    query += f"{key}={quote(val)}"
    return urlunparse(
        [
            parts.scheme,
            parts.netloc,
            parts.path,
            parts.params,
            query,
            parts.fragment,
        ]
    )


@register.simple_tag(takes_context=True)
def add_to_filters(
    context,
    key,
    val,
    remove: bool = False,
    path: Optional[str] = None,
    params: Optional[Dict] = None,
) -> str:
    """Add a filter to the url param."""
    context = context.flatten()
    if path is None:
        parts = urlparse(context["request"].path)
    else:
        parts = urlparse(path)
    if params is None:
        params = parse_qs(context["request"].GET.urlencode())

    # remove the page in case we have pagination
    params.pop(context.get("page_parameter_name", "page"), None)
    val = str(val)
    if key in params and remove:
        del params[key]
    if key in params:
        if val not in params[key]:
            params[key].append(val)
    else:
        params[key] = [val]
    url = urlunparse(
        [
            parts.scheme,
            parts.netloc,
            parts.path,
            parts.params,
            urlencode(params, doseq=True),
            parts.fragment,
        ]
    )
    return url


@register.filter
def increase_heading(start: str, inc: int):
    """Increase a heading level.

    Examples
    --------
    ```
        <{{ "h1"|increase_heading:2 }}>is heading h3</{{ "h1"|increase_heading:2 }}>
    ```
    """
    return "h%i" % (int(start[1:]) + inc)


@register.filter
def min_heading(html: str, min_heading: Union[str, int]):
    """Increase all heading levels in an html formatted string to a minimum level.

    Examples
    --------
    ```
        {% filter min_heading:"h2" %}
          <h1>some heading</h1>  <!--becomes h2 -->
          <h2>some sup heading</h2>  <!--becomes h3 -->
        {% endfilter %}
    ```
    """
    soup = BeautifulSoup(html, "html.parser")
    if isinstance(min_heading, str) and min_heading.startswith("h"):
        min_number = int(min_heading[1:])
    else:
        min_number = int(min_heading)
    elems = soup.find_all(re.compile(r"h\d+"))
    if not elems:
        # no heading found
        return html
    current_min = min(int(tag.name[1:]) for tag in elems)
    increase = min_number - current_min
    for tag in soup.find_all(re.compile(r"h\d+")):
        current_number = int(tag.name[1:])
        tag.name = "h%i" % (current_number + increase,)
    return mark_safe(soup.prettify())


@register.filter
def same_url(url1: str, url2: Optional[str] = None) -> bool:
    parts1 = urlparse(url1)
    parts2 = urlparse(url2)
    p1: str = parts1.path  # type: ignore
    p2: str = parts2.path  # type: ignore
    if not p1.endswith("/"):
        p1 += "/"
    if not p2.endswith("/"):
        p2 += "/"
    return p1 == p2


@register.inclusion_tag("academic_community/components/utils/close_icon.html")
def close_icon() -> Dict:
    """Utility tag to display a close icon."""
    return {}


@register.inclusion_tag(
    "academic_community/components/login_alert.html", takes_context=True
)
def login_alert(context):
    """Render a simple alert for login."""
    return context.flatten()


@register.filter
def take(objects: Iterable, N: int) -> List:
    """Take a certain amount of objects from a sequence."""
    return list(islice(objects, N))


@register.filter
def list_getattr(iterable: Iterable, attr: str) -> List:
    """Take a list and return a given attribute of all objects in the list.

    Suppose you have a list like ``letters = ["a", "b", "c"]``, then::

        {{ letters|list_getattr:"upper" }}

    will give you ``["A", "B", "C"]``.

    Notes
    -----
    If the given ``attr`` is callable, it will be called. Otherwise it will be
    taken as it is. If an object does not have the given `attr`, it will be
    inserted as ``None``
    """
    ret = []
    for obj in iterable:
        val = getattr(obj, attr, None)
        if callable(val):
            val = val()
        ret.append(val)
    return ret


@register.filter(name="any")
def any_filter(iterable: Optional[Iterable]) -> bool:
    """Take an iterable and check if any of its arguments evaluates to true.

    This is equivalent to pythons built-in :func:`any` function, e.g. in
    ``any([1,2,3])``. This filter also accepts None which will then return
    ``False``
    """
    if iterable is None:
        return False
    return any(iterable)


@register.tag
class FormsetRenderer(InclusionTag):
    """A template tag to render a formset."""

    name = "render_formset"

    options = Options(
        Argument("formset", required=True),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def get_template(self, context, formset, template_context: Dict):
        if getattr(formset, "template_name", None):
            return formset.template_name
        else:
            return "academic_community/components/formset.html"

    def get_context(self, context, formset, template_context: Dict):
        context = context.flatten()
        context.update(template_context)
        context["formset"] = formset
        if hasattr(formset, "get_extra_context"):
            context.update(formset.get_extra_context(context["request"]))
        return context


@register.tag
class FormRenderer(InclusionTag):
    """A template tag to render a formset."""

    name = "render_form"

    options = Options(
        Argument("form", required=True),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def get_template(self, context, form, template_context: Dict):
        if getattr(form, "template_name", None):
            return form.template_name
        else:
            return "academic_community/components/form.html"

    def get_context(self, context, form, template_context: Dict):
        context = context.flatten()
        context.update(template_context)
        context["form"] = form
        if hasattr(form, "get_extra_context"):
            context.update(form.get_extra_context(context["request"]))
        return context


def remove_script_duplicates_postprocessor(context, data, name):
    """A postprocessor for sekizai tags to remove duplicated scripts

    Duplicated script might have been added if multiple forms are rendered on
    the same page. This postprocessor removes duplicated script tags.
    """

    def filter_jquery(tag):
        src = tag.get("src")
        if src and jquery_patt.search(src):
            return False
        return True

    jquery_patt = re.compile(r"jquery-\d+\.\d+\.\d+\.min\.js$")

    soup = BeautifulSoup(data, "html.parser")
    unique_data = list(
        filter(filter_jquery, utils.unique_everseen(soup.find_all("script")))
    )
    return "\n".join(tag.prettify() for tag in unique_data)


@register.tag
class Section(InclusionTag):
    """A node to render a section with a heading and permalink."""

    name = "section"

    options = Options(
        Argument("heading", required=True),
        MultiKeywordArgument("template_context", required=False, default={}),
        blocks=[("endsection", "nodelist")],
    )

    template = "academic_community/components/section.html"

    def get_context(self, context, heading: str, **kwargs) -> Dict:
        """Generate the card content."""
        template_context = kwargs.pop("template_context")

        template_context["content"] = kwargs.pop("nodelist").render(context)
        template_context.update(kwargs)
        template_context["heading"] = heading
        template_context.setdefault("id", slugify(heading))

        context = context.flatten()
        context.update(template_context)
        return context


@register.tag
class ShowMenuWithUser(ShowMenu):
    """A tag to show the CMS menu with user-specific links"""

    name = "show_user_menu"

    def get_context(self, context, *args, **kwargs):
        ret = super().get_context(context, *args, **kwargs)
        ret["request"] = context.flatten()["request"]
        return ret


@register.filter
def get_item(d: Union[List, Dict], key: Any) -> Any:
    """Convenience filter for using get_item of a dictionary."""
    if hasattr(d, "get"):
        d = cast(Dict, d)
        return d.get(key)
    else:
        return d[key]


@register.filter
def index_of(values: List, key: Any) -> int:
    """Get the index of a key in the list.

    Returns `-1` if the item could not be found in the given values"""
    try:
        return values.index(key)
    except ValueError:
        return -1


@register.filter
def get_attr(o: Any, key: Any) -> Any:
    """Convenience filter for using get_attr of an python object."""
    return getattr(o, key, None)


@register.filter
def in_the_future(value):
    """Test if a date is in the future."""
    return value > now()


@register.filter
def in_the_past(value):
    """Test if a date is in the past."""
    return value < now()


@register.filter
def can_change_page(user, node):
    from cms.models import Page

    page = Page.objects.filter(id=node.id).first()
    if page:
        return user.is_staff and page.has_change_permission(user)
    else:
        return False


@register.filter
def page_from_node(node):
    from cms.models import Page

    return Page.objects.filter(id=node.id).first()


@register.simple_tag
def has_model_perm(user, model, perm="change"):
    return user.has_perm(utils.get_model_perm(model, perm))


@register.tag
class ProfilePicture(InclusionTag):
    """The profile picture of a user."""

    template = "academic_community/components/utils/profile.html"

    name = "profile"

    options = Options(
        Argument("user", required=True),
        MultiKeywordArgument("template_context", required=False, default={}),
        blocks=[("endprofile", "nodelist")],
    )

    def get_context(
        self, context, user: User, template_context: Dict, nodelist
    ):
        template_context.update(context.flatten())
        template_context["content"] = nodelist.render(context)
        template_context["user"] = user
        return template_context


@register.tag
class ProfilesPicture(InclusionTag):
    """The profile pictures of multiple users."""

    template = "academic_community/components/utils/profiles.html"

    name = "profiles"

    options = Options(
        Argument("users_or_members", required=True),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def get_context(
        self,
        context,
        users_or_members: Iterable[Union[CommunityMember, User]],
        template_context: Dict,
    ):
        template_context.update(context.flatten())
        users: List[User] = []
        for user_or_member in users_or_members:
            if (
                isinstance(user_or_member, CommunityMember)
                and user_or_member.user
            ):
                users.append(user_or_member.user)
            elif isinstance(user_or_member, User):
                users.append(user_or_member)
        template_context["users"] = users
        n = len(users)
        template_context["start_user"] = 0
        if n > 1:
            template_context["shifts"] = [(n - 1 - i) * 20 for i in range(n)]
            if n > 4:
                template_context["start_user"] = n - 4
                template_context["shifts"][0] = 80
        if "tooltip_label" in template_context:
            template_context["tooltip"] = "%s <ul>%s</ul>" % (
                template_context["tooltip_label"],
                "".join("<li>%s</li>" % user for user in users),
            )
        return template_context


@register.simple_tag
def get_members_group() -> Group:
    """Get the group for community members"""
    return utils.get_members_group()
