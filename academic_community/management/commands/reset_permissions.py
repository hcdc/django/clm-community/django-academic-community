# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.core.management.base import BaseCommand

from academic_community.management.utils import log_progress


class Command(BaseCommand):
    """Django command to migrate the database."""

    help = "Reset the permissions of all users."

    def handle(self, *args, **options):
        """Migrate the database."""

        from django.db.models import Q

        from academic_community.activities.models import Activity
        from academic_community.institutions.models import (
            AcademicMembership,
            AcademicOrganization,
        )
        from academic_community.members.models import CommunityMember
        from academic_community.topics.models import Topic

        members = CommunityMember.objects.filter(~Q(user=None))
        orgas = list(AcademicOrganization.objects.all())
        topics = list(Topic.objects.all())
        all_members = list(CommunityMember.objects.all())
        activities = list(Activity.objects.all())
        memberships = list(AcademicMembership.objects.all())

        for member in log_progress(members):
            for orga in orgas:
                orga.organization.update_permissions(member)
            for topic in topics:
                topic.update_permissions(member)
            for m in all_members:
                m.update_permissions(member)
            for activity in activities:
                activity.update_permissions(member)
            for membership in memberships:
                membership.update_permissions(member)
