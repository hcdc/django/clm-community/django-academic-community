# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import json
import os.path as osp
from typing import List

from django.core.management.base import BaseCommand
from guardian.shortcuts import assign_perm


def load_permissions(what) -> List[str]:
    with open(
        osp.join(osp.dirname(__file__), f"default_{what}_permissions.json")
    ) as f:
        return json.load(f)


class Command(BaseCommand):
    """Django command to migrate the database."""

    help = "Set default permissions for the groups."

    def handle(self, *args, **options):
        """Migrate the database."""

        from django.contrib.auth.models import Permission

        from academic_community import utils

        def assign_perms(permissions, group):
            for perm in permissions:
                app_label, codename = perm.split(".")
                for permission in Permission.objects.filter(
                    codename=codename, content_type__app_label=app_label
                ):
                    assign_perm(permission, group)

        assign_perms(load_permissions("manager"), utils.get_managers_group())
        assign_perms(load_permissions("member"), utils.get_members_group())
        assign_perms(load_permissions("cms"), utils.get_cms_group())
