# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List, Type

from django.core.management.base import BaseCommand

from .cms_content.activities import Command as ActivitiesCommand
from .cms_content.brand import Command as BrandCommand
from .cms_content.contact import Command as ContactCommand
from .cms_content.events import Command as EventsCommand
from .cms_content.footer import Command as FooterCommand
from .cms_content.home import Command as HomeCommand
from .cms_content.institutions import Command as InstitutionsCommand
from .cms_content.members import Command as MembersCommand
from .cms_content.top_nav import Command as TopNavCommand
from .cms_content.top_nav_collapsible import (
    Command as TopNavCollapsibleCommand,
)
from .cms_content.topics import Command as TopicsCommand

if TYPE_CHECKING:
    from .cms_content.base import ContentBaseCommand


class Command(BaseCommand):
    subcommands: List[Type[ContentBaseCommand]] = [
        HomeCommand,
        ActivitiesCommand,
        TopicsCommand,
        EventsCommand,
        InstitutionsCommand,
        MembersCommand,
        ContactCommand,
        BrandCommand,
        TopNavCollapsibleCommand,
        TopNavCommand,
        FooterCommand,
    ]
    instances: Dict[str, ContentBaseCommand] = {}
    help = "Create default content with the Content Management System"
    command_name = "default_content"
    stealth_options = ()

    subcommand_dest = "subcmd"

    def add_arguments(self, parser):
        self.instances = {}

        from guardian.conf import settings as guardian_settings

        parser.add_argument(
            "--home-id",
            help="The id for the parent page. Default %(default)s.",
            default="home",
        )

        parser.add_argument(
            "-a",
            "--all",
            help="Create and setup all pages.",
            action="store_true",
            dest="create_all",
        )

        parser.add_argument(
            "-u",
            "--user",
            default=guardian_settings.ANONYMOUS_USER_NAME,
            dest="username",
            help=(
                "The username that shall be used for creating the content. "
                "By default we use the anonymous user (%(default)s)."
            ),
        )

        parser.add_argument(
            "--no-publish",
            action="store_false",
            dest="publish",
            help="Do not publish the pages or contents.",
        )

        if self.subcommands:
            stealth_options = set(self.stealth_options)
            subparsers = parser.add_subparsers(dest=self.subcommand_dest)
            for cls in self.subcommands:
                instance = cls(self.stdout._out, self.stderr._out)
                kwargs = {}
                parser_sub = subparsers.add_parser(
                    name=instance.command_name,
                    help=instance.help,
                    description=instance.help,
                    **kwargs,
                )

                instance.add_arguments(parser_sub)
                stealth_options.update(
                    {action.dest for action in parser_sub._actions}
                )
                self.instances[cls.command_name] = instance
            self.stealth_options = tuple(stealth_options)

    def handle(self, create_all: bool = False, **options):
        from django.db import transaction

        if create_all:
            assert options.get(self.subcommand_dest) is None
            with transaction.atomic():
                for command in self.instances.values():
                    subparser_options = options.copy()
                    subparser = command.create_parser(
                        self.command_name, command.command_name
                    )
                    subparser_args = subparser.parse_args([])
                    subparser_options.update(vars(subparser_args))
                    if command.include_in_all:
                        command.handle(**subparser_options)

        elif options[self.subcommand_dest] in self.instances:
            command = self.instances[options[self.subcommand_dest]]

            command.handle(**options)
        else:
            self.print_help("manage.py", self.command_name)
