# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Command to generate the home page."""
from __future__ import annotations

from typing import TYPE_CHECKING, Dict

from django.contrib.auth.models import User

from .base import PageBaseCommand

if TYPE_CHECKING:
    from cms.models import Page


class Command(PageBaseCommand):
    """A command to generate the home page from the CMS"""

    command_name = "home"

    help = "Generate the home page"

    default_slug = "home"

    page_requires_parent = False

    include_in_all = True

    @property
    def default_title(self) -> str:  # type: ignore[override]
        from academic_community import utils

        return utils.get_community_name()

    def add_arguments(self, parser):
        parser.add_argument(
            "--no-home",
            action="store_false",
            dest="set_as_homepage",
            help="Do not set the created page as homepage",
        )

        return super().add_arguments(parser)

    def create_page(  # type: ignore[override]
        self,
        parent: Page,
        title: str,
        menu_title: str | None,
        slug: str,
        reverse_id: str,
        user: User,
        options: Dict,
        **kwargs,
    ):
        ret = super().create_page(
            parent,
            title,
            menu_title,
            slug,
            reverse_id,
            user,
            options,
            **kwargs,
        )

        if options["set_as_homepage"]:
            ret.set_as_homepage()
        return ret
