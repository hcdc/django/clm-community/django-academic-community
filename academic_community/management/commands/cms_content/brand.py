# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Command to create a static alias with the brand."""

from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List

from .base import StaticAliasBaseCommand

if TYPE_CHECKING:
    from cms.models import Placeholder


class Command(StaticAliasBaseCommand):
    """A command to create a static alias with the brand."""

    command_name = "brand"

    static_code = "brand"

    site_specific = True

    include_in_all = True

    help = "Create a static alias with brand logo."

    def get_plugin_config_for_placeholder(
        self, placeholder: Placeholder, **options
    ) -> List[Dict]:
        from academic_community import utils

        home = self.get_page(options["home_id"])
        if not home:
            raise ValueError(
                "Could not find home page with id %(home_id)s" % options
            )
        return [
            {
                "plugin_type": "ImagePlugin",
                "values": {
                    "config": {
                        "external_picture": "/static/images/logo.png",
                        "picture": None,
                        "alignment": None,
                        "picture_fluid": None,
                        "picture_rounded": None,
                        "picture_thumbnail": None,
                        "template": "default",
                        "internal_link": {"pk": home.pk, "model": "cms.page"},
                        "lazy_loading": True,
                        "link_attributes": {
                            "class": "navbar-brand float-left me-0",
                        },
                        "attributes": {
                            "class": "nav-logo",
                            "alt": utils.get_community_name(),
                        },
                    }
                },
            }
        ]
