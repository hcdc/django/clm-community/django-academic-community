# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Module to generate default content for an institutions apphook."""
from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List

from .base import AppHookPageBaseCommand

if TYPE_CHECKING:
    from cms.models import Page


class Command(AppHookPageBaseCommand):
    """A command to create an institutions page and apphook"""

    help = "Create a webpage to display institutions."

    apphook = "InstitutionsApphook"

    command_name = "institutions"

    default_slug = "institutions"

    default_title = "Institutions"

    include_in_all = True

    def get_placeholder_slots(self, **options) -> List[str]:
        return ["content", "action_buttons"]

    def setup_extensions(self, page: Page):
        from academic_community.models import PageMenuExtension

        extension, created = PageMenuExtension.objects.get_or_create(
            extended_object=page,
            defaults=dict(
                menu_get_params="end_date__isnull=true",
            ),
        )
        if not created:
            extension.menu_get_params = "end_date__isnull=true"
            extension.save()

    def get_plugin_config_for_placeholder(
        self, placeholder, **options
    ) -> List[Dict]:
        from academic_community import utils

        if placeholder.slot == "content":
            return [
                {
                    "plugin_type": "AlphabetFilterButtonsPlugin",
                    "values": {"filter_name": "abbreviation"},
                },
                {
                    "plugin_type": "UserGroupBasedContentPlugin",
                    "values": dict(
                        is_member=True,
                        is_manager=True,
                    ),
                    "children": [
                        {
                            "plugin_type": "InstitutionListPublisher",
                            "values": dict(
                                show_institution_logos=True,
                                show_child_organizations=True,
                                show_all_child_organizations=True,
                                show_all=True,
                                enable_filters=True,
                                show_filter_button=False,
                                show_active_filters=True,
                                active_institutions=True,
                                former_institutions=True,
                            ),
                        },
                    ],
                },
                {
                    "plugin_type": "UserGroupBasedContentPlugin",
                    "values": dict(
                        is_superuser=False,
                    ),
                    "m2m_values": {
                        "groups": [
                            utils.get_anonymous_group(),
                            utils.get_default_group(),
                        ],
                        "exclude_groups": [
                            utils.get_members_group(),
                            utils.get_managers_group(),
                        ],
                    },
                    "children": [
                        {
                            "plugin_type": "InstitutionListPublisher",
                            "values": dict(
                                show_institution_logos=True,
                                show_child_organizations=True,
                                show_all_child_organizations=True,
                                show_all=True,
                                enable_filters=True,
                                show_filter_button=False,
                                show_active_filters=True,
                                active_institutions=True,
                                former_institutions=False,
                            ),
                        },
                    ],
                },
            ]
        elif placeholder.slot == "action_buttons":
            return [
                {"plugin_type": "InstitutionFilterButtonPublisher"},
            ]
        else:
            return super().get_plugin_config_for_placeholder(
                placeholder, **options
            )
