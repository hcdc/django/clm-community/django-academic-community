# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
from __future__ import annotations

import importlib
from typing import TYPE_CHECKING, ClassVar, Dict, List, Optional, Type

from django.core.management.base import BaseCommand
from django.db.models import QuerySet

if TYPE_CHECKING:
    from aldryn_apphooks_config.models import AppHookConfig
    from cms.models import CMSPlugins, Page, Placeholder
    from django.contrib.auth.models import User
    from djangocms_alias.models import AliasContent


class ContentBaseCommand(BaseCommand):
    """A command to generate CMS Content"""

    help = ""

    command_name: ClassVar[str] = ""

    #: Boolean that should be set to true if the command should be called
    #: in the create all section.
    include_in_all = False

    def create_plugins(
        self, placeholder: Placeholder, conf: List[Dict]
    ) -> List[CMSPlugins]:
        """Create the config for a placeholder"""
        from academic_community.utils import create_plugins_from_conf

        return create_plugins_from_conf(placeholder, conf)

    def get_user(self, username: str) -> User:
        """Get the user for the pages"""
        from django.contrib.auth import get_user_model

        return get_user_model().objects.get(username=username)

    def get_page(self, reverse_id) -> Optional[Page]:
        """Test if a page exists."""
        from cms.models import Page

        return Page.objects.filter(reverse_id=reverse_id).first()

    def get_placeholder_slots(self, **options) -> List[str]:
        """Get the slots of the placeholders."""
        return []

    def get_placeholders(self, **options) -> QuerySet[Placeholder]:
        """Get the placeholders to add the content for."""
        from cms.models import Placeholder

        return Placeholder.objects.empty()

    def get_placeholder_for_slot(self, slot: str, **options) -> Placeholder:
        return self.get_placeholders(**options).get(slot=slot)

    def get_plugin_config_for_placeholder(
        self, placeholder: Placeholder, **options
    ) -> List[Dict]:
        """Get the plugin config for a given placeholder"""
        return []

    def handle(self, **kwargs):
        slots = self.get_placeholder_slots(**kwargs)
        for slot in slots:
            placeholder = self.get_placeholder_for_slot(slot, **kwargs)
            config = self.get_plugin_config_for_placeholder(
                placeholder, **kwargs
            )
            if config:
                self.create_plugins(placeholder, config)


class PageBaseCommand(ContentBaseCommand):
    """Django command to migrate the database."""

    help = ""

    default_slug: str = ""
    default_title: str = ""
    default_menu_title: Optional[str] = None

    #: Does the generated page require a parent?
    page_requires_parent: ClassVar[bool] = True

    def add_arguments(self, parser):
        """Add connection arguments to the parser."""

        parser.add_argument(
            "--slug",
            default=self.default_slug,
            help="The slug for the working groups page. Default %(default)s",
        )

        parser.add_argument(
            "--id",
            default=None,
            dest="reverse_id",
            help=(
                "The reverse id for the activities page (must be unique). If "
                "empty, this falls back to the slug of the page. "
                "Default %(default)s"
            ),
        )

        parser.add_argument(
            "--title",
            default=self.default_title,
            help="The title for the page. (Default: %(default)s).",
        )

        parser.add_argument(
            "--menu-title",
            default=self.default_menu_title,
            help="The menu title for the page. (Default: %(default)s).",
        )

    def create_page(
        self,
        parent: Page,
        title: str,
        menu_title: Optional[str],
        slug: str,
        reverse_id: str,
        user: User,
        options: Dict = {},
        **kwargs,
    ):
        """Create the page for the CMS Content"""
        from cms.api import create_page
        from cms.constants import TEMPLATE_INHERITANCE_MAGIC

        page = create_page(
            title,
            TEMPLATE_INHERITANCE_MAGIC,
            "en",
            menu_title,
            slug,
            parent=parent,
            in_navigation=True,
            login_required=False,
            reverse_id=reverse_id,
            created_by=user,
            **kwargs,
        )
        return page

    def get_placeholder_slots(self, **options) -> List[str]:
        return ["content"]

    def setup_extensions(self, page: Page):
        """Setup the extensions for the page."""
        pass

    def get_placeholders(  # type: ignore[override]
        self, reverse_id: str, **options
    ) -> QuerySet[Placeholder]:
        from cms.models import PageContent

        page = self.get_page(reverse_id)
        content = PageContent._base_manager.get(page=page)
        return content.placeholders

    def handle(
        self,
        home_id: str = "home",
        username: str = "AnonymousUser",
        slug: str = "",
        reverse_id: Optional[str] = None,
        # namespace: Optional[str] = None,
        title: str = "",
        menu_title: Optional[str] = None,
        publish: bool = True,
        **kwargs,
    ):
        """Generate the content"""
        from cms.models import PageContent
        from djangocms_versioning.models import Version

        reverse_id = reverse_id or slug

        user = self.get_user(username)
        parent = self.get_page(home_id)
        if not parent and self.page_requires_parent:
            raise ValueError("Cannot find parent with id %s" % home_id)
        page = self.get_page(reverse_id)
        if parent and page:
            resulting_slug = page.get_absolute_url()
        elif parent:
            parent_slug = parent.get_absolute_url()
            if not parent_slug.endswith("/"):
                parent_slug += "/"
            resulting_slug = "%s%s" % (parent_slug, slug)
        elif page:
            resulting_slug = page.get_absolute_url()
        else:
            resulting_slug = "/%s/" % slug
        self.stdout.write("Setting up page %s\n" % resulting_slug)
        if not page:
            page = self.create_page(
                parent, title, menu_title, slug, reverse_id, user, kwargs
            )

        self.setup_extensions(page)

        # create the plugins
        super().handle(
            home_id=home_id,
            username=username,
            slug=slug,
            reverse_id=reverse_id,
            title=title,
            menu_title=menu_title,
            **kwargs,
        )

        content = PageContent._base_manager.get(page=page)

        if publish:
            version = content.versions.last()
            if version is None:
                version = Version.objects.create(
                    content=content, created_by=user
                )
            version.publish(user)


class AppHookPageBaseCommand(PageBaseCommand):
    """A command that additionally generates an apphook."""

    apphook: ClassVar[str]

    app_config_type: ClassVar[str] = ""

    default_namespace: str = ""

    def add_arguments(self, parser):
        """Add connection arguments to the parser."""
        super().add_arguments(parser)

        parser.add_argument(
            "--namespace",
            default=self.default_namespace or None,
            help=(
                "The namespace for the working groups page. If empty, this "
                "falls back to the slug of the page. Default %(default)s"
            ),
        )

    def get_config_class(self) -> Type[AppHookConfig]:
        module, class_name = self.app_config_type.rsplit(".", 1)
        mod = importlib.import_module(module)
        try:
            return getattr(mod, class_name)
        except AttributeError:
            raise ValueError(
                "Invalid AppHookConfig: Module %s does not define a class %s"
                % (mod, class_name)
            )

    def get_config_defaults(self, **options) -> Dict:
        """Get the construction keywords for the AppHookConfig."""
        return {"app_data": {}}

    def get_or_create_config(self, namespace: str, **options) -> AppHookConfig:
        config_class = self.get_config_class()
        defaults = self.get_config_defaults(namespace=namespace, **options)
        app_config = config_class.objects.get_or_create(
            type=self.app_config_type,
            namespace=namespace,
            defaults=defaults,
        )[0]
        return app_config

    def create_page(
        self,
        parent: Page,
        title: str,
        menu_title: Optional[str],
        slug: str,
        reverse_id: str,
        user: User,
        options: Dict = {},
        **kwargs,
    ):
        """Create the page."""
        namespace = options["namespace"]
        page = super().create_page(
            parent,
            title,
            menu_title,
            slug,
            reverse_id,
            user,
            options,
            apphook=self.apphook,
            apphook_namespace=namespace,
            **kwargs,
        )

        if self.app_config_type:
            self.get_or_create_config(**options)
        return page

    def handle(self, slug: str, namespace: Optional[str], **kwargs):  # type: ignore[override]
        if namespace is None:
            namespace = slug
        return super().handle(slug=slug, namespace=namespace, **kwargs)


class StaticAliasBaseCommand(ContentBaseCommand):
    """A base command to create and populate static aliases."""

    #: the static code for the alias
    static_code: ClassVar[str]

    #: is the alias site specific?
    site_specific: ClassVar[bool] = False

    def get_or_create_alias_content(self) -> AliasContent:
        from cms.utils import get_current_site
        from djangocms_alias.constants import (
            DEFAULT_STATIC_ALIAS_CATEGORY_NAME,
        )
        from djangocms_alias.models import Alias, AliasContent, Category

        default_category = Category.objects.filter(
            translations__name=DEFAULT_STATIC_ALIAS_CATEGORY_NAME
        ).first()
        if not default_category:
            default_category = Category.objects.create(
                name=DEFAULT_STATIC_ALIAS_CATEGORY_NAME
            )

        alias_kws = dict(
            static_code=self.static_code,
            defaults={
                "creation_method": Alias.CREATION_BY_CODE,
                "category": default_category,
            },
        )

        if self.site_specific:
            alias_kws["site"] = get_current_site()

        alias = Alias.objects.get_or_create(**alias_kws)[0]

        alias_content = AliasContent._base_manager.filter(
            alias=alias, language="en"
        ).first()

        if not alias_content:
            alias_content = AliasContent._base_manager.create(
                alias=alias,
                name=self.static_code,
                language="en",
            )
            alias._content_cache["en"] = alias_content
        return alias_content

    def get_placeholder_for_slot(  # type: ignore[override]
        self, slot: str, alias_content: AliasContent, **options
    ) -> QuerySet:
        return alias_content.placeholder

    def get_placeholder_slots(self, **options) -> List[str]:
        return ["placeholder"]

    def handle(
        self, username: str = "AnonymousUser", publish: bool = True, **kwargs
    ):
        from djangocms_versioning.models import Version

        self.stdout.write("Creating static alias %s" % self.static_code)

        user = self.get_user(username)

        alias_content = self.get_or_create_alias_content()
        super().handle(alias_content=alias_content, **kwargs)

        if publish:
            version = alias_content.versions.last()
            if version is None:
                version = Version.objects.create(
                    content=alias_content, created_by=user
                )
            version.publish(user)
