# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Command to create a static alias with the top nav non-collapsible."""

from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List

from .base import StaticAliasBaseCommand

if TYPE_CHECKING:
    from cms.models import Page, Placeholder


class Command(StaticAliasBaseCommand):
    """A command to create a static alias with the top nav button."""

    command_name = "footer"

    static_code = "footer-nav"

    include_in_all = True

    help = "Create a static alias with links in the footer."

    def get_config_from_page(self, page: Page) -> Dict:
        content = page.get_content_obj()
        config = {
            "name": content.menu_title or content.title,
            "attributes": {"class": "footer-link"},
            "internal_link": {
                "pk": page.pk,
                "model": "cms.page",
            },
        }
        if hasattr(page, "pagemenuextension"):
            config["get_params"] = page.pagemenuextension.menu_get_params

        return config

    def get_contacts_config(self) -> Dict:
        from academic_community import app_settings

        page = self.get_page(app_settings.CONTACT_PAGE_ID)
        if page:
            return self.get_config_from_page(page)
        return {
            "name": "Contact",
            "external_link": "/contact/",
            "attributes": {"class": "footer-link"},
        }

    def get_plugin_config_for_placeholder(
        self, placeholder: Placeholder, **options
    ) -> List[Dict]:
        from academic_community import app_settings

        return [
            {
                "plugin_type": "InternalLinkPlugin",
                "values": {"config": self.get_contacts_config()},
            },
            {
                "plugin_type": "InternalLinkPlugin",
                "values": {
                    "config": {
                        "name": "FAQs",
                        "internal_url_name": "faqs:questioncategory-list",
                        "attributes": {"class": "footer-link"},
                    }
                },
            },
            {
                "plugin_type": "InternalLinkPlugin",
                "values": {
                    "config": {
                        "name": "Imprint",
                        "external_link": app_settings.IMPRINT_URL,
                        "attributes": {
                            "class": "footer-link",
                            "rel": "nofollow noopener noreferrer",
                        },
                    },
                },
            },
            {
                "plugin_type": "InternalLinkPlugin",
                "values": {
                    "config": {
                        "name": "Data Protection",
                        "external_link": app_settings.DATA_PROTECTION_URL,
                        "attributes": {
                            "class": "footer-link",
                            "rel": "nofollow noopener noreferrer",
                        },
                    },
                },
            },
            {
                "plugin_type": "InternalLinkPlugin",
                "values": {
                    "config": {
                        "name": "Accessibility",
                        "external_link": app_settings.ACCESSIBILITY_URL,
                        "attributes": {
                            "class": "footer-link",
                            "rel": "nofollow noopener noreferrer",
                        },
                    }
                },
            },
        ]
