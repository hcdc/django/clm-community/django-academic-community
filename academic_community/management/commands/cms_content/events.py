# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Module to generate default content for an events apphook."""
from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List

from .base import AppHookPageBaseCommand

if TYPE_CHECKING:
    from academic_community.events.models import EventsConfig


class Command(AppHookPageBaseCommand):
    """A command to create an activities page and apphook"""

    help = "Create a webpage to display events."

    apphook = "EventsApphook"

    app_config_type = "academic_community.events.cms_appconfig.EventsConfig"

    command_name = "events"

    default_slug = "events"

    default_title = "Events"

    include_in_all = True

    def add_arguments(self, parser):
        super().add_arguments(parser)

        # add option to restrict perms
        parser.add_argument(
            "--no-member-perms",
            action="store_false",
            dest="add_member_perms",
            help=(
                "Do not grant permission to community members to add events "
                "under this namespace."
            ),
        )

    def get_placeholder_slots(self, **options) -> List[str]:
        return ["content", "action_buttons"]

    def get_or_create_config(self, namespace: str, **options) -> EventsConfig:
        from guardian.shortcuts import assign_perm

        from academic_community import utils

        app_config = super().get_or_create_config(namespace, **options)
        if options.get("add_member_perms"):
            assign_perm(
                "add_event_for_namespace",
                utils.get_members_group(),
                app_config,
            )
        return app_config

    def get_plugin_config_for_placeholder(
        self, placeholder, **options
    ) -> List[Dict]:
        from django.contrib.auth.models import Group

        from academic_community import utils
        from academic_community.uploaded_material.models import License

        registered = utils.get_group_names("MEMBERS", "DEFAULT")
        all_groups = registered + utils.get_group_names("ANONYMOUS")
        registered_groups = Group.objects.filter(name__in=registered)
        with_anonymous = Group.objects.filter(name__in=all_groups)

        if placeholder.slot == "content":
            app_config = self.get_or_create_config(**options)
            return [
                {
                    "plugin_type": "SectionPublisher",
                    "values": {"name": "Ongoing events"},
                    "children": [
                        {
                            "plugin_type": "EventListPublisher",
                            "values": dict(
                                show_count=False,
                                show_all=True,
                                ongoing_events=True,
                                future_events=False,
                                past_events=False,
                                app_config=app_config,
                            ),
                        },
                    ],
                },
                {
                    "plugin_type": "SectionPublisher",
                    "values": {"name": "Upcoming events"},
                    "children": [
                        {
                            "plugin_type": "EventListPublisher",
                            "values": dict(
                                show_all=True,
                                show_count=False,
                                ongoing_events=False,
                                future_events=True,
                                past_events=False,
                                app_config=app_config,
                            ),
                        },
                    ],
                },
                {
                    "plugin_type": "SectionPublisher",
                    "values": {"name": "Past events"},
                    "children": [
                        {
                            "plugin_type": "EventListPublisher",
                            "values": dict(
                                show_all=True,
                                show_count=False,
                                ongoing_events=False,
                                future_events=False,
                                past_events=True,
                                app_config=app_config,
                            ),
                        },
                    ],
                },
            ]
        elif placeholder.slot == "action_buttons":
            return [
                {
                    "plugin_type": "CreateEventButtonPublisher",
                    "m2m_values": {
                        "event_view_groups": with_anonymous,
                        "view_programme_groups": with_anonymous,
                        "view_connections_groups": registered_groups,
                        "registration_groups": registered_groups,
                        "submission_groups": registered_groups,
                        "submission_licenses": License.objects.filter(
                            active=True
                        ),
                        "submission_upload_licenses": License.objects.filter(
                            active=True
                        ),
                    },
                },
            ]
        else:
            return super().get_plugin_config_for_placeholder(
                placeholder, **options
            )
