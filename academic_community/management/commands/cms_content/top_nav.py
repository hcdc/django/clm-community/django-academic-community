# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Command to create a static alias with the top nav non-collapsible."""

from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List

from .base import StaticAliasBaseCommand

if TYPE_CHECKING:
    from cms.models import Placeholder


class Command(StaticAliasBaseCommand):
    """A command to create a static alias with the top nav button."""

    command_name = "top-nav"

    static_code = "top_nav"

    site_specific = True

    include_in_all = True

    help = "Create a static alias with buttons in the top navigation."

    def get_plugin_config_for_placeholder(
        self, placeholder: Placeholder, **options
    ) -> List[Dict]:
        return [
            {
                "plugin_type": "ProfileButtonPlugin",
                "values": {
                    "classes": "nav-link ms-2",
                },
            },
            {
                "plugin_type": "UserGroupBasedContentPlugin",
                "values": {
                    "is_anonymous": True,
                    "is_superuser": False,
                },
                "children": [
                    # login button with text
                    {
                        "plugin_type": "InternalLinkPlugin",
                        "values": {
                            "config": {
                                "name": "Login",
                                "internal_url_name": "login",
                                "attributes": {
                                    "class": "nav-link btn-join d-none d-lg-inline-block"
                                },
                                "add_next": True,
                            }
                        },
                    },
                    # mobile devices
                    {
                        "plugin_type": "InternalLinkPlugin",
                        "values": {
                            "config": {
                                "name": "",
                                "internal_url_name": "login",
                                "attributes": {
                                    "class": (
                                        "nav-link btn-join d-inline-block "
                                        "d-lg-none ms-2"
                                    )
                                },
                                "add_next": True,
                            }
                        },
                    },
                ],
            },
        ]
