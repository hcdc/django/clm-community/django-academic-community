# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Command to generate the contact page."""
from __future__ import annotations

from typing import Dict, List

from .base import PageBaseCommand


class Command(PageBaseCommand):
    """A command to generate the contact page from the CMS"""

    command_name = "contact"

    help = "Generate the contact page"

    default_slug = "contact"

    page_requires_parent = False

    include_in_all = True

    default_title = "Contact the community managers"

    default_menu_title = "Contact"

    def get_plugin_config_for_placeholder(  # type: ignore[override]
        self, placeholder, **options
    ) -> List[Dict]:
        from academic_community import utils

        if placeholder.slot == "content":
            return [
                {
                    "plugin_type": "ContactFormPlugin",
                    "m2m_values": {
                        "recipient_groups": [utils.get_managers_group()],
                        "group_use_permission": utils.get_groups(
                            "DEFAULT", "ANONYMOUS"
                        ),
                    },
                }
            ]
        else:
            return super().get_plugin_config_for_placeholder(
                placeholder, **options
            )
