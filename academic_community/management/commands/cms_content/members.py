# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Module to generate default content for a topics apphook."""
from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List

from django.utils.translation import gettext as _

from .base import AppHookPageBaseCommand

if TYPE_CHECKING:
    from cms.models import Page


class Command(AppHookPageBaseCommand):
    """A command to create an activities page and apphook"""

    help = "Create a webpage to display community members."

    apphook = "MembersApphook"

    app_config_type = "academic_community.members.cms_appconfig.MembersConfig"

    command_name = "members"

    default_slug = "members"

    default_namespace = "member_area"

    default_title = _("Members")

    include_in_all = True

    def get_placeholder_slots(self, **options) -> List[str]:
        return ["content", "action_buttons"]

    def setup_extensions(self, page: Page):
        from academic_community.models import PageMenuExtension

        extension, created = PageMenuExtension.objects.get_or_create(
            extended_object=page,
            defaults=dict(
                menu_get_params="end_date__isnull=true",
            ),
        )
        if not created:
            extension.menu_get_params = "end_date__isnull=true"
            extension.save()

    def get_plugin_config_for_placeholder(
        self, placeholder, **options
    ) -> List[Dict]:
        if placeholder.slot == "content":
            app_config = self.get_or_create_config(**options)
            return [
                {
                    "plugin_type": "AlphabetFilterButtonsPlugin",
                    "values": {"filter_name": "last_name"},
                },
                {
                    "plugin_type": "CommunityMembersListPublisher",
                    "values": dict(
                        enable_filters=True,
                        show_filter_button=False,
                        show_active_filters=True,
                        former_members=True,
                        show_all=True,
                        check_permissions=False,
                        app_config=app_config,
                    ),
                },
            ]
        elif placeholder.slot == "action_buttons":
            return [
                {"plugin_type": "CommunityMemberFilterButtonPublisher"},
            ]
        else:
            return super().get_plugin_config_for_placeholder(
                placeholder, **options
            )
