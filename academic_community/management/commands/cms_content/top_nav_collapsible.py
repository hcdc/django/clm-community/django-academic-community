# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Command to create a static alias with the brand."""

from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List, Optional

from .base import StaticAliasBaseCommand

if TYPE_CHECKING:
    from cms.models import Page, Placeholder


class Command(StaticAliasBaseCommand):
    """A command to create a static alias with the brand."""

    command_name = "top-nav-collapsible"

    static_code = "top_nav_collapsible"

    site_specific = True

    include_in_all = True

    help = (
        "Create a static alias with buttons in the collapsible part of the "
        "top navigation."
    )

    def get_config_from_page(self, page: Page) -> Dict:
        content = page.get_content_obj()
        config = {
            "name": content.menu_title or content.title,
            "internal_link": {
                "pk": page.pk,
                "model": "cms.page",
            },
        }
        if hasattr(page, "pagemenuextension"):
            config["get_params"] = page.pagemenuextension.menu_get_params
        return config

    def get_config(self, apphook: str) -> Optional[Dict]:
        from cms.models import Page

        page = Page.objects.filter(application_urls=apphook).first()
        if page:
            return self.get_config_from_page(page)

        return None

    def get_institutions_config(self) -> Dict:
        config = self.get_config("InstitutionsApphook")
        if not config:
            config = {
                "name": "Institutions",
                "external_link": "/institutions/",
                "get_params": "end_date__isnull=True",
            }
        return config

    def get_members_config(self) -> Dict:
        config = self.get_config("MembersApphook")
        if not config:
            config = {
                "name": "Members",
                "external_link": "/members/",
                "get_params": "end_date__isnull=True&is_member=True",
            }
        return config

    def get_topics_config(self) -> Dict:
        config = self.get_config("TopicsApphook")
        if not config:
            config = {
                "name": "Topics",
                "external_link": "/topics/",
                "get_params": "end_date__isnull=True",
            }
        return config

    def get_activities_config(self) -> Dict:
        config = self.get_config("ActivitiesApphook")
        if not config:
            config = {
                "name": "Working Groups",
                "external_link": "/working-groups/",
                "get_params": "end_date__isnull=True",
            }
        return config

    def get_events_config(self) -> Dict:
        config = self.get_config("EventsApphook")
        if not config:
            config = {
                "name": "Events",
                "external_link": "/events/",
            }
        return config

    def get_contacts_config(self) -> Dict:
        from academic_community import app_settings

        page = self.get_page(app_settings.CONTACT_PAGE_ID)
        if page:
            return self.get_config_from_page(page)
        return {
            "name": "Contact",
            "external_link": "/contact/",
        }

    def get_plugin_config_for_placeholder(
        self, placeholder: Placeholder, **options
    ) -> List[Dict]:
        return [
            {
                "plugin_type": "MenuButtonPlugin",
                "values": {
                    "config": {
                        "name": "Community",
                        "dropdown_button_attributes": {"class": "text-start"},
                        "dropdown_attributes": {"class": "position-absolute"},
                    }
                },
                "children": [
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {"config": self.get_institutions_config()},
                    },
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {
                            "config": self.get_activities_config(),
                        },
                    },
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {"config": self.get_members_config()},
                    },
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {
                            "config": {
                                "name": "Material",
                                "internal_url_name": "material-list",
                            }
                        },
                    },
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {
                            "config": self.get_topics_config(),
                        },
                    },
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {
                            "config": {
                                "name": "Chat and Discuss",
                                "internal_url_name": "chats:channel-list",
                            }
                        },
                    },
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {"config": self.get_events_config()},
                    },
                ],
            },
            {
                "plugin_type": "MenuButtonPlugin",
                "values": {
                    "config": {
                        "name": "Help",
                        "dropdown_button_attributes": {"class": "text-start"},
                        "dropdown_attributes": {"class": "position-absolute"},
                    }
                },
                "children": [
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {
                            "config": {
                                "name": "FAQs",
                                "internal_url_name": "faqs:questioncategory-list",
                            }
                        },
                    },
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {"config": self.get_contacts_config()},
                    },
                ],
            },
            {
                "plugin_type": "UserGroupBasedContentPlugin",
                "values": {
                    "is_superuser": True,
                    "is_manager": True,
                },
                "children": [
                    {
                        "plugin_type": "MenuButtonPlugin",
                        "values": {
                            "config": {
                                "name": "Admin",
                                "icon_left": "fas fa-users-cog",
                                "dropdown_button_attributes": {
                                    "class": "text-start"
                                },
                                "dropdown_attributes": {
                                    "class": "position-absolute"
                                },
                            },
                        },
                        "children": [
                            {
                                "plugin_type": "MenuButtonPlugin",
                                "values": {
                                    "config": {
                                        "name": "Admin Area",
                                        "internal_url_name": "admin:index",
                                    }
                                },
                            },
                            {
                                "plugin_type": "MenuButtonPlugin",
                                "values": {
                                    "config": {
                                        "name": "History",
                                        "internal_url_name": "history:revision-list",
                                    }
                                },
                            },
                        ],
                    }
                ],
            },
            {
                "plugin_type": "UserGroupBasedContentPlugin",
                "values": {
                    "is_member": True,
                    "is_staff": True,
                },
                "children": [
                    {
                        "plugin_type": "SearchBarPlugin",
                        "values": {},
                    }
                ],
            },
        ]
