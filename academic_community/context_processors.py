"""Context processors that make context available for all templates."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site

from academic_community import app_settings, utils
from academic_community.uploaded_material.models import License


def get_default_context(request=None):
    """Get the context that should be available to all templates."""
    context = {}
    if request is not None:
        current_site = get_current_site(request)
        context["site_name"] = current_site.name
        context["domain"] = current_site.domain
        context["protocoll"] = "https"

        # check if the page can be edited via django CMS
        context["can_change_current_page"] = (
            getattr(request, "current_page", None)
            and request.user.is_staff
            and request.current_page.has_change_permission(request.user)
        )
    index_location = utils.get_index_location()
    context["force_script_name"] = index_location
    if request is not None:
        ROOT_URL = request.build_absolute_uri("/")[:-1]
    else:
        ROOT_URL = getattr(settings, "ROOT_URL", "")
    if ROOT_URL:
        context["root_url"] = ROOT_URL
        context["index_url"] = ROOT_URL + index_location
    if context.get("index_url"):
        index_url = context["index_url"]
        websocket_root_url = index_url.replace("http", "ws")
        if websocket_root_url.endswith("/"):
            websocket_root_url = websocket_root_url[:-1]
        context["websocket_root_url"] = websocket_root_url

    context["community_name"] = utils.get_community_name()
    context["community_abbreviation"] = utils.get_community_abbreviation()
    license = License.objects.public_default()
    context["license"] = context["public_license"] = license
    context["vapid_public_key"] = getattr(settings, "VAPID_PUBLIC_KEY", None)
    context["CONTACT_PAGE_ID"] = app_settings.CONTACT_PAGE_ID
    return context
