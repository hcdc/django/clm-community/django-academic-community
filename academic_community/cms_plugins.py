"""utility plugins for academic_community"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import copy
from typing import Any, Dict, Literal, Optional, Sequence, Tuple, Type, Union
from urllib.parse import urlencode, urlparse, urlunparse

from cms.constants import EXPIRE_NOW
from cms.models import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.core.exceptions import ImproperlyConfigured
from django.core.paginator import InvalidPage, Paginator
from django.db.models import Model, QuerySet
from django.http import Http404
from django.utils.translation import gettext as _
from django_filters import FilterSet
from django_filters.constants import ALL_FIELDS
from django_filters.filterset import filterset_factory
from djangocms_frontend.contrib.link.cms_plugins import LinkPlugin
from djangocms_frontend.helpers import get_plugin_template, insert_fields
from guardian.shortcuts import get_anonymous_user

from academic_community import forms, models, utils
from academic_community.forms import get_menu_button_templates
from academic_community.utils import concat_classes


@plugin_pool.register_plugin
class UserGroupBasedContentPlugin(CMSPluginBase):
    """A plugin to display content to certain user or groups only."""

    model = models.UserGroupBasedContent
    name = _("User/Group-specific content")
    module = _("Academic Community")
    render_template = "academic_community/user_group_based_content.html"
    allow_children = True

    filter_horizontal = ["users", "exclude_users", "groups", "exclude_groups"]

    def render(
        self,
        context,
        instance: models.UserGroupBasedContent,
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        user = context["request"].user
        context["render_content"] = False
        if user.is_anonymous:
            if instance.is_anonymous:
                context["render_content"] = True
            user = get_anonymous_user()
        elif instance.is_authenticated:
            context["render_content"] = True
        elif instance.is_member and user.is_member:
            context["render_content"] = True
        elif instance.is_staff and user.is_staff:
            context["render_content"] = True
        elif instance.is_manager and user.is_manager:
            context["render_content"] = True

        if not context["render_content"]:
            if (
                (user.is_superuser and instance.is_superuser)
                or (instance.users.filter(pk=user.pk).exists())
                or (
                    instance.groups.filter(
                        pk__in=user.groups.values_list("pk", flat=True)
                    )
                )
            ):
                # check for excluded users and groups
                if not instance.exclude_users.filter(
                    pk=user.pk
                ) and not instance.exclude_groups.filter(
                    pk__in=user.groups.values_list("pk", flat=True)
                ):
                    context["render_content"] = True
        return context


@plugin_pool.register_plugin
class AlphabetFilterButtonsPlugin(CMSPluginBase):
    """A plugin to display a row of buttons from the alphabet."""

    model = models.AlphabetFilterButtonsPluginModel
    name = _("Alphabet filters")
    module = _("Academic Community")
    render_template = (
        "academic_community/components/buttons/alphabet_buttons.html"
    )
    allow_children = True

    def render(
        self,
        context,
        instance: models.AlphabetFilterButtonsPluginModel,
        placeholder,
    ):
        """Get the template context."""
        filter_name = instance.filter_name + "__istartswith"
        context["filter_name"] = filter_name
        if instance.verbose_field_name:
            field_name = instance.verbose_field_name
        else:
            field_name = " ".join(
                s.capitalize() for s in instance.filter_name.split("_")
            )
        context["field_name"] = field_name

        if filter_name in context["request"].GET:
            active_filter = context["request"].GET[filter_name]
            if active_filter:
                context["active_filter"] = active_filter[0].upper()
        return context


class InternalLinkPluginBase(LinkPlugin):
    """Base plugin for internal bootstrap5 links."""

    fieldsets = copy.deepcopy(LinkPlugin.fieldsets)
    model = models.InternalLink
    form = forms.InternalLinkForm
    fieldsets[-1] = (
        _("Link settings"),
        {
            "classes": ("collapse",),
            "fields": (
                ("mailto", "phone"),
                ("anchor", "target"),
                ("file_link"),
                ("internal_url_name", "internal_url_args"),
                ("add_next"),
                ("get_params"),
            ),
        },
    )

    def render(
        self,
        context,
        instance: models.InternalLink,
        placeholder,
    ):
        from academic_community.templatetags.community_utils import (
            url_for_next,
        )

        context = super().render(context, instance, placeholder)
        if "link" in context and getattr(instance, "add_next", False):
            link = context["link"]
            if not link.endswith("?"):
                link += "&"
            link += "next=" + url_for_next(context)
            context["link"] = link
        return context

    def get_cache_expiration(
        self, request, instance: models.InternalLink, placeholder
    ):
        """Get the expiration time for the cache."""
        # we disable the cache when wir use an internal url name
        if instance.config.get("add_next") and utils.placeholder_is_static(
            placeholder
        ):
            return EXPIRE_NOW
        else:
            return super().get_cache_expiration(request, instance, placeholder)


@plugin_pool.register_plugin
class MenuButtonPlugin(InternalLinkPluginBase):
    """A plugin to display a menu button."""

    name = _("Menu Button")
    module = _("Academic Community")
    render_template = "academic_community/menu_button.html"
    model = models.MenuButtonPluginModel
    form = forms.MenuButtonForm
    allow_children = True
    child_classes = [
        "MenuButtonPlugin",
        "DropdownDividerPlugin",
        "DropdownHeaderPlugin",
        "TextPlugin",
        "UserGroupBasedContentPlugin",
    ]

    def get_render_template(self, context, instance, placeholder):
        return get_plugin_template(
            instance,
            "menu_button",
            "menu_button",
            get_menu_button_templates(),
        )

    def is_in_dropdown(self, instance: models.InternalLink):
        """Check if the menu button is in the dropdown of another menu."""
        while instance.parent is not None:
            if isinstance(instance, models.MenuButtonPluginModel):
                return True
        return False

    def get_fieldsets(self, request, obj=None):
        """Get the fieldsets for the admin form."""
        # reimplemented to add dropdown_button_attributes and
        # dropdown_attributes to the Advanced settings
        fields = ["dropdown_button_attributes", "dropdown_attributes"]
        fieldsets = super().get_fieldsets(request, obj)
        block = len(fieldsets) - 1  # index of the Advanced settings
        return insert_fields(fieldsets, fields, block=block)

    def render(
        self,
        context,
        instance: models.InternalLink,
        placeholder,
    ):
        attrs = getattr(instance, "attributes", {})
        classes = attrs.get("class")
        context = super().render(context, instance, placeholder)
        if not classes and not getattr(instance, "link_context", None):
            if self.is_in_dropdown(instance):
                attrs["class"] = concat_classes(
                    ["dropdown-item"] + [attrs.get("class", "")]
                )
            elif getattr(instance, "link_type", None) == "btn":
                attrs["class"] = concat_classes(
                    ["btn"] + [attrs.get("class", "")]
                )
        instance.config["attributes"] = attrs

        # add the dropdown button
        link_classes = ["btn", "dropdown-toggle"]
        if context.get("link"):
            link_classes.append("dropdown-toggle-split")
        if getattr(instance, "link_context", None):
            if instance.link_type == "link":
                link_classes.append("text-{}".format(instance.link_context))
            else:
                link_classes.append("btn")
                if not instance.link_outline:
                    link_classes.append("btn-{}".format(instance.link_context))
                else:
                    link_classes.append(
                        "btn-outline-{}".format(instance.link_context)
                    )
        if getattr(instance, "link_size", None):
            link_classes.append(instance.link_size)
        if getattr(instance, "link_block", None):
            link_classes.append("btn-block")

        dropdown_button_attrs = getattr(
            instance, "dropdown_button_attributes", {}
        )
        classes = concat_classes(
            link_classes
            + [
                dropdown_button_attrs.get("class"),
            ]
        )
        dropdown_button_attrs["class"] = classes
        instance.config["dropdown_button_attributes"] = dropdown_button_attrs

        dropdown_attrs = getattr(instance, "dropdown_attributes", {})

        dropdown_attrs["class"] = concat_classes(
            ["dropdown-menu"] + [dropdown_attrs.get("class")]
        )

        instance.config["dropdown_attributes"] = dropdown_attrs

        return context


@plugin_pool.register_plugin
class ProfileButtonPlugin(CMSPluginBase):
    """A plugin to display the profile button"""

    model = models.ProfileButton
    name = _("Profile Button")
    module = _("Academic Community")
    render_template = "academic_community/profile_button.html"


@plugin_pool.register_plugin
class SearchBarPlugin(CMSPluginBase):
    """A plugin to display the profile button"""

    model = CMSPlugin
    name = _("Search Bar")
    module = _("Academic Community")
    render_template = "academic_community/search_bar.html"


@plugin_pool.register_plugin
class DropdownDividerPlugin(CMSPluginBase):
    """A divider for a dropdown menu."""

    model = CMSPlugin
    name = _("Dropdown Divider")
    module = _("Academic Community")
    render_template = "academic_community/dropdown_divider.html"
    require_parent = True
    parent_classes = ["MenuButtonPlugin"]


@plugin_pool.register_plugin
class DropdownHeaderPlugin(CMSPluginBase):
    """A divider for a dropdown menu."""

    model = models.DropdownHeader
    name = _("Dropdown Header")
    module = _("Academic Community")
    render_template = "academic_community/dropdown_header.html"
    require_parent = True
    parent_classes = ["MenuButtonPlugin"]


class InternalLinkPlugin(InternalLinkPluginBase):
    """A Link plugin for internal links."""

    name = _("Link")
    module = _("Academic Community")


plugin_pool.unregister_plugin(LinkPlugin)
plugin_pool.register_plugin(InternalLinkPlugin)


class ListPublisher(CMSPluginBase):
    """A publisher for a list of objects."""

    list_model: Type[Model]
    context_object_name: Optional[str] = None

    queryset = None
    list_ordering: Optional[Union[Sequence, str]] = None

    def get_list_queryset(self, context, instance: CMSPlugin) -> QuerySet:
        """
        Return the list of items for this view.

        The return value must be an iterable and may be an instance of
        `QuerySet` in which case `QuerySet` specific behavior will be enabled.
        """
        queryset = self.get_full_queryset(context, instance)
        ordering = self.get_list_ordering(context, instance)
        if ordering:
            if isinstance(ordering, str):
                ordering = (ordering,)
            queryset = queryset.order_by(*ordering)
        return self.filter_list_queryset(context, instance, queryset)

    def get_list_ordering(self, context, instance: CMSPlugin):
        """Return the field or fields to use for ordering the queryset."""
        return self.list_ordering

    def get_full_queryset(self, contenxt, instance: CMSPlugin) -> QuerySet:
        """Get the full unfiltered queryset"""
        if self.queryset is not None:
            queryset = self.queryset
            if isinstance(queryset, QuerySet):
                queryset = queryset.all()
        elif self.list_model is not None:
            queryset = self.list_model._default_manager.all()  # type: ignore
        else:
            raise ImproperlyConfigured(
                "%(cls)s is missing a QuerySet. Define "
                "%(cls)s.model, %(cls)s.queryset, or override "
                "%(cls)s.get_queryset()." % {"cls": self.__class__.__name__}
            )
        return queryset

    def filter_list_queryset(
        self,
        context,
        instance: CMSPlugin,
        queryset: QuerySet,
    ) -> QuerySet:
        """Apply filters to the queryset"""
        return queryset

    def get_context_object_name(self, object_list):
        """Get the name of the item to be used in the context."""
        if self.context_object_name:
            return self.context_object_name
        elif hasattr(object_list, "model"):
            return "%s_list" % object_list.model._meta.model_name
        else:
            return None

    def render(
        self,
        context,
        instance: CMSPlugin,
        placeholder,
    ):
        """Render the context of the queryset."""
        context = super().render(context, instance, placeholder)
        queryset = self.get_list_queryset(context, instance)
        context_object_name = self.get_context_object_name(queryset)
        context[context_object_name] = context["object_list"] = queryset
        return context


class PaginationPublisherMixin(CMSPluginBase):
    """A mixin to add the pagination of a list."""

    paginator_class = Paginator

    def get_pagination_param(
        self,
        instance: models.AbstractPaginationPluginModelMixin,
    ) -> str:
        return instance.pagination_param or f"page_{instance.pk}"

    def paginate_queryset(
        self,
        context,
        instance: models.AbstractPaginationPluginModelMixin,
        queryset: QuerySet,
    ) -> Union[
        Tuple[Paginator, int, QuerySet, Literal[True]],
        Tuple[None, None, QuerySet, Literal[False]],
    ]:
        """Paginate the queryset"""
        if not instance.paginate_by:
            return None, None, queryset, False

        paginator = self.get_paginator(instance, queryset)

        page_kwarg = self.get_pagination_param(instance)

        request = context["request"]

        page = request.GET.get(page_kwarg) or 1
        try:
            page_number = int(page)
        except ValueError:
            if page == "last":
                page_number = paginator.num_pages
            else:
                raise Http404(
                    _("Page is not “last”, nor can it be converted to an int.")
                )
        try:
            page = paginator.page(page_number)
            return (paginator, page, page.object_list, page.has_other_pages())
        except InvalidPage as e:
            raise Http404(
                _("Invalid page (%(page_number)s): %(message)s")
                % {"page_number": page_number, "message": str(e)}
            )

    def get_paginator(
        self,
        instance: models.AbstractPaginationPluginModelMixin,
        queryset: QuerySet,
        **kwargs,
    ):
        """Return an instance of the paginator for this view."""
        return self.paginator_class(
            queryset,
            instance.paginate_by,  # type: ignore
            orphans=0,
            allow_empty_first_page=True,
        )

    def render(
        self,
        context,
        instance: models.AbstractPaginationPluginModelMixin,
        placeholder,
    ):
        """Render the context of the queryset."""
        context = super().render(context, instance, placeholder)
        queryset = context["object_list"]
        paginator, page, queryset, is_paginated = self.paginate_queryset(
            context, instance, queryset
        )

        context_object_name = self.get_context_object_name(queryset)  # type: ignore
        context[context_object_name] = context["object_list"] = queryset

        context["activities"] = context["object_list"] = queryset
        context["is_paginated"] = is_paginated
        if is_paginated:
            context["paginator"] = paginator
            context["page_obj"] = page
        context["show_count"] = instance.show_count
        context["page_parameter_name"] = self.get_pagination_param(instance)
        return context


class FilterMixin:
    """
    A mixin that provides a way to show and handle a FilterSet in a request.
    """

    filterset_class: Optional[Type[FilterSet]] = None
    filterset_fields = ALL_FIELDS
    strict = True
    list_model: Type[Model]

    def get_filterset_class(
        self,
        context,
        instance: models.BaseFilterPluginModelMixin,
    ) -> Type[FilterSet]:
        """
        Returns the filterset class to use in this view
        """
        if self.filterset_class:
            return self.filterset_class
        elif hasattr(self, "list_model"):
            return filterset_factory(
                model=self.list_model, fields=self.filterset_fields
            )
        else:
            msg = "'%s' must define 'filterset_class' or 'list_model'"
            raise ImproperlyConfigured(msg % self.__class__.__name__)

    def get_filterset(
        self,
        context,
        instance: models.BaseFilterPluginModelMixin,
        queryset: QuerySet,
        filterset_class: Type[FilterSet],
    ) -> FilterSet:
        """
        Returns an instance of the filterset to be used in this view.
        """
        kwargs = self.get_filterset_kwargs(
            context, instance, queryset, filterset_class
        )
        return filterset_class(**kwargs)

    def get_filterset_kwargs(
        self,
        context,
        instance: models.BaseFilterPluginModelMixin,
        queryset: QuerySet,
        filterset_class: Type[FilterSet],
    ) -> Dict[str, Any]:
        """
        Returns the keyword arguments for instantiating the filterset.
        """
        kwargs = {
            "data": context["request"].GET or None,
            "request": context["request"],
            "queryset": queryset,
        }
        if instance.filter_query_prefix:
            kwargs["prefix"] = instance.filter_query_prefix
        return kwargs

    def get_strict(self, instance: models.BaseFilterPluginModelMixin):
        return instance.strict_filters


class ActiveFiltersPublisher(FilterMixin, CMSPluginBase):
    """A mixin for rendering active filters"""

    model = models.ActiveFiltersPluginModel

    render_template = (
        "academic_community/components/active_filters_card_plugin.html"
    )

    def render(
        self,
        context,
        instance: models.ActiveFiltersPluginModel,
        placeholder,
    ):
        """Render the context of the queryset."""
        qs = self.list_model.objects.all()
        filterset_class = self.get_filterset_class(context, instance)
        filterset = self.get_filterset(
            context,
            instance,  # type: ignore[arg-type]
            qs,
            filterset_class,
        )
        context["filter"] = filterset
        context["instance"] = instance
        return context


class ExportListButtonPluginBase(
    FilterMixin,
    PaginationPublisherMixin,
):
    """A base plugin to add a button to export a list of objects."""

    cache = False

    render_template = (
        "academic_community/components/buttons/export_list_button_plugin.html"
    )

    def get_export_url(self, context, instance) -> str:
        raise NotImplementedError(
            "This method needs to be implemented by subclasses"
        )

    def render(
        self,
        context,
        instance: models.AbstractExportListButtonPluginModelMixin,  # type: ignore[override]
        placeholder,
    ):
        """Render the context"""
        qs = self.list_model.objects.all()
        filterset_class = self.get_filterset_class(context, instance)
        filterset = self.get_filterset(
            context,
            instance,  # type: ignore[arg-type]
            qs,
            filterset_class,
        )
        url = self.get_export_url(context, instance)
        pagination_param = self.get_pagination_param(instance)
        params = {}
        if filterset.is_valid():
            params.update(
                {
                    key: val
                    for key, val in filterset.form.cleaned_data.items()
                    if val
                }
            )
        parts = urlparse(url)
        if pagination_param in context["request"].GET:
            params["page"] = context["request"].GET[pagination_param]
        if instance.paginate_by:
            params["page_size"] = instance.paginate_by
        url = urlunparse(
            [
                parts.scheme,
                parts.netloc,
                parts.path,
                parts.params,
                urlencode(params, doseq=True),
                parts.fragment,
            ]
        )
        context["url"] = url
        context["formats"] = [
            renderer.format
            for renderer in utils.get_export_list_renderer_classes()
        ]
        return context


class FilterButtonPublisher(FilterMixin, CMSPluginBase):
    """A mixin for rendering active filters"""

    model = models.FilterButtonPluginModel

    render_template = "academic_community/components/filter_button_plugin.html"

    def render(
        self,
        context,
        instance: models.FilterButtonPluginModel,
        placeholder,
    ):
        """Render the context of the queryset."""
        qs = self.list_model.objects.all()
        filterset_class = self.get_filterset_class(context, instance)
        filterset = self.get_filterset(
            context,
            instance,  # type: ignore[arg-type]
            qs,
            filterset_class,
        )
        context["filter"] = filterset
        context["instance"] = instance
        return context


class FilterListPublisher(FilterMixin, ListPublisher):
    """A publisher for a list plugins with filters."""

    def filter_list_queryset(
        self,
        context,
        instance: models.FilterPluginModelMixin,
        queryset: QuerySet,
    ) -> QuerySet:
        queryset = super().filter_list_queryset(context, instance, queryset)

        if instance.enable_filters:
            filterset_class = self.get_filterset_class(context, instance)
            filterset = self.get_filterset(
                context,
                instance,  # type: ignore[arg-type]
                queryset,
                filterset_class,
            )
            context["filter"] = filterset

            if (
                not filterset.is_bound
                or filterset.is_valid()
                or not self.get_strict(instance)  # type: ignore[arg-type]
            ):
                return filterset.qs
            else:
                return filterset.queryset.none()
        else:
            return queryset


@plugin_pool.register_plugin
class SectionPublisher(CMSPluginBase):
    """A publisher for a section"""

    model = models.SectionPluginModel
    module = _("Academic Community")
    name = _("Section")

    render_template = "academic_community/components/section_plugin.html"

    allow_children = True

    def render(
        self,
        context,
        instance: models.SectionPluginModel,
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        context["instance"] = instance
        return context


@plugin_pool.register_plugin
class MenuPublisher(CMSPluginBase):
    """A publisher for a section"""

    model = models.MenuPluginModel
    module = _("Academic Community")
    name = _("Navigation Menu")

    render_template = "academic_community/components/menu_plugin.html"

    def render(self, context, instance: models.MenuPluginModel, placeholder):
        context = super().render(context, instance, placeholder)
        context["instance"] = instance
        context["root_id"] = (
            instance.root_id.reverse_id if instance.root_id else ""
        )
        return context
