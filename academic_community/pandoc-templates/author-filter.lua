-- SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
--
-- SPDX-License-Identifier: EUPL-1.2

function escapeTex(text)
  -- escape special latex characters in a string
  local reserved = "&%$#_{}"
  local escape={}
  for c in reserved:gmatch(".") do
    escape[c] = "\\" .. c
  end
  escape["~"] = "\\textasciitilde{}"
  escape["^"] = "\\^{}"
  escape["\\"] = "\\textbackslash{}"
  escape["<"] = "\\textless{}"
  escape[">"] = "\\textgreater{}"
  return text:gsub(".", escape)
end

function Div(elem)
  if FORMAT:match 'latex' then
    local classes = elem.classes
    if classes and pandoc.List.includes(classes, 'authors') then
      local ret = ''
      for i = 1, #elem.content do
        local child_elem = elem.content[i]
        local author_line = '{'
        if child_elem.attributes["orcid"] then
          author_line = author_line .. '\\href{https://orcid.org/' .. escapeTex(child_elem.attributes["orcid"]) .. '}{\\Authfont \\textcolor{orcidlogocol}{\\aiOrcid} '
        end
        author_line = author_line .. '{\\Authfont \\textcolor{black}{' .. escapeTex(pandoc.utils.stringify(child_elem.content)) .. '}}'
        if child_elem.attributes["orcid"] then
          -- finish href
          author_line = author_line .. '}'
        end
        if child_elem.attributes["affiliation"] then
          author_line = author_line .. '$^{' .. escapeTex(child_elem.attributes["affiliation"]) .. '}$'
        end
        author_line = author_line .. '}'
        if i >= 2 and i < #elem.content then
          ret = ret .. ', '
        elseif i >= 2 and i == #elem.content then
          ret = ret .. ', {\\Authfont\\textcolor{black}{and}} '
        end
        ret = ret .. author_line
      end
      return {pandoc.RawBlock('latex', ret)}
    end

    if classes and pandoc.List.includes(classes, 'affiliations') then
      local ret = ''
      for i = 1, #elem.content do
        local child_elem = elem.content[i]
        ret = ret .. '{$^{'.. escapeTex(child_elem.attributes["affiliation"]) .. '}$ \\Affilfont ' .. escapeTex(pandoc.utils.stringify(child_elem.content)) .. '}\\newline'
      end
      return {pandoc.RawBlock('latex', ret)}
    end
  end
end
