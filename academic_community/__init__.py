# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
# SPDX-License-Identifier: EUPL-1.2

"""Django Academic Community Portal

A Django app to manage members, topics, events, chats and more in an academic community
"""

from __future__ import annotations

from typing import Dict, Optional

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "Philipp S. Sommer"
__copyright__ = "2020-2023 Helmholtz-Zentrum hereon GmbH"
__credits__ = [
    "Philipp S. Sommer",
]
__license__ = "EUPL-1.2"

__maintainer__ = "Philipp S. Sommer"
__email__ = "philipp.sommer@hereon.de"

__status__ = "Pre-Alpha"

#: Apps that the academic-community portal app requires
INSTALLED_APPS = [
    "django.contrib.auth",
    "guardian",
    "academic_community",
    "django_e2ee",
    "academic_community.ckeditor5",
    "academic_community.channels",
    "academic_community.uploaded_material",
    "academic_community.institutions",
    "academic_community.mailinglists",
    "academic_community.members",
    "academic_community.activities",
    "academic_community.topics",
    "academic_community.events",
    "academic_community.events.programme",
    "academic_community.events.registrations",
    "academic_community.search",
    "academic_community.rest",
    "academic_community.history",
    "academic_community.notifications",
    "academic_community.contact",
    "academic_community.faqs",
    "private_storage",
    "rest_framework",
    "django_reactive",
    "reversion",
    "reversion_compare",
    "phonenumber_field",
    "django_bootstrap5",
    "captcha",
    "colorfield",
    "django_extensions",
    "django_filters",
    "django.contrib.postgres",
    # Apps necessary for django-cms
    "cms",
    "djangocms_versioning",
    "djangocms_alias",
    "parler",
    "treebeard",
    "django.contrib.sites",
    "django.contrib.humanize",
    "djangocms_admin_style",
    "django.contrib.admin",
    "menus",
    "sekizai",
    "djangocms_text_ckeditor",
    "filer",
    "easy_thumbnails",
    "django_select2",
    "djangocms_icon",
    "djangocms_link",
    "djangocms_frontend",
    "djangocms_frontend.contrib.accordion",
    "djangocms_frontend.contrib.alert",
    "djangocms_frontend.contrib.badge",
    "djangocms_frontend.contrib.card",
    "djangocms_frontend.contrib.carousel",
    "djangocms_frontend.contrib.collapse",
    "djangocms_frontend.contrib.content",
    "djangocms_frontend.contrib.grid",
    "djangocms_frontend.contrib.icon",
    "djangocms_frontend.contrib.image",
    "djangocms_frontend.contrib.jumbotron",
    "djangocms_frontend.contrib.link",
    "djangocms_frontend.contrib.listgroup",
    "djangocms_frontend.contrib.media",
    "djangocms_frontend.contrib.navigation",
    "djangocms_frontend.contrib.tabs",
    "djangocms_frontend.contrib.utilities",
    "djangocms_file",
    "djangocms_style",
    "aldryn_apphooks_config",
    "channels",
    # "djangocms_googlemap",
    # "djangocms_video",
]


def get_community_members_group_id() -> int:
    """Get the ID for the community members group."""
    from django.db.utils import OperationalError, ProgrammingError

    from academic_community import utils

    try:
        group = utils.get_members_group()
    except (OperationalError, ProgrammingError):
        # migrations did not run yet
        return 1
    else:
        return group.id


DJANGOCMS_FRONTEND_LINK_MODELS = [
    {
        "type": "Community Members",
        "class_path": "academic_community.members.models.CommunityMember",
        "filter": {
            "is_member": True,
        },
        "search": "last_name",
        "order_by": "first_name",
    },
    {
        "type": "Working groups",
        "class_path": "academic_community.activities.models.Activity",
        "filter": {
            "end_date__isnull": True,
            "group_view_permission__id": get_community_members_group_id,
        },
        "search": "abbreviation",
    },
    {
        "type": "Topics",
        "class_path": "academic_community.topics.models.Topic",
        "filter": {
            "end_date__isnull": True,
            "group_view_permission__id": get_community_members_group_id,
        },
        "search": "id_name",
        "order_by": "id_name",
    },
    {
        "type": "Events",
        "class_path": "academic_community.events.models.Event",
        "filter": {
            "event_view_groups__id": get_community_members_group_id,
        },
        "search": "name",
        "order_by": "-time_range",
    },
    {
        "type": "Institution",
        "class_path": "academic_community.institutions.models.Institution",
        "filter": {
            "end_date__isnull": True,
        },
        "search": "abbreviation",
    },
    {
        "type": "Uploaded Material",
        "class_path": "academic_community.uploaded_material.models.Material",
        "filter": {
            "group_view_permission__id": get_community_members_group_id,
        },
        "search": "name",
        "order_by": "-last_modification_date",
    },
    {
        "type": "Channels",
        "class_path": "academic_community.channels.models.Channel",
        "filter": {
            "group_view_permission__id": get_community_members_group_id,
        },
        "search": "name",
        "order_by": "-last_comment_modification_date",
    },
]


AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",  # this is default
    "guardian.backends.ObjectPermissionBackend",
)


REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": [
        "academic_community.rest.permissions.ReadOnly",
    ],
}


# template context processors
context_processors = [
    "academic_community.context_processors.get_default_context",
    "django_e2ee.context_processors.get_e2ee_login_context_data",
    "django.template.context_processors.i18n",
    "django.template.context_processors.media",
    "django.template.context_processors.csrf",
    "django.template.context_processors.tz",
    "sekizai.context_processors.sekizai",
    "django.template.context_processors.static",
    "cms.context_processors.cms_settings",
]


# Add reversion models to admin interface:
ADD_REVERSION_ADMIN = True


LANGUAGES = (
    # Customize this
    ("en", "en"),
)

CMS_LANGUAGES = {
    # Customize this
    1: [
        {
            "code": "en",
            "name": "en",
            "redirect_on_fallback": True,
            "public": True,
            "hide_untranslated": False,
        },
    ],
    "default": {
        "redirect_on_fallback": True,
        "public": True,
        "hide_untranslated": False,
    },
}

CMS_TEMPLATES = (
    # Customize this
    ("fullwidth.html", "Fullwidth"),
    ("navigation_sidebar.html", "With sidebar"),
    ("empty_cms.html", "Without static placeholders"),
)

X_FRAME_OPTIONS = "SAMEORIGIN"

CMS_PERMISSION = True

CMS_PLACEHOLDER_CONF: Dict[Optional[str], Dict] = {
    None: {
        "excluded_plugins": [
            "ProfileButtonPlugin",
        ]
    },
    # TODO: using default_plugins does not work with django-cms v4. this needs
    # to be migrated!
    "activity_detail": {  # default widgets for activity detail page
        "default_plugins": [
            {
                "plugin_type": "TabPlugin",
                "values": {
                    "config": {
                        "tab_index": 1,
                        "tab_effect": None,
                        "tab_type": "nav-tabs",
                    }
                },
                "children": [
                    {
                        "plugin_type": "TabItemPlugin",
                        "values": {
                            "config": {"tab_title": "Abstract"},
                        },
                        "children": [
                            {
                                "plugin_type": "CardPlugin",
                                "values": {"config": {"card_alignment": None}},
                                "children": [
                                    {
                                        "plugin_type": "CardInnerPlugin",
                                        "values": {
                                            "config": {
                                                "inner_type": "card-body"
                                            }
                                        },
                                        "children": [
                                            {
                                                "plugin_type": "ActivityAbstractPublisher",
                                                "values": {"from_url": True},
                                            }
                                        ],
                                    }
                                ],
                            }
                        ],
                    },
                    {
                        "plugin_type": "TabItemPlugin",
                        "values": {"config": {"tab_title": "Members"}},
                        "children": [
                            {
                                "plugin_type": "ActivityMembersListPublisher",
                                "values": {"from_url": True},
                            }
                        ],
                    },
                    {
                        "plugin_type": "TabItemPlugin",
                        "values": {"config": {"tab_title": "Topics"}},
                        "children": [
                            {
                                "plugin_type": "ActivityTopicListPublisher",
                                "values": {"from_url": True},
                            }
                        ],
                    },
                    {
                        "plugin_type": "TabItemPlugin",
                        "values": {"config": {"tab_title": "Institutions"}},
                        "children": [
                            {
                                "plugin_type": "ActivityAcademicOrganizationListPublisher",
                                "values": {
                                    "from_url": True,
                                    "show_parent_organizations": True,
                                    "show_institution_logos": True,
                                    "show_child_organizations": True,
                                },
                            }
                        ],
                    },
                    {
                        "plugin_type": "TabItemPlugin",
                        "values": {"config": {"tab_title": "Events"}},
                        "children": [
                            {
                                "plugin_type": "ActivityEventListPublisher",
                                "values": {
                                    "from_url": True,
                                    "past_events": True,
                                },
                            }
                        ],
                    },
                    {
                        "plugin_type": "TabItemPlugin",
                        "values": {
                            "config": {"tab_title": "Chats & Discussions"}
                        },
                        "children": [
                            {
                                "plugin_type": "ActivityChannelListPublisher",
                                "values": {
                                    "from_url": True,
                                },
                            }
                        ],
                    },
                    {
                        "plugin_type": "TabItemPlugin",
                        "values": {"config": {"tab_title": "Material"}},
                        "children": [
                            {
                                "plugin_type": "ActivityMaterialListPublisher",
                                "values": {
                                    "from_url": True,
                                },
                            }
                        ],
                    },
                ],
            },
        ],
    },
    "activity_sidebar": {
        "excluded_plugins": ["ActivitySidebarPublisher"],
        "default_plugins": [
            {
                "plugin_type": "CardPlugin",
                "values": {"config": {"card_alignment": None}},
                "children": [
                    {
                        "plugin_type": "CardInnerPlugin",
                        "values": {"config": {"inner_type": "card-header"}},
                        "children": [
                            {
                                "plugin_type": "TextPlugin",
                                "values": {"body": "Leader"},
                            },
                        ],
                    },
                    {
                        "plugin_type": "CardInnerPlugin",
                        "values": {
                            "config": {
                                "attributes": {"class": "p-0"},
                                "inner_type": "card-body",
                            }
                        },
                        "children": [
                            {
                                "plugin_type": "ActivityMembersListPublisher",
                                "values": {
                                    "from_url": True,
                                    "leaders_only": True,
                                    "show_count": False,
                                    "show_institutions": False,
                                },
                            },
                        ],
                    },
                ],
            },
            {
                "plugin_type": "CardPlugin",
                "values": {"config": {"card_alignment": None}},
                "children": [
                    {
                        "plugin_type": "CardInnerPlugin",
                        "values": {"config": {"inner_type": "card-header"}},
                        "children": [
                            {
                                "plugin_type": "TextPlugin",
                                "values": {"body": "Stats"},
                            },
                        ],
                    },
                    {
                        "plugin_type": "CardInnerPlugin",
                        "values": {
                            "config": {
                                "attributes": {"class": "p-0"},
                                "inner_type": "card-body",
                            }
                        },
                        "children": [
                            {
                                "plugin_type": "ActivityBadgesPublisher",
                                "values": {"from_url": True},
                            },
                        ],
                    },
                ],
            },
        ],
    },
}


DJANGOCMS_FRONTEND_ICON_LIBRARIES = {
    "font-awesome-5": (
        "font-awesome-5.min.json",
        "https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5/css/all.min.css",
    ),
}

DJANGOCMS_FRONTEND_ICON_LIBRARIES_SHOWN = ["font-awesome-5"]

CMS_TOOLBAR_ANONYMOUS_ON = False

# cannot set this to True, likely because of
# https://github.com/django-cms/django-cms/issues/6616
CMS_TOOLBAR_HIDE = False

THUMBNAIL_PROCESSORS = (
    "easy_thumbnails.processors.colorspace",
    "easy_thumbnails.processors.autocrop",
    "filer.thumbnail_processors.scale_and_crop_with_subject_location",
    "easy_thumbnails.processors.filters",
)

MIDDLEWARE = [
    "cms.middleware.utils.ApphookReloadMiddleware",
    "django.middleware.cache.UpdateCacheMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "cms.middleware.user.CurrentUserMiddleware",
    "cms.middleware.page.CurrentPageMiddleware",
    "cms.middleware.toolbar.ToolbarMiddleware",
    "academic_community.middleware.HideToolbarMiddleware",
    # "cms.middleware.language.LanguageCookieMiddleware",
    "django.middleware.cache.FetchFromCacheMiddleware",
]


PRIVATE_STORAGE_AUTH_FUNCTION = (
    "academic_community.uploaded_material.allow_members"
)
