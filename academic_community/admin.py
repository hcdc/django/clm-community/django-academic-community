# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
# SPDX-License-Identifier: EUPL-1.2

"""Admin interfaces
----------------

This module defines the django-academic-community
Admin interfaces.
"""
from cms.extensions import PageExtensionAdmin
from django.contrib import admin
from djangocms_versioning.admin import (
    ExtendedGrouperVersionAdminMixin,
    StateIndicatorMixin,
)

from academic_community import models


class ManagerAdminMixin:
    """A mixin to enable the django admin for managers only."""

    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser or (
            request.user.is_manager
            and super().has_view_permission(request, obj)
        )

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser or (
            request.user.is_manager
            and super().has_change_permission(request, obj)
        )

    def has_add_permission(self, request):
        return request.user.is_superuser or (
            request.user.is_manager and super().has_add_permission(request)
        )

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser or (
            request.user.is_manager
            and super().has_delete_permission(request, obj)
        )


@admin.register(models.PageStylesExtension)
class PageStylesExtensionAdmin(PageExtensionAdmin):
    pass


@admin.register(models.PageMenuExtension)
class PageMenuExtensionAdmin(PageExtensionAdmin):
    pass


@admin.register(models.ObjectPage)
class ObjectPageAdmin(
    ExtendedGrouperVersionAdminMixin, StateIndicatorMixin, admin.ModelAdmin
):
    list_display = [
        "get_author",
        "get_modified_date",
        "state_indicator",
    ]


@admin.register(models.ObjectPageContent)
class ObjectPageContentAdmin(admin.ModelAdmin):
    pass
