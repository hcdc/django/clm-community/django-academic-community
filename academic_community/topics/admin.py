"""Administation classes for the topics models."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from typing import Sequence

from aldryn_apphooks_config.admin import BaseAppHookConfig, ModelAppHookConfig
from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from reversion_compare.admin import CompareVersionAdmin

from academic_community import utils
from academic_community.admin import ManagerAdminMixin
from academic_community.channels.admin import RelatedMentionLinkAdmin
from academic_community.topics import forms, models


class TopicMembershipInline(admin.TabularInline):
    model = models.TopicMembership


class LeftTopicRelationInline(admin.TabularInline):
    """An inline for topic relations."""

    model = models.TopicRelation

    fk_name = "left"


class RightTopicRelationInline(admin.TabularInline):
    """An inline for topic relations."""

    model = models.TopicRelation

    fk_name = "right"


@admin.register(models.TopicRelation)
class TopicRelationAdmin(
    ManagerAdminMixin, GuardedModelAdmin, CompareVersionAdmin
):
    """An admin for the :model:`topics.TopicRelation` model."""

    list_display = ["__str__", "left", "relation_type", "right"]

    list_editable = ["relation_type"]

    search_fields = [
        "left_name",
        "left_id_name",
        "left_leader__first_name",
        "left_leader__last_name",
        "left_leader__email__email",
        "left_lead_organization__name",
        "left_lead_organization__institution__abbreviation",
        "left_lead_organization__department__abbreviation",
        "left_lead_organization__unit__abbreviation",
        "left_lead_organization__institution__city__name",
        "left_lead_organization__institution__city__country__name",
        "left_lead_organization__institution__city__country__code",
        "relation_type__icontains",
        "right_name",
        "right_id_name",
        "right_leader__first_name",
        "right_leader__last_name",
        "right_leader__email__email",
        "right_lead_organization__name",
        "right_lead_organization__institution__abbreviation",
        "right_lead_organization__department__abbreviation",
        "right_lead_organization__unit__abbreviation",
        "right_lead_organization__institution__city__name",
        "right_lead_organization__institution__city__country__name",
        "right_lead_organization__institution__city__country__code",
    ]


@admin.register(models.TopicsConfig)
class TopicsConfigAdmin(
    BaseAppHookConfig, GuardedModelAdmin, admin.ModelAdmin
):
    """An admin for the TopicsConfig model"""

    filter_horizontal: Sequence[str] = [
        "user_topic_add_permission",
        "group_topic_add_permission",
    ]

    def get_config_fields(self):
        return (
            "id_name_function",
            "user_topic_add_permission",
            "group_topic_add_permission",
            "config.label",
            "config.label_lower",
            "config.label_plural",
            "config.label_plural_lower",
        )

    def get_changeform_initial_data(self, request):
        ret = super().get_changeform_initial_data(request)
        if "group_topic_add_permission" not in ret:
            ret["group_topic_add_permission"] = utils.get_groups("MEMBERS")
        return ret


@admin.register(models.Topic)
class TopicAdmin(
    ManagerAdminMixin,
    GuardedModelAdmin,
    ModelAppHookConfig,
    CompareVersionAdmin,
):
    """Administration class for the :model:`topics.Topic` model."""

    class Media:
        js = (
            "https://code.jquery.com/jquery-3.6.0.min.js",  # jquery
            "js/topic_admin.js",
        )

    list_display = [
        "id_name",
        "name",
        "leader",
        "start_date",
        "end_date",
        "last_modification_date",
    ]

    list_filter = [
        "start_date",
        "end_date",
        "last_modification_date",
    ]

    form = forms.TopicAdminForm
    inlines = [
        TopicMembershipInline,
        LeftTopicRelationInline,
        RightTopicRelationInline,
    ]

    search_fields = [
        "name",
        "id_name",
        "leader__first_name",
        "leader__last_name",
        "leader__email__email",
        "lead_organization__name",
        "lead_organization__institution__abbreviation",
        "lead_organization__department__abbreviation",
        "lead_organization__unit__abbreviation",
        "lead_organization__institution__city__name",
        "lead_organization__institution__city__country__name",
        "lead_organization__institution__city__country__code",
    ]

    filter_horizontal = [
        "user_view_permission",
        "group_view_permission",
    ]

    def get_changeform_initial_data(self, request):
        ret = super().get_changeform_initial_data(request)
        if "group_view_permission" not in ret:
            ret["group_view_permission"] = utils.get_groups("MEMBERS")
        return ret


@admin.register(models.TopicMembership)
class TopicMembershipAdmin(
    ManagerAdminMixin, GuardedModelAdmin, CompareVersionAdmin
):
    """Administration class for the :model:`topics.TopicMembership`."""

    list_display = ["member", "topic", "start_date", "end_date"]

    list_filter = ["approved", "start_date", "end_date"]

    search_fields = [
        "topic__name",
        "topic__id_name",
        "member__first_name",
        "member__last_name",
        "member__email__email",
    ]


@admin.register(models.Keyword)
class KeywordAdmin(ManagerAdminMixin, admin.ModelAdmin):
    """Admin for the keyword model."""

    pass


@admin.register(models.TopicMaterialRelation)
class TopicMaterialRelationAdmin(ManagerAdminMixin, admin.ModelAdmin):
    """An admin for :model:`topics.TopicMaterialRelation`"""

    search_fields = [
        "material__name",
        "topic__name",
        "topic__id_name",
    ]

    list_display = ["material", "topic"]

    list_filter = [
        "material__date_created",
        "material__last_modification_date",
    ]


@admin.register(models.TopicMentionLink)
class TopicMentionLinkAdmin(RelatedMentionLinkAdmin):
    """An admin for topic mention links."""

    search_fields = [
        "related_model__id_name",
        "related_model__name",
    ]
