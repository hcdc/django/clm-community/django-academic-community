"""Filter sets for the topic views."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from dataclasses import dataclass
from typing import TYPE_CHECKING, Any, List, Sequence

import django_filters
from django.db.models import Model, Q
from django.utils.translation import gettext as _
from django_filters.constants import EMPTY_VALUES
from guardian.shortcuts import get_anonymous_user, get_objects_for_user

from academic_community.activities.models import Activity
from academic_community.filters import ActiveFilterSet
from academic_community.forms import FilteredSelectMultiple
from academic_community.topics import models


@dataclass
class FormCard:
    label: str
    fields: List[str]
    show: bool = False


if TYPE_CHECKING:
    from django.db.models import QuerySet


def get_activities(request) -> QuerySet[Activity]:
    user = request.user

    if user.is_anonymous:
        user = get_anonymous_user()

    return get_objects_for_user(user, "activities.view_activity", Activity)


class TopicOrderingFilter(django_filters.OrderingFilter):
    """A filter that takes lead institutions of topics into account."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.extra["choices"] += [
            ("lead_organization_name", "Lead organization name"),
            ("-lead_organization_name", "Lead organization name (descending)"),
            (
                "lead_organization_abbreviation",
                "Lead organization abbreviation",
            ),
            (
                "-lead_organization_abbreviation",
                "Lead organization abbreviation (descending)",
            ),
        ]

    def filter(self, qs, value):
        if value and any(
            v
            in [
                "lead_organization_name",
                "-lead_organization_name",
                "lead_organization_abbreviation",
                "-lead_organization_abbreviation",
            ]
            for v in value
        ):
            ordering = []
            for val in value:
                # field is lead_organization with (evenutually) leading -
                # what is `name` or `abbreviation`
                if val.startswith("lead_organization") or val.startswith(
                    "-lead_organization"
                ):
                    target, what = val.rsplit("_", 1)
                    for part in [
                        "institution",
                        "department__parent_institution",
                        "unit__parent_department__parent_institution",
                    ]:
                        ordering.append(f"{target}__{part}__{what}")
                elif val not in EMPTY_VALUES:
                    ordering.append(self.get_ordering_value(val))
            return qs.order_by(*ordering)
        else:
            return super().filter(qs, value)


class TopicFilterSet(ActiveFilterSet):
    """A filterset for topics."""

    ACTIVITY_CHOICES = (
        (None, "Both"),
        (True, "Active topic"),
        (False, "Finished topic"),
    )

    class Meta:
        model = models.Topic
        fields = {
            "name": ["search"],
            "id_name": ["istartswith"],
            "description": ["search"],
            "start_date": ["range", "gte", "lte"],
            "end_date": ["range", "gte", "lte", "isnull"],
            "last_modification_date": ["range", "gte", "lte"],
            "leader": ["exact"],
            "leader__first_name": ["search"],
            "leader__last_name": ["search"],
            "members__first_name": ["search"],
            "members__last_name": ["search"],
        }

    form_cards = {
        "filter-ordering": FormCard(
            _("Ordering"),
            ["o"],
            True,
        ),
        "filter-general": FormCard(
            _("General properties"),
            [
                "id_name__istartswith",
                "name__search",
                "end_date__isnull",
            ],
            True,
        ),
        "filter-activities": FormCard(
            _("Working group"), ["metadata__activities"]
        ),
        "filter-leader": FormCard(
            "Leader and lead organization",
            [
                "lead_organization",
                "leader",
                "leader__first_name__search",
                "leader__last_name__search",
            ],
        ),
        "filter-members": FormCard(
            _("Members"),
            [
                "members__first_name__search",
                "members__last_name__search",
                "members__membership",
            ],
        ),
        "filter-keywords": FormCard(_("Keywords"), ["metadata__keywords"]),
        "filter-start_date": FormCard(
            "Start, end or last modification date",
            [
                "start_date",
                "start_date__lte",
                "start_date__range",
                "start_date__gte",
                "end_date",
                "end_date__lte",
                "end_date__range",
                "end_date__gte",
                "last_modification_date",
                "last_modification_date__lte",
                "last_modification_date__range",
                "last_modification_date__gte",
            ],
        ),
    }

    o = TopicOrderingFilter(
        label="Order by",
        fields=(
            ("name", "name"),
            ("id_name", "id_name"),
            ("leader__first_name", "leader_first_name"),
            ("leader__last_name", "leader_last_name"),
            ("start_date", "start_date"),
            ("end_date", "end_date"),
        ),
        field_labels={
            "name": "Name",
            "id_name": "Identifier",
            "leader__first_name": "First name of the leader",
            "leader__last_name": "Last name of the leader",
            "start_date": "Start date of the institution in the community",
            "end_date": "End date of the institution in the community",
        },
    )

    start_date = django_filters.DateRangeFilter()
    end_date = django_filters.DateRangeFilter()
    last_modification_date = django_filters.DateRangeFilter()

    lead_organization = django_filters.CharFilter(
        method="filter_lead_organization", distinct=True
    )

    members__membership = django_filters.CharFilter(
        method="filter_member_organization", distinct=True
    )

    metadata__keywords = django_filters.ModelMultipleChoiceFilter(
        queryset=models.Keyword.objects.all(),
        method="filter_metadata_models_contains",
        widget=FilteredSelectMultiple("Keywords"),
        label="Keywords",
    )

    metadata__activities = django_filters.ModelMultipleChoiceFilter(
        queryset=get_activities,
        method="filter_metadata_models_contains",
        widget=FilteredSelectMultiple(_("Working groups")),
        label=_("Working groups"),
    )

    def filter_metadata_models_contains(
        self,
        queryset: QuerySet[models.Topic],
        name: str,
        value: Sequence[Model],
    ):
        """Filter for foreign keys."""
        if not value:
            return queryset
        else:
            query = None
            for instance in value:
                new_query = Q(**{name + "__p_keys__contains": instance.pk})
                if query is None:
                    query = new_query
                else:
                    query = query | new_query
            return queryset.filter(query)

    def filter_metadata_models_in(
        self,
        queryset: QuerySet[models.Topic],
        name: str,
        value: Sequence[Model],
    ):
        """Filter for foreign keys."""
        if not value:
            return queryset
        else:
            pks = [instance.pk for instance in value]
            query = Q(**{name + "__pk__in": pks})
            return queryset.filter(query)

    def filter_metadata_models_exact(
        self,
        queryset: QuerySet[models.Topic],
        name: str,
        value: Model,
    ):
        """Filter for foreign keys."""
        if not value:
            return queryset
        else:
            query = Q(**{name + "__pk": value.pk})
            return queryset.filter(query)

    def filter_metadata_choices(
        self, queryset: QuerySet[models.Topic], name: str, value: List[Any]
    ):
        if not value:
            return queryset
        else:
            query = None
            for instance in value:
                new_query = Q(**{name + "__contains": instance.pk})
                if query is None:
                    query = new_query
                else:
                    query = query | new_query
            return queryset.filter(query)

    def filter_metadata_search(
        self, queryset: QuerySet[models.Topic], name: str, value: str
    ):
        """Filter for foreign keys."""
        if not value:
            return queryset
        else:
            return queryset.filter(**{name + "__search": value})

    def filter_lead_organization(
        self, queryset: QuerySet[models.Topic], name: str, value: str
    ) -> QuerySet[models.Topic]:
        """Filter the lead organization by name and abbreviation."""
        return queryset.filter(
            Q(lead_organization__name=value)
            | Q(lead_organization__unit__parent_department__name__search=value)
            | Q(
                lead_organization__unit__parent_department__abbreviation__istartswith=value  # noqa: E501
            )
            | Q(
                lead_organization__unit__parent_department__parent_institution__name__search=value  # noqa: E501
            )
            | Q(
                lead_organization__unit__parent_department__parent_institution__abbreviation__istartswith=value  # noqa: E501
            )
            | Q(
                lead_organization__department__parent_institution__name__search=value  # noqa: E501
            )
            | Q(
                lead_organization__department__parent_institution__abbreviation__istartswith=value  # noqa: E501
            )
            | Q(
                lead_organization__institution__abbreviation__istartswith=value
            )
            | Q(lead_organization__department__abbreviation__istartswith=value)
            | Q(lead_organization__unit__abbreviation__istartswith=value)
        )

    def filter_member_organization(
        self, queryset: QuerySet[models.Topic], name: str, value: str
    ) -> QuerySet[models.Topic]:
        """Filter the lead organization by name and abbreviation."""
        return queryset.filter(
            Q(members__membership__name=value)
            | Q(
                members__membership__unit__parent_department__name__search=value  # noqa: E501
            )
            | Q(
                members__membership__unit__parent_department__abbreviation__istartswith=value  # noqa: E501
            )
            | Q(
                members__membership__unit__parent_department__parent_institution__name__search=value  # noqa: E501
            )
            | Q(
                members__membership__unit__parent_department__parent_institution__abbreviation__istartswith=value  # noqa: E501
            )
            | Q(
                members__membership__department__parent_institution__name__search=value  # noqa: E501
            )
            | Q(
                members__membership__department__parent_institution__abbreviation__istartswith=value  # noqa: E501
            )
            | Q(
                members__membership__institution__abbreviation__istartswith=value  # noqa: E501
            )
            | Q(
                members__membership__department__abbreviation__istartswith=value  # noqa: E501
            )
            | Q(members__membership__unit__abbreviation__istartswith=value)
        )

    @property
    def form(self):
        form = super().form
        form.template_name = "topics/components/topic_filter_form.html"
        return form

    def get_template_for_active_filter(self, key: str) -> List[str]:
        if key == "metadata__keywords":
            return ["topics/components/badges/keyword.html"]
        else:
            return super().get_template_for_active_filter(key)

    def get_extra_context_for_active_filter(self, key: str, value):
        """Reimplemented for keywords."""
        ret = super().get_extra_context_for_active_filter(key, value)
        if key == "keywords":
            ret["close_icon"] = True
        return ret
