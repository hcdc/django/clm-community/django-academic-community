# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Rest views for the topics app."""

from __future__ import annotations

from typing import Dict, List

from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from guardian.shortcuts import get_objects_for_user
from rest_framework import pagination

from academic_community.context_processors import get_default_context
from academic_community.mixins import (
    AppConfigMixin,
    ExportListAPIView,
    ExportRetrieveAPIView,
)
from academic_community.rest import permissions
from academic_community.topics import filters, models, serializers


class TopicPaginator(pagination.PageNumberPagination):
    """A pagination for topic api views."""

    page_size = 100

    page_size_query_param = "page_size"

    max_page_size = 200


class TopicRetrieveAPIView(AppConfigMixin, ExportRetrieveAPIView):
    """A rest-view for a single topic."""

    serializer_class = serializers.TopicSerializer

    pandoc_template_name = "topics/topic_export.html"

    permission_classes = [permissions.DjangoGlobalObjectPermissions]

    def get_queryset(self):
        return models.Topic.objects.all()

    def get_object(self):
        obj = get_object_or_404(
            self.get_queryset(),
            app_config=self.config,
            id_name=self.kwargs["slug"],
        )
        self.check_object_permissions(self.request, obj)
        return obj

    def get_pandoc_title(self, data: Dict):
        return data["name"]

    def get_pandoc_metadata(self, format, data):
        """Get the metadata for the export."""

        ret = super().get_pandoc_metadata(format, data)

        def get_affil_index(affiliation):
            return str(affiliations.index(affiliation) + 1)

        data = data[0]

        default_context = get_default_context(self.request)
        full_path = data["url"]

        ret.update(
            {
                "date": data["last_modification_date"],
                "metadata": [
                    {
                        "label": "Leader",
                        "value": "%(first_name)s %(last_name)s"
                        % data["leader"],
                    },
                    {
                        "label": "Lead organization",
                        "value": data["lead_organization"]["display_name"],
                    },
                    {
                        "date_code": "startDate",
                        "label": "Started",
                        "value": data["start_date"],
                        "is_date": True,
                    },
                    {
                        "date_code": "lastModificationDate",
                        "label": "Last modified",
                        "value": data["last_modification_date"],
                        "is_date": True,
                    },
                ],
                "links": [
                    {
                        "url": default_context["index_url"],
                        "title": default_context["community_name"],
                    },
                    {
                        "url": default_context["root_url"] + full_path,
                        "title": "View on site",
                    },
                ],
                "site_name": default_context["site_name"],
            }
        )
        if data["end_date"]:
            ret["metadata"].append(
                {
                    "label": "Ended",
                    "value": data["end_date"],
                    "is_date": True,
                    "date_code": "endDate",
                }
            )
        authors = []
        affiliations = []
        for membership in data["memberships"]:
            institutions = [ini["name"] for ini in membership["institutions"]]
            affiliations.extend(
                ini for ini in institutions if ini not in affiliations
            )
            authors.append(
                {
                    "name": "%(first_name)s %(last_name)s"
                    % membership["member"],
                    "orcid": membership["member"]["orcid"],
                    "affiliation": ", ".join(
                        map(get_affil_index, institutions)
                    ),
                }
            )
        if authors:
            ret["authors"] = authors
            ret["affiliations"] = [
                {"index": i, "name": affil}
                for i, affil in enumerate(affiliations, 1)
            ]
        return ret

    def get_pandoc_extra_args(self, format, tmpdir, data):
        """Get extra arguments for the pandoc conversion."""

        ret = super().get_pandoc_extra_args(format, tmpdir, data)

        data = data[0]

        ret += [
            "-V",
            f"footnote_paper_title={data['id_name']}: {data['name']}",
        ]
        return ret


class TopicListAPIView(AppConfigMixin, ExportListAPIView):
    """A rest-view for topics."""

    queryset = models.Topic.objects.all()
    serializer_class = serializers.TopicSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = filters.TopicFilterSet

    pagination_class = TopicPaginator

    pandoc_template_name = "topics/topic_export.html"

    def get_queryset(self):
        qs = super().get_queryset()
        return get_objects_for_user(
            self.request.user,
            "topics.view_topic",
            qs.namespace(self.namespace),
        )

    def get_context(self, format: str, data: Dict):
        if format in ["latex", "pdf"]:
            return {"heading_level": "h1"}
        else:
            return {}

    def get_pandoc_title(self, data: List[Dict]):
        return self.config.labels.label_plural

    def get_pandoc_metadata(self, format, data):
        """Get the metadata for the export."""
        ret = super().get_pandoc_metadata(format, data)
        if hasattr(self, "paginator"):
            self.add_pagination_metadata(
                data, ret, self.config.labels.label_plural
            )
        return ret

    def get_pandoc_extra_args(self, format, tmpdir, data):
        """Get extra arguments for the pandoc conversion."""

        ret = super().get_pandoc_extra_args(format, tmpdir, data)
        ret += ["-V", "toc-depth=1"]
        return ret
