"""Registry for further topic attributes."""

# SPDX-FileCopyrightText: 2020-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import Callable, Dict, Optional, Type, overload

from academic_community.topics import filters, forms

from .rest import TopicListAPIView, TopicRetrieveAPIView


class TopicViewsRegistry:
    """A registry for custom forms and filters."""

    default_create_form: Type[forms.TopicCreateForm]

    default_update_form: Type[forms.TopicUpdateForm]

    default_filterset: Type[filters.TopicFilterSet]

    default_admin_form: Type[forms.TopicCreateForm]

    default_export_list_view: Type[TopicListAPIView]

    default_export_view: Type[TopicRetrieveAPIView]

    def __init__(
        self,
        default_create_form: Type[
            forms.TopicCreateForm
        ] = forms.TopicCreateForm,
        default_update_form: Type[
            forms.TopicUpdateForm
        ] = forms.TopicUpdateForm,
        default_filterset: Type[
            filters.TopicFilterSet
        ] = filters.TopicFilterSet,
        default_admin_form: Type[forms.TopicCreateForm] = forms.TopicAdminForm,
        default_export_list_view: Type[TopicListAPIView] = TopicListAPIView,
        default_export_view: Type[TopicRetrieveAPIView] = TopicRetrieveAPIView,
    ) -> None:
        self._create_form_classes: Dict[str, Type[forms.TopicCreateForm]] = {}
        self._update_form_classes: Dict[str, Type[forms.TopicUpdateForm]] = {}
        self._filterset_classes: Dict[str, Type[filters.TopicFilterSet]] = {}
        self._export_list_view_classes: Dict[str, Type[TopicListAPIView]] = {}
        self._export_view_classes: Dict[str, Type[TopicRetrieveAPIView]] = {}
        self.filterset_classes: Dict[str, filters.TopicFilterSet] = {}
        self.default_create_form = default_create_form
        self.default_update_form = default_update_form
        self.default_filterset = default_filterset
        self.default_admin_form = default_admin_form
        self.default_export_list_view = default_export_list_view
        self.default_export_view = default_export_view

    def set_default_create_form(self, form: Type[forms.TopicCreateForm]):
        """Set the default form to use if no namespace specific form exists."""
        self.default_create_form = form

    def set_default_filterset(self, filterset: Type[filters.TopicFilterSet]):
        """Set the default filterset to use if no namespace specific one exists."""
        self.default_filterset = filterset

    def set_default_admin_form(self, form: Type[forms.TopicCreateForm]):
        """Set the default form to use if no namespace specific form exists."""
        self.default_admin_form = form

    def set_default_export_list_view(self, view: Type[TopicListAPIView]):
        """Set the default form to use if no namespace specific form exists."""
        self.default_export_list_view = view

    def set_default_export_view(self, view: Type[TopicRetrieveAPIView]):
        """Set the default form to use if no namespace specific form exists."""
        self.default_export_view = view

    @overload
    def register_namespace_export_list_view(
        self, namespace: str, view: None = None
    ) -> Callable[[Type[TopicListAPIView]], TopicListAPIView]:
        ...

    @overload
    def register_namespace_export_list_view(
        self, namespace: str, view: Type[TopicListAPIView]
    ) -> None:
        ...

    def register_namespace_export_list_view(self, namespace, view=None):
        """Register a topic form for a specific namespace"""
        if view is None:

            def decorator(
                view: Type[forms.TopicCreateForm],
            ) -> Type[forms.TopicCreateForm]:
                self.register_namespace_export_list_view(namespace, view)
                return view

            return decorator
        else:
            self._export_list_view_classes[namespace] = view

    @overload
    def register_namespace_export_view(
        self, namespace: str, view: None = None
    ) -> Callable[[Type[TopicRetrieveAPIView]], TopicRetrieveAPIView]:
        ...

    @overload
    def register_namespace_export_view(
        self, namespace: str, view: Type[TopicRetrieveAPIView]
    ) -> None:
        ...

    def register_namespace_export_view(self, namespace, view=None):
        """Register a topic form for a specific namespace"""
        if view is None:

            def decorator(
                view: Type[TopicRetrieveAPIView],
            ) -> Type[TopicRetrieveAPIView]:
                self.register_namespace_export_view(namespace, view)
                return view

            return decorator
        else:
            self._export_view_classes[namespace] = view

    @overload
    def register_namespace_form(
        self, namespace: str, form: None = None
    ) -> Callable[[Type[forms.TopicCreateForm]], forms.TopicCreateForm]:
        ...

    @overload
    def register_namespace_form(
        self, namespace: str, form: Type[forms.TopicCreateForm]
    ) -> None:
        ...

    def register_namespace_form(self, namespace, form=None):
        """Register a topic form for a specific namespace"""
        if form is None:

            def decorator(
                form: Type[forms.TopicCreateForm],
            ) -> Type[forms.TopicCreateForm]:
                self.register_namespace_form(namespace, form)
                return form

            return decorator
        else:
            self._create_form_classes[namespace] = form

    @overload
    def register_namespace_update_form(
        self, namespace: str, form: None = None
    ) -> Callable[[Type[forms.TopicUpdateForm]], forms.TopicUpdateForm]:
        ...

    @overload
    def register_namespace_update_form(
        self, namespace: str, form: Type[forms.TopicUpdateForm]
    ) -> None:
        ...

    def register_namespace_update_form(self, namespace, form=None):
        """Register a topic update form for a specific namespace"""
        if form is None:

            def decorator(
                form: Type[forms.TopicCreateForm],
            ) -> Type[forms.TopicCreateForm]:
                self.register_namespace_update_form(namespace, form)
                return form

            return decorator
        else:
            self._update_form_classes[namespace] = form

    @overload
    def register_namespace_filterset(
        self, namespace: str, filterset: None = None
    ) -> Callable[[Type[filters.TopicFilterSet]], filters.TopicFilterSet]:
        ...

    @overload
    def register_namespace_filterset(
        self, namespace: str, filterset: Type[filters.TopicFilterSet]
    ) -> None:
        ...

    def register_namespace_filterset(self, namespace, filterset=None):
        """Register a topic update form for a specific namespace"""
        if filterset is None:

            def decorator(
                filterset: Type[filters.TopicFilterSet],
            ) -> Type[filters.TopicFilterSet]:
                self.register_namespace_filterset(namespace, filterset)
                return filterset

            return decorator
        else:
            self._filterset_classes[namespace] = filterset

    def get_create_form_class(
        self, namespace: Optional[str] = None
    ) -> Type[forms.TopicCreateForm]:
        """Get the form class to use for a create view."""
        if namespace is None:
            return self.default_create_form
        else:
            return self._create_form_classes.get(
                namespace, self.default_create_form
            )

    def get_update_form_class(
        self, namespace: Optional[str] = None
    ) -> Type[forms.TopicUpdateForm]:
        """Get the form class to use for a update view."""
        if namespace is None:
            return self.default_update_form
        else:
            return self._update_form_classes.get(
                namespace, self.default_update_form
            )

    def get_filterset_class(
        self, namespace: Optional[str] = None
    ) -> Type[filters.TopicFilterSet]:
        """Get the form class to use for a create view."""
        if namespace is None:
            return self.default_filterset
        else:
            return self._filterset_classes.get(
                namespace, self.default_filterset
            )

    def get_admin_form_class(self) -> Type[forms.TopicCreateForm]:
        return self.default_admin_form

    def get_export_list_view(
        self, namespace: Optional[str] = None
    ) -> Type[TopicListAPIView]:
        """Get the list api view class for exporting topics.

        Parameters
        ----------
        namespace : Optional[str], optional
            The namespace to get the class for, by default None

        Returns
        -------
        Type[TopicListAPIView]
            The list api view for the topics.
        """
        if namespace is None:
            return self.default_export_list_view
        else:
            return self._export_list_view_classes.get(
                namespace, self.default_export_list_view
            )

    def get_export_view(
        self, namespace: Optional[str] = None
    ) -> Type[TopicRetrieveAPIView]:
        """Get the retrieve api view class for exporting topics.

        Parameters
        ----------
        namespace : Optional[str], optional
            The namespace to get the class for, by default None

        Returns
        -------
        Type[TopicRetrieveAPIView]
            The retrieve api view for the topics.
        """
        if namespace is None:
            return self.default_export_view
        else:
            return self._export_view_classes.get(
                namespace, self.default_export_view
            )


registry = TopicViewsRegistry()
