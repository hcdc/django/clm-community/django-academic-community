# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Plugins for the members app."""
from __future__ import annotations

from typing import TYPE_CHECKING, List, Optional

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.db.models import Q
from django.urls import reverse
from django.utils.translation import gettext as _
from guardian.shortcuts import get_anonymous_user, get_objects_for_user

from academic_community import utils
from academic_community.cms_plugins import (
    ActiveFiltersPublisher,
    ExportListButtonPluginBase,
    FilterButtonPublisher,
    FilterListPublisher,
    PaginationPublisherMixin,
)
from academic_community.members.cms_plugins import (
    CommunityMembersListPublisherBase,
)
from academic_community.members.models import CommunityMember
from academic_community.topics import models, views

if TYPE_CHECKING:
    from django.db.models import QuerySet


class GetTopicFiltersetClassMixin:
    """A mixin to retrieve the filterset class based on the app config"""

    def get_app_config(
        self, context, instance
    ) -> Optional[models.TopicsConfig]:
        """Get the app config of the model."""
        app_config = None

        if instance.app_config_from_request and not instance.app_config:
            _, app_config = utils.get_app_instance(context["request"])

        elif instance.app_config:
            app_config = instance.app_config
        return app_config

    def get_filterset_class(self, context, instance) -> utils.Type:
        app_config = self.get_app_config(context, instance)

        if app_config:
            return views.registry.get_filterset_class(app_config.namespace)
        else:
            return views.registry.default_filterset


@plugin_pool.register_plugin
class TopicActiveFiltersPublisher(
    GetTopicFiltersetClassMixin, ActiveFiltersPublisher
):
    """A plugin to display the applied filters for activities."""

    module = _("Topics")
    cache = False

    name = _("Display active filters for %(topics)s") % {"topics": _("Topics")}

    list_model = models.Topic

    model = models.TopicsActiveFiltersPluginModel  # type: ignore[override]


@plugin_pool.register_plugin
class TopicFilterButtonPublisher(
    GetTopicFiltersetClassMixin, FilterButtonPublisher
):
    """A plugin to display the applied filters for activities."""

    module = _("Topics")
    cache = False

    name = _("Filter button for %(topics)s") % {"topics": _("Topics")}

    list_model = models.Topic

    model = models.TopicsFilterButtonPluginModel


class TopicListPublisherBase(
    GetTopicFiltersetClassMixin, PaginationPublisherMixin, FilterListPublisher
):
    """A plugin to display material to the user."""

    module = _("Topics")
    render_template = "topics/components/topic_listplugin.html"
    cache = False

    list_model = models.Topic

    context_object_name = "topics"

    def get_query(
        self, instance: models.AbstractTopicListPluginModel
    ) -> Optional[Q]:
        """Get the query for the members."""
        query = None

        queries: List[Q] = []

        if instance.active_topics and instance.finished_topics:
            pass
        elif instance.active_topics:
            queries.append(Q(end_date__isnull=True))
        elif instance.finished_topics:
            queries.append(Q(end_date__isnull=False))
        if instance.app_config:
            queries.append(Q(app_config=instance.app_config))

        for q in queries:
            query = q if query is None else (query & q)  # type: ignore[operator]

        return query

    def filter_list_queryset(
        self,
        context,
        instance: models.TopicListPluginModel,  # type: ignore[override]
        queryset: QuerySet[models.Topic],
    ) -> QuerySet[models.Topic]:
        """Get a queryset of the filtered members"""

        queryset = super().filter_list_queryset(context, instance, queryset)
        query = self.get_query(instance)

        if query is not None:
            queryset = queryset.filter(query)
        if instance.check_permissions:
            user = context["request"].user
            if user.is_anonymous:
                user = get_anonymous_user()
            if queryset.model is not models.Topic:
                # HACK: fallback to models.Topic to make sure, we query on the
                # right model for user permissions
                assert issubclass(queryset.model, models.Topic)
                topics_queryset = models.Topic.objects.filter(
                    pk__in=queryset.values_list("pk", flat=True)
                )
                topics_queryset = get_objects_for_user(
                    user, "view_topic", topics_queryset
                )
                queryset = queryset.filter(
                    pk__in=topics_queryset.values_list("pk", flat=True)
                )
            else:
                queryset = get_objects_for_user(user, "view_topic", queryset)
        return queryset

    def render(
        self,
        context,
        instance: models.AbstractTopicListPluginModel,  # type: ignore[override]
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        context["show_logo"] = instance.show_institution_logos
        context["base_id"] = f"topic-plugin-{instance.pk}-"
        return context


@plugin_pool.register_plugin
class TopicListPublisher(TopicListPublisherBase):
    """A plugin to display selected community members"""

    name = _("Selected %(topics)s") % {"topics": _("Topics")}
    model = models.TopicListPluginModel

    filter_horizontal = ["topics"]

    def get_full_queryset(
        self, context, instance: models.TopicListPluginModel
    ):
        """Get the activities to display"""
        if instance.show_all:
            return super().get_full_queryset(context, instance)
        else:
            return instance.topics.all()


@plugin_pool.register_plugin
class TopicsMembersListPublisher(CommunityMembersListPublisherBase):
    """A publisher to display the members of an activity."""

    model = models.TopicMembersListPluginModel
    module = _("Topics")
    name = _("Members of a %(topic)s") % {"topic": _("topic")}

    autocomplete_fields = ["topic"]

    def get_full_queryset(
        self, context, instance: models.TopicMembersListPluginModel
    ) -> QuerySet[CommunityMember]:
        if instance.topic:
            topic = instance.topic
        elif instance.from_url:
            try:
                view = context["view"]
                try:
                    slug = view.kwargs["slug"]
                except KeyError:
                    slug = view.kwargs["topic_slug"]
            except KeyError:
                return CommunityMember.objects.none()
            try:
                topic = topic.objects.get(id_name=slug)
            except models.Topic.DoesNotExist:
                return CommunityMember.objects.none()
        else:
            return CommunityMember.objects.none()
        if instance.active_members and instance.former_members:
            memberships = topic.topicmembership_set.approved()  # type: ignore[attr-defined]
        elif instance.active_members:
            memberships = topic.topicmembership_set.all_active()  # type: ignore[attr-defined]
        elif instance.former_members:
            memberships = topic.topicmembership_set.all_finished()  # type: ignore[attr-defined]
        else:
            return CommunityMember.objects.none()
        return CommunityMember.objects.filter(
            pk__in=memberships.values_list("member__pk", flat=True)
        )


@plugin_pool.register_plugin
class ExportTopicListButtonPlugin(
    GetTopicFiltersetClassMixin, ExportListButtonPluginBase
):
    """A plugin to add a button to export a list of topics."""

    list_model = models.Topic

    module = _("Topics")

    name = _("Button for exporting %(topics)s") % {"topics": _("Topics")}

    model = models.ExportTopicListButtonPluginModel

    def get_export_url(self, context, instance) -> str:
        app_config = self.get_app_config(context, instance)
        if app_config:
            return reverse(app_config.namespace + ":topic-rest-list")
        else:
            return ""

    def render(
        self,
        context,
        instance: models.ExportTopicListButtonPluginModel,  # type: ignore[override]
        placeholder,
    ):
        """Render the context"""
        context = super().render(context, instance, placeholder)
        app_config = self.get_app_config(context, instance)
        if app_config:
            context["object_name"] = (
                "list of " + app_config.labels.label_plural_lower
            )
        return context


@plugin_pool.register_plugin
class CreateTopicButtonPublisher(CMSPluginBase):
    """A mixin for rendering active filters"""

    model = models.CreateTopicButtonPluginModel
    name = _("Button to add a new %(topic)s") % {"topic": _("topic")}
    module = _("Topics")

    render_template = "topics/components/create_topic_button_plugin.html"

    filter_horizontal = [
        "activities",
        "keywords",
        "user_view_permission",
        "group_view_permission",
    ]

    def render(
        self,
        context,
        instance: models.CreateTopicButtonPluginModel,
        placeholder,
    ):
        """Render the context of the queryset."""

        context["instance"] = instance

        request = context["request"]
        user = request.user

        has_perm = user.has_perm("topics.add_topic") and utils.has_perm(
            user, "topics.add_topic_for_namespace", instance.app_config
        )

        if has_perm:
            initial = {
                "activities": instance.activities.all(),
                "keywords": instance.keywords.all(),
                "user_view_permission": instance.user_view_permission.all(),
                "group_view_permission": instance.group_view_permission.all(),
            }

            if hasattr(user, "communitymember"):
                initial["leader"] = user.communitymember
                membership = user.communitymember.active_memberships.first()
                if membership:
                    initial["lead_organization"] = membership.organization
            form_class = views.registry.get_create_form_class(
                instance.app_config.namespace
            )
            context["form"] = form = form_class(
                initial=initial,
                prefix="topic-form-%i" % instance.pk,
                app_config=instance.app_config,
            )
            form.update_from_user(user)

            context["target_url"] = reverse(
                instance.app_config.namespace + ":topic-create"
            )

        return context

    def get_changeform_initial_data(self, request):
        """Get the intial data for the change form.

        This is the same as for the standard EventCreateView.
        """
        initial = super().get_changeform_initial_data(request)
        initial.update({"group_view_permission": utils.get_members_group()})
        return initial
