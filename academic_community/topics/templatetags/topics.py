"""Template tags to display a topic."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import Dict, Sequence

from classytags.arguments import Argument, MultiKeywordArgument
from classytags.core import Options
from classytags.helpers import InclusionTag, Tag
from django import template
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

from academic_community.topics.models import Keyword, Topic

register = template.Library()


badge_base = "topics/components/badges/"


@register.tag
class TopicCard(InclusionTag):
    """Display a card for a topic."""

    name = "topic_card"

    template = "topics/components/topic_card.html"

    options = Options(
        Argument("topic"),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def get_context(
        self, context, topic: Topic, template_context: Dict = {}
    ) -> str:
        context = context.flatten()
        context.update(template_context)
        context.setdefault("show_logo", False)
        context["topic"] = topic
        return context


@register.tag
class TopicRows(Tag):
    """Display multiple topics as a row."""

    name = "topic_rows"

    options = Options(
        Argument("topics"),
        MultiKeywordArgument("template_context", required=False, default={}),
    )

    def render_tag(
        self, context, topics: Sequence[Topic], template_context: Dict = {}
    ) -> str:
        context = context.flatten()
        context.update(template_context)
        context.setdefault("show_logo", False)

        rows = []
        for topic in topics:
            context["topic"] = topic
            rows.append(
                mark_safe(
                    render_to_string(
                        "topics/components/topic_card.html", context
                    )
                )
            )
        return "\n".join(rows)


@register.inclusion_tag(badge_base + "keyword.html", takes_context=True)
def keyword_badge(context, keyword: Keyword) -> Dict:
    """Render the badge of a keyword."""
    context = context.flatten()
    context["object"] = keyword
    return context
