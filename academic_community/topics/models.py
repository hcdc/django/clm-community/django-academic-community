# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

import copy
from typing import TYPE_CHECKING, Dict, List, Optional, Tuple, Union, cast

import reversion
from aldryn_apphooks_config.fields import AppHookConfigField
from aldryn_apphooks_config.managers import AppHookConfigManager
from cms.models import CMSPlugin
from cms.utils.compat import DJANGO_3_0
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models.signals import (
    m2m_changed,
    post_delete,
    post_save,
    pre_save,
)
from django.dispatch import receiver
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.translation import gettext as _
from djangocms_frontend.fields import AttributesField
from djangocms_text_ckeditor.fields import HTMLField
from guardian.models import UserObjectPermission
from guardian.shortcuts import assign_perm, remove_perm
from reversion.models import Version

from academic_community import utils
from academic_community.activities.models import Activity
from academic_community.channels.models import (
    ChannelRelation,
    ChannelSubscription,
    MentionLink,
)
from academic_community.history.models import RevisionMixin
from academic_community.institutions.models import (
    AcademicOrganization,
    Institution,
)
from academic_community.members.models import (
    AbstractCommunityMemberListPluginModel,
    CommunityMember,
)
from academic_community.models import NamedModel
from academic_community.models.cms import (
    AbstractExportListButtonPluginModelMixin,
    AbstractPaginationPluginModelMixin,
    ActiveFiltersPluginModelMixin,
    BaseFilterPluginModelMixin,
    FilterButtonPluginModelMixin,
    FilterPluginModelMixin,
)
from academic_community.topics.cms_appconfig import TopicsConfig
from academic_community.uploaded_material.models import MaterialRelation

if DJANGO_3_0:
    from django_jsonfield_backport.models import JSONField
else:
    JSONField = models.JSONField


if TYPE_CHECKING:
    from django.contrib.auth.models import User

    from academic_community.institutions.models import AcademicMembership


User = get_user_model()  # type: ignore  # noqa: F811


def get_first_topicsconfig() -> Optional[TopicsConfig]:
    return TopicsConfig.objects.first()


class Keyword(NamedModel):  # type: ignore[django-manager-missing]
    """A topic keyword."""

    class Meta:
        verbose_name = _("%(topic)s Keyword") % {"topic": _("Topic")}
        verbose_name_plural = _("%(topic)s Keywords") % {"topic": _("Topic")}

    name = models.CharField(
        max_length=150, help_text="The name of the keyword."
    )


@reversion.register
class Topic(RevisionMixin, models.Model):  # type: ignore[django-manager-missing]
    """A research topic within the community."""

    topicmembership_set: models.manager.RelatedManager[TopicMembership]

    topicchannelrelation_set: models.manager.RelatedManager[
        TopicChannelRelation
    ]

    topicmaterialrelation_set: models.manager.RelatedManager[
        TopicMaterialRelation
    ]

    class Meta:
        verbose_name = _("Topic")
        verbose_name_plural = _("Topics")
        ordering = ["id_name"]
        permissions = (
            ("approve_topicmembership", "Can approve topic memberships"),
            ("change_topic_lead", "Can change leader and lead organization"),
            ("change_id_name", "Can change the id name of the topic"),
        )
        constraints = [
            models.UniqueConstraint(
                name="unique_id_name_per_app_config",
                fields=("app_config", "id_name"),
            ),
            models.UniqueConstraint(
                name="unique_slug_per_app_config",
                fields=("app_config", "slug"),
            ),
        ]

    objects = AppHookConfigManager()  # type: ignore[django-manager-missing]

    def get_absolute_url(self) -> str:
        """Get the url to the detailed view of this topic."""
        if self.app_config is None:
            return ""
        else:
            return reverse(
                self.app_config.namespace + ":topic-detail",
                args=[str(self.slug)],
            )

    def get_edit_url(self):
        if self.app_config is None:
            return ""
        else:
            return reverse(
                self.app_config.namespace + ":edit-topic",
                args=[str(self.slug)],
            )

    # -------------------------------------------------------------------------
    # ---------------------------- Fields -------------------------------------
    # -------------------------------------------------------------------------

    name = models.CharField(max_length=255, help_text="The name of the topic.")

    id_name = models.CharField(
        max_length=25,
        verbose_name="ID name",
        help_text=_(
            "Unique identifier of the %(topic)s. HZG-001 for instance."
        )
        % {"topic": _("topic")},
    )

    slug = models.SlugField(
        max_length=25,
        blank=True,
        verbose_name="URL slug",
        help_text=_(
            "The slug of the %(topic)s. By default, the slugified id-name."
        )
        % {"topic": _("topic")},
    )

    app_config = AppHookConfigField(
        TopicsConfig,
        null=True,
        on_delete=models.SET_NULL,
        default=get_first_topicsconfig,
    )

    description = HTMLField(
        max_length=100000,
        help_text=_("More details about the %(topic)s.")
        % {"topic": _("topic")},
        null=True,
    )

    # TODO: Reimplement the stub type to make clear that it can be one of
    # instutition, department or unit
    lead_organization = models.ForeignKey(
        AcademicOrganization,
        on_delete=models.PROTECT,
        help_text=_(
            "The leading research organization (institution, department or "
            "unit) of this %(topic)s."
        )
        % {"topic": _("topic")},
    )

    leader = models.ForeignKey(
        CommunityMember,
        on_delete=models.PROTECT,
        help_text=_("The community member leading this %(topic)s.")
        % {"topic": _("topic")},
        related_name="topic_lead",
    )

    members = models.ManyToManyField(
        CommunityMember,
        help_text=_("Community members contributing to this %(topic)s.")
        % {"topic": _("topic")},
        through="TopicMembership",
        blank=True,
    )

    start_date = models.DateField(
        null=True,
        auto_now_add=True,
        help_text=_("The starting date of the %(topic)s.")
        % {"topic": _("topic")},
    )

    end_date = models.DateField(
        null=True,
        blank=True,
        help_text=_("The end date of the %(topic)s.") % {"topic": _("topic")},
    )

    last_modification_date = models.DateField(
        help_text=_("Date of the last update to the %(topic)s.")
        % {"topic": _("topic")},
        blank=True,
        null=True,
        auto_now=True,
    )

    user_view_permission = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        help_text=_(
            "Select explicit users that are allowed view this %(topic)s."
        )
        % {"topic": _("topic")},
        blank=True,
    )

    group_view_permission = models.ManyToManyField(
        Group,
        help_text=_(
            "Select explicit groups that are allowed to view this %(topic)s."
        )
        % {"topic": _("topic")},
        blank=True,
    )

    #: metadata field to allow additional attributes for topics.
    metadata = JSONField(default=dict, encoder=DjangoJSONEncoder)

    @property
    def keywords(self) -> models.QuerySet[Keyword]:
        """The keywords of the topic."""
        if "keywords" in self.metadata:
            return Keyword.objects.filter(
                pk__in=self.metadata["keywords"]["p_keys"]
            )
        return Keyword.objects.none()

    @property
    def activities(self) -> models.QuerySet[Activity]:
        """The keywords of the topic."""
        if "activities" in self.metadata:
            return Activity.objects.filter(
                pk__in=self.metadata["activities"]["p_keys"]
            )
        return Activity.objects.none()

    # -------------------------------------------------------------------------
    # ---------------------------- Properties ---------------------------------
    # -------------------------------------------------------------------------

    @cached_property
    def institutions(self) -> List[Institution]:
        """Get a list of institutions that are involved in this topic."""
        institutions = [self.lead_organization.organization.parent_institution]
        for topic_membership in self.approved_memberships:
            institutions.extend(
                [
                    institution
                    for institution in topic_membership.institutions
                    if institution not in institutions
                ]
            )
        return institutions

    @cached_property
    def can_be_opened(self) -> bool:
        """Test if the topic can be opened again.

        Once it is marked as finished, it can only be opened if the leader
        is still a member of the lead organization."""
        pk = self.lead_organization.pk
        return any(pk == orga.pk for orga in self.leader.active_organizations)

    @property
    def approved_memberships(self) -> models.QuerySet[TopicMembership]:
        """All approved topic memberships."""
        return self.topicmembership_set.filter(approved=True)

    # -------------------------------------------------------------------------
    # ---------------------------- Methods ------------------------------------
    # -------------------------------------------------------------------------

    def is_topic_member(self, member: CommunityMember):
        """Check if a communitymember is an approved member of this topic."""
        return member == self.leader or bool(
            self.topicmembership_set.filter(
                member=member, approved=True
            ).count()
        )

    @cached_property
    def topic_users(self) -> List[User]:
        """User accounts of the final members of this topic."""
        return [
            membership.member.user
            for membership in self.topicmembership_set.all_final(  # type: ignore
                models.Q(member__user__isnull=False)
            )
        ]

    @cached_property
    def lead_organization_contacts(self) -> List[CommunityMember]:
        """A list of contact persons of the lead organization."""
        organizations = [
            self.lead_organization
        ] + self.lead_organization.parent_organizations
        return [orga.contact for orga in organizations if orga.contact]

    @cached_property
    def lead_organization_contact_users(self) -> List[User]:
        """User account of the lead organization contact persons."""
        return [
            member.user
            for member in self.lead_organization_contacts
            if member.user
        ]

    def is_lead_organization_contact(
        self, member: Union[User, CommunityMember]
    ):
        """Check if a communitymember is an organization contact person."""
        if not isinstance(member, CommunityMember):
            # user is of type User
            if not hasattr(member, "communitymember"):
                return False
            else:
                member = member.communitymember
        member = cast(CommunityMember, member)
        contacts = self.lead_organization.organization_contacts
        return contacts and any(
            contact.pk == member.pk for contact in contacts
        )

    def initialize_from_form(self, form=None):
        """Populates the config JSON field based on initial values provided by the fields of form"""
        from academic_community.topics.views import registry

        if form is None:
            form = registry.get_update_form_class(self.app_config.namespace)
        if isinstance(form, type):  # if is class
            form = form()  # instantiate
        entangled_fields = getattr(
            getattr(form, "Meta", None), "entangled_fields", {}
        ).get("metadata", ())
        for field in entangled_fields:
            self.metadata.setdefault(field, form[field].initial or "")
        return self

    def update_permissions(self, member: CommunityMember, check: bool = True):
        """Update the permissions for the given community member."""

        if hasattr(self, "topic_ptr"):
            # update the permissions only on the base topic
            # everything else should be handled by subclasses
            self.topic_ptr.update_permissions(member, check)  # type: ignore
            return

        if not member.user:
            return
        if (
            not check
            or member == self.leader
            or self.is_lead_organization_contact(member)
        ):
            assign_perm("change_topic", member.user, self)
            assign_perm("view_topic", member.user, self)
            assign_perm("approve_topicmembership", member.user, self)
            assign_perm("change_topic_lead", member.user, self)
            for channel_relation in self.topicchannelrelation_set.filter(
                symbolic_relation=False
            ):
                channel_relation.channel.update_user_permissions(member.user)
                if (
                    self.is_topic_member(member)
                    and channel_relation.subscribe_members
                ):
                    channel_relation.get_or_create_subscription(member.user)
            for material_relation in self.topicmaterialrelation_set.filter(
                symbolic_relation=False
            ):
                material_relation.material.update_user_permissions(member.user)
        elif self.is_topic_member(member):
            assign_perm("change_topic", member.user, self)
            assign_perm("view_topic", member.user, self)
            remove_perm("approve_topicmembership", member.user, self)
            remove_perm("change_topic_lead", member.user, self)
            for channel_relation in self.topicchannelrelation_set.filter(
                symbolic_relation=False
            ):
                perms = channel_relation.channel.update_user_permissions(
                    member.user
                )
                if channel_relation.subscribe_members:
                    channel_relation.get_or_create_subscription(member.user)
            for material_relation in self.topicmaterialrelation_set.filter(
                symbolic_relation=False
            ):
                material_relation.material.update_user_permissions(member.user)
        else:
            remove_perm("change_topic", member.user, self)
            if not self.user_view_permission.filter(
                pk=member.user.pk
            ).exists():
                remove_perm("view_topic", member.user, self)
            remove_perm("approve_topicmembership", member.user, self)
            remove_perm("change_topic_lead", member.user, self)
            for channel_relation in self.topicchannelrelation_set.filter(
                symbolic_relation=False
            ):
                perms = channel_relation.channel.update_user_permissions(
                    member.user
                )
                if "view_channel" not in perms:
                    channel_relation.remove_subscription(member.user)
            for material_relation in self.topicmaterialrelation_set.filter(
                symbolic_relation=False
            ):
                material_relation.material.update_user_permissions(member.user)
        for membership in self.topicmembership_set.all():
            membership.update_permissions(member)

    def remove_all_permissions(self, user: User):
        """Remove all permissions for the given user."""
        permissions = [key for key, label in self.__class__._meta.permissions]
        for perm in ["view_topic", "change_topic"] + permissions:
            remove_perm(perm, user, self)

    def __str__(self):
        return "%s: %s" % (self.id_name, self.name)


@MaterialRelation.registry.register_model_name(
    _("%(topic)s Material") % {"topic": _("Topic")}
)
@MaterialRelation.registry.register_relation
class TopicMaterialRelation(MaterialRelation):
    """Activity related material."""

    class Meta:
        verbose_name = _("%(topic)s Material Relation") % {"topic": _("Topic")}
        verbose_name_plural = _("%(topic)s Material Relations") % {
            "topic": _("Topic")
        }
        constraints = [
            models.UniqueConstraint(
                name="unique_topic_relation_for_material",
                fields=("topic", "material"),
            )
        ]

    permission_map = copy.deepcopy(MaterialRelation.permission_map)
    permission_map["change"] += ["view"]

    related_permission_field = "topic"

    related_object_url_field = "slug"

    topic = models.ForeignKey(
        Topic,
        on_delete=models.CASCADE,
        help_text="The topic that this material belongs to.",
    )


@ChannelRelation.registry.register_model_name("Topic Channel")
@ChannelRelation.registry.register_relation
class TopicChannelRelation(ChannelRelation):
    """Topic related channel."""

    class Meta:
        verbose_name = _("%(topic)s Channel Relation") % {"topic": _("Topic")}
        verbose_name_plural = _("%(topic)s Channel Relations") % {
            "topic": _("Topic")
        }

    permission_map = copy.deepcopy(ChannelRelation.permission_map)
    permission_map["change"] += ["view", "start_thread", "post_comment"]

    related_permission_field = "topic"

    related_object_url_field = "slug"

    topic = models.ForeignKey(
        Topic,
        on_delete=models.CASCADE,
        help_text=("The related topic that this channel belongs to."),
    )

    subscribe_members = models.BooleanField(
        default=True,
        help_text=(
            "If enabled, all current and future topic members are "
            "subscribed to the channel."
        ),
        verbose_name="Automatically subscribe members to the channel",
    )

    def remove_subscription_and_notify(
        self, user: User, notify=True
    ) -> Optional[ChannelSubscription]:
        """Maybe remove the channel subscription of a user."""
        subscription = self.remove_subscription(user)
        if notify and subscription:
            subscription.create_subscription_removal_notification(
                reason=(
                    f"the relation to {self.topic.id_name} has been removed"
                )
            )
        return subscription

    def get_mentionlink(self, user: User) -> MentionLink:
        return self.topic.topicmentionlink


class TopicMembershipQueryset(models.QuerySet):
    """A queryset with extra methods for querying members."""

    def all_active(self, **kwargs) -> models.QuerySet[TopicMembership]:
        return self.approved(end_date__isnull=True, **kwargs)

    def all_final(self, *args) -> models.QuerySet[TopicMembership]:
        """Get all final memberships.

        The final members are memberships that are not yet finished or whose
        end date is greater or equal than the end date of the topic.
        """
        query = models.Q(end_date__isnull=True) | (
            models.Q(topic__end_date__isnull=False)
            & models.Q(end_date__gte=models.F("topic__end_date"))
        )
        if args:
            return self.approved(query & args[0], *args[1:])
        else:
            return self.approved(query)

    def all_finished(self, **kwargs) -> models.QuerySet[TopicMembership]:
        return self.approved(end_date__isnull=False, **kwargs)

    def approved(self, *args, **kwargs) -> models.QuerySet[TopicMembership]:
        if args:
            return self.filter(
                models.Q(approved=True) & args[0], *args[1:], **kwargs
            )
        else:
            return self.filter(approved=True, **kwargs)


class TopicMembershipManager(
    models.Manager.from_queryset(TopicMembershipQueryset)  # type: ignore # noqa: E501
):
    """Database manager for TopicMembership."""


@reversion.register
class TopicMembership(RevisionMixin, models.Model):
    """A membership in a Topic.

    This model is a translated version of <i>tbl_Topic_Contact</i>
    """

    class Meta:
        verbose_name = _("%(topic)s Membership") % {"topic": _("Topic")}
        verbose_name_plural = _("%(topic)s Memberships") % {
            "topic": _("Topic")
        }
        ordering = [models.F("start_date").asc(nulls_last=True)]
        permissions = (("end_topicmembership", "Can end topic memberships"),)

    objects = TopicMembershipManager()

    member = models.ForeignKey(
        CommunityMember,
        on_delete=models.CASCADE,
        help_text="The members profile.",
    )

    topic = models.ForeignKey(
        Topic,
        on_delete=models.CASCADE,
        help_text="The topic in the community that the member contributes to.",
    )

    approved = models.BooleanField(
        default=False, help_text="Topic membership approved by topic leader."
    )

    start_date = models.DateField(
        null=True,
        blank=True,
        auto_now_add=True,
        help_text="The date when the member joined the topic.",
    )

    end_date = models.DateField(
        null=True,
        blank=True,
        help_text="The date when the member left the topic.",
    )

    @property
    def is_active(self) -> bool:
        """Flag that is true if this is an active membership"""
        return self.approved and (self.end_date is None)

    @property
    def is_final(self) -> bool:
        """Flag that is true if this is a final member.

        Final members are active members or members whose membership end dates
        are greater than the topic end date."""
        return self.approved and (
            self.end_date is None
            or (
                self.topic.end_date is not None
                and self.end_date > self.topic.end_date
            )
        )

    @cached_property
    def academic_memberships(self) -> models.QuerySet[AcademicMembership]:
        """Get a list of memberships during this topic."""
        if self.start_date:
            query = models.Q(end_date__isnull=True) | models.Q(
                end_date__gte=self.start_date
            )
            if self.end_date:
                query &= models.Q(start_date__lte=self.end_date)
            return self.member.academicmembership_set.filter(query)
        elif self.end_date:
            query = models.Q(start_date__lte=self.end_date)
            return self.member.academicmembership_set.filter(query)
        else:
            return self.member.active_memberships

    @cached_property
    def institutions(self) -> List[Institution]:
        """A list of institutions involved by the member in the topic."""
        institutions: List[Institution] = []
        for ms in self.academic_memberships:
            institution = ms.organization.organization.parent_institution
            if institution not in institutions:
                institutions.append(institution)
        return institutions

    def update_permissions(self, member: CommunityMember):
        """Update the permissions for the given community member."""
        if not member.user:
            return
        user: User = member.user

        if not self.topic.end_date and (
            member == self.topic.leader
            or member == self.member
            or self.topic.is_lead_organization_contact(member)
        ):
            assign_perm("end_topicmembership", user, self)
        else:
            remove_perm("end_topicmembership", user, self)

    def reset_permissions(self):
        """Reset the permissions for topic leader, the member and contacts."""
        self.update_permissions(self.member)
        self.update_permissions(self.topic.leader)
        for contact in self.topic.lead_organization.organization_contacts:
            self.update_permissions(contact)

    def remove_all_permissions(self, user: User):
        """Remove all permissions for the given user."""
        permissions = [key for key, label in self.__class__._meta.permissions]
        for perm in ["change_topic"] + permissions:
            remove_perm(perm, user, self)

    def get_absolute_url(self):
        return self.topic.get_absolute_url()

    def __str__(self):
        return f"Membership of {self.member} in {self.topic}"


class DataCiteRelationType(models.TextChoices):
    """Relation types for data cite.

    Taken from https://support.datacite.org/docs/relationtype_for_citation.
    """

    is_cited_by = "IsCitedBy", "is cited by"
    cites = "Cites", "cites"

    is_supplement_to = "IsSupplementTo", "is supplement to"
    is_supplemented_by = "IsSupplementedBy", "is supplemented by"

    is_continued_by = "IsContinuedBy", "is continued by"
    continues = "Continues", "continues"

    is_described_by = "IsDescribedBy", "is described by"
    describes = "Describes", "describes"

    has_metadata = "HasMetadata", "has metadata"
    is_metadata_for = "IsMetadataFor", "is metadata for"

    has_version = "HasVersion", "has version"
    is_version_of = "IsVersionOf", "is version of"

    is_new_version_of = "IsNewVersionOf", "is new version of"
    previous_version_of = "PreviousVersionOf", "previous version of"

    is_part_of = "IsPartOf", "is part of"
    has_part = "HasPart", "has part"

    is_referenced_by = "IsReferencedBy", "is referenced by"
    references = "References", "references"

    is_documented_by = "IsDocumentedBy", "is documented by"
    documents = "Documents", "documents"

    is_compiled_by = "IsCompiledBy", "is compiled by"
    compiles = "Compiles", "compiles"

    is_variant_form_of = "IsVariantFormOf", "is variant form of"
    is_original_form_of = "IsOriginalFormOf", "is original form of"

    is_identical_to = "IsIdenticalTo", "is identical to"

    is_reviewed_by = "IsReviewedBy", "is reviewed by"
    reviews = "Reviews", "reviews"

    is_derived_from = "IsDerivedFrom", "is derived from"
    is_source_of = "IsSourceOf", "is source of"

    requires = "Requires", "requires"
    is_required_by = "IsRequiredBy", "is required by"

    is_obsoleted_by = "IsObsoletedBy", "is obsoleted by"
    obsoletes = "Obsoletes", "obsoletes"

    @property
    def reverse_relation(self) -> DataCiteRelationType:
        """test"""
        return reverse_relations[self]


reverse_relations: Dict[DataCiteRelationType, DataCiteRelationType] = {
    DataCiteRelationType.is_cited_by: DataCiteRelationType.cites,
    DataCiteRelationType.is_supplement_to: DataCiteRelationType.is_supplemented_by,
    DataCiteRelationType.is_continued_by: DataCiteRelationType.continues,
    DataCiteRelationType.is_described_by: DataCiteRelationType.describes,
    DataCiteRelationType.has_metadata: DataCiteRelationType.is_metadata_for,
    DataCiteRelationType.has_version: DataCiteRelationType.is_version_of,
    DataCiteRelationType.is_new_version_of: DataCiteRelationType.previous_version_of,
    DataCiteRelationType.is_part_of: DataCiteRelationType.has_part,
    DataCiteRelationType.is_referenced_by: DataCiteRelationType.references,
    DataCiteRelationType.is_documented_by: DataCiteRelationType.documents,
    DataCiteRelationType.is_compiled_by: DataCiteRelationType.compiles,
    DataCiteRelationType.is_variant_form_of: DataCiteRelationType.is_original_form_of,
    DataCiteRelationType.is_identical_to: DataCiteRelationType.is_identical_to,
    DataCiteRelationType.is_reviewed_by: DataCiteRelationType.reviews,
    DataCiteRelationType.is_derived_from: DataCiteRelationType.is_source_of,
    DataCiteRelationType.requires: DataCiteRelationType.is_required_by,
    DataCiteRelationType.is_obsoleted_by: DataCiteRelationType.obsoletes,
}

reverse_relations.update({val: key for key, val in reverse_relations.items()})


@reversion.register
class TopicRelation(models.Model):
    """A relation between two topics."""

    class Meta:
        verbose_name = _("%(topic)s Relation") % {"topic": _("Topic")}
        verbose_name_plural = _("%(topic)s Relations") % {"topic": _("Topic")}

    left = models.ForeignKey(
        Topic,
        on_delete=models.CASCADE,
        help_text="The left side of the relation",
        related_name="relation_left",
    )

    relation_type = models.CharField(
        max_length=30,
        choices=DataCiteRelationType.choices,
        help_text="The description of the relation between the two topics.",
    )

    right = models.ForeignKey(
        Topic,
        on_delete=models.CASCADE,
        help_text="The left side of the relation",
        related_name="relation_right",
    )

    @property
    def relation_type_obj(self) -> DataCiteRelationType:
        """Get the relation type object."""
        return DataCiteRelationType(self.relation_type)

    def get_absolute_url(self):
        return self.left.get_absolute_url()

    def __str__(self) -> str:
        return "{left} {relation} {right}".format(
            left=self.left.id_name,
            relation=self.relation_type_obj.label,
            right=self.right.id_name,
        )


@MentionLink.registry.register_autocreation("topics.Topic")
@MentionLink.registry.register_for_query
class TopicMentionLink(MentionLink):
    """A mention of a topic."""

    class Meta:
        verbose_name = _("%(topic)s Mention Link") % {"topic": _("Topic")}
        verbose_name_plural = _("%(topic)s Mention Links") % {
            "topic": _("Topic")
        }

    related_model: Topic = models.OneToOneField(  # type: ignore
        Topic, on_delete=models.CASCADE
    )

    badge_classes = ["topic-badge"]

    @property
    def subscribers(self) -> models.QuerySet[User]:
        topic = self.related_model
        values = topic.topicmembership_set.all_final().values_list(  # type: ignore
            "member__user", flat=True
        )
        if values:
            return User.objects.filter(pk__in=values)
        return User.objects.none()

    @classmethod
    def query_for_autocomplete(
        cls,
        queryset: models.QuerySet[MentionLink],
        query: str,
        user: User,
        prefix: str = "",
        query_name: str = "default",
    ) -> Tuple[models.QuerySet[MentionLink], Optional[models.Q]]:
        if not user.is_staff:
            if prefix:
                return queryset.filter(**{prefix + "isnull": True}), None
            else:
                return queryset.none(), None
        queryset = queryset.annotate(
            topic_id_name_clean=models.functions.Replace(  # type: ignore
                prefix + "related_model__id_name",
                models.Value("-"),
                models.Value(""),
            ),
            topic_name_clean=models.functions.Replace(  # type: ignore
                prefix + "related_model__name",
                models.Value("-"),
                models.Value(""),
            ),
        )
        q = models.Q(topic_id_name_clean__istartswith=query) | models.Q(
            topic_name_clean__icontains=query
        )
        return (queryset, q)

    @property
    def name(self) -> str:
        return self.related_model.id_name

    @property
    def subtitle(self) -> str:
        return self.related_model.name


class TopicsFilterButtonPluginModel(  # type: ignore[misc]
    BaseFilterPluginModelMixin, FilterButtonPluginModelMixin, CMSPlugin
):
    """A CMS plugin model for rendering a button to open a filter form."""

    app_config_from_request = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=(
            "This option allows to infer the namespace for the "
            "%(topics)s from the URL, so you do not have to specify it manually."
        ),
        verbose_name="Infer namespace from URL location",
    )

    app_config = AppHookConfigField(
        TopicsConfig,
        verbose_name=_("%(topics)s Namespace") % {"topics": _("Topics")},
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("The namespace whose %(topics)s to display.")
        % {"topics": _("topics")},
    )


class TopicsActiveFiltersPluginModel(  # type: ignore[misc]
    BaseFilterPluginModelMixin, ActiveFiltersPluginModelMixin, CMSPlugin
):
    """A CMS plugin model for rendering active filters for topics."""

    app_config_from_request = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=(
            "This option allows to infer the namespace for the "
            "%(topics)s from the URL, so you do not have to specify it manually."
        ),
        verbose_name="Infer namespace from URL location",
    )

    app_config = AppHookConfigField(
        TopicsConfig,
        verbose_name=_("%(topics)s Namespace") % {"topics": _("Topics")},
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("The namespace whose %(topics)s to display.")
        % {"topics": _("topics")},
    )


class AbstractTopicListPluginModel(  # type: ignore[django-manager-missing]
    FilterPluginModelMixin, AbstractPaginationPluginModelMixin, CMSPlugin
):
    """An abstract base model to display a member list."""

    class Meta:
        abstract = True

    check_permissions = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=_(
            "Only display the %(topics)s that the website visitor can view."
        )
        % {"topics": _("topics")},
    )

    active_topics = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=_("Display active %(topics)s.") % {"topics": _("topics")},
    )

    finished_topics = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=_("Display finished %(topics)s.") % {"topics": _("topics")},
    )

    show_institution_logos = models.BooleanField(  # type: ignore[var-annotated]
        default=False, help_text="Show the logo of the institutions."
    )

    app_config_from_request = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=(
            "This option allows to infer the namespace for the "
            "%(topics)s from the URL, so you do not have to specify it manually."
        ),
        verbose_name="Infer namespace from URL location",
    )

    app_config = AppHookConfigField(
        TopicsConfig,
        verbose_name=_("%(topics)s Namespace") % {"topics": _("Topics")},
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("The namespace whose %(topics)s to display.")
        % {"topics": _("topics")},
    )


class TopicListPluginModel(AbstractTopicListPluginModel):  # type: ignore[django-manager-missing]
    """A plugin model to display community members."""

    topics = models.ManyToManyField(  # type: ignore[var-annotated]
        Topic,
        blank=True,
        help_text=_("The %(topics)s to display.") % {"topics": _("topics")},
    )

    show_all = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=_("Show all %(topics)s, no matter what you selected above.")
        % {"topics": _("topics")},
    )

    def get_topics(self, context) -> models.QuerySet[Topic]:
        return self.topics.all()

    def copy_relations(self, oldinstance):
        self.topics.clear()
        self.topics.add(*oldinstance.topics.all())


class TopicMembersListPluginModel(AbstractCommunityMemberListPluginModel):  # type: ignore[django-manager-missing]
    """A plugin model to display the members of a working/project group."""

    topic = models.ForeignKey(  # type: ignore[var-annotated]
        Topic,
        null=True,
        blank=True,
        help_text="The topic whose members to show.",
        on_delete=models.SET_NULL,
    )

    active_members = models.BooleanField(  # type: ignore[var-annotated]
        default=True, help_text="Display members with active membership."
    )

    former_members = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Display members whose memberships have finished.",
    )

    from_url = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=(
            "Get the activity from the URL rather than from the activity "
            "field. Note that this only works for activity detail pages."
        ),
    )


class CreateTopicButtonPluginModel(CMSPlugin):
    """A plugin model to add a button to create a new topic."""

    button_text = models.CharField(  # type: ignore[var-annotated]
        null=True,
        blank=True,
        max_length=50,
        help_text="The text to show on the button.",
    )

    button_attributes = AttributesField(  # type: ignore[var-annotated]
        help_text="Additional attributes for the button",
        blank=True,
        verbose_name="Button attributes",
        default={"class": "btn btn-primary btn-round btn-create"},
    )

    app_config = AppHookConfigField(  # type: ignore[var-annotated]
        TopicsConfig,
        on_delete=models.CASCADE,
        default=get_first_topicsconfig,
        help_text="The namespace to register the topic for",
    )

    activities = models.ManyToManyField(  # type: ignore[var-annotated]
        Activity,
        blank=True,
        help_text=_(
            "Preselect %(working_groups)s that the topics created with this "
            "should relate to."
        )
        % {"working_groups": _("Working Groups")},
    )

    keywords = models.ManyToManyField(  # type: ignore[var-annotated]
        Keyword,
        help_text=_("Preselect keywords for the new %(topics)s.")
        % {"topics": _("Topics")},
        blank=True,
    )

    user_view_permission = models.ManyToManyField(  # type: ignore[var-annotated]
        settings.AUTH_USER_MODEL,
        help_text=_("Preselect the users that can view the new %(topic)s.")
        % {"topic": _("topic")},
        blank=True,
    )

    group_view_permission = models.ManyToManyField(  # type: ignore[var-annotated]
        Group,
        help_text=_("Preselect what groups can view the new %(topic)s.")
        % {"topic": _("topic")},
        blank=True,
    )

    def copy_relations(self, oldinstance):
        self.activities.clear()
        self.activities.add(*oldinstance.activities.all())
        self.keywords.clear()
        self.keywords.add(*oldinstance.keywords.all())
        self.user_view_permission.clear()
        self.user_view_permission.add(*oldinstance.user_view_permission.all())
        self.group_view_permission.clear()
        self.group_view_permission.add(
            *oldinstance.group_view_permission.all()
        )


class ExportTopicListButtonPluginModel(
    AbstractExportListButtonPluginModelMixin, CMSPlugin
):
    """A plugin for a button to export multiple topics"""

    app_config_from_request = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=(
            "This option allows to infer the namespace for the "
            "%(topics)s from the URL, so you do not have to specify it manually."
        ),
        verbose_name="Infer namespace from URL location",
    )

    app_config = AppHookConfigField(  # type: ignore[var-annotated]
        TopicsConfig,
        verbose_name=_("%(topics)s Namespace") % {"topics": _("Topics")},
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("The namespace whose %(topics)s to display.")
        % {"topics": _("topics")},
    )


@receiver(pre_save, sender=Topic)
def remove_styles(instance: Topic, **kwargs):
    """Remove style attributes from description and publications."""
    instance.description = utils.remove_style(instance.description)


@receiver(post_save, sender=Topic)
def update_topic_leader_permissions(sender, **kwargs):
    """Assign change_topic permission to the topic leader."""
    topic = kwargs["instance"]
    versions = Version.objects.get_for_object(topic)

    if versions:
        last_leader_id = versions[0].field_dict["leader_id"]
        if last_leader_id != topic.leader.id:
            last_leader = CommunityMember.objects.get(pk=last_leader_id)
            topic.update_permissions(last_leader)
            topic.update_permissions(topic.leader)
        last_lead_organization_id = versions[0].field_dict[
            "lead_organization_id"
        ]
        if last_lead_organization_id != topic.lead_organization.id:
            organizations = [
                topic.lead_organization
            ] + topic.lead_organization.parent_organizations
            for organization in organizations:
                if organization.contact:
                    topic.update_permissions(organization.contact)
            try:
                last_lead_organization = AcademicOrganization.objects.get(
                    pk=last_lead_organization_id
                ).organization
            except AcademicOrganization.DoesNotExist:
                pass
            else:
                organizations = [
                    last_lead_organization
                ] + last_lead_organization.parent_organizations
                for organization in organizations:
                    if organization.contact:
                        topic.update_permissions(organization.contact)
    else:
        # new topic
        topic.update_permissions(topic.leader)
        for contact in topic.lead_organization_contacts:
            topic.update_permissions(contact)


@receiver(post_save, sender=Topic)
def finish_topic_memberships(sender, **kwargs):
    """Finish all topic memberships when the topic is finished."""
    topic = kwargs["instance"]

    if not topic.end_date:
        return

    # check if it has been closed already and if yes, return
    versions = Version.objects.get_for_object(topic)
    if versions:
        end_date = versions[0].field_dict["end_date"]
        if end_date:
            return

    # close all topic memberships
    for ms in topic.topicmembership_set.all():
        if not ms.end_date:
            ms.end_date = topic.end_date
            ms.save()
        ms.reset_permissions()


@receiver(post_save, sender=Topic)
def restart_topic_memberships(sender, **kwargs):
    """Restart all topic memberships when the topic is activated again.

    Basically undoes :func:`finish_topic_memberships` but does not remove the
    end date of the topic memberships, rather allows to edit them again.
    """
    topic = kwargs["instance"]

    if topic.end_date:
        return

    # check if it been open already and if yes, return
    versions = Version.objects.get_for_object(topic)
    if versions:
        end_date = versions[0].field_dict["end_date"]
        if not end_date:
            return

    # close all topic memberships
    for ms in topic.topicmembership_set.all():
        ms.reset_permissions()


NEW_TOPIC_MEMBERSHIP_REQUEST_HEADER = _("New %(topic)s membership request") % {
    "topic": "topic"
}


@receiver(post_save, sender=TopicMembership)
def update_topic_member_permissions(
    instance: TopicMembership, created: bool, **kwargs
):
    """Assign change_topic permission to the topic leader."""
    from academic_community.notifications.models import SystemNotification

    topic = instance.topic
    member = instance.member

    topic.update_permissions(member)

    if created:
        instance.update_permissions(topic.leader)
        for contact in topic.lead_organization.organization_contacts:
            instance.update_permissions(contact)

    if created and not instance.approved and member.approved_by:
        # topic membership has just been requested
        leader = topic.leader
        args = (
            NEW_TOPIC_MEMBERSHIP_REQUEST_HEADER,
            "topics/topicmembership_email.html",
            {
                "topic": topic,
                "communitymember": member,
                "topicmembership": instance,
            },
        )
        if leader.user:
            SystemNotification.create_notifications([leader.user], *args)
        else:
            SystemNotification.create_notifications_for_managers(*args)

    # check for channel subscriptions
    versions = Version.objects.get_for_object(instance)
    latest_version = versions.first()
    user = member.user
    if latest_version:
        props = latest_version.field_dict
        latest_is_final = props["approved"] and (
            props["end_date"] is None
            or (
                instance.topic.end_date
                and props["end_date"] > instance.topic.end_date
            )
        )
    else:
        latest_is_final = False
    if user and instance.is_final and (created or not latest_is_final):
        # membership started, so add channel subscriptions
        # add channel subscriptions
        new_subscriptions = []
        for channel_relation in instance.topic.topicchannelrelation_set.filter(
            symbolic_relation=False
        ):
            channel_relation.channel.update_user_permissions(user)
            if channel_relation.subscribe_members:
                (
                    subscription,
                    is_new,
                ) = channel_relation.get_or_create_subscription(user)
                if is_new:
                    new_subscriptions.append(subscription)
        for (
            material_relation
        ) in instance.topic.topicmaterialrelation_set.filter(
            symbolic_relation=False
        ):
            material_relation.material.update_user_permissions(user)
        if new_subscriptions:
            ChannelSubscription.create_aggregated_subscription_notifications(
                new_subscriptions,
                f"New channel subscriptions for {topic}",
                reason=f"you joined {topic}",
            )
    elif (
        user and not instance.is_final and (latest_version and latest_is_final)
    ):
        # membership ended, so remove channel subscriptions and permissions
        removed_subscriptions: List[ChannelSubscription] = []
        for relation in topic.topicchannelrelation_set.filter(
            symbolic_relation=False
        ):
            perms = relation.channel.update_user_permissions(user)
            if relation.subscribe_members and "view_channel" not in perms:
                removed_subscription = relation.remove_subscription(user)
                if removed_subscription:
                    removed_subscriptions.append(removed_subscription)
        if removed_subscriptions:
            ChannelSubscription.create_aggregated_subscription_removal_notifications(
                removed_subscriptions,
                f"Channel subscriptions removed for {topic}",
                reason=f"your membership in {topic} ended",
            )


@receiver(pre_save, sender=Topic)
def set_slug(instance: Topic, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.id_name)


@receiver(post_delete, sender=Keyword)
def remove_topic_relations(sender, instance: Keyword, **kwargs):
    """Remove the activity from related topics."""
    for topic in Topic.objects.filter(
        metadata__keywords__p_keys__contains=instance.pk
    ):
        keys = topic.metadata["keywords"]["p_keys"]
        topic.metadata["keywords"]["p_keys"] = [
            key for key in keys if key != instance.pk
        ]
        topic.save()


@receiver(post_delete, sender=TopicMembership)
def remove_topic_member_permissions(instance: TopicMembership, **kwargs):
    """Remove the permissions of the member if it has been deleted."""

    topic = instance.topic
    member = instance.member
    topic.update_permissions(member)

    if member.user:
        # remove channel subscriptions
        user: User = member.user
        removed_subscriptions: List[ChannelSubscription] = []
        for relation in topic.topicchannelrelation_set.filter(
            symbolic_relation=False
        ):
            perms = relation.channel.update_user_permissions(user)
            if relation.subscribe_members and "view_channel" not in perms:
                removed_subscription = relation.remove_subscription(user)
                if removed_subscription:
                    removed_subscriptions.append(removed_subscription)
        if removed_subscriptions:
            ChannelSubscription.create_aggregated_subscription_removal_notifications(
                removed_subscriptions,
                f"Channel subscriptions removed for {topic}",
                reason="you left {topic}",
            )


@receiver(post_save, sender=TopicChannelRelation)
def create_channel_subscriptions(
    instance: TopicChannelRelation, created: bool, **kwargs
):
    """Create the channel subscriptions for the activity members."""
    channel = instance.channel
    topic: Topic = instance.topic
    users = topic.topic_users
    contact_users = topic.lead_organization_contact_users
    for user in users:
        perms = channel.update_user_permissions(user)
        if "view_channel" in perms and instance.subscribe_members:
            subscription, created = instance.get_or_create_subscription(user)
            if created:
                subscription.create_subscription_notification(
                    reason="you participate in " + str(topic)
                )
        else:
            instance.remove_subscription_and_notify(user)
    for user in contact_users:
        perms = channel.update_user_permissions(user)
        if "view_channel" not in perms:
            instance.remove_subscription_and_notify(user)


@receiver(post_delete, sender=TopicChannelRelation)
def remove_channel_view_permission(instance: TopicChannelRelation, **kwargs):
    """Remove the permission to view a channel"""
    topic: Topic = instance.topic
    users = topic.topic_users
    contact_users = topic.lead_organization_contact_users
    for user in users + contact_users:
        perms = instance.channel.update_user_permissions(user)
        if "view_channel" not in perms:
            instance.remove_subscription_and_notify(user)


@receiver(post_delete, sender=TopicMaterialRelation)
@receiver(post_save, sender=TopicMaterialRelation)
def update_material_permission_on_relation(
    instance: TopicMaterialRelation, **kwargs
):
    """Update the user permissions for the topic members."""
    material = instance.material
    topic: Topic = instance.topic
    users = topic.topic_users
    contact_users = topic.lead_organization_contact_users
    for user in users + contact_users:
        material.update_user_permissions(user)


@receiver(m2m_changed, sender=Topic.user_view_permission.through)
def update_user_view_permission(
    instance: Topic, action: str, pk_set: list[int], **kwargs
):
    """Update the view permissions of the user."""

    if action == "post_add":
        utils.bulk_update_user_permissions(
            instance, action, "view_topic", pk_set
        )
    elif action in ["post_remove", "post_clear"]:
        # we need to update here carefully because permissions might have
        # been granted via lead institution contact or topic membership
        if action == "post_clear":
            ct = ContentType.objects.get_for_model(instance.__class__)
            perm = Permission.objects.get(
                codename="view_topic",
                content_type=ct,
            )

            kws = dict(
                content_type=ct,
                object_pk=instance.pk,
                permission=perm,
            )
            pk_set = list(
                UserObjectPermission.objects.filter(**kws).values_list(
                    "user__pk", flat=True
                )
            )
        for user in User.objects.filter(pk__in=pk_set):
            if hasattr(user, "communitymember"):
                instance.update_permissions(user.communitymember)
            else:
                remove_perm("view_topic", user, instance)


@receiver(
    m2m_changed,
    sender=Topic.group_view_permission.through,
)
def update_group_view_permission(
    instance: Topic, action: str, pk_set: list[int], **kwargs
):
    """Update the view permissions of the group."""

    if action in ["post_add", "post_remove", "post_clear"]:
        utils.bulk_update_group_permissions(
            instance, action, "view_topic", pk_set
        )
