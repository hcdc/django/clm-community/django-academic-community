# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App settings for the topics app
--------------------------------

This module defines the settings options for the ``topics`` app.
"""


from __future__ import annotations

from typing import Dict, Tuple, Union

from django.conf import settings  # noqa: F401

#: Import strings for functions to be used to generate ID names
#:
#: This setting can be used to add generators for topic ID names. It is a
#: mapping from a function identifier to an import string for the function.
#:
#: identifier
#:     the identifier can either be a string, or a 2-tuple of strings. In case
#:     of a tuple, the first value is the key for the database, the second
#:     value is the label for the frontend. In case of a string, the string
#:     is the label in the database and the one for the frontend. One example
#:     (and the default) is ``("instutition-based", "Institution based")``.
#: import string
#:     The import string is a colon-separated string, first with the module
#:     name, then with the callable. The callable must accept one argument,
#:     namely the topic that is about to created. One example (and the default)
#:     is: ``"academic_community.topics.utils:get_institution_topic_id_name"``
ID_FUNCTIONS: Dict[Tuple[str, str], str] = {}

if hasattr(settings, "ID_FUNCTIONS"):
    orig_id_functions: Dict[Union[Tuple[str, str], str], str] = settings.ID_FUNCTIONS  # type: ignore
    for key, val in orig_id_functions.items():
        if isinstance(key, str):
            ID_FUNCTIONS[(key, key)] = orig_id_functions[key]
        else:
            ID_FUNCTIONS[key] = orig_id_functions[key]
else:
    ID_FUNCTIONS = {
        (
            "instutition-based",
            "Institution based",
        ): "academic_community.topics.id_names:get_institution_topic_id_name"
    }
