# SPDX-FileCopyrightText: 2020-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
from __future__ import annotations

import importlib
import re
from itertools import chain
from typing import TYPE_CHECKING, Callable

from academic_community.topics import app_settings

if TYPE_CHECKING:
    from .models import Topic


def get_institution_topic_id_name(topic: Topic) -> str:
    """Get a topic ID name based upon the lead institution and a number."""
    lead_organization = topic.lead_organization.organization
    lead_institution = lead_organization.parent_institution
    existing_id_names = lead_institution.lead_topics.filter(
        app_config=topic.app_config
    ).values_list("id_name", flat=True)
    regex = re.compile(r"\d+$")
    ids = list(chain.from_iterable(map(regex.findall, existing_id_names)))

    new_id = (max(map(int, ids)) if ids else 0) + 1
    return f"{lead_institution.abbreviation}-{new_id:03d}"


def get_topic_id_name_func(func_identifier: str) -> Callable[[Topic], str]:
    """Get the name for the function that generates the topic id name.

    Parameters
    ----------
    func_identifier : str
        The identifier for the function in
        :attr:`academic_community.topics.app_settings.ID_FUNCTIONS`

    Returns
    -------
    Callable[[Topic], str]
        The function to generate the ID name.
    """
    import_str = next(
        val
        for (key, label), val in app_settings.ID_FUNCTIONS.items()
        if key == func_identifier
    )
    mod_name, func = import_str.split(":")
    mod = importlib.import_module(mod_name)
    return getattr(mod, func)
