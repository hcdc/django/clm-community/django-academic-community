# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Serializers for the topics app."""

from rest_framework import fields, serializers

from academic_community import utils
from academic_community.activities.models import Activity
from academic_community.institutions.models import (
    AcademicMembership,
    AcademicOrganization,
)
from academic_community.members.models import CommunityMember
from academic_community.topics import models


class CommunityMemberSerializerBase(serializers.ModelSerializer):
    """A serializer for a community member"""

    class Meta:
        model = CommunityMember
        read_only = True
        fields = ["first_name", "last_name", "orcid", "email"]

    def to_representation(self, instance: CommunityMember):
        ret = super().to_representation(instance)

        request = self.context.get("request")

        if request:
            user = request.user
            if not utils.has_perm(user, "members.view_contact_info", instance):
                ret.pop("email", None)
        else:
            ret.pop("email", None)
        return ret

    email = fields.EmailField(source="default_email.email", default=None)


class AcademicOrganizationSerializerBase(serializers.ModelSerializer):
    """A serializer for an academic organization."""

    class Meta:
        model = AcademicOrganization
        read_only = True
        fields = ["name", "abbreviation", "display_name", "contact", "website"]

    display_name = fields.CharField(source="__str__")

    abbreviation = fields.CharField(source="organization.abbreviation")

    contact = CommunityMemberSerializerBase()


class AcademicOrganizationSerializer(AcademicOrganizationSerializerBase):
    """A serializer for an academic (sub-) organization"""

    class Meta(AcademicOrganizationSerializerBase.Meta):
        fields = AcademicOrganizationSerializerBase.Meta.fields + [
            "parent_institution"
        ]

    parent_institution = AcademicOrganizationSerializerBase(
        source="organization.parent_institution", default=None
    )


class AcademicMembershipSerializer(serializers.ModelSerializer):
    """A serializer for an academic membership."""

    class Meta:
        model = AcademicMembership
        fields = ["start_date", "end_date", "organization"]

    organization = AcademicOrganizationSerializer()


class CommunityMemberSerializer(CommunityMemberSerializerBase):
    """A serializer for a community member including affilations."""

    class Meta(CommunityMemberSerializerBase.Meta):
        fields = CommunityMemberSerializerBase.Meta.fields + ["affiliations"]

    affiliations = AcademicMembershipSerializer(
        source="academicmembership_set", many=True
    )


class TopicMembershipSerializer(serializers.ModelSerializer):
    """A serializer for a topic membership."""

    class Meta:
        model = models.TopicMembership
        read_only = True
        fields = ["member", "institutions", "start_date", "end_date"]

    member = CommunityMemberSerializer()

    institutions = AcademicOrganizationSerializerBase(many=True)


class ActivitySerializer(serializers.ModelSerializer):
    """A serializer for an activity."""

    class Meta:
        model = Activity
        read_only = True
        fields = [
            "name",
            "abbreviation",
            "url",
            "leaders",
            "start_date",
            "end_date",
            "display_name",
        ]

    leaders = CommunityMemberSerializer(many=True)

    display_name = fields.CharField(source="__str__")

    url = fields.URLField(source="get_absolute_url")


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Topic
        read_only = True
        fields = [
            "name",
            "id_name",
            "url",
            "description",
            "start_date",
            "end_date",
            "last_modification_date",
            "leader",
            "lead_organization",
            "memberships",
            "institutions",
            "activities",
            "keywords",
        ]

    leader = CommunityMemberSerializer()

    url = fields.CharField(source="get_absolute_url")

    lead_organization = AcademicOrganizationSerializer()

    memberships = TopicMembershipSerializer(
        source="approved_memberships", many=True
    )

    institutions = AcademicOrganizationSerializer(many=True)

    activities = serializers.StringRelatedField(many=True)

    keywords = serializers.StringRelatedField(many=True)
