# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from aldryn_apphooks_config.app_base import CMSConfigApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import gettext as _

from academic_community.topics.cms_appconfig import TopicsConfig


@apphook_pool.register
class TopicsApphook(CMSConfigApp):
    app_name = "topics"
    name = _("Topics")
    app_config = TopicsConfig

    def get_urls(self, page=None, language=None, **kwargs):
        from django.urls import include, path, re_path

        from academic_community.topics import views

        if page is not None and page.application_namespace:
            namespace = page.application_namespace
        else:
            namespace = None

        return [
            path("new/", views.TopicCreateView.as_view(), name="topic-create"),
            path(
                "export/",
                views.registry.get_export_list_view(namespace).as_view(),
                name="topic-rest-list",
            ),
            path(
                "<slug>/", views.TopicDetailView.as_view(), name="topic-detail"
            ),
            path(
                "<slug>/export/",
                views.registry.get_export_view(namespace).as_view(),
                name="topic-rest-detail",
            ),
            path(
                "<slug>/uploads/",
                include(views.TopicMaterialRelationViewSet().urls),
            ),
            path(
                "<slug>/channels/",
                include(views.TopicChannelRelationViewSet().urls),
            ),
            path(
                "<topic_slug>/signup/",
                views.TopicMembershipCreate.as_view(),
                name="topicmembership-request",
            ),
            path(
                "<slug>/edit/",
                views.TopicUpdateView.as_view(),
                name="edit-topic",
            ),
            re_path(
                r"(?P<pk>\d+)/edit/field/(?:(?P<language>en)/)?",
                views.TopicFieldUpdate.as_view(),
                name="edit-topic-field",
            ),
            path(
                "<slug>/clone/",
                views.TopicCloneView.as_view(),
                name="clone-topic",
            ),
            path(
                "<slug>/edit/members/",
                views.TopicMembershipFormsetView.as_view(),
                name="topicmembership-formset",
            ),
            path(
                "<slug>/edit/members/<pk>/approve/",
                views.TopicMembershipApproveView.as_view(),
                name="topicmembership-approve",
            ),
            path(
                "<slug>/history/",
                views.TopicRevisionList.as_view(),
                name="topic-history",
            ),
        ]
