# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App Config for the activities app
"""

from dataclasses import dataclass
from typing import TYPE_CHECKING

from aldryn_apphooks_config.models import AppHookConfig
from aldryn_apphooks_config.utils import setup_config
from app_data import AppDataForm
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.utils.functional import cached_property
from django.utils.translation import gettext as _

from academic_community import utils
from academic_community.topics import app_settings

if TYPE_CHECKING:
    from django.contrib.auth.models import User

User = get_user_model()  # type: ignore # noqa: F811


@dataclass
class TopicLabels:
    """Labels to use for a topic."""

    label: str
    label_lower: str
    label_plural: str
    label_plural_lower: str


class TopicsConfig(AppHookConfig):
    class Meta:
        permissions = (
            (
                "add_topic_for_namespace",
                "Can add a new topic under the namespace",
            ),
        )

    id_name_function = models.CharField(  # type: ignore[var-annotated]
        choices=[(None, "Let the user decide")]
        + list(app_settings.ID_FUNCTIONS),
        null=True,
        blank=True,
        help_text=_(
            "How shall we generate the name for new %(topics)s in this namespace?"
        )
        % {"topics": _("topics")},
        max_length=200,
    )

    user_topic_add_permission = models.ManyToManyField(  # type: ignore[var-annotated]
        User,
        blank=True,
        help_text=_(
            "Explicit users with permission to add new %(topics)s under this "
            "namespace."
        )
        % {"topics": _("topics")},
        related_name="topicsconfig_add_topic_permission",
    )

    group_topic_add_permission = models.ManyToManyField(  # type: ignore[var-annotated]
        Group,
        help_text=_(
            "Groups with permission to add new %(topics)s under this namespace."
        )
        % {"topics": _("topics")},
        blank=True,
        related_name="topicsconfig_add_topic_permission",
    )

    @cached_property
    def labels(self) -> TopicLabels:
        """The labels that shall be used for topics in this namespace."""
        label = self.app_data.config.label
        label_lower = self.app_data.config.label_lower or label.lower()
        label_plural = self.app_data.config.label_plural or (label + "s")
        label_plural_lower = (
            self.app_data.config.label_plural_lower or label_plural.lower()
        )
        return TopicLabels(
            label, label_lower, label_plural, label_plural_lower
        )


class TopicsConfigForm(AppDataForm):
    label = forms.CharField(
        help_text=(
            "The label that we shall use for topics under this namespace"
        ),
        initial="Topic",
        max_length=50,
    )

    label_lower = forms.CharField(
        required=False,
        label="Label in lowercase",
        help_text=(
            "The label in lowercase. If you leave this empty, we will just "
            "use the lower case representation of the <i>label</i> field."
        ),
        max_length=50,
    )

    label_plural = forms.CharField(
        required=False,
        label="Label in plural",
        help_text=(
            "If you leave this empty, we will append an <i>s</i> to the "
            "<i>label</i>."
        ),
        max_length=53,
    )

    label_plural_lower = forms.CharField(
        required=False,
        label="Label in plural and lowercase",
        help_text=(
            "If you leave this empty, we will use the lower case "
            "representation of the <i>Label in plural</i> field."
        ),
        max_length=53,
    )


setup_config(TopicsConfigForm, TopicsConfig)


@receiver(m2m_changed, sender=TopicsConfig.user_topic_add_permission.through)
def update_user_topic_add_permission(
    instance: TopicsConfig,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove view permission for users."""

    if action not in ["post_add", "post_remove", "post_clear"]:
        return

    codename = "add_topic_for_namespace"
    utils.bulk_update_user_permissions(instance, action, codename, pk_set)


@receiver(m2m_changed, sender=TopicsConfig.group_topic_add_permission.through)
def update_group_topic_add_permission(
    instance: TopicsConfig,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove view permission for users."""

    if action not in ["post_add", "post_remove", "post_clear"]:
        return

    codename = "add_topic_for_namespace"
    utils.bulk_update_group_permissions(instance, action, codename, pk_set)
