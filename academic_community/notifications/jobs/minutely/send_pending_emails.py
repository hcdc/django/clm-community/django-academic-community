"""Send pending emails to the users.

Different from pending notifications, pending emails are supposed to be sent
immediately.
"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "Send pending notifications to the users"

    def execute(self):
        from academic_community.notifications import models

        qs = models.PendingEmail.objects.filter(sent=False)
        mails = list(qs)  # load into memory to avoid sending mails twice
        qs.update(sent=True)
        for email in mails:
            try:
                email.send_mail()
            except Exception:
                email.attempts += 1
                email.sent = False
                email.save()
            else:
                email.delete()
