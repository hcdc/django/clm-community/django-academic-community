"""Send pending notifications to the users."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "Send pending notifications to the users"

    def execute(self):
        from academic_community.notifications import models

        for settings in models.SystemNotificationSettings.objects.filter(
            receive_mails=True, collate_mails=True
        ):
            settings.send_pending_notifications()
        for settings in models.UserNotificationSettings.objects.filter(
            receive_mails=True, collate_mails=True
        ):
            settings.send_pending_notifications()
        for settings in models.ChannelNotificationSettings.objects.filter(
            receive_mails=True, collate_mails=True
        ):
            settings.send_pending_notifications()
        for settings in models.ChatNotificationSettings.objects.filter(
            receive_mails=True, collate_mails=True
        ):
            settings.send_pending_notifications()
