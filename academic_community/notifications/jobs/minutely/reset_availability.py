"""Update session availabilities."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from asgiref.sync import async_to_sync
from django.utils.timezone import now
from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "Update the session availabilities of the users."

    def execute(self):
        from academic_community.channels.models import ChatSettings
        from academic_community.channels.serializers import (
            AvailabilitySerializer,
        )
        from academic_community.utils import get_connected_users
        from channels.layers import get_channel_layer

        updated_settings = list(
            ChatSettings.objects.filter(availability_reset_date__lte=now())
        )

        for settings in updated_settings:
            settings.availability = settings.availability_reset_value
            settings.availability_reset_date = None
            settings.save()

        if updated_settings:
            body = {
                "type": "availabilities",
                "users": AvailabilitySerializer(
                    updated_settings, many=True
                ).data,
            }

            online_users = get_connected_users()

            channel_layer = get_channel_layer()

            for user_pk in online_users.values_list("pk", flat=True):
                async_to_sync(channel_layer.group_send)(
                    f"notifications_{user_pk}", body
                )
