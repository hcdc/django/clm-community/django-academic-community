"""Urls of the :mod:`notifications` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from academic_community.notifications import views

app_name = "notifications"

urlpatterns = [
    path(
        "",
        views.NotificationListView.as_view(),
        name="inbox",
    ),
    path(
        "system/",
        views.SystemNotificationListView.as_view(),
        name="inbox-system",
    ),
    path(
        "user/",
        views.UserNotificationListView.as_view(),
        name="inbox-user",
    ),
    path(
        "chats/",
        views.ChatNotificationListView.as_view(),
        name="inbox-chat",
    ),
    path(
        "archive/",
        views.ArchivedNotificationListView.as_view(),
        name="inbox-archived",
    ),
    path(
        "new/",
        views.CreateOutgoingNotificationView.as_view(),
        name="create-notification",
    ),
    path(
        "settings/",
        views.NotificationSettingsUpdateView.as_view(),
        name="settings",
    ),
    path(
        "settings/system/",
        views.SystemNotificationSettingsUpdateView.as_view(),
        name="settings-system",
    ),
    path(
        "settings/user/",
        views.UserNotificationSettingsUpdateView.as_view(),
        name="settings-user",
    ),
    path(
        "settings/chat/",
        views.ChatNotificationSettingsUpdateView.as_view(),
        name="settings-channels",
    ),
    path(
        "settings/chat/<int:channel_id>/",
        views.ChannelNotificationSettingsUpdateView.as_view(),
        name="settings-channel",
    ),
    path(
        "settings/chat/<int:channel_id>/reset/",
        views.ChannelNotificationSettingsDeleteView.as_view(),
        name="settings-channel-delete",
    ),
    path(
        "webpush/<uuid:uuid>/<int:notification_id>/<token>/",
        csrf_exempt(views.NotificationSubscriptionActionView.as_view()),
        name="subscription-push",
    ),
    path(
        "<int:pk>/",
        views.NotificationDetailView.as_view(),
        name="notification-detail",
    ),
    path(
        "<int:pk>/send/",
        views.SendNotificationView.as_view(),
        name="send-notification",
    ),
]
