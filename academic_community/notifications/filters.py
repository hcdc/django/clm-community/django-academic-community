"""Filter sets for notifications."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING, Dict, Optional, Sequence

import django_filters
from django.contrib.auth import get_user_model
from django.contrib.postgres.search import SearchVector
from django.db.models import Exists, OuterRef, Q, QuerySet

from academic_community.events.models import Event
from academic_community.events.programme.models import (
    ContributingAuthor,
    PresentationType,
    Session,
)
from academic_community.events.registrations.models import Registration
from academic_community.filters import ActiveFilterSet
from academic_community.forms import FilteredSelectMultiple
from academic_community.institutions.models import (
    AcademicMembership,
    Institution,
)

if TYPE_CHECKING:
    from django.contrib.auth.models import User


class CommunityMemberNotificationFilterSet(ActiveFilterSet):
    """A filterset for community members to create outgoing notifications."""

    class Meta:
        model = get_user_model()
        fields = {
            "username": ["icontains"],
        }

    name = django_filters.CharFilter(
        method="filter_name",
        field_name="username",
        label="Communitymember name",
    )

    event = django_filters.ModelChoiceFilter(
        queryset=Event.objects.all(),
        method="filter_event",
        field_name="username",
        label="By being involved in event",
    )

    session = django_filters.ModelChoiceFilter(
        queryset=Session.objects.all(),
        method="filter_session",
        field_name="username",
        label="By contribution in event session",
    )

    institutions = django_filters.ModelMultipleChoiceFilter(
        queryset=Institution.objects.all(),
        method="filter_institutions",
        widget=FilteredSelectMultiple("Institution"),
        field_name="username",
        label="By institution",
    )

    institution_contact = django_filters.BooleanFilter(
        method="filter_institution_contact",
        field_name="username",
        label="Being contact person of an institution",
        help_text=(
            "Filter for contact persons of the institution (you need to "
            "select some institutions from above)."
        ),
    )

    active_institution = django_filters.ChoiceFilter(
        method="filter_active_institution",
        field_name="username",
        label="Members of active or inactive institutions",
        empty_label="Active and inactive institutions",
        null_value="",
        choices=[
            (True, "only active institutions"),
            (False, "only inactive institutions"),
        ],
        help_text=(
            "Filter for people in active or inactive institutions (you need "
            "to select some institutions from above)."
        ),
    )

    registration = django_filters.BooleanFilter(
        method="filter_registrations",
        field_name="username",
        label="By being registered at the event",
    )

    orga_team = django_filters.BooleanFilter(
        method="filter_orga_team",
        field_name="username",
        label="By being a member of the event organization team",
    )

    presenter = django_filters.BooleanFilter(
        method="filter_presenter",
        field_name="username",
        label="Presenting at event (or session, if selected)",
    )

    presentation_type = django_filters.ModelChoiceFilter(
        queryset=PresentationType.objects.filter(for_contributions=True),
        method="filter_presentationtype",
        field_name="username",
        label="By presentation type of the contribution",
    )

    coauthor = django_filters.BooleanFilter(
        method="filter_coauthors",
        field_name="username",
        label="Co-author in event (or session, if selected)",
    )

    convener = django_filters.BooleanFilter(
        method="filter_conveners",
        field_name="username",
        label="Session convener at event (or session, if selected)",
    )

    def __init__(
        self,
        *args,
        field_querysets: Dict[str, QuerySet] = {},
        event: Optional[Event] = None,
        session: Optional[Session] = None,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        for key, qs in field_querysets.items():
            self.filters[key].queryset = qs
        if session is not None:
            del self.filters["session"]
            event = session.event
            self.__session = session
        if event is not None:
            del self.filters["event"]
            self.__event = event

    def is_valid(self):
        return super().is_valid() & self._check_event_filters()

    def _check_event_filters(self) -> bool:
        if not getattr(self.form, "cleaned_data", None):
            return True
        elif "event" not in self.filters:
            # event has been pre-selected by the view already
            return True
        bool_fields = [
            "registration",
            "orga_team",
            "presenter",
            "coauthor",
            "convener",
        ]
        select_fields = ["event", "session"]
        data = self.form.cleaned_data
        if any(data.get(f) is not None for f in bool_fields) and not any(
            map(data.get, select_fields)
        ):
            return False
        return True

    @property
    def _event(self) -> Optional[Event]:
        try:
            return self.__event
        except AttributeError:
            return self.form.cleaned_data.get("event")

    @property
    def _session(self) -> Optional[Session]:
        try:
            return self.__session
        except AttributeError:
            return self.form.cleaned_data.get("session")

    def filter_name(
        self, queryset: QuerySet[User], name, value: Optional[str]
    ):
        if value is None:
            return queryset
        vector = SearchVector(
            "communitymember__first_name",
            "communitymember__last_name",
        )
        return queryset.annotate(search=vector).filter(search=value)

    def filter_event(
        self, queryset: QuerySet[User], name, value: Optional[Event]
    ):
        """Filter users by being involved in an event."""
        if value:
            query = (
                Q(groups=value.orga_group)
                | Q(communitymember__event_registration__event=value)
                | Q(
                    communitymember__author__contribution_map__contribution__event=value
                )
                | Q(communitymember__session__event=value)
            )
            return queryset.filter(query).distinct()
        return queryset

    def filter_session(
        self, queryset: QuerySet[User], name, value: Optional[Session]
    ):
        """Filter the users by session."""
        if value:
            query = (
                Q(communitymember__session=value)
                | Q(
                    communitymember__author__contribution_map__contribution__session=value
                )
                | Q(communitymember__slot__session=value)
            )
            return queryset.filter(query).distinct()
        return queryset

    def filter_institutions(
        self,
        queryset: QuerySet[User],
        name,
        value: Optional[Sequence[Institution]],
    ):
        """Filter users by being involved in an organization."""
        if value:
            if self.form.cleaned_data.get("active_institution") is not None:
                active_status = self.form.cleaned_data.get(
                    "active_institution"
                )
                if (
                    active_status
                    != self.filters["active_institution"].null_value
                ):
                    if active_status:
                        value = [
                            institution
                            for institution in value
                            if institution.end_date is None
                        ]
                    else:
                        value = [
                            institution
                            for institution in value
                            if institution.end_date is not None
                        ]
            pks = [institution.pk for institution in value]

            membership_query = AcademicMembership.objects.filter(
                Q(member__user__pk=OuterRef("pk"))
                & Q(end_date__isnull=True)
                & (
                    Q(organization__institution__pk__in=pks)
                    | Q(
                        organization__department__parent_institution__pk__in=pks
                    )
                    | Q(
                        organization__unit__parent_department__parent_institution__pk__in=pks
                    )
                )
            )

            annotated = queryset.annotate(
                membership_exists=Exists(membership_query)
            )

            return annotated.filter(
                Q(communitymember__organization_contact__pk__in=pks)
                | Q(membership_exists=True)
            ).distinct()
        return queryset

    def filter_institution_contact(
        self,
        queryset: QuerySet[User],
        name,
        value: Optional[bool],
    ):
        if value is None:
            return queryset
        institutions = self.form.cleaned_data["institutions"]
        if institutions:
            kwargs = {
                "pk__in": [institution.pk for institution in institutions]
            }
        else:
            kwargs = {}
        contacts = Institution.objects.filter(
            contact__user__pk=OuterRef("pk"), **kwargs
        )
        annotated = queryset.annotate(contact_query_exists=Exists(contacts))
        return annotated.filter(contact_query_exists=value).distinct()

    def filter_active_institution(
        self,
        queryset: QuerySet[User],
        name,
        value: Optional[bool],
    ):
        # dummy method as this filter is handled by :meth:`filter_institutions`
        return queryset

    def filter_presentationtype(
        self, queryset: QuerySet[User], name, value: Optional[PresentationType]
    ):
        """Filter for presentation type."""
        if value is None:
            return queryset
        if self._session:
            coauthors = ContributingAuthor.objects.filter(
                contribution__session=self._session,
                author__member__user=OuterRef("pk"),
                contribution__presentation_type=value,
            )
        else:
            coauthors = ContributingAuthor.objects.filter(
                contribution__event=self._event,
                author__member__user=OuterRef("pk"),
                contribution__presentation_type=value,
            )
        queryset = queryset.annotate(authorship_exists=Exists(coauthors))
        return queryset.filter(authorship_exists=True).distinct()

    def filter_registrations(
        self, queryset: QuerySet[User], name, value: Optional[bool]
    ):
        if value is None:
            return queryset
        if self._session:
            event = self._session.event
        else:
            event = self._event  # type: ignore

        registrations = Registration.objects.filter(
            event=event, member__user=OuterRef("pk")
        )
        annotated = queryset.annotate(
            registration_exists=Exists(registrations)
        )
        return annotated.filter(registration_exists=value).distinct()

    def filter_orga_team(
        self, queryset: QuerySet[User], name, value: Optional[bool]
    ):
        if value is None:
            return queryset
        if self._session:
            event = self._session.event
        else:
            event = self._event  # type: ignore

        if value:
            return queryset.filter(groups=event.orga_group)
        else:
            return queryset.filter(~Q(groups=event.orga_group))

    def filter_presenter(
        self, queryset: QuerySet[User], name, value: Optional[bool]
    ):
        if value is None:
            return queryset
        if self._session:
            presenters = ContributingAuthor.objects.filter(
                contribution__session=self._session,
                author__member__user=OuterRef("pk"),
                is_presenter=value,
            )
            annotated = queryset.annotate(
                presenter_query_exists=Exists(presenters)
            )
            ret = annotated.filter(
                presenter_query_exists=True,
            )

        else:
            presenters = ContributingAuthor.objects.filter(
                author__member__user=OuterRef("pk"),
                contribution__event=self._event,
                is_presenter=value,
            )
            annotated = queryset.annotate(
                presenter_query_exists=Exists(presenters)
            )
            ret = annotated.filter(
                presenter_query_exists=True,
            )

        return ret.distinct()

    def filter_coauthors(
        self, queryset: QuerySet[User], name, value: Optional[bool]
    ):
        if value is None:
            return queryset
        if self._session:
            coauthors = ContributingAuthor.objects.filter(
                contribution__session=self._session,
                author__member__user=OuterRef("pk"),
            )
        else:
            coauthors = ContributingAuthor.objects.filter(
                author__member__user=OuterRef("pk"),
                contribution__event=self._event,
            )
        queryset = queryset.annotate(authorship_exists=Exists(coauthors))
        return queryset.filter(authorship_exists=value).distinct()

    def filter_conveners(
        self, queryset: QuerySet[User], name, value: Optional[bool]
    ):
        if value is None:
            return queryset

        if self._session:
            if value:
                ret = queryset.filter(communitymember__session=self._session)
            else:
                sessions = Session.objects.filter(
                    pk=self._session.pk, conveners__user=OuterRef("pk")
                )
                ret = queryset.annotate(is_convener=Exists(sessions)).filter(
                    is_convener=value
                )
        else:
            sessions = Session.objects.filter(
                event=self._event, conveners__user=OuterRef("pk")
            )
            ret = queryset.annotate(is_convener=Exists(sessions)).filter(
                is_convener=value
            )

        return ret.distinct()
