# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
# SPDX-License-Identifier: EUPL-1.2

"""App config
----------

App config for the academic_community app.
"""

from django.apps import AppConfig


class AcademicCommunityConfig(AppConfig):
    name = "academic_community"
