"""Filters for revisions."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING

import django_filters
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from reversion import models

from academic_community.filters import ActiveFilterSet

if TYPE_CHECKING:
    from django.db.models import QuerySet


User = get_user_model()


class RevisionFilterSet(ActiveFilterSet):
    """A filterset for uploaded material."""

    class Meta:
        model = models.Revision
        fields = {
            "date_created": ["lte", "range", "gte"],
            "comment": ["icontains"],
        }

    user = django_filters.ModelChoiceFilter(
        queryset=User.objects.filter(revision__isnull=False).distinct()  # type: ignore[misc]
    )

    anonymous_user = django_filters.BooleanFilter(
        "user", label="By anonymous user", method="filter_anonymous"
    )

    revisionreview__reviewed = django_filters.BooleanFilter(
        field_name="revisionreview__reviewed", label="Reviewed"
    )

    revisionreview__reviewer = django_filters.ModelChoiceFilter(
        label="Reviewer",
        queryset=User.objects.filter(revisionreview__isnull=False).distinct(),  # type: ignore[misc]
    )

    version__content_type = django_filters.ModelChoiceFilter(
        label="Model",
        queryset=ContentType.objects.filter(version__isnull=False).distinct(),
        distinct=True,
    )

    def filter_anonymous(
        self, queryset: QuerySet[models.Revision], name, value
    ) -> QuerySet[models.Revision]:
        if value is None:
            return queryset
        return queryset.filter(user__isnull=value)
