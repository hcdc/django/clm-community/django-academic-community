"""Urls of the :mod:`institutions` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import path

from academic_community.history import views

app_name = "history"

urlpatterns = [
    path("", views.RevisionList.as_view(), name="revision-list"),
    path("<pk>/", views.RevisionDetail.as_view(), name="revision-detail"),
]
