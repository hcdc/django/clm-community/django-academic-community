# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.contrib import admin
from reversion import models

from academic_community.admin import ManagerAdminMixin


@admin.register(models.Version)
class VersionAdmin(ManagerAdminMixin, admin.ModelAdmin):
    """An admin for reversions Version."""

    list_filter = [
        "revision__revisionreview__reviewed",
        (
            "revision__revisionreview__reviewer",
            admin.RelatedOnlyFieldListFilter,
        ),
        ("content_type", admin.RelatedOnlyFieldListFilter),
    ]

    list_display = [
        "object_repr",
        "content_type",
        "date_created",
        "reviewer",
        "reviewed",
    ]

    def date_created(self, obj: models.Version):
        return obj.revision.date_created

    def reviewer(self, obj: models.Version) -> str:
        return str(obj.revision.revisionreview.reviewer)

    @admin.display(boolean=True)
    def reviewed(self, obj: models.Version) -> bool:
        return obj.revision.revisionreview.reviewed


@admin.register(models.Revision)
class RevisionAdmin(ManagerAdminMixin, admin.ModelAdmin):
    """An admin for reversions Revision."""

    list_display = [
        "display_name",
        "comment",
        "date_created",
        "reviewer",
        "reviewed",
    ]

    list_filter = [
        "date_created",
        ("revisionreview__reviewer", admin.RelatedOnlyFieldListFilter),
        "revisionreview__reviewed",
        ("version__content_type", admin.RelatedOnlyFieldListFilter),
    ]

    def reviewer(self, obj: models.Revision) -> str:
        return str(obj.revisionreview.reviewer)

    def display_name(self, obj: models.Revision) -> str:
        return str(obj)[:0]

    @admin.display(boolean=True)
    def reviewed(self, obj: models.Revision) -> bool:
        return obj.revisionreview.reviewed
