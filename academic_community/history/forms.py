"""Forms for the history app"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django import forms

from academic_community.history import models


class RevisionReviewForm(forms.ModelForm):
    """Form to select a review."""

    class Meta:
        model = models.RevisionReview

        fields = ["reviewed", "reviewer"]

        widgets = {
            "reviewed": forms.HiddenInput(),
            "reviewer": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.fields["reviewed"].disabled = True
        self.fields["reviewer"].disabled = True
