"""Utility functions for academic_community apps."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

import importlib
from itertools import chain, filterfalse
from pathlib import Path
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Optional,
    Sequence,
    Tuple,
    Type,
    Union,
)

import cssutils
from bs4 import BeautifulSoup
from cms.apphook_pool import apphook_pool
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core import exceptions, mail
from django.db.models import Exists, OuterRef
from django.forms.models import BaseInlineFormSet, BaseModelFormSet
from django.template.loader import render_to_string
from django.urls import Resolver404, resolve
from django.utils.html import strip_tags
from django.utils.module_loading import import_string
from django.utils.safestring import mark_safe
from django.utils.translation import get_language_from_request, override
from extra_views import InlineFormSetFactory
from guardian.mixins import (
    PermissionRequiredMixin as BasePermissionRequiredMixin,
)
from guardian.models import GroupObjectPermission, UserObjectPermission
from guardian.utils import get_anonymous_user

from academic_community import app_settings
from academic_community.context_processors import get_default_context

if TYPE_CHECKING:
    from aldryn_apphooks_config.models import AppHookConfig
    from cms.models import CMSPlugin, Placeholder
    from django.contrib.auth.models import User  # noqa: F401
    from django.db.models import Model, QuerySet

    from academic_community.members.models import CommunityMember, Email


User = get_user_model()  # type: ignore # noqa: F811


class PermissionRequiredMixin(BasePermissionRequiredMixin):
    """Guardian PermissionRequiredMixin accepting global perms by default."""

    accept_global_perms = True


def has_perm(user_or_group: Union[User, Group], permission: str, obj=None):
    """Check if the user has permissions for a given object.

    Different from the default ``user.has_perm``, we first check the global
    permissions and then the object permissions."""
    if hasattr(user_or_group, "is_anonymous") and user_or_group.is_anonymous:  # type: ignore
        user_or_group = get_anonymous_user()
    if isinstance(user_or_group, User):
        return user_or_group.has_perm(permission) or user_or_group.has_perm(
            permission, obj
        )
    else:
        if obj is None:
            raise ValueError(
                "Checking the group permissions requires an object!"
            )
        ct = ContentType.objects.get_for_model(obj)
        permission_obj = Permission.objects.get(
            content_type=ct,
            codename=permission.split(".")[-1],
        )
        return (
            user_or_group.permissions.filter(pk=permission_obj.pk).exists()
            or GroupObjectPermission.objects.filter(
                permission=permission_obj,
                group=user_or_group,
                content_type=ct,
                object_pk=obj.pk,
            ).exists()
        )


def concat_classes(classes: Sequence[Optional[str]]) -> str:
    """Merge a list of HTML classes and return concatinated string"""
    unique_classes = set(chain.from_iterable(s.split() for s in classes if s))
    return " ".join(filter(None, unique_classes))


def get_index_location() -> str:
    """Get the location of the index URL"""
    force_script_name = getattr(settings, "FORCE_SCRIPT_NAME", "") or "/"
    return force_script_name


def resolve_model_perm(
    model: Union[Type[Model], Model],
    permission: str = "view",
    with_app_label: bool = True,
) -> str:
    """Get the permission for a model.

    Different to `get_model_perm`, this only resolves the permission if
    `permission` is out of the builtin permissions:
    ``"view", "change", "delete", "add"`.
    """
    if permission in ["view", "change", "delete", "add"]:
        return get_model_perm(model, permission, with_app_label)
    else:
        return permission


def get_app_instance(request) -> Tuple[str, Optional[AppHookConfig]]:
    """Get the app instance for a request.

    Reimplementation of :func:`aldryn_apphooks_config.utils.get_app_instance`
    to traverse parent pages to check if any of them defines an apphook.
    """
    app = None
    current_page = getattr(request, "current_page", None)
    namespace, config = "", None

    if getattr(request, "current_app", None) is not None:
        namespace = request.current_app

        for registered_app in apphook_pool.apps.values():
            if namespace in registered_app.get_urls():
                config = registered_app.get_config(namespace)

    while current_page and not current_page.application_urls:
        current_page = current_page.parent_page
    if current_page:
        app = apphook_pool.get_apphook(current_page.application_urls)
    if app and app.app_config:
        if current_page.application_namespace:
            namespace = current_page.application_namespace
            config = app.get_config(namespace)
        else:
            try:
                config = None
                with override(
                    get_language_from_request(request, check_path=True)
                ):
                    namespace = resolve(request.path_info).namespace
                    config = app.get_config(namespace)
            except Resolver404:
                pass
    return namespace, config


def get_model_perm(
    model: Union[Type[Model], Model],
    permission: str = "view",
    with_app_label: bool = True,
) -> str:
    """Get the permission for a model."""
    app_label = model._meta.app_label
    model_name = model._meta.model_name
    if with_app_label:
        return f"{app_label}.{permission}_{model_name}"
    else:
        return f"{permission}_{model_name}"


def placeholder_is_static(placeholder: Placeholder) -> bool:
    """Test if the placeholder belongs to a static alias."""
    from djangocms_alias.models import AliasContent

    return isinstance(placeholder.source, AliasContent) and bool(
        placeholder.source.alias.static_code
    )


def bulk_update_user_permissions(
    instance: Model,
    action: str,
    codename: str,
    pk_set: list[int] = [],
):
    ct = ContentType.objects.get_for_model(instance.__class__)
    perm = Permission.objects.get(
        codename=codename,
        content_type=ct,
    )
    kws = dict(
        content_type=ct,
        object_pk=instance.pk,
        permission=perm,
    )

    if action == "post_add":
        existing_perms = UserObjectPermission.objects.filter(
            user__pk__in=pk_set, **kws
        ).values_list("user__pk", flat=True)
        UserObjectPermission.objects.bulk_create(
            [
                UserObjectPermission(user_id=pk, **kws)
                for pk in pk_set
                if pk not in existing_perms
            ]
        )

    elif action == "post_remove":
        UserObjectPermission.objects.filter(
            user__pk__in=pk_set, **kws
        ).delete()
    elif action == "post_clear":
        UserObjectPermission.objects.filter(**kws).delete()


def bulk_update_group_permissions(
    instance: Model,
    action: str,
    codename: str,
    pk_set: list[int] = [],
):
    ct = ContentType.objects.get_for_model(instance.__class__)
    perm = Permission.objects.get(
        codename=codename,
        content_type=ct,
    )
    kws = dict(
        content_type=ct,
        object_pk=instance.pk,
        permission=perm,
    )

    if action == "post_add":
        existing_perms = GroupObjectPermission.objects.filter(
            group__pk__in=pk_set, **kws
        ).values_list("group__pk", flat=True)
        GroupObjectPermission.objects.bulk_create(
            [
                GroupObjectPermission(group_id=pk, **kws)
                for pk in pk_set
                if pk not in existing_perms
            ]
        )

    elif action == "post_remove":
        GroupObjectPermission.objects.filter(
            group__pk__in=pk_set, **kws
        ).delete()
    elif action == "post_clear":
        GroupObjectPermission.objects.filter(**kws).delete()


def unique_everseen(iterable, key=None):
    """List unique elements, preserving order. Remember all elements ever seen.

    Function taken from https://docs.python.org/2/library/itertools.html"""
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in filterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element


def get_community_name() -> str:
    """Get the full name of the community.

    This function uses the ``COMMUNITY_NAME`` settings variable and defaults
    to ``'Academic Community'``"""
    return getattr(settings, "COMMUNITY_NAME", "Academic Community")


def get_community_abbreviation() -> Optional[str]:
    """Get the abbreviation of the community.

    This function retyrbs the ``COMMUNITY_ABBREVIATION`` settings variable
    or None if this is not specified"""
    return getattr(settings, "COMMUNITY_ABBREVIATION", None)


def render_alternative_template(
    template: str,
    context: Dict[str, Any],
    request: Any = None,
    plain_text_template: Optional[str] = None,
) -> Tuple[str, str]:
    """Render a template with an alternative template

    This convienience method may be used to render a template and get two
    versions, one with and one without html tags.

    Parameters
    ----------
    template : str
        The HTML template to use
    context: Dict[str, Any]
        The context to render the template
    request: Any
        The http request object
    plain_text_template: Optional[str]
        The template to use for the plain text message. If not given, we will
        use the given `template` and strip all HTML tags.

    Returns
    -------
    str
        The HTML message from `template`
    str
        The plain text message from `plain_text_template` or the first argument
        with all HTML tags stripped of.
    """
    default_context = get_default_context(request)
    default_context.update(context)

    html_message = render_to_string(template, default_context)
    if plain_text_template:
        plain_message = render_to_string(plain_text_template, default_context)
    else:
        plain_message = strip_tags(html_message)  # type: ignore
    return html_message, plain_message


def send_mail(
    recipient: Union[Email, str, Sequence[Union[str, Email]]],
    subject: str,
    template: str,
    context: Dict[str, Any],
    request: Any = None,
    plain_text_template: Optional[str] = None,
    send_now=False,
    **kwargs,
):
    """Send a mail to a community member, managers or admins

    This convienience method may be used to render a template and send it via
    mail.

    Parameters
    ----------
    recipient : Union[str, List[str]]
        Either ``'managers'`` to send it to managers, ``'admins'`` to send it
        to admins, or a list of emails.
    subject : str
        The mail subject
    template : str
        The HTML template to use
    context: Dict[str, Any]
        The context to render the template
    request: Any
        The http request object
    plain_text_template: Optional[str]
        The template to use for the plain text message. If not given, we will
        use the given `template` and strip all HTML tags.
    ``**kwargs``
        Any other parameter to the
        :class:`django.core.mail.EmailMultiAlternatives` class
    """
    from academic_community.members.models import Email
    from academic_community.notifications.models import PendingEmail

    html_message, plain_message = render_alternative_template(
        template, context, request, plain_text_template
    )

    FROM_EMAIL = getattr(
        settings, "SERVER_EMAIL", "no-reply.academic-community@example.com"
    )

    if recipient == "managers":
        mail.mail_managers(subject, plain_message, html_message=html_message)
    elif recipient == "admins":
        mail.mail_admins(subject, plain_message, html_message=html_message)
    else:
        if isinstance(recipient, (str, Email)):
            recipient = [str(recipient)]

        if not send_now:
            kwargs["subject"] = subject
            kwargs["body"] = plain_message
            kwargs["from_email"] = FROM_EMAIL
            kwargs["to"] = list(map(str, recipient))
            PendingEmail.objects.create(
                mail_parameters=kwargs, html_message=html_message
            )
        else:
            msg = mail.EmailMultiAlternatives(
                subject,
                plain_message,
                FROM_EMAIL,
                list(map(str, recipient)),  # type: ignore
                **kwargs,
            )
            msg.attach_alternative(html_message, "text/html")
            msg.send()


def create_user_for_member(
    member: CommunityMember, password: str
) -> Tuple[str, User]:
    """Utility function to create a user for a community member.

    This function is used in the
    :class:`academic_community.members.forms.CreateUserForm` class. It may be
    changed using the ``MEMBER_USER_FACTORY`` configuration setting that can
    point to a different function.

    Parameters
    ----------
    member: CommunityMember
        The community member for whom to create the Django user
    password: str
        The randomly generated password that shall be used to login with the
        new user

    Returns
    -------
    str
        The username that can be used for login
    ~django.contrib.auth.models.User
        The django User that can be used to login.
    """
    username = f"{member.first_name} {member.last_name}".lower()
    try:
        return username, User.objects.get(username=username)
    except User.DoesNotExist:
        return username, User.objects.create(
            username=username,
            email=str(member.email),
            first_name=member.first_name,
            last_name=member.last_name,
            password=password,
        )


def get_connected_users(qs=None):
    """Get the online users."""
    from academic_community.notifications.models import SessionConnection

    if qs is None:
        qs = User.objects

    connections = SessionConnection.objects.filter(
        session_availability=OuterRef("sessionavailability__pk")
    )
    return qs.annotate(connected=Exists(connections)).filter(connected=True)


def _create_user_for_member(member: CommunityMember, password: str) -> User:
    """Convenience wrapper for creating user that considers the settings

    See Also
    --------
    create_user_for_member
    """
    func = getattr(settings, "MEMBER_USER_FACTORY", create_user_for_member)
    if not callable(func):
        module_name, func_name = func.rsplit(".", 1)
        mod: Any = importlib.import_module(module_name)
        func = getattr(mod, func_name)
    return func(member, password)


DEFAULT_GROUP_NAMES = {
    "DEFAULT": "All registered users",  # all registered users
    "MEMBERS": "Community members",  # all community members
    "ANONYMOUS": "Anonymous users (public)",  # all anonymous users
    "MANAGERS": "Community managers",  # all community managers
    "CMS": "CMS Editors",
}


def get_group_names(*keys: str) -> list[str]:
    """Get multiple group names.

    This utility function takes keys from :attr:`DEFAULT_GROUP_NAMES` as
    arguments and returns the corresponding values."""
    return [DEFAULT_GROUP_NAMES[key] for key in keys]


def get_default_group() -> Group:
    """Get the default group that all users are automatically member of."""
    return Group.objects.get_or_create(name=DEFAULT_GROUP_NAMES["DEFAULT"])[0]


def get_members_group() -> Group:
    """Get the default group that all users are automatically member of."""
    return Group.objects.get_or_create(name=DEFAULT_GROUP_NAMES["MEMBERS"])[0]


def get_managers_group() -> Group:
    """Get the default group that all users are automatically member of."""
    return Group.objects.get_or_create(name=DEFAULT_GROUP_NAMES["MANAGERS"])[0]


def get_anonymous_group() -> Group:
    """Get the group of the anonymous user."""
    try:
        return Group.objects.get(name=DEFAULT_GROUP_NAMES["ANONYMOUS"])
    except Group.DoesNotExist:
        anonymous_user = get_anonymous_user()
        group = Group.objects.create(name=DEFAULT_GROUP_NAMES["ANONYMOUS"])
        anonymous_user.groups.add(group)
        return group


def get_cms_group() -> Group:
    """Get the group for CMS Editors."""
    return Group.objects.get_or_create(name=DEFAULT_GROUP_NAMES["CMS"])[0]


def get_groups(*keys) -> QuerySet[Group]:
    """Get groups from the default groups."""
    return Group.objects.filter(name__in=get_group_names(*keys))


class PermissionCheckFormMixin:
    """Abstract base class for checking permissions."""

    def update_from_user(self, user: User):
        if user.is_anonymous:  # type: ignore
            self.update_from_anonymous()
        else:
            self.update_from_registered_user(user)

    def update_from_anonymous(self):
        raise exceptions.PermissionDenied(
            "This form is not intended for anonymous users."
        )

    def update_from_registered_user(self, user: User):
        pass

    def disable_field(self, field: str, hint: Optional[str] = None):
        """Convenience method to disable a field and display a hint."""
        fields: Dict[str, Any] = self.fields  # type: ignore
        if field in fields:
            fields[field].disabled = True
            if hint:
                fields[field].help_text += hint

    def remove_field(self, field: str):
        """Convencience method to remove a field from the form."""
        fields: Dict[str, Any] = self.fields  # type: ignore
        if field in fields:
            del fields[field]

    def hide_field(self, field: str):
        """Convencience method to remove a field from the form."""
        fields: Dict[str, Any] = self.fields  # type: ignore
        if field in fields:
            fields[field].widget = forms.HiddenInput()


class PermissionCheckForm(PermissionCheckFormMixin, forms.ModelForm):
    pass


VALID_STYLE_ELEMENTS = {"height", "width"}

STYLE_ELEMENT_WHITELIST = {
    "td": {"background-color", "border", "width"},
    "th": {"background-color", "border", "width"},
    "col": {"width"},
    "figure": {"width"},
}


def remove_style(html: str) -> str:
    """Remove all style attributes from a given HTML field"""
    if html:
        soup = BeautifulSoup(html, "html.parser")
        for tag in soup():
            if "style" in tag.attrs:
                if tag.name in STYLE_ELEMENT_WHITELIST:
                    style = cssutils.parseStyle(tag.attrs["style"])
                    for key in (
                        set(style.keys()) - STYLE_ELEMENT_WHITELIST[tag.name]
                    ):
                        del style[key]
                    tag.attrs["style"] = style.getCssText(" ")
                elif not VALID_STYLE_ELEMENTS:
                    del tag.attrs["style"]
                else:
                    style = cssutils.parseStyle(tag.attrs["style"])
                    for key in set(style.keys()) - VALID_STYLE_ELEMENTS:
                        del style[key]
                    tag.attrs["style"] = style.getCssText(" ")
        return mark_safe(soup.prettify())
    else:
        return html


class PermissionCheckFormSetMixin:
    """An inline formset that defines the update_from_user method."""

    def update_from_user(self, user: User):
        form: PermissionCheckFormMixin
        for form in self.forms:  # type: ignore
            form.update_from_user(user)

    def update_from_anonymous(self):
        form: PermissionCheckFormMixin
        for form in self.forms:  # type: ignore
            form.update_from_anonymous()

    def update_from_registered_user(self, user: User):
        form: PermissionCheckFormMixin
        for form in self.forms:  # type: ignore
            form.update_from_registered_user(user)


class PermissionCheckBaseInlineFormSet(
    PermissionCheckFormSetMixin, BaseInlineFormSet
):
    pass


class PermissionCheckBaseModelFormSet(
    PermissionCheckFormSetMixin, BaseModelFormSet
):
    pass


class PermissionCheckInlineFormSetFactory(InlineFormSetFactory):
    """A mixin for the formset factory with permission check."""

    form_class = PermissionCheckForm

    def get_factory_kwargs(self):
        ret = super().get_factory_kwargs()
        ret["formset"] = PermissionCheckBaseInlineFormSet
        return ret


def create_plugins_from_conf(
    placeholder: Placeholder, plugin_conf: List[Dict], lang="en"
) -> List[CMSPlugin]:
    """Create plugins for a placeholder base on the configuration

    Parameters
    ----------
    placeholder : Placeholder
        The placeholder to add to the plugins to
    plugin_conf : Dict
        The configuration suitable for the `cms.api.add_plugin` function
    lang : str, optional
        The language, by default "en"

    Returns
    -------
    List[CMSPlugin]
        The created plugins
    """
    from cms.api import add_plugin

    def _create_default_plugins(confs: List[Dict], parent=None):
        """
        Auxiliary function that builds all of a placeholder's default plugins
        at the current level and drives the recursion down the tree.
        Returns the plugins at the current level along with all descendants.
        """
        plugins, descendants = [], []
        for conf in confs:
            plugin = add_plugin(
                placeholder,
                conf["plugin_type"],
                lang,
                target=parent,
                **conf.get("values", {}),
            )
            plugin.save()
            if conf.get("m2m_values"):
                # add m2m relations for the model
                for field_name, vals in conf["m2m_values"].items():
                    field = getattr(plugin, field_name)
                    field.add(*vals)
            if "children" in conf:
                args = conf["children"], plugin
                descendants += _create_default_plugins(*args)
            plugins.append(plugin)
        return plugins + descendants

    return _create_default_plugins(plugin_conf)


def get_pandoc_latex_template() -> Path:
    """Get the path to the template for conversion to latex.

    Returns
    -------
    Path
        The path to the template
    """
    if app_settings.PANDOC_LATEX_TEMPLATE:
        return app_settings.PANDOC_LATEX_TEMPLATE
    else:
        return get_pandoc_file("template.latex")


def get_pandoc_file(basename: str) -> Path:
    """Get a file in the pandoc-templates folder of this package

    Parameters
    ----------
    basename : str
        The basename of the file

    Returns
    -------
    Path
        The full path to the file in the pandoc-templates folder
    """
    return Path(__file__).parent / "pandoc-templates" / basename


def get_export_single_renderer_classes():
    ret = []
    for cls_ in app_settings.EXPORT_SINGLE_RENDERER:
        try:
            ret.append(import_string(cls_))
        except ImportError as e:
            msg = "Could not import %s from EXPORT_RENDERER setting: %s" % e
            raise ImportError(msg)
    return ret


def get_export_list_renderer_classes():
    ret = []
    for cls_ in app_settings.EXPORT_LIST_RENDERER:
        try:
            ret.append(import_string(cls_))
        except ImportError as e:
            msg = "Could not import %s from EXPORT_RENDERER setting: %s" % e
            raise ImportError(msg)
    return ret
