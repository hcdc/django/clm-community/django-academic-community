# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.23 on 2024-01-10 09:34

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("cms", "0022_auto_20180620_1551"),
        ("academic_community", "0019_auto_20240109_1339"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="internalnamebootstrap5link",
            name="cmsplugin_ptr",
        ),
        migrations.RemoveField(
            model_name="internalnamebootstrap5link",
            name="file_link",
        ),
        migrations.RemoveField(
            model_name="internalnamebootstrap5link",
            name="internal_link",
        ),
        migrations.RemoveField(
            model_name="menubutton",
            name="cmsplugin_ptr",
        ),
        migrations.RemoveField(
            model_name="menubutton",
            name="file_link",
        ),
        migrations.RemoveField(
            model_name="menubutton",
            name="internal_link",
        ),
        migrations.DeleteModel(
            name="Bootstrap5CarouselCaptionSlide",
        ),
        migrations.DeleteModel(
            name="InternalNameBootstrap5Link",
        ),
        migrations.DeleteModel(
            name="MenuButton",
        ),
    ]
