# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.21 on 2023-10-09 17:36

import django.db.models.deletion
import djangocms_frontend.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cms", "0022_auto_20180620_1551"),
        ("academic_community", "0012_auto_20230825_1648"),
    ]

    operations = [
        migrations.CreateModel(
            name="ActiveFiltersPluginModel",
            fields=[
                (
                    "cmsplugin_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        related_name="academic_community_activefilterspluginmodel",
                        serialize=False,
                        to="cms.cmsplugin",
                    ),
                ),
                (
                    "filter_query_prefix",
                    models.SlugField(
                        blank=True,
                        help_text="The suffix for the filter parameter in the GET request. If you have multiple plugins with filter options on a page, make sure, to give them unique suffixes.",
                        null=True,
                    ),
                ),
                (
                    "strict_filters",
                    models.BooleanField(
                        default=True,
                        help_text="Return no data at all if invalid filter parameters are provided in the URL.",
                    ),
                ),
                (
                    "show_filters_in_card",
                    models.BooleanField(
                        default=True,
                        help_text="Render a card around the badges.",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("cms.cmsplugin", models.Model),
        ),
        migrations.CreateModel(
            name="FilterButtonPluginModel",
            fields=[
                (
                    "cmsplugin_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        related_name="academic_community_filterbuttonpluginmodel",
                        serialize=False,
                        to="cms.cmsplugin",
                    ),
                ),
                (
                    "filter_query_prefix",
                    models.SlugField(
                        blank=True,
                        help_text="The suffix for the filter parameter in the GET request. If you have multiple plugins with filter options on a page, make sure, to give them unique suffixes.",
                        null=True,
                    ),
                ),
                (
                    "strict_filters",
                    models.BooleanField(
                        default=True,
                        help_text="Return no data at all if invalid filter parameters are provided in the URL.",
                    ),
                ),
                (
                    "filter_button_text",
                    models.CharField(
                        blank=True,
                        help_text="The text to show on the filter button.",
                        max_length=50,
                        null=True,
                    ),
                ),
                (
                    "filter_button_attributes",
                    djangocms_frontend.fields.AttributesField(
                        blank=True,
                        default=dict,
                        help_text="Additional attributes for the filter button",
                        verbose_name="Attributes",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("cms.cmsplugin", models.Model),
        ),
    ]
