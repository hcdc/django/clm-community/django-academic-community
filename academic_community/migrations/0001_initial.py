# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.7 on 2022-02-25 09:33

import django.contrib.postgres.fields
import django.db.models.deletion
import djangocms_attributes_field.fields
import djangocms_icon.fields
import djangocms_link.validators
import filer.fields.file
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("cms", "0022_auto_20180620_1551"),
        ("filer", "0012_file_mime_type"),
    ]

    operations = [
        migrations.CreateModel(
            name="InternalNameLink",
            fields=[
                (
                    "template",
                    models.CharField(
                        choices=[("default", "Default")],
                        default="default",
                        max_length=255,
                        verbose_name="Template",
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        blank=True, max_length=255, verbose_name="Display name"
                    ),
                ),
                (
                    "external_link",
                    models.CharField(
                        blank=True,
                        help_text="Provide a link to an external source.",
                        max_length=2040,
                        validators=[
                            djangocms_link.validators.IntranetURLValidator(
                                intranet_host_re=None
                            )
                        ],
                        verbose_name="External link",
                    ),
                ),
                (
                    "anchor",
                    models.CharField(
                        blank=True,
                        help_text='Appends the value only after the internal or external link. Do <em>not</em> include a preceding "#" symbol.',
                        max_length=255,
                        verbose_name="Anchor",
                    ),
                ),
                (
                    "mailto",
                    models.EmailField(
                        blank=True,
                        max_length=255,
                        verbose_name="Email address",
                    ),
                ),
                (
                    "phone",
                    models.CharField(
                        blank=True, max_length=255, verbose_name="Phone"
                    ),
                ),
                (
                    "target",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("_blank", "Open in new window"),
                            ("_self", "Open in same window"),
                            ("_parent", "Delegate to parent"),
                            ("_top", "Delegate to top"),
                        ],
                        max_length=255,
                        verbose_name="Target",
                    ),
                ),
                (
                    "attributes",
                    djangocms_attributes_field.fields.AttributesField(
                        blank=True, default=dict, verbose_name="Attributes"
                    ),
                ),
                (
                    "cmsplugin_ptr",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        related_name="academic_community_internalnamelink",
                        serialize=False,
                        to="cms.cmsplugin",
                    ),
                ),
                (
                    "link_type",
                    models.CharField(
                        choices=[("link", "Link"), ("btn", "Button")],
                        default="link",
                        help_text="Adds either the .btn-* or .text-* classes.",
                        max_length=255,
                        verbose_name="Type",
                    ),
                ),
                (
                    "link_context",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("link", "Link"),
                            ("primary", "Primary"),
                            ("secondary", "Secondary"),
                            ("success", "Success"),
                            ("danger", "Danger"),
                            ("warning", "Warning"),
                            ("info", "Info"),
                            ("light", "Light"),
                            ("dark", "Dark"),
                        ],
                        max_length=255,
                        verbose_name="Context",
                    ),
                ),
                (
                    "link_size",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("btn-sm", "Small"),
                            ("", "Medium"),
                            ("btn-lg", "Large"),
                        ],
                        max_length=255,
                        verbose_name="Size",
                    ),
                ),
                (
                    "link_outline",
                    models.BooleanField(
                        default=False,
                        help_text="Applies the .btn-outline class to the elements.",
                        verbose_name="Outline",
                    ),
                ),
                (
                    "link_block",
                    models.BooleanField(
                        default=False,
                        help_text="Extends the button to the width of its container.",
                        verbose_name="Block",
                    ),
                ),
                (
                    "icon_left",
                    djangocms_icon.fields.Icon(
                        blank=True,
                        default="",
                        max_length=255,
                        verbose_name="Icon left",
                    ),
                ),
                (
                    "icon_right",
                    djangocms_icon.fields.Icon(
                        blank=True,
                        default="",
                        max_length=255,
                        verbose_name="Icon right",
                    ),
                ),
                (
                    "internal_url_name",
                    models.CharField(
                        blank=True,
                        help_text="The identifier for the link in the django app. Note that this is an advanced feature. Make sure you know what you are doing.",
                        max_length=200,
                        null=True,
                    ),
                ),
                (
                    "internal_url_args",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.CharField(blank=True, max_length=50),
                        blank=True,
                        help_text="Comma-separated list of arguments necessary to resolve the internal url name",
                        null=True,
                        size=10,
                    ),
                ),
                (
                    "file_link",
                    filer.fields.file.FilerFileField(
                        blank=True,
                        help_text="If provided links a file from the filer app.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="filer.file",
                        verbose_name="File link",
                    ),
                ),
                (
                    "internal_link",
                    models.ForeignKey(
                        blank=True,
                        help_text="If provided, overrides the external link.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="cms.page",
                        verbose_name="Internal link",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("cms.cmsplugin",),
        ),
    ]
