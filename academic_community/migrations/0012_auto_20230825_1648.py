# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.20 on 2023-08-25 14:48

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cms", "0022_auto_20180620_1551"),
        (
            "academic_community",
            "0011_remove_pageadminextension_show_in_main_navigation",
        ),
    ]

    operations = [
        migrations.CreateModel(
            name="ProfileButton",
            fields=[
                (
                    "cmsplugin_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        related_name="academic_community_profilebutton",
                        serialize=False,
                        to="cms.cmsplugin",
                    ),
                ),
                (
                    "classes",
                    models.CharField(
                        blank=True,
                        help_text="CSS classes for the button",
                        max_length=200,
                        null=True,
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("cms.cmsplugin",),
        ),
        migrations.RenameField(
            model_name="usergroupbasedcontent",
            old_name="superusers",
            new_name="is_superuser",
        ),
        migrations.AddField(
            model_name="internalnamebootstrap5link",
            name="add_next",
            field=models.BooleanField(
                default=False,
                help_text="Add the `next` GET parameter to the generated URL to let the user return to the site of the plugin upon submission.",
            ),
        ),
        migrations.AddField(
            model_name="menubutton",
            name="add_next",
            field=models.BooleanField(
                default=False,
                help_text="Add the `next` GET parameter to the generated URL to let the user return to the site of the plugin upon submission.",
            ),
        ),
        migrations.AddField(
            model_name="usergroupbasedcontent",
            name="is_anonymous",
            field=models.BooleanField(
                default=False,
                help_text="Display this button to users that are not logged in.",
            ),
        ),
        migrations.AddField(
            model_name="usergroupbasedcontent",
            name="is_authenticated",
            field=models.BooleanField(
                default=False,
                help_text="Display this button to users that are logged in.",
            ),
        ),
        migrations.AddField(
            model_name="usergroupbasedcontent",
            name="is_staff",
            field=models.BooleanField(
                default=False,
                help_text="Display this button to logged-in staff users (includes community members).",
            ),
        ),
        migrations.AddField(
            model_name="usergroupbasedcontent",
            name="is_manager",
            field=models.BooleanField(
                default=False,
                help_text="Display this button to logged-in community managers.",
            ),
        ),
        migrations.AddField(
            model_name="usergroupbasedcontent",
            name="is_member",
            field=models.BooleanField(
                default=False,
                help_text="Display this button to logged-in community members.",
            ),
        ),
    ]
