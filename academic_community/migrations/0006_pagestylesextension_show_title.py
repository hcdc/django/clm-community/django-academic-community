# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.15 on 2023-05-15 21:27

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("academic_community", "0005_pagestylesextension"),
    ]

    operations = [
        migrations.AddField(
            model_name="pagestylesextension",
            name="show_title",
            field=models.BooleanField(
                default=True,
                help_text="Show the title of the page in the headline.",
            ),
        ),
    ]
