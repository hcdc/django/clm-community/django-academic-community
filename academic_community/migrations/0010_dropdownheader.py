# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.20 on 2023-08-24 19:38

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cms", "0022_auto_20180620_1551"),
        ("academic_community", "0009_usergroupbasedcontent"),
    ]

    operations = [
        migrations.CreateModel(
            name="DropdownHeader",
            fields=[
                (
                    "cmsplugin_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        related_name="academic_community_dropdownheader",
                        serialize=False,
                        to="cms.cmsplugin",
                    ),
                ),
                (
                    "text",
                    models.CharField(
                        help_text="The text to display", max_length=200
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("cms.cmsplugin",),
        ),
    ]
