# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(
        r"channels/(?P<channel_id>\w+)/$",
        consumers.ChannelConsumer.as_asgi(),
    ),
]
