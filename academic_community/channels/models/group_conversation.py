"""Issue models for channels app"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from .channel_type import BaseChannelType


@BaseChannelType.registry.register
class GroupConversation(BaseChannelType):
    """A conversation between community members.

    In contrast to a private conversation, group conversations can be made
    available to users that do not subscribe to the conversation.
    """

    @classmethod
    def get_channel_type_slug(cls) -> str:
        return "group-conversations"
