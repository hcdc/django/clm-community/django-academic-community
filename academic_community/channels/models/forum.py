"""Issue models for channels app"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from django.db import models

from .channel_type import BaseChannelType
from .core import ThreadComment


@BaseChannelType.registry.register
class Forum(BaseChannelType):
    """A forum to ask and discuss questions.

    In contrast to a conversation, forum comments can be marked as the correct
    answer and each thread has a specific topic.
    """

    pass


class ForumAnswer(models.Model):
    """An accepted answer within a forum."""

    threadcomment = models.OneToOneField(
        ThreadComment,
        on_delete=models.CASCADE,
        limit_choices_to={
            "parent_thread__parent_channel__forum__isnull": False
        },
    )
