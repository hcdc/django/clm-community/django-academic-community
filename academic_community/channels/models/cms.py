# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import Optional, Type

from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import gettext as _
from djangocms_frontend.fields import AttributesField

from academic_community.channels.models.channel_type import BaseChannelType
from academic_community.channels.models.core import Channel, ChannelKeyword
from academic_community.models.cms import AbstractPaginationPluginModelMixin


class AbstractChannelListPluginModel(  # type: ignore[django-manager-missing]
    AbstractPaginationPluginModelMixin, CMSPlugin
):
    """An abstract base model to display a member list."""

    class Meta:
        abstract = True

    check_permissions = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=(
            "Only display the members that the website visitor can view the "
            "profile of."
        ),
    )

    channel_type = models.CharField(  # type: ignore[var-annotated]
        null=True,
        blank=True,
        help_text="Only display channels of a specific type.",
        max_length=20,
        choices=[(None, "All Channels")]
        + sorted(
            (
                BaseChannelType.registry.get_model_name(model),
                model.get_verbose_name_plural(),
            )
            for model in BaseChannelType.registry.registered_models
        ),
    )

    keywords = models.ManyToManyField(  # type: ignore[var-annotated]
        ChannelKeyword,
        blank=True,
        help_text="Select keywords that you want to filter for.",
    )

    all_keywords_required = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Show only material that has all of the mentioned keywords.",
    )

    attributes = AttributesField(
        verbose_name=_("Attributes for the surrounding container"),
        blank=True,
    )

    @property
    def channel_type_model(self) -> Optional[Type[BaseChannelType]]:
        """Get the model for the channel type (or None)."""
        if self.channel_type:
            return BaseChannelType.registry.get_model(self.channel_type)
        return None

    def copy_relations(self, oldinstance):
        self.keywords.clear()
        self.keywords.add(*oldinstance.keywords.all())


class ChannelListPluginModel(AbstractChannelListPluginModel):  # type: ignore[django-manager-missing]
    """A plugin model to display community members."""

    channels = models.ManyToManyField(  # type: ignore[var-annotated]
        Channel, blank=True, help_text="The channels to display."
    )

    show_all = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Show all channels, no matter what you selected above.",
    )

    def copy_relations(self, oldinstance):
        self.channels.clear()
        self.channels.add(*oldinstance.channels.all())
