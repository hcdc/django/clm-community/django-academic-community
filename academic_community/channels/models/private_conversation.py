"""Issue models for channels app"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from .channel_type import BaseChannelType
from .subscriptions import ChannelSubscription


@BaseChannelType.registry.register
class PrivateConversation(BaseChannelType):
    """A private conversation between community members.

    In contrast to a group discussion, nobody can modify the comment but the
    community members who posted it.
    """

    @classmethod
    def get_channel_type_slug(cls) -> str:
        return "private-conversations"


@receiver(post_save, sender=ChannelSubscription)
def grant_post_permission_to_private_conversation_subscribers(
    instance: ChannelSubscription, created=True, **kwargs
):
    """Create subscriptions and grant permissions to participants."""

    if created:
        try:
            channel_type = instance.channel.channel_type
        except ValueError:  # channel type not set
            return
        if isinstance(channel_type, PrivateConversation):
            instance.channel.user_view_permission.add(instance.user)
            instance.channel.user_edit_permission.add(instance.user)
            instance.channel.user_start_thread_permission.add(instance.user)
            instance.channel.user_post_comment_permission.add(instance.user)


@receiver(post_save, sender=PrivateConversation)
def grant_post_permission_to_channel_creator(
    instance: PrivateConversation, created=True, **kwargs
):
    """Create subscriptions and grant permissions to participants."""

    if created:
        instance.channel.user_view_permission.add(instance.channel.user)
        instance.channel.user_edit_permission.add(instance.channel.user)
        instance.channel.user_start_thread_permission.add(
            instance.channel.user
        )
        instance.channel.user_post_comment_permission.add(
            instance.channel.user
        )


@receiver(post_delete, sender=ChannelSubscription)
def remove_permissions_for_private_conversation_subscriber(
    instance: ChannelSubscription, **kwargs
):
    """Remove permissions from participants."""

    try:
        channel_type = instance.channel.channel_type
    except ValueError:
        return
    if isinstance(channel_type, PrivateConversation):
        instance.channel.user_view_permission.remove(instance.user)
        instance.channel.user_edit_permission.remove(instance.user)
        instance.channel.user_start_thread_permission.remove(instance.user)
        instance.channel.user_post_comment_permission.remove(instance.user)
