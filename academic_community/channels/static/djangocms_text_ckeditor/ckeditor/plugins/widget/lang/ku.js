// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'widget', 'ku', {
	'move': 'کرتەبکە و ڕایبکێشە بۆ جوڵاندن',
	'label': '%1 ویجێت'
} );
