// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'widget', 'sl', {
	'move': 'Kliknite in povlecite, da premaknete',
	'label': '%1 widget' // MISSING
} );
