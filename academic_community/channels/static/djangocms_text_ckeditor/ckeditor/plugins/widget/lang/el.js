// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'widget', 'el', {
	'move': 'Κάνετε κλικ και σύρετε το ποντίκι για να μετακινήστε',
	'label': '%1 widget' // MISSING
} );
