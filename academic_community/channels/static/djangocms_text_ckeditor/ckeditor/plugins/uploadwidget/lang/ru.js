// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'ru', {
	abort: 'Загрузка отменена пользователем',
	doneOne: 'Файл успешно загружен',
	doneMany: 'Успешно загружено файлов: %1',
	uploadOne: 'Загрузка файла ({percentage}%)',
	uploadMany: 'Загрузка файлов, {current} из {max} загружено ({percentage}%)...'
} );
