// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'ku', {
	abort: 'بارکردنەکە بڕدرا لەلایەن بەکارهێنەر.',
	doneOne: 'پەڕگەکە بەسەرکەوتووانە بارکرا.',
	doneMany: 'بەسەرکەوتووانە بارکرا %1 پەڕگە.',
	uploadOne: 'پەڕگە باردەکرێت ({percentage}%)...',
	uploadMany: 'پەڕگە باردەکرێت, {current} لە {max} ئەنجامدراوە ({percentage}%)...'
} );
