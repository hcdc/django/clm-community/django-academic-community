// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'sr-latn', {
	abort: 'Postavljanje je prekinuto sa strane korisnika',
	doneOne: 'Datoteka je uspešno postavljena',
	doneMany: '%1 datoteka je uspešno postavljena',
	uploadOne: 'Postavljanje datoteke  ({percentage}%)...',
	uploadMany: 'Postavljanje datoteka, {current} of {max} done ({percentage}%)...'
} );
