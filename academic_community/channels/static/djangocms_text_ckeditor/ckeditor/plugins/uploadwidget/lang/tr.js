// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'tr', {
	abort: 'Gönderme işlemi kullanıcı tarafından durduruldu.',
	doneOne: 'Gönderim işlemi başarılı şekilde tamamlandı.',
	doneMany: '%1 dosya başarılı şekilde gönderildi.',
	uploadOne: 'Dosyanın ({percentage}%) gönderildi...',
	uploadMany: 'Toplam {current} / {max} dosyanın ({percentage}%) gönderildi...'
} );
