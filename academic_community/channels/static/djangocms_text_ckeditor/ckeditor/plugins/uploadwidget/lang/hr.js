// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'hr', {
	abort: 'Slanje prekinuto od strane korisnika',
	doneOne: 'Datoteka uspješno poslana.',
	doneMany: 'Uspješno poslano %1 datoteka.',
	uploadOne: 'Slanje datoteke ({percentage}%)...',
	uploadMany: 'Slanje datoteka, {current} od {max} gotovo ({percentage}%)...'
} );
