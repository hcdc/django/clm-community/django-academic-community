// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'nb', {
	abort: 'Opplasting ble avbrutt av brukeren.',
	doneOne: 'Filen har blitt lastet opp.',
	doneMany: 'Fullført opplasting av %1 filer.',
	uploadOne: 'Laster opp fil ({percentage}%)...',
	uploadMany: 'Laster opp filer, {current} av {max} fullført ({percentage}%)...'
} );
