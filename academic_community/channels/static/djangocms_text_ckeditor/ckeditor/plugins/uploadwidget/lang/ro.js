// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'ro', {
	abort: 'Încărcare întreruptă de utilizator.',
	doneOne: 'Fișier încărcat cu succes.',
	doneMany: '%1 fișiere încărcate cu succes.',
	uploadOne: 'Încărcare fișier ({percentage}%)...',
	uploadMany: 'Încărcare fișiere, {current} din {max} realizat ({percentage}%)...'
} );
