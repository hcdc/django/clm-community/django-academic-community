// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'nl', {
	abort: 'Upload gestopt door de gebruiker.',
	doneOne: 'Bestand succesvol geüpload.',
	doneMany: 'Succesvol %1 bestanden geüpload.',
	uploadOne: 'Uploaden bestand ({percentage}%)…',
	uploadMany: 'Bestanden aan het uploaden, {current} van {max} klaar ({percentage}%)…'
} );
