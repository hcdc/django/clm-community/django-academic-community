// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'es-mx', {
	abort: 'La carga ha sido abortada por el usuario.',
	doneOne: 'El archivo ha sido cargado completamente.',
	doneMany: '%1 archivos cargados completamente.',
	uploadOne: 'Cargando archivo ({percentage}%)...',
	uploadMany: 'Cargando archivos, {current} de {max} listo ({percentage}%)...'
} );
