// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'oc', {
	abort: 'Mandadís interromput per l\'utilizaire',
	doneOne: 'Fichièr mandat amb succès.',
	doneMany: '%1 fichièrs mandats amb succès.',
	uploadOne: 'Mandadís del fichièr en cors ({percentage} %)…',
	uploadMany: 'Mandadís dels fichièrs en cors, {current} sus {max} efectuats ({percentage} %)…'
} );
