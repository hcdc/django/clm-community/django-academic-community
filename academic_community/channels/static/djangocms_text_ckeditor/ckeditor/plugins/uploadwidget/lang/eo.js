// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'eo', {
	abort: 'Alŝuto ĉesigita de la uzanto',
	doneOne: 'Dosiero sukcese alŝutita.',
	doneMany: 'Sukcese alŝutitaj %1 dosieroj.',
	uploadOne: 'alŝutata dosiero ({percentage}%)...',
	uploadMany: 'Alŝutataj dosieroj, {current} el {max} faritaj ({percentage}%)...'
} );
