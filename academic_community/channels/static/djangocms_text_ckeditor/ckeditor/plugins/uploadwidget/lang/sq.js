// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'sq', {
	abort: 'Ngarkimi u ndërpre nga përdoruesi.',
	doneOne: 'Skeda u ngarkua me sukses.',
	doneMany: 'Me sukses u ngarkuan %1 skeda.',
	uploadOne: 'Duke ngarkuar skedën ({percentage}%)...',
	uploadMany: 'Duke ngarkuar skedat, {current} nga {max} , ngarkuar ({percentage}%)...'
} );
