// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'pt', {
	abort: 'Carregamento cancelado pelo utilizador.',
	doneOne: 'Ficheiro carregado com sucesso.',
	doneMany: 'Successfully uploaded %1 files.', // MISSING
	uploadOne: 'Uploading file ({percentage}%)...', // MISSING
	uploadMany: 'Uploading files, {current} of {max} done ({percentage}%)...' // MISSING
} );
