// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'gl', {
	abort: 'Envío interrompido polo usuario.',
	doneOne: 'Ficheiro enviado satisfactoriamente.',
	doneMany: '%1 ficheiros enviados satisfactoriamente.',
	uploadOne: 'Enviando o ficheiro ({percentage}%)...',
	uploadMany: 'Enviando ficheiros, {current} de {max} feito o ({percentage}%)...'
} );
