// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'id', {
	abort: 'Pengunggahan dibatalkan oleh pengguna',
	doneOne: 'Berkas telah berhasil diunggah',
	doneMany: 'Pengunggahan berkas %1 berhasil',
	uploadOne: 'Mengunggah berkas ({percentage}%)...',
	uploadMany: 'Pengunggahan berkas {current} dari {max} berhasil ({percentage}%)...'
} );
