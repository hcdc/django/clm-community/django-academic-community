// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'ca', {
	abort: 'Pujada cancel·lada per l\'usuari.',
	doneOne: 'Fitxer pujat correctament.',
	doneMany: '%1 fitxers pujats correctament.',
	uploadOne: 'Pujant fitxer ({percentage}%)...',
	uploadMany: 'Pujant fitxers, {current} de {max} finalitzats ({percentage}%)...'
} );
