// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'bg', {
	abort: 'Качването е прекратено от потребителя.',
	doneOne: 'Файлът е качен успешно.',
	doneMany: 'Успешно са качени %1 файла.',
	uploadOne: 'Качване на файл ({percentage}%)...',
	uploadMany: 'Качване на файлове, {current} от {max} качени ({percentage}%)...'
} );
