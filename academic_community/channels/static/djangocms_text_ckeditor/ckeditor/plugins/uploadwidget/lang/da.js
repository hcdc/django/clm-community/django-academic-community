// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'da', {
	abort: 'Upload er afbrudt af bruger.',
	doneOne: 'Filen er uploadet.',
	doneMany: 'Du har uploadet %1 filer.',
	uploadOne: 'Uploader fil ({percentage}%)...',
	uploadMany: 'Uploader filer, {current} af {max} er uploadet ({percentage}%)...'
} );
