// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'zh-cn', {
	abort: '上传已被用户中止',
	doneOne: '文件上传成功',
	doneMany: '成功上传了 %1 个文件',
	uploadOne: '正在上传文件（{percentage}%）...',
	uploadMany: '正在上传文件，{max} 中的 {current}（{percentage}%）...'
} );
