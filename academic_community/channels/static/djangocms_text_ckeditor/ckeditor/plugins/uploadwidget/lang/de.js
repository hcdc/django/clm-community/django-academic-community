// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'de', {
	abort: 'Hochladen durch den Benutzer abgebrochen.',
	doneOne: 'Datei erfolgreich hochgeladen.',
	doneMany: '%1 Dateien erfolgreich hochgeladen.',
	uploadOne: 'Datei wird hochgeladen ({percentage}%)...',
	uploadMany: 'Dateien werden hochgeladen, {current} von {max} fertig ({percentage}%)...'
} );
