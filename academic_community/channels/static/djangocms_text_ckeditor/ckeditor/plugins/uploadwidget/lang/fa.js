// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'fa', {
	abort: 'بارگذاری توسط کاربر لغو شد.',
	doneOne: 'فایل با موفقیت بارگذاری شد.',
	doneMany: '%1 از فایل​ها با موفقیت بارگذاری شد.',
	uploadOne: 'بارگذاری فایل ({percentage}%)...',
	uploadMany: 'بارگذاری فایل​ها, {current} از {max} انجام شده ({percentage}%)...'
} );
