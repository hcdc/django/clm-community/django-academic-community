// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'sk', {
	abort: 'Nahrávanie zrušené používateľom.',
	doneOne: 'Súbor úspešne nahraný.',
	doneMany: 'Úspešne nahraných %1 súborov.',
	uploadOne: 'Nahrávanie súboru ({percentage}%)...',
	uploadMany: 'Nahrávanie súborov, {current} z {max} hotovo ({percentage}%)...'
} );
