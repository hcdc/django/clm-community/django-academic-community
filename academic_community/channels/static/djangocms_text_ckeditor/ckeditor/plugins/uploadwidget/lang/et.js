// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'et', {
	abort: 'Kasutaja katkestas üleslaadimise.',
	doneOne: 'Fail on üles laaditud.',
	doneMany: '%1 faili laaditi edukalt üles.',
	uploadOne: 'Faili üleslaadimine ({percentage}%)...',
	uploadMany: 'Failide üleslaadimine, {current} fail {max}-st üles laaditud ({percentage}%)...'
} );
