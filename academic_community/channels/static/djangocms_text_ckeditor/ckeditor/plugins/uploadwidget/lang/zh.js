// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'zh', {
	abort: '上傳由使用者放棄。',
	doneOne: '檔案成功上傳。',
	doneMany: '成功上傳 %1 檔案。',
	uploadOne: '正在上傳檔案（{percentage}%）...',
	uploadMany: '正在上傳檔案，{max} 中的 {current} 已完成（{percentage}%）...'
} );
