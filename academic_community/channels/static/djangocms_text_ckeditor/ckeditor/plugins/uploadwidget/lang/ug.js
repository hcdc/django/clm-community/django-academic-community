// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'ug', {
	abort: 'يۈكلەشنى ئىشلەتكۈچى ئۈزۈۋەتتى.',
	doneOne: 'ھۆججەت مۇۋەپپەقىيەتلىك يۈكلەندى.',
	doneMany: 'مۇۋەپپەقىيەتلىك ھالدا %1 ھۆججەت يۈكلەندى.',
	uploadOne: 'Uploading file ({percentage}%)...', // MISSING
	uploadMany: 'Uploading files, {current} of {max} done ({percentage}%)...' // MISSING
} );
