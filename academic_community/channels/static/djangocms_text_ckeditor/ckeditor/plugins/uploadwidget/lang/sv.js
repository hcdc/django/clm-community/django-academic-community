// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'sv', {
	abort: 'Uppladdning avbruten av användaren.',
	doneOne: 'Filuppladdning lyckades.',
	doneMany: 'Uppladdning av %1 filer lyckades.',
	uploadOne: 'Laddar upp fil ({percentage}%)...',
	uploadMany: 'Laddar upp filer, {current} av {max} färdiga ({percentage}%)...'
} );
