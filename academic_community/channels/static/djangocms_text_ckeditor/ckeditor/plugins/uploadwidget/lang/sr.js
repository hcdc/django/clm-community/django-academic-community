// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'sr', {
	abort: 'Постављање је прекинуто са стране корисника',
	doneOne: 'Датотека је успешно постављена',
	doneMany: '%1  датотека је успешно постављена',
	uploadOne: 'Постављање датотеке  ({percentage}%)...',
	uploadMany: 'Постављање датотека,  {current} of {max} done ({percentage}%)...'
} );
