// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'en-au', {
	abort: 'Upload aborted by the user.',
	doneOne: 'File successfully uploaded.',
	doneMany: 'Successfully uploaded %1 files.',
	uploadOne: 'Uploading file ({percentage}%)...',
	uploadMany: 'Uploading files, {current} of {max} done ({percentage}%)...'
} );
