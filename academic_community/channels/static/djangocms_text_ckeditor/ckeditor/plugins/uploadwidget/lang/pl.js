// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'pl', {
	abort: 'Wysyłanie przerwane przez użytkownika.',
	doneOne: 'Plik został pomyślnie wysłany.',
	doneMany: 'Pomyślnie wysłane pliki: %1.',
	uploadOne: 'Wysyłanie pliku ({percentage}%)...',
	uploadMany: 'Wysyłanie plików, gotowe {current} z {max} ({percentage}%)...'
} );
