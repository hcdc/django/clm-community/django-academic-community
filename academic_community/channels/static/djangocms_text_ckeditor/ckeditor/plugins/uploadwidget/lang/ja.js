// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'ja', {
	abort: 'アップロードを中止しました。',
	doneOne: 'ファイルのアップロードに成功しました。',
	doneMany: '%1個のファイルのアップロードに成功しました。',
	uploadOne: 'ファイルのアップロード中 ({percentage}%)...',
	uploadMany: '{max} 個中 {current} 個のファイルをアップロードしました。 ({percentage}%)...'
} );
