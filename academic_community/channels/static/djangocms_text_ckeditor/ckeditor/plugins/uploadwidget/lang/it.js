// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'it', {
	abort: 'Caricamento interrotto dall\'utente.',
	doneOne: 'Il file è stato caricato correttamente.',
	doneMany: '%1 file sono stati caricati correttamente.',
	uploadOne: 'Caricamento del file ({percentage}%)...',
	uploadMany: 'Caricamento dei file, {current} di {max} completati ({percentage}%)...'
} );
