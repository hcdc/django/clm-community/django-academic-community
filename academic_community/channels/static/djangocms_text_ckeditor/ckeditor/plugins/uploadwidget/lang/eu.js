// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'eu', {
	abort: 'Karga erabiltzaileak bertan behera utzita.',
	doneOne: 'Fitxategia behar bezala kargatu da.',
	doneMany: 'Behar bezala kargatu dira %1 fitxategi.',
	uploadOne: 'Fitxategia kargatzen ({percentage}%)...',
	uploadMany: 'Fitxategiak kargatzen, {current} / {max} eginda ({percentage}%)...'
} );
