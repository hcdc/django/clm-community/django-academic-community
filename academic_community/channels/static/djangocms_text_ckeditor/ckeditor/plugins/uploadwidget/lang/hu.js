// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'hu', {
	abort: 'A feltöltést a felhasználó megszakította.',
	doneOne: 'A fájl sikeresen feltöltve.',
	doneMany: '%1 fájl sikeresen feltöltve.',
	uploadOne: 'Fájl feltöltése ({percentage}%)...',
	uploadMany: 'Fájlok feltöltése, {current}/{max} kész ({percentage}%)...'
} );
