// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'cs', {
	abort: 'Nahrávání zrušeno uživatelem.',
	doneOne: 'Soubor úspěšně nahrán.',
	doneMany: 'Úspěšně nahráno %1 souborů.',
	uploadOne: 'Nahrávání souboru ({percentage}%)...',
	uploadMany: 'Nahrávání souborů, {current} z {max} hotovo ({percentage}%)...'
} );
