// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'az', {
	abort: 'Serverə yükləmə istifadəçi tərəfindən dayandırılıb',
	doneOne: 'Fayl müvəffəqiyyətlə yüklənib',
	doneMany: '%1 fayllar müvəffəqiyyətlə yüklənib',
	uploadOne: 'Faylın yüklənməsi ({percentage}%)',
	uploadMany: 'Faylların yüklənməsi,  {max}-dan {current} hazır ({percentage}%)...'
} );
