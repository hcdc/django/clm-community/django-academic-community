// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'el', {
	abort: 'Αποστολή ακυρώθηκε απο χρήστη.',
	doneOne: 'Αρχείο εστάλη επιτυχώς.',
	doneMany: 'Επιτυχής αποστολή %1 αρχείων.',
	uploadOne: 'Αποστολή αρχείου ({percentage}%)…',
	uploadMany: 'Αποστολή αρχείων, {current} από {max} ολοκληρωμένα ({percentage}%)…'
} );
