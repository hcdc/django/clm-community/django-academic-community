// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'fr', {
	abort: 'Téléversement interrompu par l\'utilisateur.',
	doneOne: 'Fichier téléversé avec succès.',
	doneMany: '%1 fichiers téléversés avec succès.',
	uploadOne: 'Téléversement du fichier en cours ({percentage} %)…',
	uploadMany: 'Téléversement des fichiers en cours, {current} sur {max} effectués ({percentage} %)…'
} );
