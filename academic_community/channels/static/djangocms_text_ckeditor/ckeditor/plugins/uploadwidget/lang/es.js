// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'es', {
	abort: 'Carga abortada por el usuario.',
	doneOne: 'Archivo cargado exitósamente.',
	doneMany: '%1 archivos exitósamente cargados.',
	uploadOne: 'Cargando archivo ({percentage}%)...',
	uploadMany: 'Cargando archivos, {current} de {max} hecho ({percentage}%)...'
} );
