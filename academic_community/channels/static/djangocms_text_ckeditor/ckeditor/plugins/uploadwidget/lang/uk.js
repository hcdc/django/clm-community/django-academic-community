// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'uk', {
	abort: 'Завантаження перервано користувачем.',
	doneOne: 'Файл цілком завантажено.',
	doneMany: 'Цілком завантажено %1 файлів.',
	uploadOne: 'Завантаження файлу ({percentage}%)...',
	uploadMany: 'Завантажено {current} із {max} файлів завершено на ({percentage}%)...'
} );
