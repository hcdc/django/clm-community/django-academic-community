// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'uploadwidget', 'pt-br', {
	abort: 'Envio cancelado pelo usuário.',
	doneOne: 'Arquivo enviado com sucesso.',
	doneMany: 'Enviados %1 arquivos com sucesso.',
	uploadOne: 'Enviando arquivo({percentage}%)...',
	uploadMany: 'Enviando arquivos, {current} de {max} completos ({percentage}%)...'
} );
