// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'hr', {
	button: 'Ubaci isječak kôda',
	codeContents: 'Sadržaj kôda',
	emptySnippetError: 'Isječak kôda ne može biti prazan.',
	language: 'Jezik',
	title: 'Isječak kôda',
	pathName: 'isječak kôda'
} );
