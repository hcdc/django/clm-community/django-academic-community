// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'ca', {
	button: 'Insereix el fragment de codi',
	codeContents: 'Contingut del codi',
	emptySnippetError: 'El fragment de codi no pot estar buit.',
	language: 'Idioma',
	title: 'Fragment de codi',
	pathName: 'fragment de codi'
} );
