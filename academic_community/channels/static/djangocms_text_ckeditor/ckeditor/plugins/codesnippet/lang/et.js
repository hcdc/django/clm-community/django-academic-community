// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'et', {
	button: 'Koodijupi sisestamine',
	codeContents: 'Koodi sisu',
	emptySnippetError: 'Koodijupp ei saa olla tühi.',
	language: 'Keel',
	title: 'Koodijupp',
	pathName: 'koodijupp'
} );
