// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'el', {
	button: 'Εισαγωγή Αποσπάσματος Κώδικα',
	codeContents: 'Περιεχόμενο κώδικα',
	emptySnippetError: 'Δεν γίνεται να είναι κενά τα αποσπάσματα κώδικα.',
	language: 'Γλώσσα',
	title: 'Απόσπασμα κώδικα',
	pathName: 'απόσπασμα κώδικα'
} );
