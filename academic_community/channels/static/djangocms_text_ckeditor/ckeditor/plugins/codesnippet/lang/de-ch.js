// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'de-ch', {
	button: 'Codeschnipsel einfügen',
	codeContents: 'Codeinhalt',
	emptySnippetError: 'Ein Codeschnipsel darf nicht leer sein.',
	language: 'Sprache',
	title: 'Codeschnipsel',
	pathName: 'Codeschnipsel'
} );
