// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'nb', {
	button: 'Sett inn kodesnutt',
	codeContents: 'Kodeinnhold',
	emptySnippetError: 'En kodesnutt kan ikke være tom.',
	language: 'Språk',
	title: 'Kodesnutt',
	pathName: 'kodesnutt'
} );
