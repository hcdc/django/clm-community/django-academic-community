// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'ko', {
	button: '코드 스니펫 삽입',
	codeContents: '코드 본문',
	emptySnippetError: '코드 스니펫은 빈칸일 수 없습니다.',
	language: '언어',
	title: '코드 스니펫',
	pathName: '코드 스니펫'
} );
