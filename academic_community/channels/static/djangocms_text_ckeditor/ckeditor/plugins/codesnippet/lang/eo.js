// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'eo', {
	button: 'Enmeti kodaĵeron',
	codeContents: 'Kodenhavo',
	emptySnippetError: 'Kodaĵero ne povas esti malplena.',
	language: 'Lingvo',
	title: 'Kodaĵero',
	pathName: 'kodaĵero'
} );
