// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'es', {
	button: 'Insertar fragmento de código',
	codeContents: 'Contenido del código',
	emptySnippetError: 'Un fragmento de código no puede estar vacío.',
	language: 'Lenguaje',
	title: 'Fragmento de código',
	pathName: 'fragmento de código'
} );
