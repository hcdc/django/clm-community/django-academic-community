// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'fa', {
	button: 'قرار دادن کد قطعه',
	codeContents: 'محتوای کد',
	emptySnippetError: 'کد نمی تواند خالی باشد.',
	language: 'زبان',
	title: 'کد قطعه',
	pathName: 'کد قطعه'
} );
