// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'pt', {
	button: 'Inserir fragmento de código',
	codeContents: 'Conteúdo do código',
	emptySnippetError: 'A code snippet cannot be empty.', // MISSING
	language: 'Idioma',
	title: 'Segmento de código',
	pathName: 'Fragmento de código'
} );
