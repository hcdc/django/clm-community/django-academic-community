// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'id', {
	button: 'Masukkan potongan kode',
	codeContents: 'Konten kode',
	emptySnippetError: 'Potongan kode tidak boleh kosong',
	language: 'Bahasa',
	title: 'Potongan kode',
	pathName: 'potongan kode'
} );
