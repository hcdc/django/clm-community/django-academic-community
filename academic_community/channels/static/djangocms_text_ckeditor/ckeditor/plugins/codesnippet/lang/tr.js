// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'tr', {
	button: 'Kod parçacığı ekle',
	codeContents: 'Kod',
	emptySnippetError: 'Kod parçacığı boş bırakılamaz',
	language: 'Dil',
	title: 'Kod parçacığı',
	pathName: 'kod parçacığı'
} );
