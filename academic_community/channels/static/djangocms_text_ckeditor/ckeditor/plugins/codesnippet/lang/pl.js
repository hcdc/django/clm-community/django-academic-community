// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'pl', {
	button: 'Wstaw fragment kodu',
	codeContents: 'Treść kodu',
	emptySnippetError: 'Kod nie może być pusty.',
	language: 'Język',
	title: 'Fragment kodu',
	pathName: 'fragment kodu'
} );
