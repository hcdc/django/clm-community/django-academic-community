// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'sr', {
	button: 'Налепи делић кода',
	codeContents: 'Садржај кода',
	emptySnippetError: 'Делић кода не може бити празан',
	language: 'Језик',
	title: 'Делић кода',
	pathName: 'Делић кода'
} );
