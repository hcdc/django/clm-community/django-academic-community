// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'ru', {
	button: 'Вставить сниппет',
	codeContents: 'Содержимое кода',
	emptySnippetError: 'Сниппет не может быть пустым',
	language: 'Язык',
	title: 'Сниппет',
	pathName: 'сниппет'
} );
