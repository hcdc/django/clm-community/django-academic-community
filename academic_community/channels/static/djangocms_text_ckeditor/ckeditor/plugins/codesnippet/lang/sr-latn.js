// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'sr-latn', {
	button: 'Nalepi delić koda',
	codeContents: 'Sadržaj koda',
	emptySnippetError: 'Delić koda ne može biti prazan',
	language: 'Jezik',
	title: 'Delić koda',
	pathName: 'Delić koda'
} );
