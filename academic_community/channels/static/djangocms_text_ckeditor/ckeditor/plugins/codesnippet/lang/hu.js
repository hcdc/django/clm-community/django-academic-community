// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'hu', {
	button: 'Illeszd be a kódtöredéket',
	codeContents: 'Kód tartalom',
	emptySnippetError: 'A kódtöredék nem lehet üres.',
	language: 'Nyelv',
	title: 'Kódtöredék',
	pathName: 'kódtöredék'
} );
