// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'ug', {
	button: 'كود پارچىسى قىستۇرۇش',
	codeContents: 'كود مەزمۇنى',
	emptySnippetError: 'كود پارچىسى بوش قالمايدۇ',
	language: 'تىل',
	title: 'كود پارچىسى',
	pathName: 'كود پارچىسى'
} );
