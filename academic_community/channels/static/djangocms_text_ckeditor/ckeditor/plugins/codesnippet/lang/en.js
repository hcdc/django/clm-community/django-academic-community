// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'en', {
	button: 'Insert Code Snippet',
	codeContents: 'Code content',
	emptySnippetError: 'A code snippet cannot be empty.',
	language: 'Language',
	title: 'Code snippet',
	pathName: 'code snippet'
} );
