// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'zh-cn', {
	button: '插入代码段',
	codeContents: '代码内容',
	emptySnippetError: '插入的代码不能为空。',
	language: '代码语言',
	title: '代码段',
	pathName: '代码段'
} );
