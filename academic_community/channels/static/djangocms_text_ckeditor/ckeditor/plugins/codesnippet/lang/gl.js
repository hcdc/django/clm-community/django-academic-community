// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'gl', {
	button: 'Inserir fragmento de código',
	codeContents: 'Contido do código',
	emptySnippetError: 'Un fragmento de código non pode estar baleiro.',
	language: 'Linguaxe',
	title: 'Fragmento de código',
	pathName: 'fragmento de código'
} );
