// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'ro', {
	button: 'Adaugă segment de cod',
	codeContents: 'Conținutul codului',
	emptySnippetError: 'Un segment de cod nu poate fi gol.',
	language: 'Limba',
	title: 'Segment de cod',
	pathName: 'segment de cod'
} );
