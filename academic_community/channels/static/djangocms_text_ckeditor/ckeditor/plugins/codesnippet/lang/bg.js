// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'bg', {
	button: 'Въвеждане на блок с код',
	codeContents: 'Съдържание на кода',
	emptySnippetError: 'Блока с код не може да бъде празен.',
	language: 'Език',
	title: 'Блок с код',
	pathName: 'блок с код'
} );
