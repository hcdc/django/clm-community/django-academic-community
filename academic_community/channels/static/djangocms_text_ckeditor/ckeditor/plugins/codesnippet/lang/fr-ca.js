// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'fr-ca', {
	button: 'Insérer du code',
	codeContents: 'Code content', // MISSING
	emptySnippetError: 'A code snippet cannot be empty.', // MISSING
	language: 'Language', // MISSING
	title: 'Code snippet', // MISSING
	pathName: 'code snippet' // MISSING
} );
