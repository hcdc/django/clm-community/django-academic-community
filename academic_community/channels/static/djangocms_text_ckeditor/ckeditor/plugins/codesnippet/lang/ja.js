// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'ja', {
	button: 'コードスニペットを挿入',
	codeContents: 'コード内容',
	emptySnippetError: 'コードスニペットを入力してください。',
	language: '言語',
	title: 'コードスニペット',
	pathName: 'コードスニペット'
} );
