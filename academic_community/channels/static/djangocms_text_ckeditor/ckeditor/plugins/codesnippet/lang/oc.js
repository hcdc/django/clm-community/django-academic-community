// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'oc', {
	button: 'Inserir un extrait de còdi',
	codeContents: 'Còdi',
	emptySnippetError: 'Un extrait de còdi pòt pas èsser void.',
	language: 'Lenga',
	title: 'Extrait de còdi',
	pathName: 'extrait de còdi'
} );
