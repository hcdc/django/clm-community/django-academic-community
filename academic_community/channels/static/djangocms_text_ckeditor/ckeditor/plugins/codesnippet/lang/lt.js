// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'lt', {
	button: 'Įterpkite kodo gabaliuką',
	codeContents: 'Kodo turinys',
	emptySnippetError: 'Kodo fragmentas negali būti tusčias.',
	language: 'Kalba',
	title: 'Kodo fragmentas',
	pathName: 'kodo fragmentas'
} );
