// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'vi', {
	button: 'Chèn đoạn mã',
	codeContents: 'Nội dung mã',
	emptySnippetError: 'Một đoạn mã không thể để trống.',
	language: 'Ngôn ngữ',
	title: 'Đoạn mã',
	pathName: 'mã dính'
} );
