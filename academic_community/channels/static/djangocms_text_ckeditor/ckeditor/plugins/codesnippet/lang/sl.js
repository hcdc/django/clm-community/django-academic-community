// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'sl', {
	button: 'Vstavi odsek kode',
	codeContents: 'Vsebina kode',
	emptySnippetError: 'Odsek kode ne more biti prazen.',
	language: 'Jezik',
	title: 'Odsek kode',
	pathName: 'odsek kode'
} );
