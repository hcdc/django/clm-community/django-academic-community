// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'he', {
	button: 'הכנס קטע קוד',
	codeContents: 'תוכן קוד',
	emptySnippetError: 'קטע קוד לא יכול להיות ריק.',
	language: 'שפה',
	title: 'קטע קוד',
	pathName: 'code snippet' // MISSING
} );
