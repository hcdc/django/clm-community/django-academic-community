// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'uk', {
	button: 'Вставити фрагмент коду',
	codeContents: 'Код',
	emptySnippetError: 'Фрагмент коду не може бути порожнім.',
	language: 'Мова',
	title: 'Фрагмент коду',
	pathName: 'фрагмент коду'
} );
