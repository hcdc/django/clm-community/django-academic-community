// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'sq', {
	button: 'Shto kod copëze',
	codeContents: 'Përmbajtja e kodit',
	emptySnippetError: 'Copëza e kodit nuk mund të jetë e zbrazët.',
	language: 'Gjuha',
	title: 'Copëza e kodit',
	pathName: 'copëza e kodit'
} );
