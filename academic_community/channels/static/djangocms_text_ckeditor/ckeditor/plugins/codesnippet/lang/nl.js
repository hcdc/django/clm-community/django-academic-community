// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'nl', {
	button: 'Stuk code invoegen',
	codeContents: 'Code',
	emptySnippetError: 'Een stuk code kan niet leeg zijn.',
	language: 'Taal',
	title: 'Stuk code',
	pathName: 'stuk code'
} );
