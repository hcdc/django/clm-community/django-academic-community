// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'ku', {
	button: 'تێخستنی تیتکی کۆد',
	codeContents: 'ناوەڕۆکی کۆد',
	emptySnippetError: 'تیتکی کۆد نابێت بەتاڵ بێت.',
	language: 'زمان',
	title: 'تیتکی کۆد',
	pathName: 'تیتکی کۆد'
} );
