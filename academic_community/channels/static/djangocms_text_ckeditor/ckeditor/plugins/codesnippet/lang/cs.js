// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'cs', {
	button: 'Vložit úryvek kódu',
	codeContents: 'Obsah kódu',
	emptySnippetError: 'Úryvek kódu nemůže být prázdný.',
	language: 'Jazyk',
	title: 'Úryvek kódu',
	pathName: 'úryvek kódu'
} );
