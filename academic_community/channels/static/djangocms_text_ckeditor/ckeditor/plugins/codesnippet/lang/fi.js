// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'fi', {
	button: 'Lisää koodileike',
	codeContents: 'Koodisisältö',
	emptySnippetError: 'Koodileike ei voi olla tyhjä.',
	language: 'Kieli',
	title: 'Koodileike',
	pathName: 'koodileike'
} );
