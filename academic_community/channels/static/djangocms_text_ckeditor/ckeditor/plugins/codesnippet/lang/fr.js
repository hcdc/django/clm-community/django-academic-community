// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'fr', {
	button: 'Insérer un extrait de code',
	codeContents: 'Code',
	emptySnippetError: 'Un extrait de code ne peut pas être vide.',
	language: 'Langue',
	title: 'Extrait de code',
	pathName: 'extrait de code'
} );
