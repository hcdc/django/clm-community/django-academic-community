// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'pt-br', {
	button: 'Inserir fragmento de código',
	codeContents: 'Conteúdo do código',
	emptySnippetError: 'Um fragmento de código não pode ser vazio',
	language: 'Idioma',
	title: 'Fragmento de código',
	pathName: 'fragmento de código'
} );
