// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'codesnippet', 'lv', {
	button: 'Ievietot koda fragmentu',
	codeContents: 'Koda saturs',
	emptySnippetError: 'Koda fragments nevar būt tukšs.',
	language: 'Valoda',
	title: 'Koda fragments',
	pathName: 'koda fragments'
} );
