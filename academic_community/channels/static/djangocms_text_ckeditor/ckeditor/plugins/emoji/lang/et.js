// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'et', {
	searchPlaceholder: 'Otsi emotikoni...',
	searchLabel: 'Lahter, mille abil paneelil otsida ja filtreerida emotikone.',
	navigationLabel: 'Emotikonide gruppide vahel liikumine.',
	title: 'Emotikonide loend',
	groups: {
		people: 'Inimesed',
		nature: 'Loodus ja loomad',
		food: 'Toit ja joogid',
		travel: 'Reisimine ja kohad',
		activities: 'Tegevused',
		objects: 'Asjad',
		symbols: 'Sümbolid',
		flags: 'Lipud'
	}
} );
