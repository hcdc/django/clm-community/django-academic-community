// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'sr-latn', {
	searchPlaceholder: 'Traži emoji...',
	searchLabel: 'Polje za pretraživanje i filtriranje emojia u panelu.',
	navigationLabel: 'Grupna navigacija za emoji sekcije',
	title: 'Lista emojia',
	groups: {
		people: 'Lljudi',
		nature: 'Priroda i životinje',
		food: 'Hrana i piće',
		travel: 'Putovanja i mesta',
		activities: 'Aktivnosti',
		objects: 'Predmeti',
		symbols: 'Simboli',
		flags: 'Zastave'
	}
} );
