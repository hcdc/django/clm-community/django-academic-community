// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'hr', {
	searchPlaceholder: 'Traži emoji',
	searchLabel: 'Polje za unos odgovorno za traženje i filtriranje emoji-a.',
	navigationLabel: 'Navigacijska grupa za emoji sekcije',
	title: 'Lista emoji-a',
	groups: {
		people: 'Ljudi',
		nature: 'Priroda i životinje',
		food: 'Hrana i pića',
		travel: 'Putovanja i mjesta',
		activities: 'Aktivnosti',
		objects: 'Objekti',
		symbols: 'Simboli',
		flags: 'Zastave'
	}
} );
