// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'pl', {
	searchPlaceholder: 'Wyszukaj emoji...',
	searchLabel: 'Pole odpowiedzialne za wyszukiwanie i filtrowanie emoji wewnątrz panelu.',
	navigationLabel: 'Groups navigation for emoji sections.', // MISSING
	title: 'Lista emoji',
	groups: {
		people: 'Ludzie',
		nature: 'Natura i zwierzęta',
		food: 'Jedzenie i picie',
		travel: 'Podróże i miejsca',
		activities: 'Aktywności',
		objects: 'Obiekty',
		symbols: 'Symbole',
		flags: 'Flagi'
	}
} );
