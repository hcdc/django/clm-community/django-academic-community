// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'da', {
	searchPlaceholder: 'Søg smileys...',
	searchLabel: 'Input field responsible for searching and filtering emoji inside panel.', // MISSING
	navigationLabel: 'Groups navigation for emoji sections.', // MISSING
	title: 'Emoji List', // MISSING
	groups: {
		people: 'People', // MISSING
		nature: 'Nature and animals', // MISSING
		food: 'Food and drinks', // MISSING
		travel: 'Travel and places', // MISSING
		activities: 'Activities', // MISSING
		objects: 'Objects', // MISSING
		symbols: 'Symbols', // MISSING
		flags: 'Flags' // MISSING
	}
} );
