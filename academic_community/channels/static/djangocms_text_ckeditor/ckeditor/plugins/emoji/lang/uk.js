// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'uk', {
	searchPlaceholder: 'Пошук емодзі…',
	searchLabel: 'Поле введення, відповідальне за пошук та фільтрацію емодзі всередині панелі.',
	navigationLabel: 'Групова навігація по розділах емодзі.',
	title: 'Список емодзі',
	groups: {
		people: 'Люди',
		nature: 'Природа та тварини',
		food: 'Їжа та напої',
		travel: 'Подорожі та місця',
		activities: 'Діяльність',
		objects: 'Об\'єкти',
		symbols: 'Символи',
		flags: 'Прапори'
	}
} );
