// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'fr', {
	searchPlaceholder: 'Chercher un émoticône',
	searchLabel: 'Champ de saisie chargé de rechercher et de filtrer les émoticônes à l\'intérieur du panneau',
	navigationLabel: 'Catégorisation des sections d\'émoticône',
	title: 'Liste des émoticônes',
	groups: {
		people: 'Personnes',
		nature: 'Nature et animal',
		food: 'Nourriture et boisson',
		travel: 'Lieu et voyage',
		activities: 'Activités',
		objects: 'Objets',
		symbols: 'Symboles',
		flags: 'Drapeaux'
	}
} );
