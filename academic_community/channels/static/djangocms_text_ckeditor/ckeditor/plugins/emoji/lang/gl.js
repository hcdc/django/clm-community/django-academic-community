// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'gl', {
	searchPlaceholder: 'Buscar emoji…',
	searchLabel: 'Campo de entrada encargado de buscar e filtrar os emojis no panel.',
	navigationLabel: 'Navegación de grupos para seccións de emojis.',
	title: 'Lista de emojis',
	groups: {
		people: 'Xente',
		nature: 'Natureza e aimais',
		food: 'Comida e bebida',
		travel: 'Viaxes e lugares',
		activities: 'Actividades',
		objects: 'Obxectos',
		symbols: 'Símbolos',
		flags: 'Bandeiras'
	}
} );
