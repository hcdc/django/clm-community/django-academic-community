// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'sk', {
	searchPlaceholder: 'Nájsť emodži...',
	searchLabel: 'Vstupné pole na hľadanie a filtrovanie emodži v paneli.',
	navigationLabel: 'Emodži podľa skupín.',
	title: 'Zoznam emodži',
	groups: {
		people: 'Ľudia',
		nature: 'Príroda a zvieratá',
		food: 'Jedlá a nápoje',
		travel: 'Cestovanie a miesta',
		activities: 'Činnosti',
		objects: 'Objekty',
		symbols: 'Symboly',
		flags: 'Vlajky'
	}
} );
