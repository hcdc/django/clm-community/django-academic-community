// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'pt-br', {
	searchPlaceholder: 'Buscar emoji...',
	searchLabel: 'Campo de texto responsável por buscar e filtrar emojis dentro do painel.',
	navigationLabel: 'Agrupa a navegação das categorias de emojis.',
	title: 'Lista de Emojis',
	groups: {
		people: 'Pessoas',
		nature: 'Natureza e animais',
		food: 'Comidas e bebidas',
		travel: 'Viagem e Lugares',
		activities: 'Atividades',
		objects: 'Objetos',
		symbols: 'Símbolos',
		flags: 'Bandeiras'
	}
} );
