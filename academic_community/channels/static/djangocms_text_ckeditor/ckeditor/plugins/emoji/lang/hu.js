// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'hu', {
	searchPlaceholder: 'Emoji keresése...',
	searchLabel: 'A beviteli mezőben kereshetőek és szűrhetőek az emojik a panelban.',
	navigationLabel: 'Csoport navigáció az emoji szekciókhoz.',
	title: 'Emoji lista',
	groups: {
		people: 'Emberek',
		nature: 'Természet és állatok',
		food: 'Ételek és italok',
		travel: 'Utazás és helyek',
		activities: 'Tevékenységek',
		objects: 'Tárgyak',
		symbols: 'Szimbólumok',
		flags: 'Zászlók'
	}
} );
