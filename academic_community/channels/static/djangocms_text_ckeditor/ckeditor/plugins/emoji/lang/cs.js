// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'cs', {
	searchPlaceholder: 'Vyhledat emodži...',
	searchLabel: 'Vstupní pole používané pro vyhledávání a filtrování emodži v rámci panelu.',
	navigationLabel: 'Procházení podle skupin emodži.',
	title: 'Seznam emodži',
	groups: {
		people: 'Lidé',
		nature: 'Příroda a zvířata',
		food: 'Jídlo a pití',
		travel: 'Cestování a místa',
		activities: 'Činnosti',
		objects: 'Objekty',
		symbols: 'Symboly',
		flags: 'Vlajky'
	}
} );
