// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'de', {
	searchPlaceholder: 'Emoji suchen…',
	searchLabel: 'Input field responsible for searching and filtering emoji inside panel.', // MISSING
	navigationLabel: 'Groups navigation for emoji sections.', // MISSING
	title: 'Emoji-Liste',
	groups: {
		people: 'Personen',
		nature: 'Natur und Tiere',
		food: 'Essen und Getränke',
		travel: 'Reisen und Orte',
		activities: 'Aktivitäten',
		objects: 'Objekte',
		symbols: 'Symbole',
		flags: 'Flaggen'
	}
} );
