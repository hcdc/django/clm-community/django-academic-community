// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'de-ch', {
	searchPlaceholder: 'Emoji suchen…',
	searchLabel: 'Das Eingabefeld ermöglicht die Suche nach Emojis.',
	navigationLabel: 'Navigieren Sie zwischen den Emoji-Kategorien.',
	title: 'Emoji-Liste',
	groups: {
		people: 'Personen',
		nature: 'Natur und Tiere',
		food: 'Essen und Getränke',
		travel: 'Reisen und Orte',
		activities: 'Aktivitäten',
		objects: 'Objekte',
		symbols: 'Symbole',
		flags: 'Flaggen'
	}
} );
