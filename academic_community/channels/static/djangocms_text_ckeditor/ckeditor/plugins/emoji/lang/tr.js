// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'tr', {
	searchPlaceholder: 'Emoji ara...',
	searchLabel: 'Panel içindeki emojiyi aramaya ve filtrelemeye ait giriş alanı.',
	navigationLabel: 'Emoji bölümleri için gezinti grupları',
	title: 'Emoji Listesi',
	groups: {
		people: 'İnsanlar',
		nature: 'Doğa ve hayvanlar',
		food: 'Yiyecek ve içecekler',
		travel: 'Seyahat ve mekanlar',
		activities: 'Faaliyetler',
		objects: 'Nesneler',
		symbols: 'Semboller',
		flags: 'Bayraklar'
	}
} );
