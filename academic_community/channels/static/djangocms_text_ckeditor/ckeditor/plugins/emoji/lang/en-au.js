// SPDX-FileCopyrightText: 2003-2022 CKSource Holding sp. z o.o.
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
//

CKEDITOR.plugins.setLang( 'emoji', 'en-au', {
	searchPlaceholder: 'Search emoji…',
	searchLabel: 'Input field responsible for searching and filtering emoji inside panel.',
	navigationLabel: 'Groups navigation for emoji sections.',
	title: 'Emoji List',
	groups: {
		people: 'People',
		nature: 'Nature and animals',
		food: 'Food and drinks',
		travel: 'Travel and places',
		activities: 'Activities',
		objects: 'Objects',
		symbols: 'Symbols',
		flags: 'Flags'
	}
} );
