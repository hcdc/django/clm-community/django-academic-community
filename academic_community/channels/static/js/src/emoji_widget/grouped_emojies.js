// SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: EUPL-1.2

var emojiData = [
  {
    "text": "Smileys & Emotion",
    "children": [
      {
        "text": "\ud83d\ude00",
        "id": "GRINNING FACE",
        "alt_names": [
          "grinning",
          ":D"
        ]
      },
      {
        "text": "\ud83d\ude03",
        "id": "SMILING FACE WITH OPEN MOUTH",
        "alt_names": [
          "smiley",
          ":)",
          "=)",
          "=-)"
        ]
      },
      {
        "text": "\ud83d\ude04",
        "id": "SMILING FACE WITH OPEN MOUTH AND SMILING EYES",
        "alt_names": [
          "smile",
          ":)",
          "C:",
          "c:",
          ":D",
          ":-D"
        ]
      },
      {
        "text": "\ud83d\ude01",
        "id": "GRINNING FACE WITH SMILING EYES",
        "alt_names": [
          "grin"
        ]
      },
      {
        "text": "\ud83d\ude06",
        "id": "SMILING FACE WITH OPEN MOUTH AND TIGHTLY-CLOSED EYES",
        "alt_names": [
          "laughing",
          "satisfied",
          ":>",
          ":->"
        ]
      },
      {
        "text": "\ud83d\ude05",
        "id": "SMILING FACE WITH OPEN MOUTH AND COLD SWEAT",
        "alt_names": [
          "sweat_smile"
        ]
      },
      {
        "text": "\ud83e\udd23",
        "id": "ROLLING ON THE FLOOR LAUGHING",
        "alt_names": [
          "rolling_on_the_floor_laughing",
          "rofl"
        ]
      },
      {
        "text": "\ud83d\ude02",
        "id": "FACE WITH TEARS OF JOY",
        "alt_names": [
          "joy"
        ]
      },
      {
        "text": "\ud83d\ude42",
        "id": "SLIGHTLY SMILING FACE",
        "alt_names": [
          "slightly_smiling_face",
          ":)",
          "(:",
          ":-)"
        ]
      },
      {
        "text": "\ud83d\ude43",
        "id": "UPSIDE-DOWN FACE",
        "alt_names": [
          "upside_down_face"
        ]
      },
      {
        "text": "\ud83d\ude09",
        "id": "WINKING FACE",
        "alt_names": [
          "wink",
          ";)",
          ";)",
          ";-)"
        ]
      },
      {
        "text": "\ud83d\ude0a",
        "id": "SMILING FACE WITH SMILING EYES",
        "alt_names": [
          "blush",
          ":)"
        ]
      },
      {
        "text": "\ud83d\ude07",
        "id": "SMILING FACE WITH HALO",
        "alt_names": [
          "innocent"
        ]
      },
      {
        "text": "\ud83e\udd70",
        "id": "SMILING FACE WITH SMILING EYES AND THREE HEARTS",
        "alt_names": [
          "smiling_face_with_3_hearts"
        ]
      },
      {
        "text": "\ud83d\ude0d",
        "id": "SMILING FACE WITH HEART-SHAPED EYES",
        "alt_names": [
          "heart_eyes"
        ]
      },
      {
        "text": "\ud83e\udd29",
        "id": "GRINNING FACE WITH STAR EYES",
        "alt_names": [
          "star-struck",
          "grinning_face_with_star_eyes"
        ]
      },
      {
        "text": "\ud83d\ude18",
        "id": "FACE THROWING A KISS",
        "alt_names": [
          "kissing_heart",
          ":*",
          ":-*"
        ]
      },
      {
        "text": "\ud83d\ude17",
        "id": "KISSING FACE",
        "alt_names": [
          "kissing"
        ]
      },
      {
        "text": "\ud83d\ude1a",
        "id": "KISSING FACE WITH CLOSED EYES",
        "alt_names": [
          "kissing_closed_eyes"
        ]
      },
      {
        "text": "\ud83d\ude19",
        "id": "KISSING FACE WITH SMILING EYES",
        "alt_names": [
          "kissing_smiling_eyes"
        ]
      },
      {
        "text": "\ud83e\udd72",
        "id": "SMILING FACE WITH TEAR",
        "alt_names": [
          "smiling_face_with_tear"
        ]
      },
      {
        "text": "\ud83d\ude0b",
        "id": "FACE SAVOURING DELICIOUS FOOD",
        "alt_names": [
          "yum"
        ]
      },
      {
        "text": "\ud83d\ude1b",
        "id": "FACE WITH STUCK-OUT TONGUE",
        "alt_names": [
          "stuck_out_tongue",
          ":p",
          ":p",
          ":-p",
          ":P",
          ":-P",
          ":b",
          ":-b"
        ]
      },
      {
        "text": "\ud83d\ude1c",
        "id": "FACE WITH STUCK-OUT TONGUE AND WINKING EYE",
        "alt_names": [
          "stuck_out_tongue_winking_eye",
          ";p",
          ";p",
          ";-p",
          ";b",
          ";-b",
          ";P",
          ";-P"
        ]
      },
      {
        "text": "\ud83e\udd2a",
        "id": "GRINNING FACE WITH ONE LARGE AND ONE SMALL EYE",
        "alt_names": [
          "zany_face",
          "grinning_face_with_one_large_and_one_small_eye"
        ]
      },
      {
        "text": "\ud83d\ude1d",
        "id": "FACE WITH STUCK-OUT TONGUE AND TIGHTLY-CLOSED EYES",
        "alt_names": [
          "stuck_out_tongue_closed_eyes"
        ]
      },
      {
        "text": "\ud83e\udd11",
        "id": "MONEY-MOUTH FACE",
        "alt_names": [
          "money_mouth_face"
        ]
      },
      {
        "text": "\ud83e\udd17",
        "id": "HUGGING FACE",
        "alt_names": [
          "hugging_face",
          "hugs"
        ]
      },
      {
        "text": "\ud83e\udd2d",
        "id": "SMILING FACE WITH SMILING EYES AND HAND COVERING MOUTH",
        "alt_names": [
          "face_with_hand_over_mouth",
          "smiling_face_with_smiling_eyes_and_hand_covering_mouth"
        ]
      },
      {
        "text": "\ud83e\udd2b",
        "id": "FACE WITH FINGER COVERING CLOSED LIPS",
        "alt_names": [
          "shushing_face",
          "face_with_finger_covering_closed_lips"
        ]
      },
      {
        "text": "\ud83e\udd14",
        "id": "THINKING FACE",
        "alt_names": [
          "thinking_face",
          "thinking"
        ]
      },
      {
        "text": "\ud83e\udd10",
        "id": "ZIPPER-MOUTH FACE",
        "alt_names": [
          "zipper_mouth_face"
        ]
      },
      {
        "text": "\ud83e\udd28",
        "id": "FACE WITH ONE EYEBROW RAISED",
        "alt_names": [
          "face_with_raised_eyebrow",
          "face_with_one_eyebrow_raised"
        ]
      },
      {
        "text": "\ud83d\ude10",
        "id": "NEUTRAL FACE",
        "alt_names": [
          "neutral_face",
          ":|",
          ":-|"
        ]
      },
      {
        "text": "\ud83d\ude11",
        "id": "EXPRESSIONLESS FACE",
        "alt_names": [
          "expressionless"
        ]
      },
      {
        "text": "\ud83d\ude36",
        "id": "FACE WITHOUT MOUTH",
        "alt_names": [
          "no_mouth"
        ]
      },
      {
        "text": "\ud83d\ude0f",
        "id": "SMIRKING FACE",
        "alt_names": [
          "smirk"
        ]
      },
      {
        "text": "\ud83d\ude12",
        "id": "UNAMUSED FACE",
        "alt_names": [
          "unamused",
          ":("
        ]
      },
      {
        "text": "\ud83d\ude44",
        "id": "FACE WITH ROLLING EYES",
        "alt_names": [
          "face_with_rolling_eyes",
          "roll_eyes"
        ]
      },
      {
        "text": "\ud83d\ude2c",
        "id": "GRIMACING FACE",
        "alt_names": [
          "grimacing"
        ]
      },
      {
        "text": "\ud83e\udd25",
        "id": "LYING FACE",
        "alt_names": [
          "lying_face"
        ]
      },
      {
        "text": "\ud83d\ude0c",
        "id": "RELIEVED FACE",
        "alt_names": [
          "relieved"
        ]
      },
      {
        "text": "\ud83d\ude14",
        "id": "PENSIVE FACE",
        "alt_names": [
          "pensive"
        ]
      },
      {
        "text": "\ud83d\ude2a",
        "id": "SLEEPY FACE",
        "alt_names": [
          "sleepy"
        ]
      },
      {
        "text": "\ud83e\udd24",
        "id": "DROOLING FACE",
        "alt_names": [
          "drooling_face"
        ]
      },
      {
        "text": "\ud83d\ude34",
        "id": "SLEEPING FACE",
        "alt_names": [
          "sleeping"
        ]
      },
      {
        "text": "\ud83d\ude37",
        "id": "FACE WITH MEDICAL MASK",
        "alt_names": [
          "mask"
        ]
      },
      {
        "text": "\ud83e\udd12",
        "id": "FACE WITH THERMOMETER",
        "alt_names": [
          "face_with_thermometer"
        ]
      },
      {
        "text": "\ud83e\udd15",
        "id": "FACE WITH HEAD-BANDAGE",
        "alt_names": [
          "face_with_head_bandage"
        ]
      },
      {
        "text": "\ud83e\udd22",
        "id": "NAUSEATED FACE",
        "alt_names": [
          "nauseated_face"
        ]
      },
      {
        "text": "\ud83e\udd2e",
        "id": "FACE WITH OPEN MOUTH VOMITING",
        "alt_names": [
          "face_vomiting",
          "face_with_open_mouth_vomiting"
        ]
      },
      {
        "text": "\ud83e\udd27",
        "id": "SNEEZING FACE",
        "alt_names": [
          "sneezing_face"
        ]
      },
      {
        "text": "\ud83e\udd75",
        "id": "OVERHEATED FACE",
        "alt_names": [
          "hot_face"
        ]
      },
      {
        "text": "\ud83e\udd76",
        "id": "FREEZING FACE",
        "alt_names": [
          "cold_face"
        ]
      },
      {
        "text": "\ud83e\udd74",
        "id": "FACE WITH UNEVEN EYES AND WAVY MOUTH",
        "alt_names": [
          "woozy_face"
        ]
      },
      {
        "text": "\ud83d\ude35",
        "id": "DIZZY FACE",
        "alt_names": [
          "dizzy_face"
        ]
      },
      {
        "text": "\ud83e\udd2f",
        "id": "SHOCKED FACE WITH EXPLODING HEAD",
        "alt_names": [
          "exploding_head",
          "shocked_face_with_exploding_head"
        ]
      },
      {
        "text": "\ud83e\udd20",
        "id": "FACE WITH COWBOY HAT",
        "alt_names": [
          "face_with_cowboy_hat",
          "cowboy_hat_face"
        ]
      },
      {
        "text": "\ud83e\udd73",
        "id": "FACE WITH PARTY HORN AND PARTY HAT",
        "alt_names": [
          "partying_face"
        ]
      },
      {
        "text": "\ud83e\udd78",
        "id": "DISGUISED FACE",
        "alt_names": [
          "disguised_face"
        ]
      },
      {
        "text": "\ud83d\ude0e",
        "id": "SMILING FACE WITH SUNGLASSES",
        "alt_names": [
          "sunglasses",
          "8)"
        ]
      },
      {
        "text": "\ud83e\udd13",
        "id": "NERD FACE",
        "alt_names": [
          "nerd_face"
        ]
      },
      {
        "text": "\ud83e\uddd0",
        "id": "FACE WITH MONOCLE",
        "alt_names": [
          "face_with_monocle"
        ]
      },
      {
        "text": "\ud83d\ude15",
        "id": "CONFUSED FACE",
        "alt_names": [
          "confused",
          ":\\",
          ":-\\",
          ":/",
          ":-/"
        ]
      },
      {
        "text": "\ud83d\ude1f",
        "id": "WORRIED FACE",
        "alt_names": [
          "worried"
        ]
      },
      {
        "text": "\ud83d\ude41",
        "id": "SLIGHTLY FROWNING FACE",
        "alt_names": [
          "slightly_frowning_face"
        ]
      },
      {
        "text": "\ud83d\ude2e",
        "id": "FACE WITH OPEN MOUTH",
        "alt_names": [
          "open_mouth",
          ":o",
          ":-o",
          ":O",
          ":-O"
        ]
      },
      {
        "text": "\ud83d\ude2f",
        "id": "HUSHED FACE",
        "alt_names": [
          "hushed"
        ]
      },
      {
        "text": "\ud83d\ude32",
        "id": "ASTONISHED FACE",
        "alt_names": [
          "astonished"
        ]
      },
      {
        "text": "\ud83d\ude33",
        "id": "FLUSHED FACE",
        "alt_names": [
          "flushed"
        ]
      },
      {
        "text": "\ud83e\udd7a",
        "id": "FACE WITH PLEADING EYES",
        "alt_names": [
          "pleading_face"
        ]
      },
      {
        "text": "\ud83d\ude26",
        "id": "FROWNING FACE WITH OPEN MOUTH",
        "alt_names": [
          "frowning"
        ]
      },
      {
        "text": "\ud83d\ude27",
        "id": "ANGUISHED FACE",
        "alt_names": [
          "anguished",
          "D:"
        ]
      },
      {
        "text": "\ud83d\ude28",
        "id": "FEARFUL FACE",
        "alt_names": [
          "fearful"
        ]
      },
      {
        "text": "\ud83d\ude30",
        "id": "FACE WITH OPEN MOUTH AND COLD SWEAT",
        "alt_names": [
          "cold_sweat"
        ]
      },
      {
        "text": "\ud83d\ude25",
        "id": "DISAPPOINTED BUT RELIEVED FACE",
        "alt_names": [
          "disappointed_relieved"
        ]
      },
      {
        "text": "\ud83d\ude22",
        "id": "CRYING FACE",
        "alt_names": [
          "cry",
          ":'(",
          ":'("
        ]
      },
      {
        "text": "\ud83d\ude2d",
        "id": "LOUDLY CRYING FACE",
        "alt_names": [
          "sob",
          ":'("
        ]
      },
      {
        "text": "\ud83d\ude31",
        "id": "FACE SCREAMING IN FEAR",
        "alt_names": [
          "scream"
        ]
      },
      {
        "text": "\ud83d\ude16",
        "id": "CONFOUNDED FACE",
        "alt_names": [
          "confounded"
        ]
      },
      {
        "text": "\ud83d\ude23",
        "id": "PERSEVERING FACE",
        "alt_names": [
          "persevere"
        ]
      },
      {
        "text": "\ud83d\ude1e",
        "id": "DISAPPOINTED FACE",
        "alt_names": [
          "disappointed",
          ":(",
          "):",
          ":(",
          ":-("
        ]
      },
      {
        "text": "\ud83d\ude13",
        "id": "FACE WITH COLD SWEAT",
        "alt_names": [
          "sweat"
        ]
      },
      {
        "text": "\ud83d\ude29",
        "id": "WEARY FACE",
        "alt_names": [
          "weary"
        ]
      },
      {
        "text": "\ud83d\ude2b",
        "id": "TIRED FACE",
        "alt_names": [
          "tired_face"
        ]
      },
      {
        "text": "\ud83e\udd71",
        "id": "YAWNING FACE",
        "alt_names": [
          "yawning_face"
        ]
      },
      {
        "text": "\ud83d\ude24",
        "id": "FACE WITH LOOK OF TRIUMPH",
        "alt_names": [
          "triumph"
        ]
      },
      {
        "text": "\ud83d\ude21",
        "id": "POUTING FACE",
        "alt_names": [
          "rage",
          "pout"
        ]
      },
      {
        "text": "\ud83d\ude20",
        "id": "ANGRY FACE",
        "alt_names": [
          "angry",
          ">:(",
          ">:-("
        ]
      },
      {
        "text": "\ud83e\udd2c",
        "id": "SERIOUS FACE WITH SYMBOLS COVERING MOUTH",
        "alt_names": [
          "face_with_symbols_on_mouth",
          "serious_face_with_symbols_covering_mouth"
        ]
      },
      {
        "text": "\ud83d\ude08",
        "id": "SMILING FACE WITH HORNS",
        "alt_names": [
          "smiling_imp"
        ]
      },
      {
        "text": "\ud83d\udc7f",
        "id": "IMP",
        "alt_names": [
          "imp"
        ]
      },
      {
        "text": "\ud83d\udc80",
        "id": "SKULL",
        "alt_names": [
          "skull"
        ]
      },
      {
        "text": "\ud83d\udca9",
        "id": "PILE OF POO",
        "alt_names": [
          "hankey",
          "poop",
          "shit"
        ]
      },
      {
        "text": "\ud83e\udd21",
        "id": "CLOWN FACE",
        "alt_names": [
          "clown_face"
        ]
      },
      {
        "text": "\ud83d\udc79",
        "id": "JAPANESE OGRE",
        "alt_names": [
          "japanese_ogre"
        ]
      },
      {
        "text": "\ud83d\udc7a",
        "id": "JAPANESE GOBLIN",
        "alt_names": [
          "japanese_goblin"
        ]
      },
      {
        "text": "\ud83d\udc7b",
        "id": "GHOST",
        "alt_names": [
          "ghost"
        ]
      },
      {
        "text": "\ud83d\udc7d",
        "id": "EXTRATERRESTRIAL ALIEN",
        "alt_names": [
          "alien"
        ]
      },
      {
        "text": "\ud83d\udc7e",
        "id": "ALIEN MONSTER",
        "alt_names": [
          "space_invader"
        ]
      },
      {
        "text": "\ud83e\udd16",
        "id": "ROBOT FACE",
        "alt_names": [
          "robot_face",
          "robot"
        ]
      },
      {
        "text": "\ud83d\ude3a",
        "id": "SMILING CAT FACE WITH OPEN MOUTH",
        "alt_names": [
          "smiley_cat"
        ]
      },
      {
        "text": "\ud83d\ude38",
        "id": "GRINNING CAT FACE WITH SMILING EYES",
        "alt_names": [
          "smile_cat"
        ]
      },
      {
        "text": "\ud83d\ude39",
        "id": "CAT FACE WITH TEARS OF JOY",
        "alt_names": [
          "joy_cat"
        ]
      },
      {
        "text": "\ud83d\ude3b",
        "id": "SMILING CAT FACE WITH HEART-SHAPED EYES",
        "alt_names": [
          "heart_eyes_cat"
        ]
      },
      {
        "text": "\ud83d\ude3c",
        "id": "CAT FACE WITH WRY SMILE",
        "alt_names": [
          "smirk_cat"
        ]
      },
      {
        "text": "\ud83d\ude3d",
        "id": "KISSING CAT FACE WITH CLOSED EYES",
        "alt_names": [
          "kissing_cat"
        ]
      },
      {
        "text": "\ud83d\ude40",
        "id": "WEARY CAT FACE",
        "alt_names": [
          "scream_cat"
        ]
      },
      {
        "text": "\ud83d\ude3f",
        "id": "CRYING CAT FACE",
        "alt_names": [
          "crying_cat_face"
        ]
      },
      {
        "text": "\ud83d\ude3e",
        "id": "POUTING CAT FACE",
        "alt_names": [
          "pouting_cat"
        ]
      },
      {
        "text": "\ud83d\ude48",
        "id": "SEE-NO-EVIL MONKEY",
        "alt_names": [
          "see_no_evil"
        ]
      },
      {
        "text": "\ud83d\ude49",
        "id": "HEAR-NO-EVIL MONKEY",
        "alt_names": [
          "hear_no_evil"
        ]
      },
      {
        "text": "\ud83d\ude4a",
        "id": "SPEAK-NO-EVIL MONKEY",
        "alt_names": [
          "speak_no_evil"
        ]
      },
      {
        "text": "\ud83d\udc8b",
        "id": "KISS MARK",
        "alt_names": [
          "kiss"
        ]
      },
      {
        "text": "\ud83d\udc8c",
        "id": "LOVE LETTER",
        "alt_names": [
          "love_letter"
        ]
      },
      {
        "text": "\ud83d\udc98",
        "id": "HEART WITH ARROW",
        "alt_names": [
          "cupid"
        ]
      },
      {
        "text": "\ud83d\udc9d",
        "id": "HEART WITH RIBBON",
        "alt_names": [
          "gift_heart"
        ]
      },
      {
        "text": "\ud83d\udc96",
        "id": "SPARKLING HEART",
        "alt_names": [
          "sparkling_heart"
        ]
      },
      {
        "text": "\ud83d\udc97",
        "id": "GROWING HEART",
        "alt_names": [
          "heartpulse"
        ]
      },
      {
        "text": "\ud83d\udc93",
        "id": "BEATING HEART",
        "alt_names": [
          "heartbeat"
        ]
      },
      {
        "text": "\ud83d\udc9e",
        "id": "REVOLVING HEARTS",
        "alt_names": [
          "revolving_hearts"
        ]
      },
      {
        "text": "\ud83d\udc95",
        "id": "TWO HEARTS",
        "alt_names": [
          "two_hearts"
        ]
      },
      {
        "text": "\ud83d\udc9f",
        "id": "HEART DECORATION",
        "alt_names": [
          "heart_decoration"
        ]
      },
      {
        "text": "\ud83d\udc94",
        "id": "BROKEN HEART",
        "alt_names": [
          "broken_heart",
          "</3",
          "</3"
        ]
      },
      {
        "text": "\ud83e\udde1",
        "id": "ORANGE HEART",
        "alt_names": [
          "orange_heart"
        ]
      },
      {
        "text": "\ud83d\udc9b",
        "id": "YELLOW HEART",
        "alt_names": [
          "yellow_heart",
          "<3"
        ]
      },
      {
        "text": "\ud83d\udc9a",
        "id": "GREEN HEART",
        "alt_names": [
          "green_heart",
          "<3"
        ]
      },
      {
        "text": "\ud83d\udc99",
        "id": "BLUE HEART",
        "alt_names": [
          "blue_heart",
          "<3"
        ]
      },
      {
        "text": "\ud83d\udc9c",
        "id": "PURPLE HEART",
        "alt_names": [
          "purple_heart",
          "<3"
        ]
      },
      {
        "text": "\ud83e\udd0e",
        "id": "BROWN HEART",
        "alt_names": [
          "brown_heart"
        ]
      },
      {
        "text": "\ud83d\udda4",
        "id": "BLACK HEART",
        "alt_names": [
          "black_heart"
        ]
      },
      {
        "text": "\ud83e\udd0d",
        "id": "WHITE HEART",
        "alt_names": [
          "white_heart"
        ]
      },
      {
        "text": "\ud83d\udcaf",
        "id": "HUNDRED POINTS SYMBOL",
        "alt_names": [
          "100"
        ]
      },
      {
        "text": "\ud83d\udca2",
        "id": "ANGER SYMBOL",
        "alt_names": [
          "anger"
        ]
      },
      {
        "text": "\ud83d\udca5",
        "id": "COLLISION SYMBOL",
        "alt_names": [
          "boom",
          "collision"
        ]
      },
      {
        "text": "\ud83d\udcab",
        "id": "DIZZY SYMBOL",
        "alt_names": [
          "dizzy"
        ]
      },
      {
        "text": "\ud83d\udca6",
        "id": "SPLASHING SWEAT SYMBOL",
        "alt_names": [
          "sweat_drops"
        ]
      },
      {
        "text": "\ud83d\udca8",
        "id": "DASH SYMBOL",
        "alt_names": [
          "dash"
        ]
      },
      {
        "text": "\ud83d\udca3",
        "id": "BOMB",
        "alt_names": [
          "bomb"
        ]
      },
      {
        "text": "\ud83d\udcac",
        "id": "SPEECH BALLOON",
        "alt_names": [
          "speech_balloon"
        ]
      },
      {
        "text": "\ud83d\udcad",
        "id": "THOUGHT BALLOON",
        "alt_names": [
          "thought_balloon"
        ]
      },
      {
        "text": "\ud83d\udca4",
        "id": "SLEEPING SYMBOL",
        "alt_names": [
          "zzz"
        ]
      }
    ]
  },
  {
    "text": "People & Body",
    "children": [
      {
        "text": "\ud83d\udc4b",
        "id": "WAVING HAND SIGN",
        "alt_names": [
          "wave"
        ]
      },
      {
        "text": "\ud83e\udd1a",
        "id": "RAISED BACK OF HAND",
        "alt_names": [
          "raised_back_of_hand"
        ]
      },
      {
        "text": "\u270b",
        "id": "RAISED HAND",
        "alt_names": [
          "hand",
          "raised_hand"
        ]
      },
      {
        "text": "\ud83d\udd96",
        "id": "RAISED HAND WITH PART BETWEEN MIDDLE AND RING FINGERS",
        "alt_names": [
          "spock-hand",
          "vulcan_salute"
        ]
      },
      {
        "text": "\ud83d\udc4c",
        "id": "OK HAND SIGN",
        "alt_names": [
          "ok_hand"
        ]
      },
      {
        "text": "\ud83e\udd0c",
        "id": "PINCHED FINGERS",
        "alt_names": [
          "pinched_fingers"
        ]
      },
      {
        "text": "\ud83e\udd0f",
        "id": "PINCHING HAND",
        "alt_names": [
          "pinching_hand"
        ]
      },
      {
        "text": "\ud83e\udd1e",
        "id": "HAND WITH INDEX AND MIDDLE FINGERS CROSSED",
        "alt_names": [
          "crossed_fingers",
          "hand_with_index_and_middle_fingers_crossed"
        ]
      },
      {
        "text": "\ud83e\udd1f",
        "id": "I LOVE YOU HAND SIGN",
        "alt_names": [
          "i_love_you_hand_sign"
        ]
      },
      {
        "text": "\ud83e\udd18",
        "id": "SIGN OF THE HORNS",
        "alt_names": [
          "the_horns",
          "sign_of_the_horns",
          "metal"
        ]
      },
      {
        "text": "\ud83e\udd19",
        "id": "CALL ME HAND",
        "alt_names": [
          "call_me_hand"
        ]
      },
      {
        "text": "\ud83d\udc48",
        "id": "WHITE LEFT POINTING BACKHAND INDEX",
        "alt_names": [
          "point_left"
        ]
      },
      {
        "text": "\ud83d\udc49",
        "id": "WHITE RIGHT POINTING BACKHAND INDEX",
        "alt_names": [
          "point_right"
        ]
      },
      {
        "text": "\ud83d\udc46",
        "id": "WHITE UP POINTING BACKHAND INDEX",
        "alt_names": [
          "point_up_2"
        ]
      },
      {
        "text": "\ud83d\udd95",
        "id": "REVERSED HAND WITH MIDDLE FINGER EXTENDED",
        "alt_names": [
          "middle_finger",
          "reversed_hand_with_middle_finger_extended",
          "fu"
        ]
      },
      {
        "text": "\ud83d\udc47",
        "id": "WHITE DOWN POINTING BACKHAND INDEX",
        "alt_names": [
          "point_down"
        ]
      },
      {
        "text": "\ud83d\udc4d",
        "id": "THUMBS UP SIGN",
        "alt_names": [
          "+1",
          "thumbsup"
        ]
      },
      {
        "text": "\ud83d\udc4e",
        "id": "THUMBS DOWN SIGN",
        "alt_names": [
          "-1",
          "thumbsdown"
        ]
      },
      {
        "text": "\u270a",
        "id": "RAISED FIST",
        "alt_names": [
          "fist",
          "fist_raised"
        ]
      },
      {
        "text": "\ud83d\udc4a",
        "id": "FISTED HAND SIGN",
        "alt_names": [
          "facepunch",
          "punch",
          "fist_oncoming"
        ]
      },
      {
        "text": "\ud83e\udd1b",
        "id": "LEFT-FACING FIST",
        "alt_names": [
          "left-facing_fist",
          "fist_left"
        ]
      },
      {
        "text": "\ud83e\udd1c",
        "id": "RIGHT-FACING FIST",
        "alt_names": [
          "right-facing_fist",
          "fist_right"
        ]
      },
      {
        "text": "\ud83d\udc4f",
        "id": "CLAPPING HANDS SIGN",
        "alt_names": [
          "clap"
        ]
      },
      {
        "text": "\ud83d\ude4c",
        "id": "PERSON RAISING BOTH HANDS IN CELEBRATION",
        "alt_names": [
          "raised_hands"
        ]
      },
      {
        "text": "\ud83d\udc50",
        "id": "OPEN HANDS SIGN",
        "alt_names": [
          "open_hands"
        ]
      },
      {
        "text": "\ud83e\udd32",
        "id": "PALMS UP TOGETHER",
        "alt_names": [
          "palms_up_together"
        ]
      },
      {
        "text": "\ud83e\udd1d",
        "id": "HANDSHAKE",
        "alt_names": [
          "handshake"
        ]
      },
      {
        "text": "\ud83d\ude4f",
        "id": "PERSON WITH FOLDED HANDS",
        "alt_names": [
          "pray"
        ]
      },
      {
        "text": "\ud83d\udc85",
        "id": "NAIL POLISH",
        "alt_names": [
          "nail_care"
        ]
      },
      {
        "text": "\ud83e\udd33",
        "id": "SELFIE",
        "alt_names": [
          "selfie"
        ]
      },
      {
        "text": "\ud83d\udcaa",
        "id": "FLEXED BICEPS",
        "alt_names": [
          "muscle"
        ]
      },
      {
        "text": "\ud83e\uddbe",
        "id": "MECHANICAL ARM",
        "alt_names": [
          "mechanical_arm"
        ]
      },
      {
        "text": "\ud83e\uddbf",
        "id": "MECHANICAL LEG",
        "alt_names": [
          "mechanical_leg"
        ]
      },
      {
        "text": "\ud83e\uddb5",
        "id": "LEG",
        "alt_names": [
          "leg"
        ]
      },
      {
        "text": "\ud83e\uddb6",
        "id": "FOOT",
        "alt_names": [
          "foot"
        ]
      },
      {
        "text": "\ud83d\udc42",
        "id": "EAR",
        "alt_names": [
          "ear"
        ]
      },
      {
        "text": "\ud83e\uddbb",
        "id": "EAR WITH HEARING AID",
        "alt_names": [
          "ear_with_hearing_aid"
        ]
      },
      {
        "text": "\ud83d\udc43",
        "id": "NOSE",
        "alt_names": [
          "nose"
        ]
      },
      {
        "text": "\ud83e\udde0",
        "id": "BRAIN",
        "alt_names": [
          "brain"
        ]
      },
      {
        "text": "\ud83e\udec0",
        "id": "ANATOMICAL HEART",
        "alt_names": [
          "anatomical_heart"
        ]
      },
      {
        "text": "\ud83e\udec1",
        "id": "LUNGS",
        "alt_names": [
          "lungs"
        ]
      },
      {
        "text": "\ud83e\uddb7",
        "id": "TOOTH",
        "alt_names": [
          "tooth"
        ]
      },
      {
        "text": "\ud83e\uddb4",
        "id": "BONE",
        "alt_names": [
          "bone"
        ]
      },
      {
        "text": "\ud83d\udc40",
        "id": "EYES",
        "alt_names": [
          "eyes"
        ]
      },
      {
        "text": "\ud83d\udc45",
        "id": "TONGUE",
        "alt_names": [
          "tongue"
        ]
      },
      {
        "text": "\ud83d\udc44",
        "id": "MOUTH",
        "alt_names": [
          "lips"
        ]
      },
      {
        "text": "\ud83d\udc76",
        "id": "BABY",
        "alt_names": [
          "baby"
        ]
      },
      {
        "text": "\ud83e\uddd2",
        "id": "CHILD",
        "alt_names": [
          "child"
        ]
      },
      {
        "text": "\ud83d\udc66",
        "id": "BOY",
        "alt_names": [
          "boy"
        ]
      },
      {
        "text": "\ud83d\udc67",
        "id": "GIRL",
        "alt_names": [
          "girl"
        ]
      },
      {
        "text": "\ud83e\uddd1",
        "id": "ADULT",
        "alt_names": [
          "adult"
        ]
      },
      {
        "text": "\ud83d\udc71",
        "id": "PERSON WITH BLOND HAIR",
        "alt_names": [
          "person_with_blond_hair"
        ]
      },
      {
        "text": "\ud83d\udc68",
        "id": "MAN",
        "alt_names": [
          "man"
        ]
      },
      {
        "text": "\ud83e\uddd4",
        "id": "BEARDED PERSON",
        "alt_names": [
          "bearded_person"
        ]
      },
      {
        "text": "\ud83d\udc69",
        "id": "WOMAN",
        "alt_names": [
          "woman"
        ]
      },
      {
        "text": "\ud83e\uddd3",
        "id": "OLDER ADULT",
        "alt_names": [
          "older_adult"
        ]
      },
      {
        "text": "\ud83d\udc74",
        "id": "OLDER MAN",
        "alt_names": [
          "older_man"
        ]
      },
      {
        "text": "\ud83d\udc75",
        "id": "OLDER WOMAN",
        "alt_names": [
          "older_woman"
        ]
      },
      {
        "text": "\ud83d\ude4d",
        "id": "PERSON FROWNING",
        "alt_names": [
          "person_frowning"
        ]
      },
      {
        "text": "\ud83d\ude4e",
        "id": "PERSON WITH POUTING FACE",
        "alt_names": [
          "person_with_pouting_face"
        ]
      },
      {
        "text": "\ud83d\ude45",
        "id": "FACE WITH NO GOOD GESTURE",
        "alt_names": [
          "no_good"
        ]
      },
      {
        "text": "\ud83d\ude46",
        "id": "FACE WITH OK GESTURE",
        "alt_names": [
          "ok_woman"
        ]
      },
      {
        "text": "\ud83d\udc81",
        "id": "INFORMATION DESK PERSON",
        "alt_names": [
          "information_desk_person"
        ]
      },
      {
        "text": "\ud83d\ude4b",
        "id": "HAPPY PERSON RAISING ONE HAND",
        "alt_names": [
          "raising_hand"
        ]
      },
      {
        "text": "\ud83e\uddcf",
        "id": "DEAF PERSON",
        "alt_names": [
          "deaf_person"
        ]
      },
      {
        "text": "\ud83d\ude47",
        "id": "PERSON BOWING DEEPLY",
        "alt_names": [
          "bow"
        ]
      },
      {
        "text": "\ud83e\udd26",
        "id": "FACE PALM",
        "alt_names": [
          "face_palm"
        ]
      },
      {
        "text": "\ud83e\udd37",
        "id": "SHRUG",
        "alt_names": [
          "shrug"
        ]
      },
      {
        "text": "\ud83d\udc6e",
        "id": "POLICE OFFICER",
        "alt_names": [
          "cop"
        ]
      },
      {
        "text": "\ud83d\udc82",
        "id": "GUARDSMAN",
        "alt_names": [
          "guardsman"
        ]
      },
      {
        "text": "\ud83e\udd77",
        "id": "NINJA",
        "alt_names": [
          "ninja"
        ]
      },
      {
        "text": "\ud83d\udc77",
        "id": "CONSTRUCTION WORKER",
        "alt_names": [
          "construction_worker"
        ]
      },
      {
        "text": "\ud83e\udd34",
        "id": "PRINCE",
        "alt_names": [
          "prince"
        ]
      },
      {
        "text": "\ud83d\udc78",
        "id": "PRINCESS",
        "alt_names": [
          "princess"
        ]
      },
      {
        "text": "\ud83d\udc73",
        "id": "MAN WITH TURBAN",
        "alt_names": [
          "man_with_turban"
        ]
      },
      {
        "text": "\ud83d\udc72",
        "id": "MAN WITH GUA PI MAO",
        "alt_names": [
          "man_with_gua_pi_mao"
        ]
      },
      {
        "text": "\ud83e\uddd5",
        "id": "PERSON WITH HEADSCARF",
        "alt_names": [
          "person_with_headscarf"
        ]
      },
      {
        "text": "\ud83e\udd35",
        "id": "MAN IN TUXEDO",
        "alt_names": [
          "person_in_tuxedo"
        ]
      },
      {
        "text": "\ud83d\udc70",
        "id": "BRIDE WITH VEIL",
        "alt_names": [
          "bride_with_veil"
        ]
      },
      {
        "text": "\ud83e\udd30",
        "id": "PREGNANT WOMAN",
        "alt_names": [
          "pregnant_woman"
        ]
      },
      {
        "text": "\ud83e\udd31",
        "id": "BREAST-FEEDING",
        "alt_names": [
          "breast-feeding"
        ]
      },
      {
        "text": "\ud83d\udc7c",
        "id": "BABY ANGEL",
        "alt_names": [
          "angel"
        ]
      },
      {
        "text": "\ud83c\udf85",
        "id": "FATHER CHRISTMAS",
        "alt_names": [
          "santa"
        ]
      },
      {
        "text": "\ud83e\udd36",
        "id": "MOTHER CHRISTMAS",
        "alt_names": [
          "mrs_claus",
          "mother_christmas"
        ]
      },
      {
        "text": "\ud83e\uddb8",
        "id": "SUPERHERO",
        "alt_names": [
          "superhero"
        ]
      },
      {
        "text": "\ud83e\uddb9",
        "id": "SUPERVILLAIN",
        "alt_names": [
          "supervillain"
        ]
      },
      {
        "text": "\ud83e\uddd9",
        "id": "MAGE",
        "alt_names": [
          "mage"
        ]
      },
      {
        "text": "\ud83e\uddda",
        "id": "FAIRY",
        "alt_names": [
          "fairy"
        ]
      },
      {
        "text": "\ud83e\udddb",
        "id": "VAMPIRE",
        "alt_names": [
          "vampire"
        ]
      },
      {
        "text": "\ud83e\udddc",
        "id": "MERPERSON",
        "alt_names": [
          "merperson"
        ]
      },
      {
        "text": "\ud83e\udddd",
        "id": "ELF",
        "alt_names": [
          "elf"
        ]
      },
      {
        "text": "\ud83e\uddde",
        "id": "GENIE",
        "alt_names": [
          "genie"
        ]
      },
      {
        "text": "\ud83e\udddf",
        "id": "ZOMBIE",
        "alt_names": [
          "zombie"
        ]
      },
      {
        "text": "\ud83d\udc86",
        "id": "FACE MASSAGE",
        "alt_names": [
          "massage"
        ]
      },
      {
        "text": "\ud83d\udc87",
        "id": "HAIRCUT",
        "alt_names": [
          "haircut"
        ]
      },
      {
        "text": "\ud83d\udeb6",
        "id": "PEDESTRIAN",
        "alt_names": [
          "walking"
        ]
      },
      {
        "text": "\ud83e\uddcd",
        "id": "STANDING PERSON",
        "alt_names": [
          "standing_person"
        ]
      },
      {
        "text": "\ud83e\uddce",
        "id": "KNEELING PERSON",
        "alt_names": [
          "kneeling_person"
        ]
      },
      {
        "text": "\ud83c\udfc3",
        "id": "RUNNER",
        "alt_names": [
          "runner",
          "running"
        ]
      },
      {
        "text": "\ud83d\udc83",
        "id": "DANCER",
        "alt_names": [
          "dancer"
        ]
      },
      {
        "text": "\ud83d\udd7a",
        "id": "MAN DANCING",
        "alt_names": [
          "man_dancing"
        ]
      },
      {
        "text": "\ud83d\udc6f",
        "id": "WOMAN WITH BUNNY EARS",
        "alt_names": [
          "dancers"
        ]
      },
      {
        "text": "\ud83e\uddd6",
        "id": "PERSON IN STEAMY ROOM",
        "alt_names": [
          "person_in_steamy_room"
        ]
      },
      {
        "text": "\ud83e\uddd7",
        "id": "PERSON CLIMBING",
        "alt_names": [
          "person_climbing"
        ]
      },
      {
        "text": "\ud83e\udd3a",
        "id": "FENCER",
        "alt_names": [
          "fencer",
          "person_fencing"
        ]
      },
      {
        "text": "\ud83c\udfc7",
        "id": "HORSE RACING",
        "alt_names": [
          "horse_racing"
        ]
      },
      {
        "text": "\ud83c\udfc2",
        "id": "SNOWBOARDER",
        "alt_names": [
          "snowboarder"
        ]
      },
      {
        "text": "\ud83c\udfc4",
        "id": "SURFER",
        "alt_names": [
          "surfer"
        ]
      },
      {
        "text": "\ud83d\udea3",
        "id": "ROWBOAT",
        "alt_names": [
          "rowboat"
        ]
      },
      {
        "text": "\ud83c\udfca",
        "id": "SWIMMER",
        "alt_names": [
          "swimmer"
        ]
      },
      {
        "text": "\ud83d\udeb4",
        "id": "BICYCLIST",
        "alt_names": [
          "bicyclist"
        ]
      },
      {
        "text": "\ud83d\udeb5",
        "id": "MOUNTAIN BICYCLIST",
        "alt_names": [
          "mountain_bicyclist"
        ]
      },
      {
        "text": "\ud83e\udd38",
        "id": "PERSON DOING CARTWHEEL",
        "alt_names": [
          "person_doing_cartwheel"
        ]
      },
      {
        "text": "\ud83e\udd3c",
        "id": "WRESTLERS",
        "alt_names": [
          "wrestlers"
        ]
      },
      {
        "text": "\ud83e\udd3d",
        "id": "WATER POLO",
        "alt_names": [
          "water_polo"
        ]
      },
      {
        "text": "\ud83e\udd3e",
        "id": "HANDBALL",
        "alt_names": [
          "handball"
        ]
      },
      {
        "text": "\ud83e\udd39",
        "id": "JUGGLING",
        "alt_names": [
          "juggling"
        ]
      },
      {
        "text": "\ud83e\uddd8",
        "id": "PERSON IN LOTUS POSITION",
        "alt_names": [
          "person_in_lotus_position"
        ]
      },
      {
        "text": "\ud83d\udec0",
        "id": "BATH",
        "alt_names": [
          "bath"
        ]
      },
      {
        "text": "\ud83d\udecc",
        "id": "SLEEPING ACCOMMODATION",
        "alt_names": [
          "sleeping_accommodation",
          "sleeping_bed"
        ]
      },
      {
        "text": "\ud83d\udc6d",
        "id": "TWO WOMEN HOLDING HANDS",
        "alt_names": [
          "two_women_holding_hands",
          "women_holding_hands"
        ]
      },
      {
        "text": "\ud83d\udc6b",
        "id": "MAN AND WOMAN HOLDING HANDS",
        "alt_names": [
          "man_and_woman_holding_hands",
          "woman_and_man_holding_hands",
          "couple"
        ]
      },
      {
        "text": "\ud83d\udc6c",
        "id": "TWO MEN HOLDING HANDS",
        "alt_names": [
          "two_men_holding_hands",
          "men_holding_hands"
        ]
      },
      {
        "text": "\ud83d\udc8f",
        "id": "KISS",
        "alt_names": [
          "couplekiss"
        ]
      },
      {
        "text": "\ud83d\udc91",
        "id": "COUPLE WITH HEART",
        "alt_names": [
          "couple_with_heart"
        ]
      },
      {
        "text": "\ud83d\udc6a",
        "id": "FAMILY",
        "alt_names": [
          "family"
        ]
      },
      {
        "text": "\ud83d\udc64",
        "id": "BUST IN SILHOUETTE",
        "alt_names": [
          "bust_in_silhouette"
        ]
      },
      {
        "text": "\ud83d\udc65",
        "id": "BUSTS IN SILHOUETTE",
        "alt_names": [
          "busts_in_silhouette"
        ]
      },
      {
        "text": "\ud83e\udec2",
        "id": "PEOPLE HUGGING",
        "alt_names": [
          "people_hugging"
        ]
      },
      {
        "text": "\ud83d\udc63",
        "id": "FOOTPRINTS",
        "alt_names": [
          "footprints"
        ]
      }
    ]
  },
  {
    "text": "Component",
    "children": [
      {
        "text": "\ud83c\udffb",
        "id": "EMOJI MODIFIER FITZPATRICK TYPE-1-2",
        "alt_names": [
          "skin-tone-2"
        ]
      },
      {
        "text": "\ud83c\udffc",
        "id": "EMOJI MODIFIER FITZPATRICK TYPE-3",
        "alt_names": [
          "skin-tone-3"
        ]
      },
      {
        "text": "\ud83c\udffd",
        "id": "EMOJI MODIFIER FITZPATRICK TYPE-4",
        "alt_names": [
          "skin-tone-4"
        ]
      },
      {
        "text": "\ud83c\udffe",
        "id": "EMOJI MODIFIER FITZPATRICK TYPE-5",
        "alt_names": [
          "skin-tone-5"
        ]
      },
      {
        "text": "\ud83c\udfff",
        "id": "EMOJI MODIFIER FITZPATRICK TYPE-6",
        "alt_names": [
          "skin-tone-6"
        ]
      }
    ]
  },
  {
    "text": "Animals & Nature",
    "children": [
      {
        "text": "\ud83d\udc35",
        "id": "MONKEY FACE",
        "alt_names": [
          "monkey_face",
          ":o)"
        ]
      },
      {
        "text": "\ud83d\udc12",
        "id": "MONKEY",
        "alt_names": [
          "monkey"
        ]
      },
      {
        "text": "\ud83e\udd8d",
        "id": "GORILLA",
        "alt_names": [
          "gorilla"
        ]
      },
      {
        "text": "\ud83e\udda7",
        "id": "ORANGUTAN",
        "alt_names": [
          "orangutan"
        ]
      },
      {
        "text": "\ud83d\udc36",
        "id": "DOG FACE",
        "alt_names": [
          "dog"
        ]
      },
      {
        "text": "\ud83d\udc15",
        "id": "DOG",
        "alt_names": [
          "dog2"
        ]
      },
      {
        "text": "\ud83e\uddae",
        "id": "GUIDE DOG",
        "alt_names": [
          "guide_dog"
        ]
      },
      {
        "text": "\ud83d\udc29",
        "id": "POODLE",
        "alt_names": [
          "poodle"
        ]
      },
      {
        "text": "\ud83d\udc3a",
        "id": "WOLF FACE",
        "alt_names": [
          "wolf"
        ]
      },
      {
        "text": "\ud83e\udd8a",
        "id": "FOX FACE",
        "alt_names": [
          "fox_face"
        ]
      },
      {
        "text": "\ud83e\udd9d",
        "id": "RACCOON",
        "alt_names": [
          "raccoon"
        ]
      },
      {
        "text": "\ud83d\udc31",
        "id": "CAT FACE",
        "alt_names": [
          "cat"
        ]
      },
      {
        "text": "\ud83d\udc08",
        "id": "CAT",
        "alt_names": [
          "cat2"
        ]
      },
      {
        "text": "\ud83e\udd81",
        "id": "LION FACE",
        "alt_names": [
          "lion_face",
          "lion"
        ]
      },
      {
        "text": "\ud83d\udc2f",
        "id": "TIGER FACE",
        "alt_names": [
          "tiger"
        ]
      },
      {
        "text": "\ud83d\udc05",
        "id": "TIGER",
        "alt_names": [
          "tiger2"
        ]
      },
      {
        "text": "\ud83d\udc06",
        "id": "LEOPARD",
        "alt_names": [
          "leopard"
        ]
      },
      {
        "text": "\ud83d\udc34",
        "id": "HORSE FACE",
        "alt_names": [
          "horse"
        ]
      },
      {
        "text": "\ud83d\udc0e",
        "id": "HORSE",
        "alt_names": [
          "racehorse"
        ]
      },
      {
        "text": "\ud83e\udd84",
        "id": "UNICORN FACE",
        "alt_names": [
          "unicorn_face",
          "unicorn"
        ]
      },
      {
        "text": "\ud83e\udd93",
        "id": "ZEBRA FACE",
        "alt_names": [
          "zebra_face"
        ]
      },
      {
        "text": "\ud83e\udd8c",
        "id": "DEER",
        "alt_names": [
          "deer"
        ]
      },
      {
        "text": "\ud83e\uddac",
        "id": "BISON",
        "alt_names": [
          "bison"
        ]
      },
      {
        "text": "\ud83d\udc2e",
        "id": "COW FACE",
        "alt_names": [
          "cow"
        ]
      },
      {
        "text": "\ud83d\udc02",
        "id": "OX",
        "alt_names": [
          "ox"
        ]
      },
      {
        "text": "\ud83d\udc03",
        "id": "WATER BUFFALO",
        "alt_names": [
          "water_buffalo"
        ]
      },
      {
        "text": "\ud83d\udc04",
        "id": "COW",
        "alt_names": [
          "cow2"
        ]
      },
      {
        "text": "\ud83d\udc37",
        "id": "PIG FACE",
        "alt_names": [
          "pig"
        ]
      },
      {
        "text": "\ud83d\udc16",
        "id": "PIG",
        "alt_names": [
          "pig2"
        ]
      },
      {
        "text": "\ud83d\udc17",
        "id": "BOAR",
        "alt_names": [
          "boar"
        ]
      },
      {
        "text": "\ud83d\udc3d",
        "id": "PIG NOSE",
        "alt_names": [
          "pig_nose"
        ]
      },
      {
        "text": "\ud83d\udc0f",
        "id": "RAM",
        "alt_names": [
          "ram"
        ]
      },
      {
        "text": "\ud83d\udc11",
        "id": "SHEEP",
        "alt_names": [
          "sheep"
        ]
      },
      {
        "text": "\ud83d\udc10",
        "id": "GOAT",
        "alt_names": [
          "goat"
        ]
      },
      {
        "text": "\ud83d\udc2a",
        "id": "DROMEDARY CAMEL",
        "alt_names": [
          "dromedary_camel"
        ]
      },
      {
        "text": "\ud83d\udc2b",
        "id": "BACTRIAN CAMEL",
        "alt_names": [
          "camel"
        ]
      },
      {
        "text": "\ud83e\udd99",
        "id": "LLAMA",
        "alt_names": [
          "llama"
        ]
      },
      {
        "text": "\ud83e\udd92",
        "id": "GIRAFFE FACE",
        "alt_names": [
          "giraffe_face"
        ]
      },
      {
        "text": "\ud83d\udc18",
        "id": "ELEPHANT",
        "alt_names": [
          "elephant"
        ]
      },
      {
        "text": "\ud83e\udda3",
        "id": "MAMMOTH",
        "alt_names": [
          "mammoth"
        ]
      },
      {
        "text": "\ud83e\udd8f",
        "id": "RHINOCEROS",
        "alt_names": [
          "rhinoceros"
        ]
      },
      {
        "text": "\ud83e\udd9b",
        "id": "HIPPOPOTAMUS",
        "alt_names": [
          "hippopotamus"
        ]
      },
      {
        "text": "\ud83d\udc2d",
        "id": "MOUSE FACE",
        "alt_names": [
          "mouse"
        ]
      },
      {
        "text": "\ud83d\udc01",
        "id": "MOUSE",
        "alt_names": [
          "mouse2"
        ]
      },
      {
        "text": "\ud83d\udc00",
        "id": "RAT",
        "alt_names": [
          "rat"
        ]
      },
      {
        "text": "\ud83d\udc39",
        "id": "HAMSTER FACE",
        "alt_names": [
          "hamster"
        ]
      },
      {
        "text": "\ud83d\udc30",
        "id": "RABBIT FACE",
        "alt_names": [
          "rabbit"
        ]
      },
      {
        "text": "\ud83d\udc07",
        "id": "RABBIT",
        "alt_names": [
          "rabbit2"
        ]
      },
      {
        "text": "\ud83e\uddab",
        "id": "BEAVER",
        "alt_names": [
          "beaver"
        ]
      },
      {
        "text": "\ud83e\udd94",
        "id": "HEDGEHOG",
        "alt_names": [
          "hedgehog"
        ]
      },
      {
        "text": "\ud83e\udd87",
        "id": "BAT",
        "alt_names": [
          "bat"
        ]
      },
      {
        "text": "\ud83d\udc3b",
        "id": "BEAR FACE",
        "alt_names": [
          "bear"
        ]
      },
      {
        "text": "\ud83d\udc28",
        "id": "KOALA",
        "alt_names": [
          "koala"
        ]
      },
      {
        "text": "\ud83d\udc3c",
        "id": "PANDA FACE",
        "alt_names": [
          "panda_face"
        ]
      },
      {
        "text": "\ud83e\udda5",
        "id": "SLOTH",
        "alt_names": [
          "sloth"
        ]
      },
      {
        "text": "\ud83e\udda6",
        "id": "OTTER",
        "alt_names": [
          "otter"
        ]
      },
      {
        "text": "\ud83e\udda8",
        "id": "SKUNK",
        "alt_names": [
          "skunk"
        ]
      },
      {
        "text": "\ud83e\udd98",
        "id": "KANGAROO",
        "alt_names": [
          "kangaroo"
        ]
      },
      {
        "text": "\ud83e\udda1",
        "id": "BADGER",
        "alt_names": [
          "badger"
        ]
      },
      {
        "text": "\ud83d\udc3e",
        "id": "PAW PRINTS",
        "alt_names": [
          "feet",
          "paw_prints"
        ]
      },
      {
        "text": "\ud83e\udd83",
        "id": "TURKEY",
        "alt_names": [
          "turkey"
        ]
      },
      {
        "text": "\ud83d\udc14",
        "id": "CHICKEN",
        "alt_names": [
          "chicken"
        ]
      },
      {
        "text": "\ud83d\udc13",
        "id": "ROOSTER",
        "alt_names": [
          "rooster"
        ]
      },
      {
        "text": "\ud83d\udc23",
        "id": "HATCHING CHICK",
        "alt_names": [
          "hatching_chick"
        ]
      },
      {
        "text": "\ud83d\udc24",
        "id": "BABY CHICK",
        "alt_names": [
          "baby_chick"
        ]
      },
      {
        "text": "\ud83d\udc25",
        "id": "FRONT-FACING BABY CHICK",
        "alt_names": [
          "hatched_chick"
        ]
      },
      {
        "text": "\ud83d\udc26",
        "id": "BIRD",
        "alt_names": [
          "bird"
        ]
      },
      {
        "text": "\ud83d\udc27",
        "id": "PENGUIN",
        "alt_names": [
          "penguin"
        ]
      },
      {
        "text": "\ud83e\udd85",
        "id": "EAGLE",
        "alt_names": [
          "eagle"
        ]
      },
      {
        "text": "\ud83e\udd86",
        "id": "DUCK",
        "alt_names": [
          "duck"
        ]
      },
      {
        "text": "\ud83e\udda2",
        "id": "SWAN",
        "alt_names": [
          "swan"
        ]
      },
      {
        "text": "\ud83e\udd89",
        "id": "OWL",
        "alt_names": [
          "owl"
        ]
      },
      {
        "text": "\ud83e\udda4",
        "id": "DODO",
        "alt_names": [
          "dodo"
        ]
      },
      {
        "text": "\ud83e\udeb6",
        "id": "FEATHER",
        "alt_names": [
          "feather"
        ]
      },
      {
        "text": "\ud83e\udda9",
        "id": "FLAMINGO",
        "alt_names": [
          "flamingo"
        ]
      },
      {
        "text": "\ud83e\udd9a",
        "id": "PEACOCK",
        "alt_names": [
          "peacock"
        ]
      },
      {
        "text": "\ud83e\udd9c",
        "id": "PARROT",
        "alt_names": [
          "parrot"
        ]
      },
      {
        "text": "\ud83d\udc38",
        "id": "FROG FACE",
        "alt_names": [
          "frog"
        ]
      },
      {
        "text": "\ud83d\udc0a",
        "id": "CROCODILE",
        "alt_names": [
          "crocodile"
        ]
      },
      {
        "text": "\ud83d\udc22",
        "id": "TURTLE",
        "alt_names": [
          "turtle"
        ]
      },
      {
        "text": "\ud83e\udd8e",
        "id": "LIZARD",
        "alt_names": [
          "lizard"
        ]
      },
      {
        "text": "\ud83d\udc0d",
        "id": "SNAKE",
        "alt_names": [
          "snake"
        ]
      },
      {
        "text": "\ud83d\udc32",
        "id": "DRAGON FACE",
        "alt_names": [
          "dragon_face"
        ]
      },
      {
        "text": "\ud83d\udc09",
        "id": "DRAGON",
        "alt_names": [
          "dragon"
        ]
      },
      {
        "text": "\ud83e\udd95",
        "id": "SAUROPOD",
        "alt_names": [
          "sauropod"
        ]
      },
      {
        "text": "\ud83e\udd96",
        "id": "T-REX",
        "alt_names": [
          "t-rex"
        ]
      },
      {
        "text": "\ud83d\udc33",
        "id": "SPOUTING WHALE",
        "alt_names": [
          "whale"
        ]
      },
      {
        "text": "\ud83d\udc0b",
        "id": "WHALE",
        "alt_names": [
          "whale2"
        ]
      },
      {
        "text": "\ud83d\udc2c",
        "id": "DOLPHIN",
        "alt_names": [
          "dolphin",
          "flipper"
        ]
      },
      {
        "text": "\ud83e\uddad",
        "id": "SEAL",
        "alt_names": [
          "seal"
        ]
      },
      {
        "text": "\ud83d\udc1f",
        "id": "FISH",
        "alt_names": [
          "fish"
        ]
      },
      {
        "text": "\ud83d\udc20",
        "id": "TROPICAL FISH",
        "alt_names": [
          "tropical_fish"
        ]
      },
      {
        "text": "\ud83d\udc21",
        "id": "BLOWFISH",
        "alt_names": [
          "blowfish"
        ]
      },
      {
        "text": "\ud83e\udd88",
        "id": "SHARK",
        "alt_names": [
          "shark"
        ]
      },
      {
        "text": "\ud83d\udc19",
        "id": "OCTOPUS",
        "alt_names": [
          "octopus"
        ]
      },
      {
        "text": "\ud83d\udc1a",
        "id": "SPIRAL SHELL",
        "alt_names": [
          "shell"
        ]
      },
      {
        "text": "\ud83d\udc0c",
        "id": "SNAIL",
        "alt_names": [
          "snail"
        ]
      },
      {
        "text": "\ud83e\udd8b",
        "id": "BUTTERFLY",
        "alt_names": [
          "butterfly"
        ]
      },
      {
        "text": "\ud83d\udc1b",
        "id": "BUG",
        "alt_names": [
          "bug"
        ]
      },
      {
        "text": "\ud83d\udc1c",
        "id": "ANT",
        "alt_names": [
          "ant"
        ]
      },
      {
        "text": "\ud83d\udc1d",
        "id": "HONEYBEE",
        "alt_names": [
          "bee",
          "honeybee"
        ]
      },
      {
        "text": "\ud83e\udeb2",
        "id": "BEETLE",
        "alt_names": [
          "beetle"
        ]
      },
      {
        "text": "\ud83d\udc1e",
        "id": "LADY BEETLE",
        "alt_names": [
          "ladybug",
          "lady_beetle"
        ]
      },
      {
        "text": "\ud83e\udd97",
        "id": "CRICKET",
        "alt_names": [
          "cricket"
        ]
      },
      {
        "text": "\ud83e\udeb3",
        "id": "COCKROACH",
        "alt_names": [
          "cockroach"
        ]
      },
      {
        "text": "\ud83e\udd82",
        "id": "SCORPION",
        "alt_names": [
          "scorpion"
        ]
      },
      {
        "text": "\ud83e\udd9f",
        "id": "MOSQUITO",
        "alt_names": [
          "mosquito"
        ]
      },
      {
        "text": "\ud83e\udeb0",
        "id": "FLY",
        "alt_names": [
          "fly"
        ]
      },
      {
        "text": "\ud83e\udeb1",
        "id": "WORM",
        "alt_names": [
          "worm"
        ]
      },
      {
        "text": "\ud83e\udda0",
        "id": "MICROBE",
        "alt_names": [
          "microbe"
        ]
      },
      {
        "text": "\ud83d\udc90",
        "id": "BOUQUET",
        "alt_names": [
          "bouquet"
        ]
      },
      {
        "text": "\ud83c\udf38",
        "id": "CHERRY BLOSSOM",
        "alt_names": [
          "cherry_blossom"
        ]
      },
      {
        "text": "\ud83d\udcae",
        "id": "WHITE FLOWER",
        "alt_names": [
          "white_flower"
        ]
      },
      {
        "text": "\ud83c\udf39",
        "id": "ROSE",
        "alt_names": [
          "rose"
        ]
      },
      {
        "text": "\ud83e\udd40",
        "id": "WILTED FLOWER",
        "alt_names": [
          "wilted_flower"
        ]
      },
      {
        "text": "\ud83c\udf3a",
        "id": "HIBISCUS",
        "alt_names": [
          "hibiscus"
        ]
      },
      {
        "text": "\ud83c\udf3b",
        "id": "SUNFLOWER",
        "alt_names": [
          "sunflower"
        ]
      },
      {
        "text": "\ud83c\udf3c",
        "id": "BLOSSOM",
        "alt_names": [
          "blossom"
        ]
      },
      {
        "text": "\ud83c\udf37",
        "id": "TULIP",
        "alt_names": [
          "tulip"
        ]
      },
      {
        "text": "\ud83c\udf31",
        "id": "SEEDLING",
        "alt_names": [
          "seedling"
        ]
      },
      {
        "text": "\ud83e\udeb4",
        "id": "POTTED PLANT",
        "alt_names": [
          "potted_plant"
        ]
      },
      {
        "text": "\ud83c\udf32",
        "id": "EVERGREEN TREE",
        "alt_names": [
          "evergreen_tree"
        ]
      },
      {
        "text": "\ud83c\udf33",
        "id": "DECIDUOUS TREE",
        "alt_names": [
          "deciduous_tree"
        ]
      },
      {
        "text": "\ud83c\udf34",
        "id": "PALM TREE",
        "alt_names": [
          "palm_tree"
        ]
      },
      {
        "text": "\ud83c\udf35",
        "id": "CACTUS",
        "alt_names": [
          "cactus"
        ]
      },
      {
        "text": "\ud83c\udf3e",
        "id": "EAR OF RICE",
        "alt_names": [
          "ear_of_rice"
        ]
      },
      {
        "text": "\ud83c\udf3f",
        "id": "HERB",
        "alt_names": [
          "herb"
        ]
      },
      {
        "text": "\ud83c\udf40",
        "id": "FOUR LEAF CLOVER",
        "alt_names": [
          "four_leaf_clover"
        ]
      },
      {
        "text": "\ud83c\udf41",
        "id": "MAPLE LEAF",
        "alt_names": [
          "maple_leaf"
        ]
      },
      {
        "text": "\ud83c\udf42",
        "id": "FALLEN LEAF",
        "alt_names": [
          "fallen_leaf"
        ]
      },
      {
        "text": "\ud83c\udf43",
        "id": "LEAF FLUTTERING IN WIND",
        "alt_names": [
          "leaves"
        ]
      }
    ]
  },
  {
    "text": "Food & Drink",
    "children": [
      {
        "text": "\ud83c\udf47",
        "id": "GRAPES",
        "alt_names": [
          "grapes"
        ]
      },
      {
        "text": "\ud83c\udf48",
        "id": "MELON",
        "alt_names": [
          "melon"
        ]
      },
      {
        "text": "\ud83c\udf49",
        "id": "WATERMELON",
        "alt_names": [
          "watermelon"
        ]
      },
      {
        "text": "\ud83c\udf4a",
        "id": "TANGERINE",
        "alt_names": [
          "tangerine",
          "mandarin",
          "orange"
        ]
      },
      {
        "text": "\ud83c\udf4b",
        "id": "LEMON",
        "alt_names": [
          "lemon"
        ]
      },
      {
        "text": "\ud83c\udf4c",
        "id": "BANANA",
        "alt_names": [
          "banana"
        ]
      },
      {
        "text": "\ud83c\udf4d",
        "id": "PINEAPPLE",
        "alt_names": [
          "pineapple"
        ]
      },
      {
        "text": "\ud83e\udd6d",
        "id": "MANGO",
        "alt_names": [
          "mango"
        ]
      },
      {
        "text": "\ud83c\udf4e",
        "id": "RED APPLE",
        "alt_names": [
          "apple"
        ]
      },
      {
        "text": "\ud83c\udf4f",
        "id": "GREEN APPLE",
        "alt_names": [
          "green_apple"
        ]
      },
      {
        "text": "\ud83c\udf50",
        "id": "PEAR",
        "alt_names": [
          "pear"
        ]
      },
      {
        "text": "\ud83c\udf51",
        "id": "PEACH",
        "alt_names": [
          "peach"
        ]
      },
      {
        "text": "\ud83c\udf52",
        "id": "CHERRIES",
        "alt_names": [
          "cherries"
        ]
      },
      {
        "text": "\ud83c\udf53",
        "id": "STRAWBERRY",
        "alt_names": [
          "strawberry"
        ]
      },
      {
        "text": "\ud83e\uded0",
        "id": "BLUEBERRIES",
        "alt_names": [
          "blueberries"
        ]
      },
      {
        "text": "\ud83e\udd5d",
        "id": "KIWIFRUIT",
        "alt_names": [
          "kiwifruit",
          "kiwi_fruit"
        ]
      },
      {
        "text": "\ud83c\udf45",
        "id": "TOMATO",
        "alt_names": [
          "tomato"
        ]
      },
      {
        "text": "\ud83e\uded2",
        "id": "OLIVE",
        "alt_names": [
          "olive"
        ]
      },
      {
        "text": "\ud83e\udd65",
        "id": "COCONUT",
        "alt_names": [
          "coconut"
        ]
      },
      {
        "text": "\ud83e\udd51",
        "id": "AVOCADO",
        "alt_names": [
          "avocado"
        ]
      },
      {
        "text": "\ud83c\udf46",
        "id": "AUBERGINE",
        "alt_names": [
          "eggplant"
        ]
      },
      {
        "text": "\ud83e\udd54",
        "id": "POTATO",
        "alt_names": [
          "potato"
        ]
      },
      {
        "text": "\ud83e\udd55",
        "id": "CARROT",
        "alt_names": [
          "carrot"
        ]
      },
      {
        "text": "\ud83c\udf3d",
        "id": "EAR OF MAIZE",
        "alt_names": [
          "corn"
        ]
      },
      {
        "text": "\ud83e\uded1",
        "id": "BELL PEPPER",
        "alt_names": [
          "bell_pepper"
        ]
      },
      {
        "text": "\ud83e\udd52",
        "id": "CUCUMBER",
        "alt_names": [
          "cucumber"
        ]
      },
      {
        "text": "\ud83e\udd6c",
        "id": "LEAFY GREEN",
        "alt_names": [
          "leafy_green"
        ]
      },
      {
        "text": "\ud83e\udd66",
        "id": "BROCCOLI",
        "alt_names": [
          "broccoli"
        ]
      },
      {
        "text": "\ud83e\uddc4",
        "id": "GARLIC",
        "alt_names": [
          "garlic"
        ]
      },
      {
        "text": "\ud83e\uddc5",
        "id": "ONION",
        "alt_names": [
          "onion"
        ]
      },
      {
        "text": "\ud83c\udf44",
        "id": "MUSHROOM",
        "alt_names": [
          "mushroom"
        ]
      },
      {
        "text": "\ud83e\udd5c",
        "id": "PEANUTS",
        "alt_names": [
          "peanuts"
        ]
      },
      {
        "text": "\ud83c\udf30",
        "id": "CHESTNUT",
        "alt_names": [
          "chestnut"
        ]
      },
      {
        "text": "\ud83c\udf5e",
        "id": "BREAD",
        "alt_names": [
          "bread"
        ]
      },
      {
        "text": "\ud83e\udd50",
        "id": "CROISSANT",
        "alt_names": [
          "croissant"
        ]
      },
      {
        "text": "\ud83e\udd56",
        "id": "BAGUETTE BREAD",
        "alt_names": [
          "baguette_bread"
        ]
      },
      {
        "text": "\ud83e\uded3",
        "id": "FLATBREAD",
        "alt_names": [
          "flatbread"
        ]
      },
      {
        "text": "\ud83e\udd68",
        "id": "PRETZEL",
        "alt_names": [
          "pretzel"
        ]
      },
      {
        "text": "\ud83e\udd6f",
        "id": "BAGEL",
        "alt_names": [
          "bagel"
        ]
      },
      {
        "text": "\ud83e\udd5e",
        "id": "PANCAKES",
        "alt_names": [
          "pancakes"
        ]
      },
      {
        "text": "\ud83e\uddc7",
        "id": "WAFFLE",
        "alt_names": [
          "waffle"
        ]
      },
      {
        "text": "\ud83e\uddc0",
        "id": "CHEESE WEDGE",
        "alt_names": [
          "cheese_wedge",
          "cheese"
        ]
      },
      {
        "text": "\ud83c\udf56",
        "id": "MEAT ON BONE",
        "alt_names": [
          "meat_on_bone"
        ]
      },
      {
        "text": "\ud83c\udf57",
        "id": "POULTRY LEG",
        "alt_names": [
          "poultry_leg"
        ]
      },
      {
        "text": "\ud83e\udd69",
        "id": "CUT OF MEAT",
        "alt_names": [
          "cut_of_meat"
        ]
      },
      {
        "text": "\ud83e\udd53",
        "id": "BACON",
        "alt_names": [
          "bacon"
        ]
      },
      {
        "text": "\ud83c\udf54",
        "id": "HAMBURGER",
        "alt_names": [
          "hamburger"
        ]
      },
      {
        "text": "\ud83c\udf5f",
        "id": "FRENCH FRIES",
        "alt_names": [
          "fries"
        ]
      },
      {
        "text": "\ud83c\udf55",
        "id": "SLICE OF PIZZA",
        "alt_names": [
          "pizza"
        ]
      },
      {
        "text": "\ud83c\udf2d",
        "id": "HOT DOG",
        "alt_names": [
          "hotdog"
        ]
      },
      {
        "text": "\ud83e\udd6a",
        "id": "SANDWICH",
        "alt_names": [
          "sandwich"
        ]
      },
      {
        "text": "\ud83c\udf2e",
        "id": "TACO",
        "alt_names": [
          "taco"
        ]
      },
      {
        "text": "\ud83c\udf2f",
        "id": "BURRITO",
        "alt_names": [
          "burrito"
        ]
      },
      {
        "text": "\ud83e\uded4",
        "id": "TAMALE",
        "alt_names": [
          "tamale"
        ]
      },
      {
        "text": "\ud83e\udd59",
        "id": "STUFFED FLATBREAD",
        "alt_names": [
          "stuffed_flatbread"
        ]
      },
      {
        "text": "\ud83e\uddc6",
        "id": "FALAFEL",
        "alt_names": [
          "falafel"
        ]
      },
      {
        "text": "\ud83e\udd5a",
        "id": "EGG",
        "alt_names": [
          "egg"
        ]
      },
      {
        "text": "\ud83c\udf73",
        "id": "COOKING",
        "alt_names": [
          "fried_egg",
          "cooking"
        ]
      },
      {
        "text": "\ud83e\udd58",
        "id": "SHALLOW PAN OF FOOD",
        "alt_names": [
          "shallow_pan_of_food"
        ]
      },
      {
        "text": "\ud83c\udf72",
        "id": "POT OF FOOD",
        "alt_names": [
          "stew"
        ]
      },
      {
        "text": "\ud83e\uded5",
        "id": "FONDUE",
        "alt_names": [
          "fondue"
        ]
      },
      {
        "text": "\ud83e\udd63",
        "id": "BOWL WITH SPOON",
        "alt_names": [
          "bowl_with_spoon"
        ]
      },
      {
        "text": "\ud83e\udd57",
        "id": "GREEN SALAD",
        "alt_names": [
          "green_salad"
        ]
      },
      {
        "text": "\ud83c\udf7f",
        "id": "POPCORN",
        "alt_names": [
          "popcorn"
        ]
      },
      {
        "text": "\ud83e\uddc8",
        "id": "BUTTER",
        "alt_names": [
          "butter"
        ]
      },
      {
        "text": "\ud83e\uddc2",
        "id": "SALT SHAKER",
        "alt_names": [
          "salt"
        ]
      },
      {
        "text": "\ud83e\udd6b",
        "id": "CANNED FOOD",
        "alt_names": [
          "canned_food"
        ]
      },
      {
        "text": "\ud83c\udf71",
        "id": "BENTO BOX",
        "alt_names": [
          "bento"
        ]
      },
      {
        "text": "\ud83c\udf58",
        "id": "RICE CRACKER",
        "alt_names": [
          "rice_cracker"
        ]
      },
      {
        "text": "\ud83c\udf59",
        "id": "RICE BALL",
        "alt_names": [
          "rice_ball"
        ]
      },
      {
        "text": "\ud83c\udf5a",
        "id": "COOKED RICE",
        "alt_names": [
          "rice"
        ]
      },
      {
        "text": "\ud83c\udf5b",
        "id": "CURRY AND RICE",
        "alt_names": [
          "curry"
        ]
      },
      {
        "text": "\ud83c\udf5c",
        "id": "STEAMING BOWL",
        "alt_names": [
          "ramen"
        ]
      },
      {
        "text": "\ud83c\udf5d",
        "id": "SPAGHETTI",
        "alt_names": [
          "spaghetti"
        ]
      },
      {
        "text": "\ud83c\udf60",
        "id": "ROASTED SWEET POTATO",
        "alt_names": [
          "sweet_potato"
        ]
      },
      {
        "text": "\ud83c\udf62",
        "id": "ODEN",
        "alt_names": [
          "oden"
        ]
      },
      {
        "text": "\ud83c\udf63",
        "id": "SUSHI",
        "alt_names": [
          "sushi"
        ]
      },
      {
        "text": "\ud83c\udf64",
        "id": "FRIED SHRIMP",
        "alt_names": [
          "fried_shrimp"
        ]
      },
      {
        "text": "\ud83c\udf65",
        "id": "FISH CAKE WITH SWIRL DESIGN",
        "alt_names": [
          "fish_cake"
        ]
      },
      {
        "text": "\ud83e\udd6e",
        "id": "MOON CAKE",
        "alt_names": [
          "moon_cake"
        ]
      },
      {
        "text": "\ud83c\udf61",
        "id": "DANGO",
        "alt_names": [
          "dango"
        ]
      },
      {
        "text": "\ud83e\udd5f",
        "id": "DUMPLING",
        "alt_names": [
          "dumpling"
        ]
      },
      {
        "text": "\ud83e\udd60",
        "id": "FORTUNE COOKIE",
        "alt_names": [
          "fortune_cookie"
        ]
      },
      {
        "text": "\ud83e\udd61",
        "id": "TAKEOUT BOX",
        "alt_names": [
          "takeout_box"
        ]
      },
      {
        "text": "\ud83e\udd80",
        "id": "CRAB",
        "alt_names": [
          "crab"
        ]
      },
      {
        "text": "\ud83e\udd9e",
        "id": "LOBSTER",
        "alt_names": [
          "lobster"
        ]
      },
      {
        "text": "\ud83e\udd90",
        "id": "SHRIMP",
        "alt_names": [
          "shrimp"
        ]
      },
      {
        "text": "\ud83e\udd91",
        "id": "SQUID",
        "alt_names": [
          "squid"
        ]
      },
      {
        "text": "\ud83e\uddaa",
        "id": "OYSTER",
        "alt_names": [
          "oyster"
        ]
      },
      {
        "text": "\ud83c\udf66",
        "id": "SOFT ICE CREAM",
        "alt_names": [
          "icecream"
        ]
      },
      {
        "text": "\ud83c\udf67",
        "id": "SHAVED ICE",
        "alt_names": [
          "shaved_ice"
        ]
      },
      {
        "text": "\ud83c\udf68",
        "id": "ICE CREAM",
        "alt_names": [
          "ice_cream"
        ]
      },
      {
        "text": "\ud83c\udf69",
        "id": "DOUGHNUT",
        "alt_names": [
          "doughnut"
        ]
      },
      {
        "text": "\ud83c\udf6a",
        "id": "COOKIE",
        "alt_names": [
          "cookie"
        ]
      },
      {
        "text": "\ud83c\udf82",
        "id": "BIRTHDAY CAKE",
        "alt_names": [
          "birthday"
        ]
      },
      {
        "text": "\ud83c\udf70",
        "id": "SHORTCAKE",
        "alt_names": [
          "cake"
        ]
      },
      {
        "text": "\ud83e\uddc1",
        "id": "CUPCAKE",
        "alt_names": [
          "cupcake"
        ]
      },
      {
        "text": "\ud83e\udd67",
        "id": "PIE",
        "alt_names": [
          "pie"
        ]
      },
      {
        "text": "\ud83c\udf6b",
        "id": "CHOCOLATE BAR",
        "alt_names": [
          "chocolate_bar"
        ]
      },
      {
        "text": "\ud83c\udf6c",
        "id": "CANDY",
        "alt_names": [
          "candy"
        ]
      },
      {
        "text": "\ud83c\udf6d",
        "id": "LOLLIPOP",
        "alt_names": [
          "lollipop"
        ]
      },
      {
        "text": "\ud83c\udf6e",
        "id": "CUSTARD",
        "alt_names": [
          "custard"
        ]
      },
      {
        "text": "\ud83c\udf6f",
        "id": "HONEY POT",
        "alt_names": [
          "honey_pot"
        ]
      },
      {
        "text": "\ud83c\udf7c",
        "id": "BABY BOTTLE",
        "alt_names": [
          "baby_bottle"
        ]
      },
      {
        "text": "\ud83e\udd5b",
        "id": "GLASS OF MILK",
        "alt_names": [
          "glass_of_milk",
          "milk_glass"
        ]
      },
      {
        "text": "\u2615",
        "id": "HOT BEVERAGE",
        "alt_names": [
          "coffee"
        ]
      },
      {
        "text": "\ud83e\uded6",
        "id": "TEAPOT",
        "alt_names": [
          "teapot"
        ]
      },
      {
        "text": "\ud83c\udf75",
        "id": "TEACUP WITHOUT HANDLE",
        "alt_names": [
          "tea"
        ]
      },
      {
        "text": "\ud83c\udf76",
        "id": "SAKE BOTTLE AND CUP",
        "alt_names": [
          "sake"
        ]
      },
      {
        "text": "\ud83c\udf7e",
        "id": "BOTTLE WITH POPPING CORK",
        "alt_names": [
          "champagne"
        ]
      },
      {
        "text": "\ud83c\udf77",
        "id": "WINE GLASS",
        "alt_names": [
          "wine_glass"
        ]
      },
      {
        "text": "\ud83c\udf78",
        "id": "COCKTAIL GLASS",
        "alt_names": [
          "cocktail"
        ]
      },
      {
        "text": "\ud83c\udf79",
        "id": "TROPICAL DRINK",
        "alt_names": [
          "tropical_drink"
        ]
      },
      {
        "text": "\ud83c\udf7a",
        "id": "BEER MUG",
        "alt_names": [
          "beer"
        ]
      },
      {
        "text": "\ud83c\udf7b",
        "id": "CLINKING BEER MUGS",
        "alt_names": [
          "beers"
        ]
      },
      {
        "text": "\ud83e\udd42",
        "id": "CLINKING GLASSES",
        "alt_names": [
          "clinking_glasses"
        ]
      },
      {
        "text": "\ud83e\udd43",
        "id": "TUMBLER GLASS",
        "alt_names": [
          "tumbler_glass"
        ]
      },
      {
        "text": "\ud83e\udd64",
        "id": "CUP WITH STRAW",
        "alt_names": [
          "cup_with_straw"
        ]
      },
      {
        "text": "\ud83e\uddcb",
        "id": "BUBBLE TEA",
        "alt_names": [
          "bubble_tea"
        ]
      },
      {
        "text": "\ud83e\uddc3",
        "id": "BEVERAGE BOX",
        "alt_names": [
          "beverage_box"
        ]
      },
      {
        "text": "\ud83e\uddc9",
        "id": "MATE DRINK",
        "alt_names": [
          "mate_drink"
        ]
      },
      {
        "text": "\ud83e\uddca",
        "id": "ICE CUBE",
        "alt_names": [
          "ice_cube"
        ]
      },
      {
        "text": "\ud83e\udd62",
        "id": "CHOPSTICKS",
        "alt_names": [
          "chopsticks"
        ]
      },
      {
        "text": "\ud83c\udf74",
        "id": "FORK AND KNIFE",
        "alt_names": [
          "fork_and_knife"
        ]
      },
      {
        "text": "\ud83e\udd44",
        "id": "SPOON",
        "alt_names": [
          "spoon"
        ]
      },
      {
        "text": "\ud83d\udd2a",
        "id": "HOCHO",
        "alt_names": [
          "hocho",
          "knife"
        ]
      },
      {
        "text": "\ud83c\udffa",
        "id": "AMPHORA",
        "alt_names": [
          "amphora"
        ]
      }
    ]
  },
  {
    "text": "Travel Places",
    "children": [
      {
        "text": "\ud83c\udf0d",
        "id": "EARTH GLOBE EUROPE-AFRICA",
        "alt_names": [
          "earth_africa"
        ]
      },
      {
        "text": "\ud83c\udf0e",
        "id": "EARTH GLOBE AMERICAS",
        "alt_names": [
          "earth_americas"
        ]
      },
      {
        "text": "\ud83c\udf0f",
        "id": "EARTH GLOBE ASIA-AUSTRALIA",
        "alt_names": [
          "earth_asia"
        ]
      },
      {
        "text": "\ud83c\udf10",
        "id": "GLOBE WITH MERIDIANS",
        "alt_names": [
          "globe_with_meridians"
        ]
      },
      {
        "text": "\ud83d\uddfe",
        "id": "SILHOUETTE OF JAPAN",
        "alt_names": [
          "japan"
        ]
      },
      {
        "text": "\ud83e\udded",
        "id": "COMPASS",
        "alt_names": [
          "compass"
        ]
      },
      {
        "text": "\ud83c\udf0b",
        "id": "VOLCANO",
        "alt_names": [
          "volcano"
        ]
      },
      {
        "text": "\ud83d\uddfb",
        "id": "MOUNT FUJI",
        "alt_names": [
          "mount_fuji"
        ]
      },
      {
        "text": "\ud83e\uddf1",
        "id": "BRICK",
        "alt_names": [
          "bricks"
        ]
      },
      {
        "text": "\ud83e\udea8",
        "id": "ROCK",
        "alt_names": [
          "rock"
        ]
      },
      {
        "text": "\ud83e\udeb5",
        "id": "WOOD",
        "alt_names": [
          "wood"
        ]
      },
      {
        "text": "\ud83d\uded6",
        "id": "HUT",
        "alt_names": [
          "hut"
        ]
      },
      {
        "text": "\ud83c\udfe0",
        "id": "HOUSE BUILDING",
        "alt_names": [
          "house"
        ]
      },
      {
        "text": "\ud83c\udfe1",
        "id": "HOUSE WITH GARDEN",
        "alt_names": [
          "house_with_garden"
        ]
      },
      {
        "text": "\ud83c\udfe2",
        "id": "OFFICE BUILDING",
        "alt_names": [
          "office"
        ]
      },
      {
        "text": "\ud83c\udfe3",
        "id": "JAPANESE POST OFFICE",
        "alt_names": [
          "post_office"
        ]
      },
      {
        "text": "\ud83c\udfe4",
        "id": "EUROPEAN POST OFFICE",
        "alt_names": [
          "european_post_office"
        ]
      },
      {
        "text": "\ud83c\udfe5",
        "id": "HOSPITAL",
        "alt_names": [
          "hospital"
        ]
      },
      {
        "text": "\ud83c\udfe6",
        "id": "BANK",
        "alt_names": [
          "bank"
        ]
      },
      {
        "text": "\ud83c\udfe8",
        "id": "HOTEL",
        "alt_names": [
          "hotel"
        ]
      },
      {
        "text": "\ud83c\udfe9",
        "id": "LOVE HOTEL",
        "alt_names": [
          "love_hotel"
        ]
      },
      {
        "text": "\ud83c\udfea",
        "id": "CONVENIENCE STORE",
        "alt_names": [
          "convenience_store"
        ]
      },
      {
        "text": "\ud83c\udfeb",
        "id": "SCHOOL",
        "alt_names": [
          "school"
        ]
      },
      {
        "text": "\ud83c\udfec",
        "id": "DEPARTMENT STORE",
        "alt_names": [
          "department_store"
        ]
      },
      {
        "text": "\ud83c\udfed",
        "id": "FACTORY",
        "alt_names": [
          "factory"
        ]
      },
      {
        "text": "\ud83c\udfef",
        "id": "JAPANESE CASTLE",
        "alt_names": [
          "japanese_castle"
        ]
      },
      {
        "text": "\ud83c\udff0",
        "id": "EUROPEAN CASTLE",
        "alt_names": [
          "european_castle"
        ]
      },
      {
        "text": "\ud83d\udc92",
        "id": "WEDDING",
        "alt_names": [
          "wedding"
        ]
      },
      {
        "text": "\ud83d\uddfc",
        "id": "TOKYO TOWER",
        "alt_names": [
          "tokyo_tower"
        ]
      },
      {
        "text": "\ud83d\uddfd",
        "id": "STATUE OF LIBERTY",
        "alt_names": [
          "statue_of_liberty"
        ]
      },
      {
        "text": "\u26ea",
        "id": "CHURCH",
        "alt_names": [
          "church"
        ]
      },
      {
        "text": "\ud83d\udd4c",
        "id": "MOSQUE",
        "alt_names": [
          "mosque"
        ]
      },
      {
        "text": "\ud83d\uded5",
        "id": "HINDU TEMPLE",
        "alt_names": [
          "hindu_temple"
        ]
      },
      {
        "text": "\ud83d\udd4d",
        "id": "SYNAGOGUE",
        "alt_names": [
          "synagogue"
        ]
      },
      {
        "text": "\ud83d\udd4b",
        "id": "KAABA",
        "alt_names": [
          "kaaba"
        ]
      },
      {
        "text": "\u26f2",
        "id": "FOUNTAIN",
        "alt_names": [
          "fountain"
        ]
      },
      {
        "text": "\u26fa",
        "id": "TENT",
        "alt_names": [
          "tent"
        ]
      },
      {
        "text": "\ud83c\udf01",
        "id": "FOGGY",
        "alt_names": [
          "foggy"
        ]
      },
      {
        "text": "\ud83c\udf03",
        "id": "NIGHT WITH STARS",
        "alt_names": [
          "night_with_stars"
        ]
      },
      {
        "text": "\ud83c\udf04",
        "id": "SUNRISE OVER MOUNTAINS",
        "alt_names": [
          "sunrise_over_mountains"
        ]
      },
      {
        "text": "\ud83c\udf05",
        "id": "SUNRISE",
        "alt_names": [
          "sunrise"
        ]
      },
      {
        "text": "\ud83c\udf06",
        "id": "CITYSCAPE AT DUSK",
        "alt_names": [
          "city_sunset"
        ]
      },
      {
        "text": "\ud83c\udf07",
        "id": "SUNSET OVER BUILDINGS",
        "alt_names": [
          "city_sunrise"
        ]
      },
      {
        "text": "\ud83c\udf09",
        "id": "BRIDGE AT NIGHT",
        "alt_names": [
          "bridge_at_night"
        ]
      },
      {
        "text": "\ud83c\udfa0",
        "id": "CAROUSEL HORSE",
        "alt_names": [
          "carousel_horse"
        ]
      },
      {
        "text": "\ud83c\udfa1",
        "id": "FERRIS WHEEL",
        "alt_names": [
          "ferris_wheel"
        ]
      },
      {
        "text": "\ud83c\udfa2",
        "id": "ROLLER COASTER",
        "alt_names": [
          "roller_coaster"
        ]
      },
      {
        "text": "\ud83d\udc88",
        "id": "BARBER POLE",
        "alt_names": [
          "barber"
        ]
      },
      {
        "text": "\ud83c\udfaa",
        "id": "CIRCUS TENT",
        "alt_names": [
          "circus_tent"
        ]
      },
      {
        "text": "\ud83d\ude82",
        "id": "STEAM LOCOMOTIVE",
        "alt_names": [
          "steam_locomotive"
        ]
      },
      {
        "text": "\ud83d\ude83",
        "id": "RAILWAY CAR",
        "alt_names": [
          "railway_car"
        ]
      },
      {
        "text": "\ud83d\ude84",
        "id": "HIGH-SPEED TRAIN",
        "alt_names": [
          "bullettrain_side"
        ]
      },
      {
        "text": "\ud83d\ude85",
        "id": "HIGH-SPEED TRAIN WITH BULLET NOSE",
        "alt_names": [
          "bullettrain_front"
        ]
      },
      {
        "text": "\ud83d\ude86",
        "id": "TRAIN",
        "alt_names": [
          "train2"
        ]
      },
      {
        "text": "\ud83d\ude87",
        "id": "METRO",
        "alt_names": [
          "metro"
        ]
      },
      {
        "text": "\ud83d\ude88",
        "id": "LIGHT RAIL",
        "alt_names": [
          "light_rail"
        ]
      },
      {
        "text": "\ud83d\ude89",
        "id": "STATION",
        "alt_names": [
          "station"
        ]
      },
      {
        "text": "\ud83d\ude8a",
        "id": "TRAM",
        "alt_names": [
          "tram"
        ]
      },
      {
        "text": "\ud83d\ude9d",
        "id": "MONORAIL",
        "alt_names": [
          "monorail"
        ]
      },
      {
        "text": "\ud83d\ude9e",
        "id": "MOUNTAIN RAILWAY",
        "alt_names": [
          "mountain_railway"
        ]
      },
      {
        "text": "\ud83d\ude8b",
        "id": "TRAM CAR",
        "alt_names": [
          "train"
        ]
      },
      {
        "text": "\ud83d\ude8c",
        "id": "BUS",
        "alt_names": [
          "bus"
        ]
      },
      {
        "text": "\ud83d\ude8d",
        "id": "ONCOMING BUS",
        "alt_names": [
          "oncoming_bus"
        ]
      },
      {
        "text": "\ud83d\ude8e",
        "id": "TROLLEYBUS",
        "alt_names": [
          "trolleybus"
        ]
      },
      {
        "text": "\ud83d\ude90",
        "id": "MINIBUS",
        "alt_names": [
          "minibus"
        ]
      },
      {
        "text": "\ud83d\ude91",
        "id": "AMBULANCE",
        "alt_names": [
          "ambulance"
        ]
      },
      {
        "text": "\ud83d\ude92",
        "id": "FIRE ENGINE",
        "alt_names": [
          "fire_engine"
        ]
      },
      {
        "text": "\ud83d\ude93",
        "id": "POLICE CAR",
        "alt_names": [
          "police_car"
        ]
      },
      {
        "text": "\ud83d\ude94",
        "id": "ONCOMING POLICE CAR",
        "alt_names": [
          "oncoming_police_car"
        ]
      },
      {
        "text": "\ud83d\ude95",
        "id": "TAXI",
        "alt_names": [
          "taxi"
        ]
      },
      {
        "text": "\ud83d\ude96",
        "id": "ONCOMING TAXI",
        "alt_names": [
          "oncoming_taxi"
        ]
      },
      {
        "text": "\ud83d\ude97",
        "id": "AUTOMOBILE",
        "alt_names": [
          "car",
          "red_car"
        ]
      },
      {
        "text": "\ud83d\ude98",
        "id": "ONCOMING AUTOMOBILE",
        "alt_names": [
          "oncoming_automobile"
        ]
      },
      {
        "text": "\ud83d\ude99",
        "id": "RECREATIONAL VEHICLE",
        "alt_names": [
          "blue_car"
        ]
      },
      {
        "text": "\ud83d\udefb",
        "id": "PICKUP TRUCK",
        "alt_names": [
          "pickup_truck"
        ]
      },
      {
        "text": "\ud83d\ude9a",
        "id": "DELIVERY TRUCK",
        "alt_names": [
          "truck"
        ]
      },
      {
        "text": "\ud83d\ude9b",
        "id": "ARTICULATED LORRY",
        "alt_names": [
          "articulated_lorry"
        ]
      },
      {
        "text": "\ud83d\ude9c",
        "id": "TRACTOR",
        "alt_names": [
          "tractor"
        ]
      },
      {
        "text": "\ud83d\udef5",
        "id": "MOTOR SCOOTER",
        "alt_names": [
          "motor_scooter"
        ]
      },
      {
        "text": "\ud83e\uddbd",
        "id": "MANUAL WHEELCHAIR",
        "alt_names": [
          "manual_wheelchair"
        ]
      },
      {
        "text": "\ud83e\uddbc",
        "id": "MOTORIZED WHEELCHAIR",
        "alt_names": [
          "motorized_wheelchair"
        ]
      },
      {
        "text": "\ud83d\udefa",
        "id": "AUTO RICKSHAW",
        "alt_names": [
          "auto_rickshaw"
        ]
      },
      {
        "text": "\ud83d\udeb2",
        "id": "BICYCLE",
        "alt_names": [
          "bike"
        ]
      },
      {
        "text": "\ud83d\udef4",
        "id": "SCOOTER",
        "alt_names": [
          "scooter",
          "kick_scooter"
        ]
      },
      {
        "text": "\ud83d\udef9",
        "id": "SKATEBOARD",
        "alt_names": [
          "skateboard"
        ]
      },
      {
        "text": "\ud83d\udefc",
        "id": "ROLLER SKATE",
        "alt_names": [
          "roller_skate"
        ]
      },
      {
        "text": "\ud83d\ude8f",
        "id": "BUS STOP",
        "alt_names": [
          "busstop"
        ]
      },
      {
        "text": "\u26fd",
        "id": "FUEL PUMP",
        "alt_names": [
          "fuelpump"
        ]
      },
      {
        "text": "\ud83d\udea8",
        "id": "POLICE CARS REVOLVING LIGHT",
        "alt_names": [
          "rotating_light"
        ]
      },
      {
        "text": "\ud83d\udea5",
        "id": "HORIZONTAL TRAFFIC LIGHT",
        "alt_names": [
          "traffic_light"
        ]
      },
      {
        "text": "\ud83d\udea6",
        "id": "VERTICAL TRAFFIC LIGHT",
        "alt_names": [
          "vertical_traffic_light"
        ]
      },
      {
        "text": "\ud83d\uded1",
        "id": "OCTAGONAL SIGN",
        "alt_names": [
          "octagonal_sign",
          "stop_sign"
        ]
      },
      {
        "text": "\ud83d\udea7",
        "id": "CONSTRUCTION SIGN",
        "alt_names": [
          "construction"
        ]
      },
      {
        "text": "\u2693",
        "id": "ANCHOR",
        "alt_names": [
          "anchor"
        ]
      },
      {
        "text": "\u26f5",
        "id": "SAILBOAT",
        "alt_names": [
          "boat",
          "sailboat"
        ]
      },
      {
        "text": "\ud83d\udef6",
        "id": "CANOE",
        "alt_names": [
          "canoe"
        ]
      },
      {
        "text": "\ud83d\udea4",
        "id": "SPEEDBOAT",
        "alt_names": [
          "speedboat"
        ]
      },
      {
        "text": "\ud83d\udea2",
        "id": "SHIP",
        "alt_names": [
          "ship"
        ]
      },
      {
        "text": "\ud83d\udeeb",
        "id": "AIRPLANE DEPARTURE",
        "alt_names": [
          "airplane_departure",
          "flight_departure"
        ]
      },
      {
        "text": "\ud83d\udeec",
        "id": "AIRPLANE ARRIVING",
        "alt_names": [
          "airplane_arriving",
          "flight_arrival"
        ]
      },
      {
        "text": "\ud83e\ude82",
        "id": "PARACHUTE",
        "alt_names": [
          "parachute"
        ]
      },
      {
        "text": "\ud83d\udcba",
        "id": "SEAT",
        "alt_names": [
          "seat"
        ]
      },
      {
        "text": "\ud83d\ude81",
        "id": "HELICOPTER",
        "alt_names": [
          "helicopter"
        ]
      },
      {
        "text": "\ud83d\ude9f",
        "id": "SUSPENSION RAILWAY",
        "alt_names": [
          "suspension_railway"
        ]
      },
      {
        "text": "\ud83d\udea0",
        "id": "MOUNTAIN CABLEWAY",
        "alt_names": [
          "mountain_cableway"
        ]
      },
      {
        "text": "\ud83d\udea1",
        "id": "AERIAL TRAMWAY",
        "alt_names": [
          "aerial_tramway"
        ]
      },
      {
        "text": "\ud83d\ude80",
        "id": "ROCKET",
        "alt_names": [
          "rocket"
        ]
      },
      {
        "text": "\ud83d\udef8",
        "id": "FLYING SAUCER",
        "alt_names": [
          "flying_saucer"
        ]
      },
      {
        "text": "\ud83e\uddf3",
        "id": "LUGGAGE",
        "alt_names": [
          "luggage"
        ]
      },
      {
        "text": "\u231b",
        "id": "HOURGLASS",
        "alt_names": [
          "hourglass"
        ]
      },
      {
        "text": "\u23f3",
        "id": "HOURGLASS WITH FLOWING SAND",
        "alt_names": [
          "hourglass_flowing_sand"
        ]
      },
      {
        "text": "\u231a",
        "id": "WATCH",
        "alt_names": [
          "watch"
        ]
      },
      {
        "text": "\u23f0",
        "id": "ALARM CLOCK",
        "alt_names": [
          "alarm_clock"
        ]
      },
      {
        "text": "\ud83d\udd5b",
        "id": "CLOCK FACE TWELVE OCLOCK",
        "alt_names": [
          "clock12"
        ]
      },
      {
        "text": "\ud83d\udd67",
        "id": "CLOCK FACE TWELVE-THIRTY",
        "alt_names": [
          "clock1230"
        ]
      },
      {
        "text": "\ud83d\udd50",
        "id": "CLOCK FACE ONE OCLOCK",
        "alt_names": [
          "clock1"
        ]
      },
      {
        "text": "\ud83d\udd5c",
        "id": "CLOCK FACE ONE-THIRTY",
        "alt_names": [
          "clock130"
        ]
      },
      {
        "text": "\ud83d\udd51",
        "id": "CLOCK FACE TWO OCLOCK",
        "alt_names": [
          "clock2"
        ]
      },
      {
        "text": "\ud83d\udd5d",
        "id": "CLOCK FACE TWO-THIRTY",
        "alt_names": [
          "clock230"
        ]
      },
      {
        "text": "\ud83d\udd52",
        "id": "CLOCK FACE THREE OCLOCK",
        "alt_names": [
          "clock3"
        ]
      },
      {
        "text": "\ud83d\udd5e",
        "id": "CLOCK FACE THREE-THIRTY",
        "alt_names": [
          "clock330"
        ]
      },
      {
        "text": "\ud83d\udd53",
        "id": "CLOCK FACE FOUR OCLOCK",
        "alt_names": [
          "clock4"
        ]
      },
      {
        "text": "\ud83d\udd5f",
        "id": "CLOCK FACE FOUR-THIRTY",
        "alt_names": [
          "clock430"
        ]
      },
      {
        "text": "\ud83d\udd54",
        "id": "CLOCK FACE FIVE OCLOCK",
        "alt_names": [
          "clock5"
        ]
      },
      {
        "text": "\ud83d\udd60",
        "id": "CLOCK FACE FIVE-THIRTY",
        "alt_names": [
          "clock530"
        ]
      },
      {
        "text": "\ud83d\udd55",
        "id": "CLOCK FACE SIX OCLOCK",
        "alt_names": [
          "clock6"
        ]
      },
      {
        "text": "\ud83d\udd61",
        "id": "CLOCK FACE SIX-THIRTY",
        "alt_names": [
          "clock630"
        ]
      },
      {
        "text": "\ud83d\udd56",
        "id": "CLOCK FACE SEVEN OCLOCK",
        "alt_names": [
          "clock7"
        ]
      },
      {
        "text": "\ud83d\udd62",
        "id": "CLOCK FACE SEVEN-THIRTY",
        "alt_names": [
          "clock730"
        ]
      },
      {
        "text": "\ud83d\udd57",
        "id": "CLOCK FACE EIGHT OCLOCK",
        "alt_names": [
          "clock8"
        ]
      },
      {
        "text": "\ud83d\udd63",
        "id": "CLOCK FACE EIGHT-THIRTY",
        "alt_names": [
          "clock830"
        ]
      },
      {
        "text": "\ud83d\udd58",
        "id": "CLOCK FACE NINE OCLOCK",
        "alt_names": [
          "clock9"
        ]
      },
      {
        "text": "\ud83d\udd64",
        "id": "CLOCK FACE NINE-THIRTY",
        "alt_names": [
          "clock930"
        ]
      },
      {
        "text": "\ud83d\udd59",
        "id": "CLOCK FACE TEN OCLOCK",
        "alt_names": [
          "clock10"
        ]
      },
      {
        "text": "\ud83d\udd65",
        "id": "CLOCK FACE TEN-THIRTY",
        "alt_names": [
          "clock1030"
        ]
      },
      {
        "text": "\ud83d\udd5a",
        "id": "CLOCK FACE ELEVEN OCLOCK",
        "alt_names": [
          "clock11"
        ]
      },
      {
        "text": "\ud83d\udd66",
        "id": "CLOCK FACE ELEVEN-THIRTY",
        "alt_names": [
          "clock1130"
        ]
      },
      {
        "text": "\ud83c\udf11",
        "id": "NEW MOON SYMBOL",
        "alt_names": [
          "new_moon"
        ]
      },
      {
        "text": "\ud83c\udf12",
        "id": "WAXING CRESCENT MOON SYMBOL",
        "alt_names": [
          "waxing_crescent_moon"
        ]
      },
      {
        "text": "\ud83c\udf13",
        "id": "FIRST QUARTER MOON SYMBOL",
        "alt_names": [
          "first_quarter_moon"
        ]
      },
      {
        "text": "\ud83c\udf14",
        "id": "WAXING GIBBOUS MOON SYMBOL",
        "alt_names": [
          "moon",
          "waxing_gibbous_moon"
        ]
      },
      {
        "text": "\ud83c\udf15",
        "id": "FULL MOON SYMBOL",
        "alt_names": [
          "full_moon"
        ]
      },
      {
        "text": "\ud83c\udf16",
        "id": "WANING GIBBOUS MOON SYMBOL",
        "alt_names": [
          "waning_gibbous_moon"
        ]
      },
      {
        "text": "\ud83c\udf17",
        "id": "LAST QUARTER MOON SYMBOL",
        "alt_names": [
          "last_quarter_moon"
        ]
      },
      {
        "text": "\ud83c\udf18",
        "id": "WANING CRESCENT MOON SYMBOL",
        "alt_names": [
          "waning_crescent_moon"
        ]
      },
      {
        "text": "\ud83c\udf19",
        "id": "CRESCENT MOON",
        "alt_names": [
          "crescent_moon"
        ]
      },
      {
        "text": "\ud83c\udf1a",
        "id": "NEW MOON WITH FACE",
        "alt_names": [
          "new_moon_with_face"
        ]
      },
      {
        "text": "\ud83c\udf1b",
        "id": "FIRST QUARTER MOON WITH FACE",
        "alt_names": [
          "first_quarter_moon_with_face"
        ]
      },
      {
        "text": "\ud83c\udf1c",
        "id": "LAST QUARTER MOON WITH FACE",
        "alt_names": [
          "last_quarter_moon_with_face"
        ]
      },
      {
        "text": "\ud83c\udf1d",
        "id": "FULL MOON WITH FACE",
        "alt_names": [
          "full_moon_with_face"
        ]
      },
      {
        "text": "\ud83c\udf1e",
        "id": "SUN WITH FACE",
        "alt_names": [
          "sun_with_face"
        ]
      },
      {
        "text": "\ud83e\ude90",
        "id": "RINGED PLANET",
        "alt_names": [
          "ringed_planet"
        ]
      },
      {
        "text": "\u2b50",
        "id": "WHITE MEDIUM STAR",
        "alt_names": [
          "star"
        ]
      },
      {
        "text": "\ud83c\udf1f",
        "id": "GLOWING STAR",
        "alt_names": [
          "star2"
        ]
      },
      {
        "text": "\ud83c\udf20",
        "id": "SHOOTING STAR",
        "alt_names": [
          "stars"
        ]
      },
      {
        "text": "\ud83c\udf0c",
        "id": "MILKY WAY",
        "alt_names": [
          "milky_way"
        ]
      },
      {
        "text": "\u26c5",
        "id": "SUN BEHIND CLOUD",
        "alt_names": [
          "partly_sunny"
        ]
      },
      {
        "text": "\ud83c\udf00",
        "id": "CYCLONE",
        "alt_names": [
          "cyclone"
        ]
      },
      {
        "text": "\ud83c\udf08",
        "id": "RAINBOW",
        "alt_names": [
          "rainbow"
        ]
      },
      {
        "text": "\ud83c\udf02",
        "id": "CLOSED UMBRELLA",
        "alt_names": [
          "closed_umbrella"
        ]
      },
      {
        "text": "\u2614",
        "id": "UMBRELLA WITH RAIN DROPS",
        "alt_names": [
          "umbrella_with_rain_drops"
        ]
      },
      {
        "text": "\u26a1",
        "id": "HIGH VOLTAGE SIGN",
        "alt_names": [
          "zap"
        ]
      },
      {
        "text": "\u26c4",
        "id": "SNOWMAN WITHOUT SNOW",
        "alt_names": [
          "snowman_without_snow"
        ]
      },
      {
        "text": "\ud83d\udd25",
        "id": "FIRE",
        "alt_names": [
          "fire"
        ]
      },
      {
        "text": "\ud83d\udca7",
        "id": "DROPLET",
        "alt_names": [
          "droplet"
        ]
      },
      {
        "text": "\ud83c\udf0a",
        "id": "WATER WAVE",
        "alt_names": [
          "ocean"
        ]
      }
    ]
  },
  {
    "text": "Activities",
    "children": [
      {
        "text": "\ud83c\udf83",
        "id": "JACK-O-LANTERN",
        "alt_names": [
          "jack_o_lantern"
        ]
      },
      {
        "text": "\ud83c\udf84",
        "id": "CHRISTMAS TREE",
        "alt_names": [
          "christmas_tree"
        ]
      },
      {
        "text": "\ud83c\udf86",
        "id": "FIREWORKS",
        "alt_names": [
          "fireworks"
        ]
      },
      {
        "text": "\ud83c\udf87",
        "id": "FIREWORK SPARKLER",
        "alt_names": [
          "sparkler"
        ]
      },
      {
        "text": "\ud83e\udde8",
        "id": "FIRECRACKER",
        "alt_names": [
          "firecracker"
        ]
      },
      {
        "text": "\u2728",
        "id": "SPARKLES",
        "alt_names": [
          "sparkles"
        ]
      },
      {
        "text": "\ud83c\udf88",
        "id": "BALLOON",
        "alt_names": [
          "balloon"
        ]
      },
      {
        "text": "\ud83c\udf89",
        "id": "PARTY POPPER",
        "alt_names": [
          "tada"
        ]
      },
      {
        "text": "\ud83c\udf8a",
        "id": "CONFETTI BALL",
        "alt_names": [
          "confetti_ball"
        ]
      },
      {
        "text": "\ud83c\udf8b",
        "id": "TANABATA TREE",
        "alt_names": [
          "tanabata_tree"
        ]
      },
      {
        "text": "\ud83c\udf8d",
        "id": "PINE DECORATION",
        "alt_names": [
          "bamboo"
        ]
      },
      {
        "text": "\ud83c\udf8e",
        "id": "JAPANESE DOLLS",
        "alt_names": [
          "dolls"
        ]
      },
      {
        "text": "\ud83c\udf8f",
        "id": "CARP STREAMER",
        "alt_names": [
          "flags"
        ]
      },
      {
        "text": "\ud83c\udf90",
        "id": "WIND CHIME",
        "alt_names": [
          "wind_chime"
        ]
      },
      {
        "text": "\ud83c\udf91",
        "id": "MOON VIEWING CEREMONY",
        "alt_names": [
          "rice_scene"
        ]
      },
      {
        "text": "\ud83e\udde7",
        "id": "RED GIFT ENVELOPE",
        "alt_names": [
          "red_envelope"
        ]
      },
      {
        "text": "\ud83c\udf80",
        "id": "RIBBON",
        "alt_names": [
          "ribbon"
        ]
      },
      {
        "text": "\ud83c\udf81",
        "id": "WRAPPED PRESENT",
        "alt_names": [
          "gift"
        ]
      },
      {
        "text": "\ud83c\udfab",
        "id": "TICKET",
        "alt_names": [
          "ticket"
        ]
      },
      {
        "text": "\ud83c\udfc6",
        "id": "TROPHY",
        "alt_names": [
          "trophy"
        ]
      },
      {
        "text": "\ud83c\udfc5",
        "id": "SPORTS MEDAL",
        "alt_names": [
          "sports_medal",
          "medal_sports"
        ]
      },
      {
        "text": "\ud83e\udd47",
        "id": "FIRST PLACE MEDAL",
        "alt_names": [
          "first_place_medal",
          "1st_place_medal"
        ]
      },
      {
        "text": "\ud83e\udd48",
        "id": "SECOND PLACE MEDAL",
        "alt_names": [
          "second_place_medal",
          "2nd_place_medal"
        ]
      },
      {
        "text": "\ud83e\udd49",
        "id": "THIRD PLACE MEDAL",
        "alt_names": [
          "third_place_medal",
          "3rd_place_medal"
        ]
      },
      {
        "text": "\u26bd",
        "id": "SOCCER BALL",
        "alt_names": [
          "soccer"
        ]
      },
      {
        "text": "\u26be",
        "id": "BASEBALL",
        "alt_names": [
          "baseball"
        ]
      },
      {
        "text": "\ud83e\udd4e",
        "id": "SOFTBALL",
        "alt_names": [
          "softball"
        ]
      },
      {
        "text": "\ud83c\udfc0",
        "id": "BASKETBALL AND HOOP",
        "alt_names": [
          "basketball"
        ]
      },
      {
        "text": "\ud83c\udfd0",
        "id": "VOLLEYBALL",
        "alt_names": [
          "volleyball"
        ]
      },
      {
        "text": "\ud83c\udfc8",
        "id": "AMERICAN FOOTBALL",
        "alt_names": [
          "football"
        ]
      },
      {
        "text": "\ud83c\udfc9",
        "id": "RUGBY FOOTBALL",
        "alt_names": [
          "rugby_football"
        ]
      },
      {
        "text": "\ud83c\udfbe",
        "id": "TENNIS RACQUET AND BALL",
        "alt_names": [
          "tennis"
        ]
      },
      {
        "text": "\ud83e\udd4f",
        "id": "FLYING DISC",
        "alt_names": [
          "flying_disc"
        ]
      },
      {
        "text": "\ud83c\udfb3",
        "id": "BOWLING",
        "alt_names": [
          "bowling"
        ]
      },
      {
        "text": "\ud83c\udfcf",
        "id": "CRICKET BAT AND BALL",
        "alt_names": [
          "cricket_bat_and_ball"
        ]
      },
      {
        "text": "\ud83c\udfd1",
        "id": "FIELD HOCKEY STICK AND BALL",
        "alt_names": [
          "field_hockey_stick_and_ball",
          "field_hockey"
        ]
      },
      {
        "text": "\ud83c\udfd2",
        "id": "ICE HOCKEY STICK AND PUCK",
        "alt_names": [
          "ice_hockey_stick_and_puck",
          "ice_hockey"
        ]
      },
      {
        "text": "\ud83e\udd4d",
        "id": "LACROSSE STICK AND BALL",
        "alt_names": [
          "lacrosse"
        ]
      },
      {
        "text": "\ud83c\udfd3",
        "id": "TABLE TENNIS PADDLE AND BALL",
        "alt_names": [
          "table_tennis_paddle_and_ball",
          "ping_pong"
        ]
      },
      {
        "text": "\ud83c\udff8",
        "id": "BADMINTON RACQUET AND SHUTTLECOCK",
        "alt_names": [
          "badminton_racquet_and_shuttlecock",
          "badminton"
        ]
      },
      {
        "text": "\ud83e\udd4a",
        "id": "BOXING GLOVE",
        "alt_names": [
          "boxing_glove"
        ]
      },
      {
        "text": "\ud83e\udd4b",
        "id": "MARTIAL ARTS UNIFORM",
        "alt_names": [
          "martial_arts_uniform"
        ]
      },
      {
        "text": "\ud83e\udd45",
        "id": "GOAL NET",
        "alt_names": [
          "goal_net"
        ]
      },
      {
        "text": "\u26f3",
        "id": "FLAG IN HOLE",
        "alt_names": [
          "golf"
        ]
      },
      {
        "text": "\ud83c\udfa3",
        "id": "FISHING POLE AND FISH",
        "alt_names": [
          "fishing_pole_and_fish"
        ]
      },
      {
        "text": "\ud83e\udd3f",
        "id": "DIVING MASK",
        "alt_names": [
          "diving_mask"
        ]
      },
      {
        "text": "\ud83c\udfbd",
        "id": "RUNNING SHIRT WITH SASH",
        "alt_names": [
          "running_shirt_with_sash"
        ]
      },
      {
        "text": "\ud83c\udfbf",
        "id": "SKI AND SKI BOOT",
        "alt_names": [
          "ski"
        ]
      },
      {
        "text": "\ud83d\udef7",
        "id": "SLED",
        "alt_names": [
          "sled"
        ]
      },
      {
        "text": "\ud83e\udd4c",
        "id": "CURLING STONE",
        "alt_names": [
          "curling_stone"
        ]
      },
      {
        "text": "\ud83c\udfaf",
        "id": "DIRECT HIT",
        "alt_names": [
          "dart"
        ]
      },
      {
        "text": "\ud83e\ude80",
        "id": "YO-YO",
        "alt_names": [
          "yo-yo"
        ]
      },
      {
        "text": "\ud83e\ude81",
        "id": "KITE",
        "alt_names": [
          "kite"
        ]
      },
      {
        "text": "\ud83c\udfb1",
        "id": "BILLIARDS",
        "alt_names": [
          "8ball"
        ]
      },
      {
        "text": "\ud83d\udd2e",
        "id": "CRYSTAL BALL",
        "alt_names": [
          "crystal_ball"
        ]
      },
      {
        "text": "\ud83e\ude84",
        "id": "MAGIC WAND",
        "alt_names": [
          "magic_wand"
        ]
      },
      {
        "text": "\ud83e\uddff",
        "id": "NAZAR AMULET",
        "alt_names": [
          "nazar_amulet"
        ]
      },
      {
        "text": "\ud83c\udfae",
        "id": "VIDEO GAME",
        "alt_names": [
          "video_game"
        ]
      },
      {
        "text": "\ud83c\udfb0",
        "id": "SLOT MACHINE",
        "alt_names": [
          "slot_machine"
        ]
      },
      {
        "text": "\ud83c\udfb2",
        "id": "GAME DIE",
        "alt_names": [
          "game_die"
        ]
      },
      {
        "text": "\ud83e\udde9",
        "id": "JIGSAW PUZZLE PIECE",
        "alt_names": [
          "jigsaw"
        ]
      },
      {
        "text": "\ud83e\uddf8",
        "id": "TEDDY BEAR",
        "alt_names": [
          "teddy_bear"
        ]
      },
      {
        "text": "\ud83e\ude85",
        "id": "PINATA",
        "alt_names": [
          "pinata"
        ]
      },
      {
        "text": "\ud83e\ude86",
        "id": "NESTING DOLLS",
        "alt_names": [
          "nesting_dolls"
        ]
      },
      {
        "text": "\ud83c\udccf",
        "id": "PLAYING CARD BLACK JOKER",
        "alt_names": [
          "black_joker"
        ]
      },
      {
        "text": "\ud83c\udc04",
        "id": "MAHJONG TILE RED DRAGON",
        "alt_names": [
          "mahjong"
        ]
      },
      {
        "text": "\ud83c\udfb4",
        "id": "FLOWER PLAYING CARDS",
        "alt_names": [
          "flower_playing_cards"
        ]
      },
      {
        "text": "\ud83c\udfad",
        "id": "PERFORMING ARTS",
        "alt_names": [
          "performing_arts"
        ]
      },
      {
        "text": "\ud83c\udfa8",
        "id": "ARTIST PALETTE",
        "alt_names": [
          "art"
        ]
      },
      {
        "text": "\ud83e\uddf5",
        "id": "SPOOL OF THREAD",
        "alt_names": [
          "thread"
        ]
      },
      {
        "text": "\ud83e\udea1",
        "id": "SEWING NEEDLE",
        "alt_names": [
          "sewing_needle"
        ]
      },
      {
        "text": "\ud83e\uddf6",
        "id": "BALL OF YARN",
        "alt_names": [
          "yarn"
        ]
      },
      {
        "text": "\ud83e\udea2",
        "id": "KNOT",
        "alt_names": [
          "knot"
        ]
      }
    ]
  },
  {
    "text": "Objects",
    "children": [
      {
        "text": "\ud83d\udc53",
        "id": "EYEGLASSES",
        "alt_names": [
          "eyeglasses"
        ]
      },
      {
        "text": "\ud83e\udd7d",
        "id": "GOGGLES",
        "alt_names": [
          "goggles"
        ]
      },
      {
        "text": "\ud83e\udd7c",
        "id": "LAB COAT",
        "alt_names": [
          "lab_coat"
        ]
      },
      {
        "text": "\ud83e\uddba",
        "id": "SAFETY VEST",
        "alt_names": [
          "safety_vest"
        ]
      },
      {
        "text": "\ud83d\udc54",
        "id": "NECKTIE",
        "alt_names": [
          "necktie"
        ]
      },
      {
        "text": "\ud83d\udc55",
        "id": "T-SHIRT",
        "alt_names": [
          "shirt",
          "tshirt"
        ]
      },
      {
        "text": "\ud83d\udc56",
        "id": "JEANS",
        "alt_names": [
          "jeans"
        ]
      },
      {
        "text": "\ud83e\udde3",
        "id": "SCARF",
        "alt_names": [
          "scarf"
        ]
      },
      {
        "text": "\ud83e\udde4",
        "id": "GLOVES",
        "alt_names": [
          "gloves"
        ]
      },
      {
        "text": "\ud83e\udde5",
        "id": "COAT",
        "alt_names": [
          "coat"
        ]
      },
      {
        "text": "\ud83e\udde6",
        "id": "SOCKS",
        "alt_names": [
          "socks"
        ]
      },
      {
        "text": "\ud83d\udc57",
        "id": "DRESS",
        "alt_names": [
          "dress"
        ]
      },
      {
        "text": "\ud83d\udc58",
        "id": "KIMONO",
        "alt_names": [
          "kimono"
        ]
      },
      {
        "text": "\ud83e\udd7b",
        "id": "SARI",
        "alt_names": [
          "sari"
        ]
      },
      {
        "text": "\ud83e\ude71",
        "id": "ONE-PIECE SWIMSUIT",
        "alt_names": [
          "one-piece_swimsuit"
        ]
      },
      {
        "text": "\ud83e\ude72",
        "id": "BRIEFS",
        "alt_names": [
          "briefs"
        ]
      },
      {
        "text": "\ud83e\ude73",
        "id": "SHORTS",
        "alt_names": [
          "shorts"
        ]
      },
      {
        "text": "\ud83d\udc59",
        "id": "BIKINI",
        "alt_names": [
          "bikini"
        ]
      },
      {
        "text": "\ud83d\udc5a",
        "id": "WOMANS CLOTHES",
        "alt_names": [
          "womans_clothes"
        ]
      },
      {
        "text": "\ud83d\udc5b",
        "id": "PURSE",
        "alt_names": [
          "purse"
        ]
      },
      {
        "text": "\ud83d\udc5c",
        "id": "HANDBAG",
        "alt_names": [
          "handbag"
        ]
      },
      {
        "text": "\ud83d\udc5d",
        "id": "POUCH",
        "alt_names": [
          "pouch"
        ]
      },
      {
        "text": "\ud83c\udf92",
        "id": "SCHOOL SATCHEL",
        "alt_names": [
          "school_satchel"
        ]
      },
      {
        "text": "\ud83e\ude74",
        "id": "THONG SANDAL",
        "alt_names": [
          "thong_sandal"
        ]
      },
      {
        "text": "\ud83d\udc5e",
        "id": "MANS SHOE",
        "alt_names": [
          "mans_shoe",
          "shoe"
        ]
      },
      {
        "text": "\ud83d\udc5f",
        "id": "ATHLETIC SHOE",
        "alt_names": [
          "athletic_shoe"
        ]
      },
      {
        "text": "\ud83e\udd7e",
        "id": "HIKING BOOT",
        "alt_names": [
          "hiking_boot"
        ]
      },
      {
        "text": "\ud83e\udd7f",
        "id": "FLAT SHOE",
        "alt_names": [
          "womans_flat_shoe"
        ]
      },
      {
        "text": "\ud83d\udc60",
        "id": "HIGH-HEELED SHOE",
        "alt_names": [
          "high_heel"
        ]
      },
      {
        "text": "\ud83d\udc61",
        "id": "WOMANS SANDAL",
        "alt_names": [
          "sandal"
        ]
      },
      {
        "text": "\ud83e\ude70",
        "id": "BALLET SHOES",
        "alt_names": [
          "ballet_shoes"
        ]
      },
      {
        "text": "\ud83d\udc62",
        "id": "WOMANS BOOTS",
        "alt_names": [
          "boot"
        ]
      },
      {
        "text": "\ud83d\udc51",
        "id": "CROWN",
        "alt_names": [
          "crown"
        ]
      },
      {
        "text": "\ud83d\udc52",
        "id": "WOMANS HAT",
        "alt_names": [
          "womans_hat"
        ]
      },
      {
        "text": "\ud83c\udfa9",
        "id": "TOP HAT",
        "alt_names": [
          "tophat"
        ]
      },
      {
        "text": "\ud83c\udf93",
        "id": "GRADUATION CAP",
        "alt_names": [
          "mortar_board"
        ]
      },
      {
        "text": "\ud83e\udde2",
        "id": "BILLED CAP",
        "alt_names": [
          "billed_cap"
        ]
      },
      {
        "text": "\ud83e\ude96",
        "id": "MILITARY HELMET",
        "alt_names": [
          "military_helmet"
        ]
      },
      {
        "text": "\ud83d\udcff",
        "id": "PRAYER BEADS",
        "alt_names": [
          "prayer_beads"
        ]
      },
      {
        "text": "\ud83d\udc84",
        "id": "LIPSTICK",
        "alt_names": [
          "lipstick"
        ]
      },
      {
        "text": "\ud83d\udc8d",
        "id": "RING",
        "alt_names": [
          "ring"
        ]
      },
      {
        "text": "\ud83d\udc8e",
        "id": "GEM STONE",
        "alt_names": [
          "gem"
        ]
      },
      {
        "text": "\ud83d\udd07",
        "id": "SPEAKER WITH CANCELLATION STROKE",
        "alt_names": [
          "mute"
        ]
      },
      {
        "text": "\ud83d\udd08",
        "id": "SPEAKER",
        "alt_names": [
          "speaker"
        ]
      },
      {
        "text": "\ud83d\udd09",
        "id": "SPEAKER WITH ONE SOUND WAVE",
        "alt_names": [
          "sound"
        ]
      },
      {
        "text": "\ud83d\udd0a",
        "id": "SPEAKER WITH THREE SOUND WAVES",
        "alt_names": [
          "loud_sound"
        ]
      },
      {
        "text": "\ud83d\udce2",
        "id": "PUBLIC ADDRESS LOUDSPEAKER",
        "alt_names": [
          "loudspeaker"
        ]
      },
      {
        "text": "\ud83d\udce3",
        "id": "CHEERING MEGAPHONE",
        "alt_names": [
          "mega"
        ]
      },
      {
        "text": "\ud83d\udcef",
        "id": "POSTAL HORN",
        "alt_names": [
          "postal_horn"
        ]
      },
      {
        "text": "\ud83d\udd14",
        "id": "BELL",
        "alt_names": [
          "bell"
        ]
      },
      {
        "text": "\ud83d\udd15",
        "id": "BELL WITH CANCELLATION STROKE",
        "alt_names": [
          "no_bell"
        ]
      },
      {
        "text": "\ud83c\udfbc",
        "id": "MUSICAL SCORE",
        "alt_names": [
          "musical_score"
        ]
      },
      {
        "text": "\ud83c\udfb5",
        "id": "MUSICAL NOTE",
        "alt_names": [
          "musical_note"
        ]
      },
      {
        "text": "\ud83c\udfb6",
        "id": "MULTIPLE MUSICAL NOTES",
        "alt_names": [
          "notes"
        ]
      },
      {
        "text": "\ud83c\udfa4",
        "id": "MICROPHONE",
        "alt_names": [
          "microphone"
        ]
      },
      {
        "text": "\ud83c\udfa7",
        "id": "HEADPHONE",
        "alt_names": [
          "headphones"
        ]
      },
      {
        "text": "\ud83d\udcfb",
        "id": "RADIO",
        "alt_names": [
          "radio"
        ]
      },
      {
        "text": "\ud83c\udfb7",
        "id": "SAXOPHONE",
        "alt_names": [
          "saxophone"
        ]
      },
      {
        "text": "\ud83e\ude97",
        "id": "ACCORDION",
        "alt_names": [
          "accordion"
        ]
      },
      {
        "text": "\ud83c\udfb8",
        "id": "GUITAR",
        "alt_names": [
          "guitar"
        ]
      },
      {
        "text": "\ud83c\udfb9",
        "id": "MUSICAL KEYBOARD",
        "alt_names": [
          "musical_keyboard"
        ]
      },
      {
        "text": "\ud83c\udfba",
        "id": "TRUMPET",
        "alt_names": [
          "trumpet"
        ]
      },
      {
        "text": "\ud83c\udfbb",
        "id": "VIOLIN",
        "alt_names": [
          "violin"
        ]
      },
      {
        "text": "\ud83e\ude95",
        "id": "BANJO",
        "alt_names": [
          "banjo"
        ]
      },
      {
        "text": "\ud83e\udd41",
        "id": "DRUM WITH DRUMSTICKS",
        "alt_names": [
          "drum_with_drumsticks",
          "drum"
        ]
      },
      {
        "text": "\ud83e\ude98",
        "id": "LONG DRUM",
        "alt_names": [
          "long_drum"
        ]
      },
      {
        "text": "\ud83d\udcf1",
        "id": "MOBILE PHONE",
        "alt_names": [
          "iphone"
        ]
      },
      {
        "text": "\ud83d\udcf2",
        "id": "MOBILE PHONE WITH RIGHTWARDS ARROW AT LEFT",
        "alt_names": [
          "calling"
        ]
      },
      {
        "text": "\ud83d\udcde",
        "id": "TELEPHONE RECEIVER",
        "alt_names": [
          "telephone_receiver"
        ]
      },
      {
        "text": "\ud83d\udcdf",
        "id": "PAGER",
        "alt_names": [
          "pager"
        ]
      },
      {
        "text": "\ud83d\udce0",
        "id": "FAX MACHINE",
        "alt_names": [
          "fax"
        ]
      },
      {
        "text": "\ud83d\udd0b",
        "id": "BATTERY",
        "alt_names": [
          "battery"
        ]
      },
      {
        "text": "\ud83d\udd0c",
        "id": "ELECTRIC PLUG",
        "alt_names": [
          "electric_plug"
        ]
      },
      {
        "text": "\ud83d\udcbb",
        "id": "PERSONAL COMPUTER",
        "alt_names": [
          "computer"
        ]
      },
      {
        "text": "\ud83d\udcbd",
        "id": "MINIDISC",
        "alt_names": [
          "minidisc"
        ]
      },
      {
        "text": "\ud83d\udcbe",
        "id": "FLOPPY DISK",
        "alt_names": [
          "floppy_disk"
        ]
      },
      {
        "text": "\ud83d\udcbf",
        "id": "OPTICAL DISC",
        "alt_names": [
          "cd"
        ]
      },
      {
        "text": "\ud83d\udcc0",
        "id": "DVD",
        "alt_names": [
          "dvd"
        ]
      },
      {
        "text": "\ud83e\uddee",
        "id": "ABACUS",
        "alt_names": [
          "abacus"
        ]
      },
      {
        "text": "\ud83c\udfa5",
        "id": "MOVIE CAMERA",
        "alt_names": [
          "movie_camera"
        ]
      },
      {
        "text": "\ud83c\udfac",
        "id": "CLAPPER BOARD",
        "alt_names": [
          "clapper"
        ]
      },
      {
        "text": "\ud83d\udcfa",
        "id": "TELEVISION",
        "alt_names": [
          "tv"
        ]
      },
      {
        "text": "\ud83d\udcf7",
        "id": "CAMERA",
        "alt_names": [
          "camera"
        ]
      },
      {
        "text": "\ud83d\udcf8",
        "id": "CAMERA WITH FLASH",
        "alt_names": [
          "camera_with_flash",
          "camera_flash"
        ]
      },
      {
        "text": "\ud83d\udcf9",
        "id": "VIDEO CAMERA",
        "alt_names": [
          "video_camera"
        ]
      },
      {
        "text": "\ud83d\udcfc",
        "id": "VIDEOCASSETTE",
        "alt_names": [
          "vhs"
        ]
      },
      {
        "text": "\ud83d\udd0d",
        "id": "LEFT-POINTING MAGNIFYING GLASS",
        "alt_names": [
          "mag"
        ]
      },
      {
        "text": "\ud83d\udd0e",
        "id": "RIGHT-POINTING MAGNIFYING GLASS",
        "alt_names": [
          "mag_right"
        ]
      },
      {
        "text": "\ud83d\udca1",
        "id": "ELECTRIC LIGHT BULB",
        "alt_names": [
          "bulb"
        ]
      },
      {
        "text": "\ud83d\udd26",
        "id": "ELECTRIC TORCH",
        "alt_names": [
          "flashlight"
        ]
      },
      {
        "text": "\ud83c\udfee",
        "id": "IZAKAYA LANTERN",
        "alt_names": [
          "izakaya_lantern",
          "lantern"
        ]
      },
      {
        "text": "\ud83e\ude94",
        "id": "DIYA LAMP",
        "alt_names": [
          "diya_lamp"
        ]
      },
      {
        "text": "\ud83d\udcd4",
        "id": "NOTEBOOK WITH DECORATIVE COVER",
        "alt_names": [
          "notebook_with_decorative_cover"
        ]
      },
      {
        "text": "\ud83d\udcd5",
        "id": "CLOSED BOOK",
        "alt_names": [
          "closed_book"
        ]
      },
      {
        "text": "\ud83d\udcd6",
        "id": "OPEN BOOK",
        "alt_names": [
          "book",
          "open_book"
        ]
      },
      {
        "text": "\ud83d\udcd7",
        "id": "GREEN BOOK",
        "alt_names": [
          "green_book"
        ]
      },
      {
        "text": "\ud83d\udcd8",
        "id": "BLUE BOOK",
        "alt_names": [
          "blue_book"
        ]
      },
      {
        "text": "\ud83d\udcd9",
        "id": "ORANGE BOOK",
        "alt_names": [
          "orange_book"
        ]
      },
      {
        "text": "\ud83d\udcda",
        "id": "BOOKS",
        "alt_names": [
          "books"
        ]
      },
      {
        "text": "\ud83d\udcd3",
        "id": "NOTEBOOK",
        "alt_names": [
          "notebook"
        ]
      },
      {
        "text": "\ud83d\udcd2",
        "id": "LEDGER",
        "alt_names": [
          "ledger"
        ]
      },
      {
        "text": "\ud83d\udcc3",
        "id": "PAGE WITH CURL",
        "alt_names": [
          "page_with_curl"
        ]
      },
      {
        "text": "\ud83d\udcdc",
        "id": "SCROLL",
        "alt_names": [
          "scroll"
        ]
      },
      {
        "text": "\ud83d\udcc4",
        "id": "PAGE FACING UP",
        "alt_names": [
          "page_facing_up"
        ]
      },
      {
        "text": "\ud83d\udcf0",
        "id": "NEWSPAPER",
        "alt_names": [
          "newspaper"
        ]
      },
      {
        "text": "\ud83d\udcd1",
        "id": "BOOKMARK TABS",
        "alt_names": [
          "bookmark_tabs"
        ]
      },
      {
        "text": "\ud83d\udd16",
        "id": "BOOKMARK",
        "alt_names": [
          "bookmark"
        ]
      },
      {
        "text": "\ud83d\udcb0",
        "id": "MONEY BAG",
        "alt_names": [
          "moneybag"
        ]
      },
      {
        "text": "\ud83e\ude99",
        "id": "COIN",
        "alt_names": [
          "coin"
        ]
      },
      {
        "text": "\ud83d\udcb4",
        "id": "BANKNOTE WITH YEN SIGN",
        "alt_names": [
          "yen"
        ]
      },
      {
        "text": "\ud83d\udcb5",
        "id": "BANKNOTE WITH DOLLAR SIGN",
        "alt_names": [
          "dollar"
        ]
      },
      {
        "text": "\ud83d\udcb6",
        "id": "BANKNOTE WITH EURO SIGN",
        "alt_names": [
          "euro"
        ]
      },
      {
        "text": "\ud83d\udcb7",
        "id": "BANKNOTE WITH POUND SIGN",
        "alt_names": [
          "pound"
        ]
      },
      {
        "text": "\ud83d\udcb8",
        "id": "MONEY WITH WINGS",
        "alt_names": [
          "money_with_wings"
        ]
      },
      {
        "text": "\ud83d\udcb3",
        "id": "CREDIT CARD",
        "alt_names": [
          "credit_card"
        ]
      },
      {
        "text": "\ud83e\uddfe",
        "id": "RECEIPT",
        "alt_names": [
          "receipt"
        ]
      },
      {
        "text": "\ud83d\udcb9",
        "id": "CHART WITH UPWARDS TREND AND YEN SIGN",
        "alt_names": [
          "chart"
        ]
      },
      {
        "text": "\ud83d\udce7",
        "id": "E-MAIL SYMBOL",
        "alt_names": [
          "e-mail"
        ]
      },
      {
        "text": "\ud83d\udce8",
        "id": "INCOMING ENVELOPE",
        "alt_names": [
          "incoming_envelope"
        ]
      },
      {
        "text": "\ud83d\udce9",
        "id": "ENVELOPE WITH DOWNWARDS ARROW ABOVE",
        "alt_names": [
          "envelope_with_arrow"
        ]
      },
      {
        "text": "\ud83d\udce4",
        "id": "OUTBOX TRAY",
        "alt_names": [
          "outbox_tray"
        ]
      },
      {
        "text": "\ud83d\udce5",
        "id": "INBOX TRAY",
        "alt_names": [
          "inbox_tray"
        ]
      },
      {
        "text": "\ud83d\udce6",
        "id": "PACKAGE",
        "alt_names": [
          "package"
        ]
      },
      {
        "text": "\ud83d\udceb",
        "id": "CLOSED MAILBOX WITH RAISED FLAG",
        "alt_names": [
          "mailbox"
        ]
      },
      {
        "text": "\ud83d\udcea",
        "id": "CLOSED MAILBOX WITH LOWERED FLAG",
        "alt_names": [
          "mailbox_closed"
        ]
      },
      {
        "text": "\ud83d\udcec",
        "id": "OPEN MAILBOX WITH RAISED FLAG",
        "alt_names": [
          "mailbox_with_mail"
        ]
      },
      {
        "text": "\ud83d\udced",
        "id": "OPEN MAILBOX WITH LOWERED FLAG",
        "alt_names": [
          "mailbox_with_no_mail"
        ]
      },
      {
        "text": "\ud83d\udcee",
        "id": "POSTBOX",
        "alt_names": [
          "postbox"
        ]
      },
      {
        "text": "\ud83d\udcdd",
        "id": "MEMO",
        "alt_names": [
          "memo",
          "pencil"
        ]
      },
      {
        "text": "\ud83d\udcbc",
        "id": "BRIEFCASE",
        "alt_names": [
          "briefcase"
        ]
      },
      {
        "text": "\ud83d\udcc1",
        "id": "FILE FOLDER",
        "alt_names": [
          "file_folder"
        ]
      },
      {
        "text": "\ud83d\udcc2",
        "id": "OPEN FILE FOLDER",
        "alt_names": [
          "open_file_folder"
        ]
      },
      {
        "text": "\ud83d\udcc5",
        "id": "CALENDAR",
        "alt_names": [
          "date"
        ]
      },
      {
        "text": "\ud83d\udcc6",
        "id": "TEAR-OFF CALENDAR",
        "alt_names": [
          "calendar"
        ]
      },
      {
        "text": "\ud83d\udcc7",
        "id": "CARD INDEX",
        "alt_names": [
          "card_index"
        ]
      },
      {
        "text": "\ud83d\udcc8",
        "id": "CHART WITH UPWARDS TREND",
        "alt_names": [
          "chart_with_upwards_trend"
        ]
      },
      {
        "text": "\ud83d\udcc9",
        "id": "CHART WITH DOWNWARDS TREND",
        "alt_names": [
          "chart_with_downwards_trend"
        ]
      },
      {
        "text": "\ud83d\udcca",
        "id": "BAR CHART",
        "alt_names": [
          "bar_chart"
        ]
      },
      {
        "text": "\ud83d\udccb",
        "id": "CLIPBOARD",
        "alt_names": [
          "clipboard"
        ]
      },
      {
        "text": "\ud83d\udccc",
        "id": "PUSHPIN",
        "alt_names": [
          "pushpin"
        ]
      },
      {
        "text": "\ud83d\udccd",
        "id": "ROUND PUSHPIN",
        "alt_names": [
          "round_pushpin"
        ]
      },
      {
        "text": "\ud83d\udcce",
        "id": "PAPERCLIP",
        "alt_names": [
          "paperclip"
        ]
      },
      {
        "text": "\ud83d\udccf",
        "id": "STRAIGHT RULER",
        "alt_names": [
          "straight_ruler"
        ]
      },
      {
        "text": "\ud83d\udcd0",
        "id": "TRIANGULAR RULER",
        "alt_names": [
          "triangular_ruler"
        ]
      },
      {
        "text": "\ud83d\udd12",
        "id": "LOCK",
        "alt_names": [
          "lock"
        ]
      },
      {
        "text": "\ud83d\udd13",
        "id": "OPEN LOCK",
        "alt_names": [
          "unlock"
        ]
      },
      {
        "text": "\ud83d\udd0f",
        "id": "LOCK WITH INK PEN",
        "alt_names": [
          "lock_with_ink_pen"
        ]
      },
      {
        "text": "\ud83d\udd10",
        "id": "CLOSED LOCK WITH KEY",
        "alt_names": [
          "closed_lock_with_key"
        ]
      },
      {
        "text": "\ud83d\udd11",
        "id": "KEY",
        "alt_names": [
          "key"
        ]
      },
      {
        "text": "\ud83d\udd28",
        "id": "HAMMER",
        "alt_names": [
          "hammer"
        ]
      },
      {
        "text": "\ud83e\ude93",
        "id": "AXE",
        "alt_names": [
          "axe"
        ]
      },
      {
        "text": "\ud83d\udd2b",
        "id": "PISTOL",
        "alt_names": [
          "gun"
        ]
      },
      {
        "text": "\ud83e\ude83",
        "id": "BOOMERANG",
        "alt_names": [
          "boomerang"
        ]
      },
      {
        "text": "\ud83c\udff9",
        "id": "BOW AND ARROW",
        "alt_names": [
          "bow_and_arrow"
        ]
      },
      {
        "text": "\ud83e\ude9a",
        "id": "CARPENTRY SAW",
        "alt_names": [
          "carpentry_saw"
        ]
      },
      {
        "text": "\ud83d\udd27",
        "id": "WRENCH",
        "alt_names": [
          "wrench"
        ]
      },
      {
        "text": "\ud83e\ude9b",
        "id": "SCREWDRIVER",
        "alt_names": [
          "screwdriver"
        ]
      },
      {
        "text": "\ud83d\udd29",
        "id": "NUT AND BOLT",
        "alt_names": [
          "nut_and_bolt"
        ]
      },
      {
        "text": "\ud83e\uddaf",
        "id": "PROBING CANE",
        "alt_names": [
          "probing_cane"
        ]
      },
      {
        "text": "\ud83d\udd17",
        "id": "LINK SYMBOL",
        "alt_names": [
          "link"
        ]
      },
      {
        "text": "\ud83e\ude9d",
        "id": "HOOK",
        "alt_names": [
          "hook"
        ]
      },
      {
        "text": "\ud83e\uddf0",
        "id": "TOOLBOX",
        "alt_names": [
          "toolbox"
        ]
      },
      {
        "text": "\ud83e\uddf2",
        "id": "MAGNET",
        "alt_names": [
          "magnet"
        ]
      },
      {
        "text": "\ud83e\ude9c",
        "id": "LADDER",
        "alt_names": [
          "ladder"
        ]
      },
      {
        "text": "\ud83e\uddea",
        "id": "TEST TUBE",
        "alt_names": [
          "test_tube"
        ]
      },
      {
        "text": "\ud83e\uddeb",
        "id": "PETRI DISH",
        "alt_names": [
          "petri_dish"
        ]
      },
      {
        "text": "\ud83e\uddec",
        "id": "DNA DOUBLE HELIX",
        "alt_names": [
          "dna"
        ]
      },
      {
        "text": "\ud83d\udd2c",
        "id": "MICROSCOPE",
        "alt_names": [
          "microscope"
        ]
      },
      {
        "text": "\ud83d\udd2d",
        "id": "TELESCOPE",
        "alt_names": [
          "telescope"
        ]
      },
      {
        "text": "\ud83d\udce1",
        "id": "SATELLITE ANTENNA",
        "alt_names": [
          "satellite_antenna"
        ]
      },
      {
        "text": "\ud83d\udc89",
        "id": "SYRINGE",
        "alt_names": [
          "syringe"
        ]
      },
      {
        "text": "\ud83e\ude78",
        "id": "DROP OF BLOOD",
        "alt_names": [
          "drop_of_blood"
        ]
      },
      {
        "text": "\ud83d\udc8a",
        "id": "PILL",
        "alt_names": [
          "pill"
        ]
      },
      {
        "text": "\ud83e\ude79",
        "id": "ADHESIVE BANDAGE",
        "alt_names": [
          "adhesive_bandage"
        ]
      },
      {
        "text": "\ud83e\ude7a",
        "id": "STETHOSCOPE",
        "alt_names": [
          "stethoscope"
        ]
      },
      {
        "text": "\ud83d\udeaa",
        "id": "DOOR",
        "alt_names": [
          "door"
        ]
      },
      {
        "text": "\ud83d\uded7",
        "id": "ELEVATOR",
        "alt_names": [
          "elevator"
        ]
      },
      {
        "text": "\ud83e\ude9e",
        "id": "MIRROR",
        "alt_names": [
          "mirror"
        ]
      },
      {
        "text": "\ud83e\ude9f",
        "id": "WINDOW",
        "alt_names": [
          "window"
        ]
      },
      {
        "text": "\ud83e\ude91",
        "id": "CHAIR",
        "alt_names": [
          "chair"
        ]
      },
      {
        "text": "\ud83d\udebd",
        "id": "TOILET",
        "alt_names": [
          "toilet"
        ]
      },
      {
        "text": "\ud83e\udea0",
        "id": "PLUNGER",
        "alt_names": [
          "plunger"
        ]
      },
      {
        "text": "\ud83d\udebf",
        "id": "SHOWER",
        "alt_names": [
          "shower"
        ]
      },
      {
        "text": "\ud83d\udec1",
        "id": "BATHTUB",
        "alt_names": [
          "bathtub"
        ]
      },
      {
        "text": "\ud83e\udea4",
        "id": "MOUSE TRAP",
        "alt_names": [
          "mouse_trap"
        ]
      },
      {
        "text": "\ud83e\ude92",
        "id": "RAZOR",
        "alt_names": [
          "razor"
        ]
      },
      {
        "text": "\ud83e\uddf4",
        "id": "LOTION BOTTLE",
        "alt_names": [
          "lotion_bottle"
        ]
      },
      {
        "text": "\ud83e\uddf7",
        "id": "SAFETY PIN",
        "alt_names": [
          "safety_pin"
        ]
      },
      {
        "text": "\ud83e\uddf9",
        "id": "BROOM",
        "alt_names": [
          "broom"
        ]
      },
      {
        "text": "\ud83e\uddfa",
        "id": "BASKET",
        "alt_names": [
          "basket"
        ]
      },
      {
        "text": "\ud83e\uddfb",
        "id": "ROLL OF PAPER",
        "alt_names": [
          "roll_of_paper"
        ]
      },
      {
        "text": "\ud83e\udea3",
        "id": "BUCKET",
        "alt_names": [
          "bucket"
        ]
      },
      {
        "text": "\ud83e\uddfc",
        "id": "BAR OF SOAP",
        "alt_names": [
          "soap"
        ]
      },
      {
        "text": "\ud83e\udea5",
        "id": "TOOTHBRUSH",
        "alt_names": [
          "toothbrush"
        ]
      },
      {
        "text": "\ud83e\uddfd",
        "id": "SPONGE",
        "alt_names": [
          "sponge"
        ]
      },
      {
        "text": "\ud83e\uddef",
        "id": "FIRE EXTINGUISHER",
        "alt_names": [
          "fire_extinguisher"
        ]
      },
      {
        "text": "\ud83d\uded2",
        "id": "SHOPPING TROLLEY",
        "alt_names": [
          "shopping_trolley",
          "shopping_cart"
        ]
      },
      {
        "text": "\ud83d\udeac",
        "id": "SMOKING SYMBOL",
        "alt_names": [
          "smoking"
        ]
      },
      {
        "text": "\ud83e\udea6",
        "id": "HEADSTONE",
        "alt_names": [
          "headstone"
        ]
      },
      {
        "text": "\ud83d\uddff",
        "id": "MOYAI",
        "alt_names": [
          "moyai"
        ]
      },
      {
        "text": "\ud83e\udea7",
        "id": "PLACARD",
        "alt_names": [
          "placard"
        ]
      }
    ]
  },
  {
    "text": "Symbols",
    "children": [
      {
        "text": "\ud83c\udfe7",
        "id": "AUTOMATED TELLER MACHINE",
        "alt_names": [
          "atm"
        ]
      },
      {
        "text": "\ud83d\udeae",
        "id": "PUT LITTER IN ITS PLACE SYMBOL",
        "alt_names": [
          "put_litter_in_its_place"
        ]
      },
      {
        "text": "\ud83d\udeb0",
        "id": "POTABLE WATER SYMBOL",
        "alt_names": [
          "potable_water"
        ]
      },
      {
        "text": "\u267f",
        "id": "WHEELCHAIR SYMBOL",
        "alt_names": [
          "wheelchair"
        ]
      },
      {
        "text": "\ud83d\udeb9",
        "id": "MENS SYMBOL",
        "alt_names": [
          "mens"
        ]
      },
      {
        "text": "\ud83d\udeba",
        "id": "WOMENS SYMBOL",
        "alt_names": [
          "womens"
        ]
      },
      {
        "text": "\ud83d\udebb",
        "id": "RESTROOM",
        "alt_names": [
          "restroom"
        ]
      },
      {
        "text": "\ud83d\udebc",
        "id": "BABY SYMBOL",
        "alt_names": [
          "baby_symbol"
        ]
      },
      {
        "text": "\ud83d\udebe",
        "id": "WATER CLOSET",
        "alt_names": [
          "wc"
        ]
      },
      {
        "text": "\ud83d\udec2",
        "id": "PASSPORT CONTROL",
        "alt_names": [
          "passport_control"
        ]
      },
      {
        "text": "\ud83d\udec3",
        "id": "CUSTOMS",
        "alt_names": [
          "customs"
        ]
      },
      {
        "text": "\ud83d\udec4",
        "id": "BAGGAGE CLAIM",
        "alt_names": [
          "baggage_claim"
        ]
      },
      {
        "text": "\ud83d\udec5",
        "id": "LEFT LUGGAGE",
        "alt_names": [
          "left_luggage"
        ]
      },
      {
        "text": "\ud83d\udeb8",
        "id": "CHILDREN CROSSING",
        "alt_names": [
          "children_crossing"
        ]
      },
      {
        "text": "\u26d4",
        "id": "NO ENTRY",
        "alt_names": [
          "no_entry"
        ]
      },
      {
        "text": "\ud83d\udeab",
        "id": "NO ENTRY SIGN",
        "alt_names": [
          "no_entry_sign"
        ]
      },
      {
        "text": "\ud83d\udeb3",
        "id": "NO BICYCLES",
        "alt_names": [
          "no_bicycles"
        ]
      },
      {
        "text": "\ud83d\udead",
        "id": "NO SMOKING SYMBOL",
        "alt_names": [
          "no_smoking"
        ]
      },
      {
        "text": "\ud83d\udeaf",
        "id": "DO NOT LITTER SYMBOL",
        "alt_names": [
          "do_not_litter"
        ]
      },
      {
        "text": "\ud83d\udeb1",
        "id": "NON-POTABLE WATER SYMBOL",
        "alt_names": [
          "non-potable_water"
        ]
      },
      {
        "text": "\ud83d\udeb7",
        "id": "NO PEDESTRIANS",
        "alt_names": [
          "no_pedestrians"
        ]
      },
      {
        "text": "\ud83d\udcf5",
        "id": "NO MOBILE PHONES",
        "alt_names": [
          "no_mobile_phones"
        ]
      },
      {
        "text": "\ud83d\udd1e",
        "id": "NO ONE UNDER EIGHTEEN SYMBOL",
        "alt_names": [
          "underage"
        ]
      },
      {
        "text": "\ud83d\udd03",
        "id": "CLOCKWISE DOWNWARDS AND UPWARDS OPEN CIRCLE ARROWS",
        "alt_names": [
          "arrows_clockwise"
        ]
      },
      {
        "text": "\ud83d\udd04",
        "id": "ANTICLOCKWISE DOWNWARDS AND UPWARDS OPEN CIRCLE ARROWS",
        "alt_names": [
          "arrows_counterclockwise"
        ]
      },
      {
        "text": "\ud83d\udd19",
        "id": "BACK WITH LEFTWARDS ARROW ABOVE",
        "alt_names": [
          "back"
        ]
      },
      {
        "text": "\ud83d\udd1a",
        "id": "END WITH LEFTWARDS ARROW ABOVE",
        "alt_names": [
          "end"
        ]
      },
      {
        "text": "\ud83d\udd1b",
        "id": "ON WITH EXCLAMATION MARK WITH LEFT RIGHT ARROW ABOVE",
        "alt_names": [
          "on"
        ]
      },
      {
        "text": "\ud83d\udd1c",
        "id": "SOON WITH RIGHTWARDS ARROW ABOVE",
        "alt_names": [
          "soon"
        ]
      },
      {
        "text": "\ud83d\udd1d",
        "id": "TOP WITH UPWARDS ARROW ABOVE",
        "alt_names": [
          "top"
        ]
      },
      {
        "text": "\ud83d\uded0",
        "id": "PLACE OF WORSHIP",
        "alt_names": [
          "place_of_worship"
        ]
      },
      {
        "text": "\ud83d\udd4e",
        "id": "MENORAH WITH NINE BRANCHES",
        "alt_names": [
          "menorah_with_nine_branches",
          "menorah"
        ]
      },
      {
        "text": "\ud83d\udd2f",
        "id": "SIX POINTED STAR WITH MIDDLE DOT",
        "alt_names": [
          "six_pointed_star"
        ]
      },
      {
        "text": "\u2648",
        "id": "ARIES",
        "alt_names": [
          "aries"
        ]
      },
      {
        "text": "\u2649",
        "id": "TAURUS",
        "alt_names": [
          "taurus"
        ]
      },
      {
        "text": "\u264a",
        "id": "GEMINI",
        "alt_names": [
          "gemini"
        ]
      },
      {
        "text": "\u264b",
        "id": "CANCER",
        "alt_names": [
          "cancer"
        ]
      },
      {
        "text": "\u264c",
        "id": "LEO",
        "alt_names": [
          "leo"
        ]
      },
      {
        "text": "\u264d",
        "id": "VIRGO",
        "alt_names": [
          "virgo"
        ]
      },
      {
        "text": "\u264e",
        "id": "LIBRA",
        "alt_names": [
          "libra"
        ]
      },
      {
        "text": "\u264f",
        "id": "SCORPIUS",
        "alt_names": [
          "scorpius"
        ]
      },
      {
        "text": "\u2650",
        "id": "SAGITTARIUS",
        "alt_names": [
          "sagittarius"
        ]
      },
      {
        "text": "\u2651",
        "id": "CAPRICORN",
        "alt_names": [
          "capricorn"
        ]
      },
      {
        "text": "\u2652",
        "id": "AQUARIUS",
        "alt_names": [
          "aquarius"
        ]
      },
      {
        "text": "\u2653",
        "id": "PISCES",
        "alt_names": [
          "pisces"
        ]
      },
      {
        "text": "\u26ce",
        "id": "OPHIUCHUS",
        "alt_names": [
          "ophiuchus"
        ]
      },
      {
        "text": "\ud83d\udd00",
        "id": "TWISTED RIGHTWARDS ARROWS",
        "alt_names": [
          "twisted_rightwards_arrows"
        ]
      },
      {
        "text": "\ud83d\udd01",
        "id": "CLOCKWISE RIGHTWARDS AND LEFTWARDS OPEN CIRCLE ARROWS",
        "alt_names": [
          "repeat"
        ]
      },
      {
        "text": "\ud83d\udd02",
        "id": "CLOCKWISE RIGHTWARDS AND LEFTWARDS OPEN CIRCLE ARROWS WITH CIRCLED ONE OVERLAY",
        "alt_names": [
          "repeat_one"
        ]
      },
      {
        "text": "\u23e9",
        "id": "BLACK RIGHT-POINTING DOUBLE TRIANGLE",
        "alt_names": [
          "fast_forward"
        ]
      },
      {
        "text": "\u23ea",
        "id": "BLACK LEFT-POINTING DOUBLE TRIANGLE",
        "alt_names": [
          "rewind"
        ]
      },
      {
        "text": "\ud83d\udd3c",
        "id": "UP-POINTING SMALL RED TRIANGLE",
        "alt_names": [
          "arrow_up_small"
        ]
      },
      {
        "text": "\u23eb",
        "id": "BLACK UP-POINTING DOUBLE TRIANGLE",
        "alt_names": [
          "arrow_double_up"
        ]
      },
      {
        "text": "\ud83d\udd3d",
        "id": "DOWN-POINTING SMALL RED TRIANGLE",
        "alt_names": [
          "arrow_down_small"
        ]
      },
      {
        "text": "\u23ec",
        "id": "BLACK DOWN-POINTING DOUBLE TRIANGLE",
        "alt_names": [
          "arrow_double_down"
        ]
      },
      {
        "text": "\ud83c\udfa6",
        "id": "CINEMA",
        "alt_names": [
          "cinema"
        ]
      },
      {
        "text": "\ud83d\udd05",
        "id": "LOW BRIGHTNESS SYMBOL",
        "alt_names": [
          "low_brightness"
        ]
      },
      {
        "text": "\ud83d\udd06",
        "id": "HIGH BRIGHTNESS SYMBOL",
        "alt_names": [
          "high_brightness"
        ]
      },
      {
        "text": "\ud83d\udcf6",
        "id": "ANTENNA WITH BARS",
        "alt_names": [
          "signal_strength"
        ]
      },
      {
        "text": "\ud83d\udcf3",
        "id": "VIBRATION MODE",
        "alt_names": [
          "vibration_mode"
        ]
      },
      {
        "text": "\ud83d\udcf4",
        "id": "MOBILE PHONE OFF",
        "alt_names": [
          "mobile_phone_off"
        ]
      },
      {
        "text": "\u2795",
        "id": "HEAVY PLUS SIGN",
        "alt_names": [
          "heavy_plus_sign"
        ]
      },
      {
        "text": "\u2796",
        "id": "HEAVY MINUS SIGN",
        "alt_names": [
          "heavy_minus_sign"
        ]
      },
      {
        "text": "\u2797",
        "id": "HEAVY DIVISION SIGN",
        "alt_names": [
          "heavy_division_sign"
        ]
      },
      {
        "text": "\u2753",
        "id": "BLACK QUESTION MARK ORNAMENT",
        "alt_names": [
          "question"
        ]
      },
      {
        "text": "\u2754",
        "id": "WHITE QUESTION MARK ORNAMENT",
        "alt_names": [
          "grey_question"
        ]
      },
      {
        "text": "\u2755",
        "id": "WHITE EXCLAMATION MARK ORNAMENT",
        "alt_names": [
          "grey_exclamation"
        ]
      },
      {
        "text": "\u2757",
        "id": "HEAVY EXCLAMATION MARK SYMBOL",
        "alt_names": [
          "exclamation",
          "heavy_exclamation_mark"
        ]
      },
      {
        "text": "\ud83d\udcb1",
        "id": "CURRENCY EXCHANGE",
        "alt_names": [
          "currency_exchange"
        ]
      },
      {
        "text": "\ud83d\udcb2",
        "id": "HEAVY DOLLAR SIGN",
        "alt_names": [
          "heavy_dollar_sign"
        ]
      },
      {
        "text": "\ud83d\udd31",
        "id": "TRIDENT EMBLEM",
        "alt_names": [
          "trident"
        ]
      },
      {
        "text": "\ud83d\udcdb",
        "id": "NAME BADGE",
        "alt_names": [
          "name_badge"
        ]
      },
      {
        "text": "\ud83d\udd30",
        "id": "JAPANESE SYMBOL FOR BEGINNER",
        "alt_names": [
          "beginner"
        ]
      },
      {
        "text": "\u2b55",
        "id": "HEAVY LARGE CIRCLE",
        "alt_names": [
          "o"
        ]
      },
      {
        "text": "\u2705",
        "id": "WHITE HEAVY CHECK MARK",
        "alt_names": [
          "white_check_mark"
        ]
      },
      {
        "text": "\u274c",
        "id": "CROSS MARK",
        "alt_names": [
          "x"
        ]
      },
      {
        "text": "\u274e",
        "id": "NEGATIVE SQUARED CROSS MARK",
        "alt_names": [
          "negative_squared_cross_mark"
        ]
      },
      {
        "text": "\u27b0",
        "id": "CURLY LOOP",
        "alt_names": [
          "curly_loop"
        ]
      },
      {
        "text": "\u27bf",
        "id": "DOUBLE CURLY LOOP",
        "alt_names": [
          "loop"
        ]
      },
      {
        "text": "\ud83d\udd1f",
        "id": "KEYCAP TEN",
        "alt_names": [
          "keycap_ten"
        ]
      },
      {
        "text": "\ud83d\udd20",
        "id": "INPUT SYMBOL FOR LATIN CAPITAL LETTERS",
        "alt_names": [
          "capital_abcd"
        ]
      },
      {
        "text": "\ud83d\udd21",
        "id": "INPUT SYMBOL FOR LATIN SMALL LETTERS",
        "alt_names": [
          "abcd"
        ]
      },
      {
        "text": "\ud83d\udd22",
        "id": "INPUT SYMBOL FOR NUMBERS",
        "alt_names": [
          "1234"
        ]
      },
      {
        "text": "\ud83d\udd23",
        "id": "INPUT SYMBOL FOR SYMBOLS",
        "alt_names": [
          "symbols"
        ]
      },
      {
        "text": "\ud83d\udd24",
        "id": "INPUT SYMBOL FOR LATIN LETTERS",
        "alt_names": [
          "abc"
        ]
      },
      {
        "text": "\ud83c\udd8e",
        "id": "NEGATIVE SQUARED AB",
        "alt_names": [
          "ab"
        ]
      },
      {
        "text": "\ud83c\udd91",
        "id": "SQUARED CL",
        "alt_names": [
          "cl"
        ]
      },
      {
        "text": "\ud83c\udd92",
        "id": "SQUARED COOL",
        "alt_names": [
          "cool"
        ]
      },
      {
        "text": "\ud83c\udd93",
        "id": "SQUARED FREE",
        "alt_names": [
          "free"
        ]
      },
      {
        "text": "\ud83c\udd94",
        "id": "SQUARED ID",
        "alt_names": [
          "id"
        ]
      },
      {
        "text": "\ud83c\udd95",
        "id": "SQUARED NEW",
        "alt_names": [
          "new"
        ]
      },
      {
        "text": "\ud83c\udd96",
        "id": "SQUARED NG",
        "alt_names": [
          "ng"
        ]
      },
      {
        "text": "\ud83c\udd97",
        "id": "SQUARED OK",
        "alt_names": [
          "ok"
        ]
      },
      {
        "text": "\ud83c\udd98",
        "id": "SQUARED SOS",
        "alt_names": [
          "sos"
        ]
      },
      {
        "text": "\ud83c\udd99",
        "id": "SQUARED UP WITH EXCLAMATION MARK",
        "alt_names": [
          "up"
        ]
      },
      {
        "text": "\ud83c\udd9a",
        "id": "SQUARED VS",
        "alt_names": [
          "vs"
        ]
      },
      {
        "text": "\ud83c\ude01",
        "id": "SQUARED KATAKANA KOKO",
        "alt_names": [
          "koko"
        ]
      },
      {
        "text": "\ud83c\ude36",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-6709",
        "alt_names": [
          "u6709"
        ]
      },
      {
        "text": "\ud83c\ude2f",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-6307",
        "alt_names": [
          "u6307"
        ]
      },
      {
        "text": "\ud83c\ude50",
        "id": "CIRCLED IDEOGRAPH ADVANTAGE",
        "alt_names": [
          "ideograph_advantage"
        ]
      },
      {
        "text": "\ud83c\ude39",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-5272",
        "alt_names": [
          "u5272"
        ]
      },
      {
        "text": "\ud83c\ude1a",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-7121",
        "alt_names": [
          "u7121"
        ]
      },
      {
        "text": "\ud83c\ude32",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-7981",
        "alt_names": [
          "u7981"
        ]
      },
      {
        "text": "\ud83c\ude51",
        "id": "CIRCLED IDEOGRAPH ACCEPT",
        "alt_names": [
          "accept"
        ]
      },
      {
        "text": "\ud83c\ude38",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-7533",
        "alt_names": [
          "u7533"
        ]
      },
      {
        "text": "\ud83c\ude34",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-5408",
        "alt_names": [
          "u5408"
        ]
      },
      {
        "text": "\ud83c\ude33",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-7A7A",
        "alt_names": [
          "u7a7a"
        ]
      },
      {
        "text": "\ud83c\ude3a",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-55B6",
        "alt_names": [
          "u55b6"
        ]
      },
      {
        "text": "\ud83c\ude35",
        "id": "SQUARED CJK UNIFIED IDEOGRAPH-6E80",
        "alt_names": [
          "u6e80"
        ]
      },
      {
        "text": "\ud83d\udd34",
        "id": "LARGE RED CIRCLE",
        "alt_names": [
          "red_circle"
        ]
      },
      {
        "text": "\ud83d\udfe0",
        "id": "LARGE ORANGE CIRCLE",
        "alt_names": [
          "large_orange_circle"
        ]
      },
      {
        "text": "\ud83d\udfe1",
        "id": "LARGE YELLOW CIRCLE",
        "alt_names": [
          "large_yellow_circle"
        ]
      },
      {
        "text": "\ud83d\udfe2",
        "id": "LARGE GREEN CIRCLE",
        "alt_names": [
          "large_green_circle"
        ]
      },
      {
        "text": "\ud83d\udd35",
        "id": "LARGE BLUE CIRCLE",
        "alt_names": [
          "large_blue_circle"
        ]
      },
      {
        "text": "\ud83d\udfe3",
        "id": "LARGE PURPLE CIRCLE",
        "alt_names": [
          "large_purple_circle"
        ]
      },
      {
        "text": "\ud83d\udfe4",
        "id": "LARGE BROWN CIRCLE",
        "alt_names": [
          "large_brown_circle"
        ]
      },
      {
        "text": "\u26ab",
        "id": "MEDIUM BLACK CIRCLE",
        "alt_names": [
          "black_circle"
        ]
      },
      {
        "text": "\u26aa",
        "id": "MEDIUM WHITE CIRCLE",
        "alt_names": [
          "white_circle"
        ]
      },
      {
        "text": "\ud83d\udfe5",
        "id": "LARGE RED SQUARE",
        "alt_names": [
          "large_red_square"
        ]
      },
      {
        "text": "\ud83d\udfe7",
        "id": "LARGE ORANGE SQUARE",
        "alt_names": [
          "large_orange_square"
        ]
      },
      {
        "text": "\ud83d\udfe8",
        "id": "LARGE YELLOW SQUARE",
        "alt_names": [
          "large_yellow_square"
        ]
      },
      {
        "text": "\ud83d\udfe9",
        "id": "LARGE GREEN SQUARE",
        "alt_names": [
          "large_green_square"
        ]
      },
      {
        "text": "\ud83d\udfe6",
        "id": "LARGE BLUE SQUARE",
        "alt_names": [
          "large_blue_square"
        ]
      },
      {
        "text": "\ud83d\udfea",
        "id": "LARGE PURPLE SQUARE",
        "alt_names": [
          "large_purple_square"
        ]
      },
      {
        "text": "\ud83d\udfeb",
        "id": "LARGE BROWN SQUARE",
        "alt_names": [
          "large_brown_square"
        ]
      },
      {
        "text": "\u2b1b",
        "id": "BLACK LARGE SQUARE",
        "alt_names": [
          "black_large_square"
        ]
      },
      {
        "text": "\u2b1c",
        "id": "WHITE LARGE SQUARE",
        "alt_names": [
          "white_large_square"
        ]
      },
      {
        "text": "\u25fe",
        "id": "BLACK MEDIUM SMALL SQUARE",
        "alt_names": [
          "black_medium_small_square"
        ]
      },
      {
        "text": "\u25fd",
        "id": "WHITE MEDIUM SMALL SQUARE",
        "alt_names": [
          "white_medium_small_square"
        ]
      },
      {
        "text": "\ud83d\udd36",
        "id": "LARGE ORANGE DIAMOND",
        "alt_names": [
          "large_orange_diamond"
        ]
      },
      {
        "text": "\ud83d\udd37",
        "id": "LARGE BLUE DIAMOND",
        "alt_names": [
          "large_blue_diamond"
        ]
      },
      {
        "text": "\ud83d\udd38",
        "id": "SMALL ORANGE DIAMOND",
        "alt_names": [
          "small_orange_diamond"
        ]
      },
      {
        "text": "\ud83d\udd39",
        "id": "SMALL BLUE DIAMOND",
        "alt_names": [
          "small_blue_diamond"
        ]
      },
      {
        "text": "\ud83d\udd3a",
        "id": "UP-POINTING RED TRIANGLE",
        "alt_names": [
          "small_red_triangle"
        ]
      },
      {
        "text": "\ud83d\udd3b",
        "id": "DOWN-POINTING RED TRIANGLE",
        "alt_names": [
          "small_red_triangle_down"
        ]
      },
      {
        "text": "\ud83d\udca0",
        "id": "DIAMOND SHAPE WITH A DOT INSIDE",
        "alt_names": [
          "diamond_shape_with_a_dot_inside"
        ]
      },
      {
        "text": "\ud83d\udd18",
        "id": "RADIO BUTTON",
        "alt_names": [
          "radio_button"
        ]
      },
      {
        "text": "\ud83d\udd33",
        "id": "WHITE SQUARE BUTTON",
        "alt_names": [
          "white_square_button"
        ]
      },
      {
        "text": "\ud83d\udd32",
        "id": "BLACK SQUARE BUTTON",
        "alt_names": [
          "black_square_button"
        ]
      }
    ]
  },
  {
    "text": "Flags",
    "children": [
      {
        "text": "\ud83c\udfc1",
        "id": "CHEQUERED FLAG",
        "alt_names": [
          "checkered_flag"
        ]
      },
      {
        "text": "\ud83d\udea9",
        "id": "TRIANGULAR FLAG ON POST",
        "alt_names": [
          "triangular_flag_on_post"
        ]
      },
      {
        "text": "\ud83c\udf8c",
        "id": "CROSSED FLAGS",
        "alt_names": [
          "crossed_flags"
        ]
      },
      {
        "text": "\ud83c\udff4",
        "id": "WAVING BLACK FLAG",
        "alt_names": [
          "waving_black_flag",
          "black_flag"
        ]
      }
    ]
  }
];
export default emojiData;
