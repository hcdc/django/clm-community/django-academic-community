# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Plugins for the chats app."""
from __future__ import annotations

from typing import TYPE_CHECKING, ClassVar, Dict, Optional, Type

from cms.plugin_pool import plugin_pool
from django.db.models import Q
from django.template.loader import select_template
from django.utils.translation import gettext as _
from guardian.shortcuts import get_anonymous_user, get_objects_for_user

from academic_community.channels import app_settings, models
from academic_community.cms_plugins import (
    ListPublisher,
    PaginationPublisherMixin,
)
from academic_community.utils import concat_classes

if TYPE_CHECKING:
    from django.db.models import QuerySet


class ChannelListPublisherBase(PaginationPublisherMixin, ListPublisher):
    """A plugin to display material to the user."""

    module = _("Chats & Discussions")
    cache = False

    channel_relation_model: ClassVar[Type[models.ChannelBase]] = models.Channel

    default_classes = "list-group"

    channel_type_classes = {"issue": ""}

    filter_horizontal = ["keywords"]

    list_model = models.Channel

    context_object_name = "channels"

    list_ordering = "-last_comment_modification_date"

    def get_query(
        self, instance: models.AbstractChannelListPluginModel
    ) -> Optional[Q]:
        """Get the query for the members."""
        query = None
        if not instance.all_keywords_required:
            keywords = instance.keywords.values_list("pk", flat=True)
            if keywords:
                query = Q(keywords__pk__in=keywords)

        if instance.channel_type:
            channel_type_query = Q(
                **{instance.channel_type + "__isnull": False}
            )
            if query:
                query &= channel_type_query
            else:
                query = channel_type_query

        return query

    def filter_list_queryset(
        self,
        context,
        instance: models.AbstractChannelListPluginModel,
        queryset: QuerySet[models.Channel],
    ) -> QuerySet[models.Channel]:
        """Get a queryset of the filtered members"""
        query = self.get_query(instance)
        if query is not None:
            queryset = queryset.filter(query)
        if instance.all_keywords_required:
            keywords = instance.keywords.values_list("pk", flat=True)
            if keywords:
                for pk in keywords:
                    queryset = queryset.filter(keywords__pk=pk)
        if instance.check_permissions:
            user = context["request"].user
            if user.is_anonymous:
                user = get_anonymous_user()
            queryset = get_objects_for_user(user, "view_channel", queryset)
        return queryset

    def render(
        self,
        context,
        instance: models.AbstractChannelListPluginModel,  # type: ignore[override]
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        channel_type = instance.channel_type
        context["channel_type"] = channel_type
        context["base_id"] = f"member-plugin-{instance.pk}-"
        if not channel_type or channel_type not in self.channel_type_classes:
            instance.attributes["class"] = concat_classes(
                [self.default_classes] + [instance.attributes.get("class")]
            )
        elif channel_type in self.channel_type_classes:
            instance.attributes["class"] = concat_classes(
                [self.channel_type_classes[channel_type]]
                + [instance.attributes.get("class")]
            )
        context.update(self.get_urls(context, instance))
        return context

    def get_url_kws(
        self, context, instance: models.AbstractChannelListPluginModel
    ) -> Dict[str, str]:
        channel_type_model = instance.channel_type_model
        if channel_type_model:
            kws = {"channel_type": channel_type_model.get_channel_type_slug()}
        else:
            kws = {}
        return kws

    def get_urls(
        self, context, instance: models.AbstractChannelListPluginModel
    ) -> Dict[str, str]:
        """Get the urls for listing all channels and displaying lists"""
        kws = self.get_url_kws(context, instance)
        ret = {
            "channel_list_url": self.channel_relation_model.get_list_url_from_kwargs(
                **kws
            ),
        }
        if app_settings.is_channel_type_enabled("group-conversations"):
            kws.setdefault("channel_type", "group-conversations")
        else:
            kws.setdefault("channel_type", app_settings.CHANNEL_TYPES[0])
        ret[
            "channel_create_url"
        ] = self.channel_relation_model.get_create_url_from_kwargs(**kws)
        return ret

    def get_render_template(
        self,
        context,
        instance: models.AbstractChannelListPluginModel,
        placeholder,
    ):
        """Get the rendering template"""
        channel_template = "chats/components/%s_listplugin.html"
        templates = []
        if instance.channel_type:
            templates.append(channel_template % instance.channel_type)
        templates.append(channel_template % "channel")
        template = select_template(templates)
        return template.template.name


@plugin_pool.register_plugin
class ChannelListPublisher(ChannelListPublisherBase):
    """A plugin to display selected channels"""

    name = _("Selected Channels")
    model = models.ChannelListPluginModel

    filter_horizontal = ChannelListPublisherBase.filter_horizontal + [
        "channels"
    ]

    def get_full_queryset(
        self, context, instance: models.ChannelListPluginModel
    ):
        if instance.show_all:
            return super().get_full_queryset(context, instance)
        else:
            return instance.channels.all()

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        channels = form.base_fields["channels"].queryset
        channels = get_objects_for_user(request.user, "view_channel", channels)
        form.base_fields["channels"].queryset = channels

        return form
