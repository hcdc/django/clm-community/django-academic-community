"""Update thread tree.

Asynchronous job to update the thread tree in all channels. This needs to be
handled in a separate job to make sure the tree stays valid.
"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "Garbage collect channels without channel type"

    def execute(self):
        import reversion

        from academic_community.channels import models

        with reversion.create_revision():
            reversion.set_comment("Garbage collect channels [reviewed] [cron]")

            kws = {
                name + "__isnull": True
                for name in models.BaseChannelType.registry.registered_model_names
            }

            channels = list(models.Channel.objects.filter(**kws))
            n = len(channels)

            print("Removing %i channels" % n)

            for i, channel in enumerate(channels):
                print("Removing channel %i of %i: %s" % (i, n, channel))
                channel.delete()
            print("Done.")
