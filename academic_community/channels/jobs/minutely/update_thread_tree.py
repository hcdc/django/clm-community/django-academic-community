"""Update thread tree.

Asynchronous job to update the thread tree in all channels. This needs to be
handled in a separate job to make sure the tree stays valid.
"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "Update the thread tree for each channel"

    def execute(self):
        import reversion

        from academic_community.channels import models

        with reversion.create_revision():
            reversion.set_comment("Update thread tree [reviewed] [cron]")

            for channel in models.Channel.objects.all():
                threads = channel.thread_set.filter(
                    previous_thread__isnull=True
                ).order_by("date_created")[1:]
                if threads:
                    latest_thread = (
                        channel.thread_set.filter(
                            previous_thread__isnull=False
                        )
                        .order_by("-date_created")
                        .first()
                    )
                    if not latest_thread:
                        latest_thread = (
                            channel.thread_set.all()
                            .order_by("date_created")
                            .first()
                        )
                    for thread in threads:
                        thread.previous_thread = latest_thread
                        thread.save()
                        latest_thread = thread
