"""Permissions
-----------

Custom permissions for the restAPI.
"""

# Copyright (C) 2022 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from rest_framework.permissions import BasePermission


class SubscribedToChannelPermission(BasePermission):
    """Check if the user has subscribed to the channel."""

    def has_permission(self, request, view) -> bool:
        from academic_community.channels.models import ChannelSubscription

        return ChannelSubscription.objects.filter(
            channel__channel_id=view.kwargs["channel_id"],
            user=request.user,
        ).exists()

    def has_object_permission(self, request, view, obj) -> bool:
        return self.has_permission(request, view)


class CanPostCommentPermission(BasePermission):
    """Check if the user has subscribed to the channel."""

    def has_permission(self, request, view) -> bool:
        from academic_community.channels.models import Channel

        try:
            channel = Channel.objects.get(channel_id=view.kwargs["channel_id"])
        except Channel.DoesNotExist:
            return False
        else:
            return channel.has_post_comment_permission(request.user)

    def has_object_permission(self, request, view, obj) -> bool:
        return self.has_permission(request, view)


class CanViewChannelPermission(BasePermission):
    """Check if the user has subscribed to the channel."""

    def has_permission(self, request, view) -> bool:
        from academic_community.channels.models import Channel

        try:
            channel = Channel.objects.get(channel_id=view.kwargs["channel_id"])
        except Channel.DoesNotExist:
            return False
        else:
            return channel.has_view_permission(request.user)

    def has_object_permission(self, request, view, obj) -> bool:
        return self.has_permission(request, view)
