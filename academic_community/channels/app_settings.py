# SPDX-FileCopyrightText: 2020-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App settings
------------

This module defines the settings options for the
:mod:`academic_community.channels` app.
"""


from __future__ import annotations

from typing import List, Literal, Union

from django.conf import settings  # noqa: F401

CHANNEL_TYPES: Union[
    Literal["all"],
    List[
        Literal[
            "issues", "forums", "group-conversations", "private-conversations"
        ]
    ],
] = getattr(settings, "CHANNEL_TYPES", "all")


def is_channel_type_enabled(channel_type: str) -> bool:
    """Check if the channel type is enabled on the platform."""
    return CHANNEL_TYPES == "all" or channel_type in CHANNEL_TYPES
