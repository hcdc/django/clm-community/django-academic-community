"""Urls of the :mod:`members` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import path

from academic_community.mailinglists import views

app_name = "mailinglists"

urlpatterns = [
    path(
        "<slug>/",
        views.SympaMailingListDetailView.as_view(),
        name="mailinglist-detail",
    ),
    path(
        "<slug>/status/",
        views.SympaMailingListStatusView.as_view(),
        name="mailinglist-status",
    ),
]
