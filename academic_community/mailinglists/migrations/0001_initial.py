# Generated by Django 3.2.7 on 2021-11-15 23:10

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("activities", "0008_activity_groups"),
    ]

    operations = [
        migrations.CreateModel(
            name="SympaMailingList",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.SlugField(max_length=30, unique=True)),
                (
                    "all_members",
                    models.BooleanField(
                        default=False,
                        help_text="Should all community members be subscribed to this list?",
                    ),
                ),
                (
                    "activities",
                    models.ManyToManyField(
                        blank=True, to="activities.Activity"
                    ),
                ),
            ],
        ),
    ]
