"""Update members in remote mailing lists."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django_extensions.management.jobs import MinutelyJob


class Job(MinutelyJob):
    help = "Update remote mailing lists"

    def execute(self):
        from academic_community.mailinglists import models

        qs = models.SympaMailingList.objects.filter(requires_update=True)
        lists = list(qs)  # load into memory to avoid sending mails twice
        qs.update(requires_update=False)
        for mailinglist in lists:
            try:
                print(f"Updating mailinglist {mailinglist}.")
                mailinglist.update_sympa()
            except Exception:
                print(
                    f"Failed to update mailinglist {mailinglist}. Marking "
                    "for another try."
                )
                mailinglist.requires_update = True
                mailinglist.save()
