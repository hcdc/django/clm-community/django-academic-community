"""Settings for the mailinglists app.

All variables defined here can be overwritten in djangos settings.py file.
"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.conf import settings

SYMPA_URL = getattr(settings, "SYMPA_URL", "https://www.listserv.dfn.de/sympa")
SYMPA_USER = getattr(settings, "SYMPA_USER", "")
SYMPA_PASSWD = getattr(settings, "SYMPA_PASSWD", "")
SYMPA_DOMAIN = getattr(settings, "SYMPA_DOMAIN", "listserv.dfn.de")
SYMPA_DISABLE_SYNC = getattr(settings, "SYMPA_DISABLE_SYNC", False)
