# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.apps import AppConfig


class MailinglistsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "academic_community.mailinglists"
    label = "mailinglists"
