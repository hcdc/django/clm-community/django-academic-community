# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from academic_community.admin import ManagerAdminMixin
from academic_community.mailinglists import models


@admin.register(models.SympaMailingList)
class SympaMailingListAdmin(ManagerAdminMixin, GuardedModelAdmin):
    list_display = ["name", "activity_names", "member_count"]

    filter_horizontal = ["activities"]

    def activity_names(self, obj: models.SympaMailingList) -> str:
        return ", ".join(
            activity.abbreviation for activity in obj.activities.all()
        )

    def member_count(self, obj: models.SympaMailingList) -> str:
        return str(len(obj.members))
