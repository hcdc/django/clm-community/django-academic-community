# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.apps import AppConfig


class Ckeditor5Config(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "academic_community.ckeditor5"
    label = "ckeditor5"
