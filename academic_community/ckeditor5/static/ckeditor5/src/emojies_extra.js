// SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: EUPL-1.2

var emojies = [
  {
    "from": ":grinning:",
    "to": "\ud83d\ude00"
  },
  {
    "from": ":D",
    "to": "\ud83d\ude04"
  },
  {
    "from": ":smiley:",
    "to": "\ud83d\ude03"
  },
  {
    "from": ":)",
    "to": "\ud83d\ude0a"
  },
  {
    "from": "=)",
    "to": "\ud83d\ude03"
  },
  {
    "from": "=-)",
    "to": "\ud83d\ude03"
  },
  {
    "from": ":smile:",
    "to": "\ud83d\ude04"
  },
  {
    "from": "C:",
    "to": "\ud83d\ude04"
  },
  {
    "from": "c:",
    "to": "\ud83d\ude04"
  },
  {
    "from": ":-D",
    "to": "\ud83d\ude04"
  },
  {
    "from": ":grin:",
    "to": "\ud83d\ude01"
  },
  {
    "from": ":laughing:",
    "to": "\ud83d\ude06"
  },
  {
    "from": ":satisfied:",
    "to": "\ud83d\ude06"
  },
  {
    "from": ":>",
    "to": "\ud83d\ude06"
  },
  {
    "from": ":->",
    "to": "\ud83d\ude06"
  },
  {
    "from": ":sweat_smile:",
    "to": "\ud83d\ude05"
  },
  {
    "from": ":rolling_on_the_floor_laughing:",
    "to": "\ud83e\udd23"
  },
  {
    "from": ":rofl:",
    "to": "\ud83e\udd23"
  },
  {
    "from": ":joy:",
    "to": "\ud83d\ude02"
  },
  {
    "from": ":slightly_smiling_face:",
    "to": "\ud83d\ude42"
  },
  {
    "from": "(:",
    "to": "\ud83d\ude42"
  },
  {
    "from": ":-)",
    "to": "\ud83d\ude42"
  },
  {
    "from": ":upside_down_face:",
    "to": "\ud83d\ude43"
  },
  {
    "from": ":wink:",
    "to": "\ud83d\ude09"
  },
  {
    "from": ";)",
    "to": "\ud83d\ude09"
  },
  {
    "from": ";-)",
    "to": "\ud83d\ude09"
  },
  {
    "from": ":blush:",
    "to": "\ud83d\ude0a"
  },
  {
    "from": ":innocent:",
    "to": "\ud83d\ude07"
  },
  {
    "from": ":smiling_face_with_3_hearts:",
    "to": "\ud83e\udd70"
  },
  {
    "from": ":heart_eyes:",
    "to": "\ud83d\ude0d"
  },
  {
    "from": ":star-struck:",
    "to": "\ud83e\udd29"
  },
  {
    "from": ":grinning_face_with_star_eyes:",
    "to": "\ud83e\udd29"
  },
  {
    "from": ":kissing_heart:",
    "to": "\ud83d\ude18"
  },
  {
    "from": ":*",
    "to": "\ud83d\ude18"
  },
  {
    "from": ":-*",
    "to": "\ud83d\ude18"
  },
  {
    "from": ":kissing:",
    "to": "\ud83d\ude17"
  },
  {
    "from": ":kissing_closed_eyes:",
    "to": "\ud83d\ude1a"
  },
  {
    "from": ":kissing_smiling_eyes:",
    "to": "\ud83d\ude19"
  },
  {
    "from": ":smiling_face_with_tear:",
    "to": "\ud83e\udd72"
  },
  {
    "from": ":yum:",
    "to": "\ud83d\ude0b"
  },
  {
    "from": ":stuck_out_tongue:",
    "to": "\ud83d\ude1b"
  },
  {
    "from": ":p",
    "to": "\ud83d\ude1b"
  },
  {
    "from": ":-p",
    "to": "\ud83d\ude1b"
  },
  {
    "from": ":P",
    "to": "\ud83d\ude1b"
  },
  {
    "from": ":-P",
    "to": "\ud83d\ude1b"
  },
  {
    "from": ":b",
    "to": "\ud83d\ude1b"
  },
  {
    "from": ":-b",
    "to": "\ud83d\ude1b"
  },
  {
    "from": ":stuck_out_tongue_winking_eye:",
    "to": "\ud83d\ude1c"
  },
  {
    "from": ";p",
    "to": "\ud83d\ude1c"
  },
  {
    "from": ";-p",
    "to": "\ud83d\ude1c"
  },
  {
    "from": ";b",
    "to": "\ud83d\ude1c"
  },
  {
    "from": ";-b",
    "to": "\ud83d\ude1c"
  },
  {
    "from": ";P",
    "to": "\ud83d\ude1c"
  },
  {
    "from": ";-P",
    "to": "\ud83d\ude1c"
  },
  {
    "from": ":zany_face:",
    "to": "\ud83e\udd2a"
  },
  {
    "from": ":grinning_face_with_one_large_and_one_small_eye:",
    "to": "\ud83e\udd2a"
  },
  {
    "from": ":stuck_out_tongue_closed_eyes:",
    "to": "\ud83d\ude1d"
  },
  {
    "from": ":money_mouth_face:",
    "to": "\ud83e\udd11"
  },
  {
    "from": ":hugging_face:",
    "to": "\ud83e\udd17"
  },
  {
    "from": ":hugs:",
    "to": "\ud83e\udd17"
  },
  {
    "from": ":face_with_hand_over_mouth:",
    "to": "\ud83e\udd2d"
  },
  {
    "from": ":smiling_face_with_smiling_eyes_and_hand_covering_mouth:",
    "to": "\ud83e\udd2d"
  },
  {
    "from": ":shushing_face:",
    "to": "\ud83e\udd2b"
  },
  {
    "from": ":face_with_finger_covering_closed_lips:",
    "to": "\ud83e\udd2b"
  },
  {
    "from": ":thinking_face:",
    "to": "\ud83e\udd14"
  },
  {
    "from": ":thinking:",
    "to": "\ud83e\udd14"
  },
  {
    "from": ":zipper_mouth_face:",
    "to": "\ud83e\udd10"
  },
  {
    "from": ":face_with_raised_eyebrow:",
    "to": "\ud83e\udd28"
  },
  {
    "from": ":face_with_one_eyebrow_raised:",
    "to": "\ud83e\udd28"
  },
  {
    "from": ":neutral_face:",
    "to": "\ud83d\ude10"
  },
  {
    "from": ":|",
    "to": "\ud83d\ude10"
  },
  {
    "from": ":-|",
    "to": "\ud83d\ude10"
  },
  {
    "from": ":expressionless:",
    "to": "\ud83d\ude11"
  },
  {
    "from": ":no_mouth:",
    "to": "\ud83d\ude36"
  },
  {
    "from": ":smirk:",
    "to": "\ud83d\ude0f"
  },
  {
    "from": ":unamused:",
    "to": "\ud83d\ude12"
  },
  {
    "from": ":(",
    "to": "\ud83d\ude1e"
  },
  {
    "from": ":face_with_rolling_eyes:",
    "to": "\ud83d\ude44"
  },
  {
    "from": ":roll_eyes:",
    "to": "\ud83d\ude44"
  },
  {
    "from": ":grimacing:",
    "to": "\ud83d\ude2c"
  },
  {
    "from": ":lying_face:",
    "to": "\ud83e\udd25"
  },
  {
    "from": ":relieved:",
    "to": "\ud83d\ude0c"
  },
  {
    "from": ":pensive:",
    "to": "\ud83d\ude14"
  },
  {
    "from": ":sleepy:",
    "to": "\ud83d\ude2a"
  },
  {
    "from": ":drooling_face:",
    "to": "\ud83e\udd24"
  },
  {
    "from": ":sleeping:",
    "to": "\ud83d\ude34"
  },
  {
    "from": ":mask:",
    "to": "\ud83d\ude37"
  },
  {
    "from": ":face_with_thermometer:",
    "to": "\ud83e\udd12"
  },
  {
    "from": ":face_with_head_bandage:",
    "to": "\ud83e\udd15"
  },
  {
    "from": ":nauseated_face:",
    "to": "\ud83e\udd22"
  },
  {
    "from": ":face_vomiting:",
    "to": "\ud83e\udd2e"
  },
  {
    "from": ":face_with_open_mouth_vomiting:",
    "to": "\ud83e\udd2e"
  },
  {
    "from": ":sneezing_face:",
    "to": "\ud83e\udd27"
  },
  {
    "from": ":hot_face:",
    "to": "\ud83e\udd75"
  },
  {
    "from": ":cold_face:",
    "to": "\ud83e\udd76"
  },
  {
    "from": ":woozy_face:",
    "to": "\ud83e\udd74"
  },
  {
    "from": ":dizzy_face:",
    "to": "\ud83d\ude35"
  },
  {
    "from": ":exploding_head:",
    "to": "\ud83e\udd2f"
  },
  {
    "from": ":shocked_face_with_exploding_head:",
    "to": "\ud83e\udd2f"
  },
  {
    "from": ":face_with_cowboy_hat:",
    "to": "\ud83e\udd20"
  },
  {
    "from": ":cowboy_hat_face:",
    "to": "\ud83e\udd20"
  },
  {
    "from": ":partying_face:",
    "to": "\ud83e\udd73"
  },
  {
    "from": ":disguised_face:",
    "to": "\ud83e\udd78"
  },
  {
    "from": ":sunglasses:",
    "to": "\ud83d\ude0e"
  },
  {
    "from": "8)",
    "to": "\ud83d\ude0e"
  },
  {
    "from": ":nerd_face:",
    "to": "\ud83e\udd13"
  },
  {
    "from": ":face_with_monocle:",
    "to": "\ud83e\uddd0"
  },
  {
    "from": ":confused:",
    "to": "\ud83d\ude15"
  },
  {
    "from": ":\\",
    "to": "\ud83d\ude15"
  },
  {
    "from": ":-\\",
    "to": "\ud83d\ude15"
  },
  {
    "from": ":/",
    "to": "\ud83d\ude15"
  },
  {
    "from": ":-/",
    "to": "\ud83d\ude15"
  },
  {
    "from": ":worried:",
    "to": "\ud83d\ude1f"
  },
  {
    "from": ":slightly_frowning_face:",
    "to": "\ud83d\ude41"
  },
  {
    "from": ":open_mouth:",
    "to": "\ud83d\ude2e"
  },
  {
    "from": ":o",
    "to": "\ud83d\ude2e"
  },
  {
    "from": ":-o",
    "to": "\ud83d\ude2e"
  },
  {
    "from": ":O",
    "to": "\ud83d\ude2e"
  },
  {
    "from": ":-O",
    "to": "\ud83d\ude2e"
  },
  {
    "from": ":hushed:",
    "to": "\ud83d\ude2f"
  },
  {
    "from": ":astonished:",
    "to": "\ud83d\ude32"
  },
  {
    "from": ":flushed:",
    "to": "\ud83d\ude33"
  },
  {
    "from": ":pleading_face:",
    "to": "\ud83e\udd7a"
  },
  {
    "from": ":frowning:",
    "to": "\ud83d\ude26"
  },
  {
    "from": ":anguished:",
    "to": "\ud83d\ude27"
  },
  {
    "from": "D:",
    "to": "\ud83d\ude27"
  },
  {
    "from": ":fearful:",
    "to": "\ud83d\ude28"
  },
  {
    "from": ":cold_sweat:",
    "to": "\ud83d\ude30"
  },
  {
    "from": ":disappointed_relieved:",
    "to": "\ud83d\ude25"
  },
  {
    "from": ":cry:",
    "to": "\ud83d\ude22"
  },
  {
    "from": ":'(",
    "to": "\ud83d\ude2d"
  },
  {
    "from": ":sob:",
    "to": "\ud83d\ude2d"
  },
  {
    "from": ":scream:",
    "to": "\ud83d\ude31"
  },
  {
    "from": ":confounded:",
    "to": "\ud83d\ude16"
  },
  {
    "from": ":persevere:",
    "to": "\ud83d\ude23"
  },
  {
    "from": ":disappointed:",
    "to": "\ud83d\ude1e"
  },
  {
    "from": "):",
    "to": "\ud83d\ude1e"
  },
  {
    "from": ":-(",
    "to": "\ud83d\ude1e"
  },
  {
    "from": ":sweat:",
    "to": "\ud83d\ude13"
  },
  {
    "from": ":weary:",
    "to": "\ud83d\ude29"
  },
  {
    "from": ":tired_face:",
    "to": "\ud83d\ude2b"
  },
  {
    "from": ":yawning_face:",
    "to": "\ud83e\udd71"
  },
  {
    "from": ":triumph:",
    "to": "\ud83d\ude24"
  },
  {
    "from": ":rage:",
    "to": "\ud83d\ude21"
  },
  {
    "from": ":pout:",
    "to": "\ud83d\ude21"
  },
  {
    "from": ":angry:",
    "to": "\ud83d\ude20"
  },
  {
    "from": ">:(",
    "to": "\ud83d\ude20"
  },
  {
    "from": ">:-(",
    "to": "\ud83d\ude20"
  },
  {
    "from": ":face_with_symbols_on_mouth:",
    "to": "\ud83e\udd2c"
  },
  {
    "from": ":serious_face_with_symbols_covering_mouth:",
    "to": "\ud83e\udd2c"
  },
  {
    "from": ":smiling_imp:",
    "to": "\ud83d\ude08"
  },
  {
    "from": ":imp:",
    "to": "\ud83d\udc7f"
  },
  {
    "from": ":skull:",
    "to": "\ud83d\udc80"
  },
  {
    "from": ":hankey:",
    "to": "\ud83d\udca9"
  },
  {
    "from": ":poop:",
    "to": "\ud83d\udca9"
  },
  {
    "from": ":shit:",
    "to": "\ud83d\udca9"
  },
  {
    "from": ":clown_face:",
    "to": "\ud83e\udd21"
  },
  {
    "from": ":japanese_ogre:",
    "to": "\ud83d\udc79"
  },
  {
    "from": ":japanese_goblin:",
    "to": "\ud83d\udc7a"
  },
  {
    "from": ":ghost:",
    "to": "\ud83d\udc7b"
  },
  {
    "from": ":alien:",
    "to": "\ud83d\udc7d"
  },
  {
    "from": ":space_invader:",
    "to": "\ud83d\udc7e"
  },
  {
    "from": ":robot_face:",
    "to": "\ud83e\udd16"
  },
  {
    "from": ":robot:",
    "to": "\ud83e\udd16"
  },
  {
    "from": ":smiley_cat:",
    "to": "\ud83d\ude3a"
  },
  {
    "from": ":smile_cat:",
    "to": "\ud83d\ude38"
  },
  {
    "from": ":joy_cat:",
    "to": "\ud83d\ude39"
  },
  {
    "from": ":heart_eyes_cat:",
    "to": "\ud83d\ude3b"
  },
  {
    "from": ":smirk_cat:",
    "to": "\ud83d\ude3c"
  },
  {
    "from": ":kissing_cat:",
    "to": "\ud83d\ude3d"
  },
  {
    "from": ":scream_cat:",
    "to": "\ud83d\ude40"
  },
  {
    "from": ":crying_cat_face:",
    "to": "\ud83d\ude3f"
  },
  {
    "from": ":pouting_cat:",
    "to": "\ud83d\ude3e"
  },
  {
    "from": ":see_no_evil:",
    "to": "\ud83d\ude48"
  },
  {
    "from": ":hear_no_evil:",
    "to": "\ud83d\ude49"
  },
  {
    "from": ":speak_no_evil:",
    "to": "\ud83d\ude4a"
  },
  {
    "from": ":kiss:",
    "to": "\ud83d\udc8b"
  },
  {
    "from": ":love_letter:",
    "to": "\ud83d\udc8c"
  },
  {
    "from": ":cupid:",
    "to": "\ud83d\udc98"
  },
  {
    "from": ":gift_heart:",
    "to": "\ud83d\udc9d"
  },
  {
    "from": ":sparkling_heart:",
    "to": "\ud83d\udc96"
  },
  {
    "from": ":heartpulse:",
    "to": "\ud83d\udc97"
  },
  {
    "from": ":heartbeat:",
    "to": "\ud83d\udc93"
  },
  {
    "from": ":revolving_hearts:",
    "to": "\ud83d\udc9e"
  },
  {
    "from": ":two_hearts:",
    "to": "\ud83d\udc95"
  },
  {
    "from": ":heart_decoration:",
    "to": "\ud83d\udc9f"
  },
  {
    "from": ":broken_heart:",
    "to": "\ud83d\udc94"
  },
  {
    "from": "</3",
    "to": "\ud83d\udc94"
  },
  {
    "from": ":orange_heart:",
    "to": "\ud83e\udde1"
  },
  {
    "from": ":yellow_heart:",
    "to": "\ud83d\udc9b"
  },
  {
    "from": "<3",
    "to": "\ud83d\udc9c"
  },
  {
    "from": ":green_heart:",
    "to": "\ud83d\udc9a"
  },
  {
    "from": ":blue_heart:",
    "to": "\ud83d\udc99"
  },
  {
    "from": ":purple_heart:",
    "to": "\ud83d\udc9c"
  },
  {
    "from": ":brown_heart:",
    "to": "\ud83e\udd0e"
  },
  {
    "from": ":black_heart:",
    "to": "\ud83d\udda4"
  },
  {
    "from": ":white_heart:",
    "to": "\ud83e\udd0d"
  },
  {
    "from": ":100:",
    "to": "\ud83d\udcaf"
  },
  {
    "from": ":anger:",
    "to": "\ud83d\udca2"
  },
  {
    "from": ":boom:",
    "to": "\ud83d\udca5"
  },
  {
    "from": ":collision:",
    "to": "\ud83d\udca5"
  },
  {
    "from": ":dizzy:",
    "to": "\ud83d\udcab"
  },
  {
    "from": ":sweat_drops:",
    "to": "\ud83d\udca6"
  },
  {
    "from": ":dash:",
    "to": "\ud83d\udca8"
  },
  {
    "from": ":bomb:",
    "to": "\ud83d\udca3"
  },
  {
    "from": ":speech_balloon:",
    "to": "\ud83d\udcac"
  },
  {
    "from": ":thought_balloon:",
    "to": "\ud83d\udcad"
  },
  {
    "from": ":zzz:",
    "to": "\ud83d\udca4"
  },
  {
    "from": ":wave:",
    "to": "\ud83d\udc4b"
  },
  {
    "from": ":raised_back_of_hand:",
    "to": "\ud83e\udd1a"
  },
  {
    "from": ":hand:",
    "to": "\u270b"
  },
  {
    "from": ":raised_hand:",
    "to": "\u270b"
  },
  {
    "from": ":spock-hand:",
    "to": "\ud83d\udd96"
  },
  {
    "from": ":vulcan_salute:",
    "to": "\ud83d\udd96"
  },
  {
    "from": ":ok_hand:",
    "to": "\ud83d\udc4c"
  },
  {
    "from": ":pinched_fingers:",
    "to": "\ud83e\udd0c"
  },
  {
    "from": ":pinching_hand:",
    "to": "\ud83e\udd0f"
  },
  {
    "from": ":crossed_fingers:",
    "to": "\ud83e\udd1e"
  },
  {
    "from": ":hand_with_index_and_middle_fingers_crossed:",
    "to": "\ud83e\udd1e"
  },
  {
    "from": ":i_love_you_hand_sign:",
    "to": "\ud83e\udd1f"
  },
  {
    "from": ":the_horns:",
    "to": "\ud83e\udd18"
  },
  {
    "from": ":sign_of_the_horns:",
    "to": "\ud83e\udd18"
  },
  {
    "from": ":metal:",
    "to": "\ud83e\udd18"
  },
  {
    "from": ":call_me_hand:",
    "to": "\ud83e\udd19"
  },
  {
    "from": ":point_left:",
    "to": "\ud83d\udc48"
  },
  {
    "from": ":point_right:",
    "to": "\ud83d\udc49"
  },
  {
    "from": ":point_up_2:",
    "to": "\ud83d\udc46"
  },
  {
    "from": ":middle_finger:",
    "to": "\ud83d\udd95"
  },
  {
    "from": ":reversed_hand_with_middle_finger_extended:",
    "to": "\ud83d\udd95"
  },
  {
    "from": ":fu:",
    "to": "\ud83d\udd95"
  },
  {
    "from": ":point_down:",
    "to": "\ud83d\udc47"
  },
  {
    "from": ":+1:",
    "to": "\ud83d\udc4d"
  },
  {
    "from": ":thumbsup:",
    "to": "\ud83d\udc4d"
  },
  {
    "from": ":-1:",
    "to": "\ud83d\udc4e"
  },
  {
    "from": ":thumbsdown:",
    "to": "\ud83d\udc4e"
  },
  {
    "from": ":fist:",
    "to": "\u270a"
  },
  {
    "from": ":fist_raised:",
    "to": "\u270a"
  },
  {
    "from": ":facepunch:",
    "to": "\ud83d\udc4a"
  },
  {
    "from": ":punch:",
    "to": "\ud83d\udc4a"
  },
  {
    "from": ":fist_oncoming:",
    "to": "\ud83d\udc4a"
  },
  {
    "from": ":left-facing_fist:",
    "to": "\ud83e\udd1b"
  },
  {
    "from": ":fist_left:",
    "to": "\ud83e\udd1b"
  },
  {
    "from": ":right-facing_fist:",
    "to": "\ud83e\udd1c"
  },
  {
    "from": ":fist_right:",
    "to": "\ud83e\udd1c"
  },
  {
    "from": ":clap:",
    "to": "\ud83d\udc4f"
  },
  {
    "from": ":raised_hands:",
    "to": "\ud83d\ude4c"
  },
  {
    "from": ":open_hands:",
    "to": "\ud83d\udc50"
  },
  {
    "from": ":palms_up_together:",
    "to": "\ud83e\udd32"
  },
  {
    "from": ":handshake:",
    "to": "\ud83e\udd1d"
  },
  {
    "from": ":pray:",
    "to": "\ud83d\ude4f"
  },
  {
    "from": ":nail_care:",
    "to": "\ud83d\udc85"
  },
  {
    "from": ":selfie:",
    "to": "\ud83e\udd33"
  },
  {
    "from": ":muscle:",
    "to": "\ud83d\udcaa"
  },
  {
    "from": ":mechanical_arm:",
    "to": "\ud83e\uddbe"
  },
  {
    "from": ":mechanical_leg:",
    "to": "\ud83e\uddbf"
  },
  {
    "from": ":leg:",
    "to": "\ud83e\uddb5"
  },
  {
    "from": ":foot:",
    "to": "\ud83e\uddb6"
  },
  {
    "from": ":ear:",
    "to": "\ud83d\udc42"
  },
  {
    "from": ":ear_with_hearing_aid:",
    "to": "\ud83e\uddbb"
  },
  {
    "from": ":nose:",
    "to": "\ud83d\udc43"
  },
  {
    "from": ":brain:",
    "to": "\ud83e\udde0"
  },
  {
    "from": ":anatomical_heart:",
    "to": "\ud83e\udec0"
  },
  {
    "from": ":lungs:",
    "to": "\ud83e\udec1"
  },
  {
    "from": ":tooth:",
    "to": "\ud83e\uddb7"
  },
  {
    "from": ":bone:",
    "to": "\ud83e\uddb4"
  },
  {
    "from": ":eyes:",
    "to": "\ud83d\udc40"
  },
  {
    "from": ":tongue:",
    "to": "\ud83d\udc45"
  },
  {
    "from": ":lips:",
    "to": "\ud83d\udc44"
  },
  {
    "from": ":baby:",
    "to": "\ud83d\udc76"
  },
  {
    "from": ":child:",
    "to": "\ud83e\uddd2"
  },
  {
    "from": ":boy:",
    "to": "\ud83d\udc66"
  },
  {
    "from": ":girl:",
    "to": "\ud83d\udc67"
  },
  {
    "from": ":adult:",
    "to": "\ud83e\uddd1"
  },
  {
    "from": ":person_with_blond_hair:",
    "to": "\ud83d\udc71"
  },
  {
    "from": ":man:",
    "to": "\ud83d\udc68"
  },
  {
    "from": ":bearded_person:",
    "to": "\ud83e\uddd4"
  },
  {
    "from": ":woman:",
    "to": "\ud83d\udc69"
  },
  {
    "from": ":older_adult:",
    "to": "\ud83e\uddd3"
  },
  {
    "from": ":older_man:",
    "to": "\ud83d\udc74"
  },
  {
    "from": ":older_woman:",
    "to": "\ud83d\udc75"
  },
  {
    "from": ":person_frowning:",
    "to": "\ud83d\ude4d"
  },
  {
    "from": ":person_with_pouting_face:",
    "to": "\ud83d\ude4e"
  },
  {
    "from": ":no_good:",
    "to": "\ud83d\ude45"
  },
  {
    "from": ":ok_woman:",
    "to": "\ud83d\ude46"
  },
  {
    "from": ":information_desk_person:",
    "to": "\ud83d\udc81"
  },
  {
    "from": ":raising_hand:",
    "to": "\ud83d\ude4b"
  },
  {
    "from": ":deaf_person:",
    "to": "\ud83e\uddcf"
  },
  {
    "from": ":bow:",
    "to": "\ud83d\ude47"
  },
  {
    "from": ":face_palm:",
    "to": "\ud83e\udd26"
  },
  {
    "from": ":shrug:",
    "to": "\ud83e\udd37"
  },
  {
    "from": ":cop:",
    "to": "\ud83d\udc6e"
  },
  {
    "from": ":guardsman:",
    "to": "\ud83d\udc82"
  },
  {
    "from": ":ninja:",
    "to": "\ud83e\udd77"
  },
  {
    "from": ":construction_worker:",
    "to": "\ud83d\udc77"
  },
  {
    "from": ":prince:",
    "to": "\ud83e\udd34"
  },
  {
    "from": ":princess:",
    "to": "\ud83d\udc78"
  },
  {
    "from": ":man_with_turban:",
    "to": "\ud83d\udc73"
  },
  {
    "from": ":man_with_gua_pi_mao:",
    "to": "\ud83d\udc72"
  },
  {
    "from": ":person_with_headscarf:",
    "to": "\ud83e\uddd5"
  },
  {
    "from": ":person_in_tuxedo:",
    "to": "\ud83e\udd35"
  },
  {
    "from": ":bride_with_veil:",
    "to": "\ud83d\udc70"
  },
  {
    "from": ":pregnant_woman:",
    "to": "\ud83e\udd30"
  },
  {
    "from": ":breast-feeding:",
    "to": "\ud83e\udd31"
  },
  {
    "from": ":angel:",
    "to": "\ud83d\udc7c"
  },
  {
    "from": ":santa:",
    "to": "\ud83c\udf85"
  },
  {
    "from": ":mrs_claus:",
    "to": "\ud83e\udd36"
  },
  {
    "from": ":mother_christmas:",
    "to": "\ud83e\udd36"
  },
  {
    "from": ":superhero:",
    "to": "\ud83e\uddb8"
  },
  {
    "from": ":supervillain:",
    "to": "\ud83e\uddb9"
  },
  {
    "from": ":mage:",
    "to": "\ud83e\uddd9"
  },
  {
    "from": ":fairy:",
    "to": "\ud83e\uddda"
  },
  {
    "from": ":vampire:",
    "to": "\ud83e\udddb"
  },
  {
    "from": ":merperson:",
    "to": "\ud83e\udddc"
  },
  {
    "from": ":elf:",
    "to": "\ud83e\udddd"
  },
  {
    "from": ":genie:",
    "to": "\ud83e\uddde"
  },
  {
    "from": ":zombie:",
    "to": "\ud83e\udddf"
  },
  {
    "from": ":massage:",
    "to": "\ud83d\udc86"
  },
  {
    "from": ":haircut:",
    "to": "\ud83d\udc87"
  },
  {
    "from": ":walking:",
    "to": "\ud83d\udeb6"
  },
  {
    "from": ":standing_person:",
    "to": "\ud83e\uddcd"
  },
  {
    "from": ":kneeling_person:",
    "to": "\ud83e\uddce"
  },
  {
    "from": ":runner:",
    "to": "\ud83c\udfc3"
  },
  {
    "from": ":running:",
    "to": "\ud83c\udfc3"
  },
  {
    "from": ":dancer:",
    "to": "\ud83d\udc83"
  },
  {
    "from": ":man_dancing:",
    "to": "\ud83d\udd7a"
  },
  {
    "from": ":dancers:",
    "to": "\ud83d\udc6f"
  },
  {
    "from": ":person_in_steamy_room:",
    "to": "\ud83e\uddd6"
  },
  {
    "from": ":person_climbing:",
    "to": "\ud83e\uddd7"
  },
  {
    "from": ":fencer:",
    "to": "\ud83e\udd3a"
  },
  {
    "from": ":person_fencing:",
    "to": "\ud83e\udd3a"
  },
  {
    "from": ":horse_racing:",
    "to": "\ud83c\udfc7"
  },
  {
    "from": ":snowboarder:",
    "to": "\ud83c\udfc2"
  },
  {
    "from": ":surfer:",
    "to": "\ud83c\udfc4"
  },
  {
    "from": ":rowboat:",
    "to": "\ud83d\udea3"
  },
  {
    "from": ":swimmer:",
    "to": "\ud83c\udfca"
  },
  {
    "from": ":bicyclist:",
    "to": "\ud83d\udeb4"
  },
  {
    "from": ":mountain_bicyclist:",
    "to": "\ud83d\udeb5"
  },
  {
    "from": ":person_doing_cartwheel:",
    "to": "\ud83e\udd38"
  },
  {
    "from": ":wrestlers:",
    "to": "\ud83e\udd3c"
  },
  {
    "from": ":water_polo:",
    "to": "\ud83e\udd3d"
  },
  {
    "from": ":handball:",
    "to": "\ud83e\udd3e"
  },
  {
    "from": ":juggling:",
    "to": "\ud83e\udd39"
  },
  {
    "from": ":person_in_lotus_position:",
    "to": "\ud83e\uddd8"
  },
  {
    "from": ":bath:",
    "to": "\ud83d\udec0"
  },
  {
    "from": ":sleeping_accommodation:",
    "to": "\ud83d\udecc"
  },
  {
    "from": ":sleeping_bed:",
    "to": "\ud83d\udecc"
  },
  {
    "from": ":two_women_holding_hands:",
    "to": "\ud83d\udc6d"
  },
  {
    "from": ":women_holding_hands:",
    "to": "\ud83d\udc6d"
  },
  {
    "from": ":man_and_woman_holding_hands:",
    "to": "\ud83d\udc6b"
  },
  {
    "from": ":woman_and_man_holding_hands:",
    "to": "\ud83d\udc6b"
  },
  {
    "from": ":couple:",
    "to": "\ud83d\udc6b"
  },
  {
    "from": ":two_men_holding_hands:",
    "to": "\ud83d\udc6c"
  },
  {
    "from": ":men_holding_hands:",
    "to": "\ud83d\udc6c"
  },
  {
    "from": ":couplekiss:",
    "to": "\ud83d\udc8f"
  },
  {
    "from": ":couple_with_heart:",
    "to": "\ud83d\udc91"
  },
  {
    "from": ":family:",
    "to": "\ud83d\udc6a"
  },
  {
    "from": ":bust_in_silhouette:",
    "to": "\ud83d\udc64"
  },
  {
    "from": ":busts_in_silhouette:",
    "to": "\ud83d\udc65"
  },
  {
    "from": ":people_hugging:",
    "to": "\ud83e\udec2"
  },
  {
    "from": ":footprints:",
    "to": "\ud83d\udc63"
  },
  {
    "from": ":skin-tone-2:",
    "to": "\ud83c\udffb"
  },
  {
    "from": ":skin-tone-3:",
    "to": "\ud83c\udffc"
  },
  {
    "from": ":skin-tone-4:",
    "to": "\ud83c\udffd"
  },
  {
    "from": ":skin-tone-5:",
    "to": "\ud83c\udffe"
  },
  {
    "from": ":skin-tone-6:",
    "to": "\ud83c\udfff"
  },
  {
    "from": ":monkey_face:",
    "to": "\ud83d\udc35"
  },
  {
    "from": ":o)",
    "to": "\ud83d\udc35"
  },
  {
    "from": ":monkey:",
    "to": "\ud83d\udc12"
  },
  {
    "from": ":gorilla:",
    "to": "\ud83e\udd8d"
  },
  {
    "from": ":orangutan:",
    "to": "\ud83e\udda7"
  },
  {
    "from": ":dog:",
    "to": "\ud83d\udc36"
  },
  {
    "from": ":dog2:",
    "to": "\ud83d\udc15"
  },
  {
    "from": ":guide_dog:",
    "to": "\ud83e\uddae"
  },
  {
    "from": ":poodle:",
    "to": "\ud83d\udc29"
  },
  {
    "from": ":wolf:",
    "to": "\ud83d\udc3a"
  },
  {
    "from": ":fox_face:",
    "to": "\ud83e\udd8a"
  },
  {
    "from": ":raccoon:",
    "to": "\ud83e\udd9d"
  },
  {
    "from": ":cat:",
    "to": "\ud83d\udc31"
  },
  {
    "from": ":cat2:",
    "to": "\ud83d\udc08"
  },
  {
    "from": ":lion_face:",
    "to": "\ud83e\udd81"
  },
  {
    "from": ":lion:",
    "to": "\ud83e\udd81"
  },
  {
    "from": ":tiger:",
    "to": "\ud83d\udc2f"
  },
  {
    "from": ":tiger2:",
    "to": "\ud83d\udc05"
  },
  {
    "from": ":leopard:",
    "to": "\ud83d\udc06"
  },
  {
    "from": ":horse:",
    "to": "\ud83d\udc34"
  },
  {
    "from": ":racehorse:",
    "to": "\ud83d\udc0e"
  },
  {
    "from": ":unicorn_face:",
    "to": "\ud83e\udd84"
  },
  {
    "from": ":unicorn:",
    "to": "\ud83e\udd84"
  },
  {
    "from": ":zebra_face:",
    "to": "\ud83e\udd93"
  },
  {
    "from": ":deer:",
    "to": "\ud83e\udd8c"
  },
  {
    "from": ":bison:",
    "to": "\ud83e\uddac"
  },
  {
    "from": ":cow:",
    "to": "\ud83d\udc2e"
  },
  {
    "from": ":ox:",
    "to": "\ud83d\udc02"
  },
  {
    "from": ":water_buffalo:",
    "to": "\ud83d\udc03"
  },
  {
    "from": ":cow2:",
    "to": "\ud83d\udc04"
  },
  {
    "from": ":pig:",
    "to": "\ud83d\udc37"
  },
  {
    "from": ":pig2:",
    "to": "\ud83d\udc16"
  },
  {
    "from": ":boar:",
    "to": "\ud83d\udc17"
  },
  {
    "from": ":pig_nose:",
    "to": "\ud83d\udc3d"
  },
  {
    "from": ":ram:",
    "to": "\ud83d\udc0f"
  },
  {
    "from": ":sheep:",
    "to": "\ud83d\udc11"
  },
  {
    "from": ":goat:",
    "to": "\ud83d\udc10"
  },
  {
    "from": ":dromedary_camel:",
    "to": "\ud83d\udc2a"
  },
  {
    "from": ":camel:",
    "to": "\ud83d\udc2b"
  },
  {
    "from": ":llama:",
    "to": "\ud83e\udd99"
  },
  {
    "from": ":giraffe_face:",
    "to": "\ud83e\udd92"
  },
  {
    "from": ":elephant:",
    "to": "\ud83d\udc18"
  },
  {
    "from": ":mammoth:",
    "to": "\ud83e\udda3"
  },
  {
    "from": ":rhinoceros:",
    "to": "\ud83e\udd8f"
  },
  {
    "from": ":hippopotamus:",
    "to": "\ud83e\udd9b"
  },
  {
    "from": ":mouse:",
    "to": "\ud83d\udc2d"
  },
  {
    "from": ":mouse2:",
    "to": "\ud83d\udc01"
  },
  {
    "from": ":rat:",
    "to": "\ud83d\udc00"
  },
  {
    "from": ":hamster:",
    "to": "\ud83d\udc39"
  },
  {
    "from": ":rabbit:",
    "to": "\ud83d\udc30"
  },
  {
    "from": ":rabbit2:",
    "to": "\ud83d\udc07"
  },
  {
    "from": ":beaver:",
    "to": "\ud83e\uddab"
  },
  {
    "from": ":hedgehog:",
    "to": "\ud83e\udd94"
  },
  {
    "from": ":bat:",
    "to": "\ud83e\udd87"
  },
  {
    "from": ":bear:",
    "to": "\ud83d\udc3b"
  },
  {
    "from": ":koala:",
    "to": "\ud83d\udc28"
  },
  {
    "from": ":panda_face:",
    "to": "\ud83d\udc3c"
  },
  {
    "from": ":sloth:",
    "to": "\ud83e\udda5"
  },
  {
    "from": ":otter:",
    "to": "\ud83e\udda6"
  },
  {
    "from": ":skunk:",
    "to": "\ud83e\udda8"
  },
  {
    "from": ":kangaroo:",
    "to": "\ud83e\udd98"
  },
  {
    "from": ":badger:",
    "to": "\ud83e\udda1"
  },
  {
    "from": ":feet:",
    "to": "\ud83d\udc3e"
  },
  {
    "from": ":paw_prints:",
    "to": "\ud83d\udc3e"
  },
  {
    "from": ":turkey:",
    "to": "\ud83e\udd83"
  },
  {
    "from": ":chicken:",
    "to": "\ud83d\udc14"
  },
  {
    "from": ":rooster:",
    "to": "\ud83d\udc13"
  },
  {
    "from": ":hatching_chick:",
    "to": "\ud83d\udc23"
  },
  {
    "from": ":baby_chick:",
    "to": "\ud83d\udc24"
  },
  {
    "from": ":hatched_chick:",
    "to": "\ud83d\udc25"
  },
  {
    "from": ":bird:",
    "to": "\ud83d\udc26"
  },
  {
    "from": ":penguin:",
    "to": "\ud83d\udc27"
  },
  {
    "from": ":eagle:",
    "to": "\ud83e\udd85"
  },
  {
    "from": ":duck:",
    "to": "\ud83e\udd86"
  },
  {
    "from": ":swan:",
    "to": "\ud83e\udda2"
  },
  {
    "from": ":owl:",
    "to": "\ud83e\udd89"
  },
  {
    "from": ":dodo:",
    "to": "\ud83e\udda4"
  },
  {
    "from": ":feather:",
    "to": "\ud83e\udeb6"
  },
  {
    "from": ":flamingo:",
    "to": "\ud83e\udda9"
  },
  {
    "from": ":peacock:",
    "to": "\ud83e\udd9a"
  },
  {
    "from": ":parrot:",
    "to": "\ud83e\udd9c"
  },
  {
    "from": ":frog:",
    "to": "\ud83d\udc38"
  },
  {
    "from": ":crocodile:",
    "to": "\ud83d\udc0a"
  },
  {
    "from": ":turtle:",
    "to": "\ud83d\udc22"
  },
  {
    "from": ":lizard:",
    "to": "\ud83e\udd8e"
  },
  {
    "from": ":snake:",
    "to": "\ud83d\udc0d"
  },
  {
    "from": ":dragon_face:",
    "to": "\ud83d\udc32"
  },
  {
    "from": ":dragon:",
    "to": "\ud83d\udc09"
  },
  {
    "from": ":sauropod:",
    "to": "\ud83e\udd95"
  },
  {
    "from": ":t-rex:",
    "to": "\ud83e\udd96"
  },
  {
    "from": ":whale:",
    "to": "\ud83d\udc33"
  },
  {
    "from": ":whale2:",
    "to": "\ud83d\udc0b"
  },
  {
    "from": ":dolphin:",
    "to": "\ud83d\udc2c"
  },
  {
    "from": ":flipper:",
    "to": "\ud83d\udc2c"
  },
  {
    "from": ":seal:",
    "to": "\ud83e\uddad"
  },
  {
    "from": ":fish:",
    "to": "\ud83d\udc1f"
  },
  {
    "from": ":tropical_fish:",
    "to": "\ud83d\udc20"
  },
  {
    "from": ":blowfish:",
    "to": "\ud83d\udc21"
  },
  {
    "from": ":shark:",
    "to": "\ud83e\udd88"
  },
  {
    "from": ":octopus:",
    "to": "\ud83d\udc19"
  },
  {
    "from": ":shell:",
    "to": "\ud83d\udc1a"
  },
  {
    "from": ":snail:",
    "to": "\ud83d\udc0c"
  },
  {
    "from": ":butterfly:",
    "to": "\ud83e\udd8b"
  },
  {
    "from": ":bug:",
    "to": "\ud83d\udc1b"
  },
  {
    "from": ":ant:",
    "to": "\ud83d\udc1c"
  },
  {
    "from": ":bee:",
    "to": "\ud83d\udc1d"
  },
  {
    "from": ":honeybee:",
    "to": "\ud83d\udc1d"
  },
  {
    "from": ":beetle:",
    "to": "\ud83e\udeb2"
  },
  {
    "from": ":ladybug:",
    "to": "\ud83d\udc1e"
  },
  {
    "from": ":lady_beetle:",
    "to": "\ud83d\udc1e"
  },
  {
    "from": ":cricket:",
    "to": "\ud83e\udd97"
  },
  {
    "from": ":cockroach:",
    "to": "\ud83e\udeb3"
  },
  {
    "from": ":scorpion:",
    "to": "\ud83e\udd82"
  },
  {
    "from": ":mosquito:",
    "to": "\ud83e\udd9f"
  },
  {
    "from": ":fly:",
    "to": "\ud83e\udeb0"
  },
  {
    "from": ":worm:",
    "to": "\ud83e\udeb1"
  },
  {
    "from": ":microbe:",
    "to": "\ud83e\udda0"
  },
  {
    "from": ":bouquet:",
    "to": "\ud83d\udc90"
  },
  {
    "from": ":cherry_blossom:",
    "to": "\ud83c\udf38"
  },
  {
    "from": ":white_flower:",
    "to": "\ud83d\udcae"
  },
  {
    "from": ":rose:",
    "to": "\ud83c\udf39"
  },
  {
    "from": ":wilted_flower:",
    "to": "\ud83e\udd40"
  },
  {
    "from": ":hibiscus:",
    "to": "\ud83c\udf3a"
  },
  {
    "from": ":sunflower:",
    "to": "\ud83c\udf3b"
  },
  {
    "from": ":blossom:",
    "to": "\ud83c\udf3c"
  },
  {
    "from": ":tulip:",
    "to": "\ud83c\udf37"
  },
  {
    "from": ":seedling:",
    "to": "\ud83c\udf31"
  },
  {
    "from": ":potted_plant:",
    "to": "\ud83e\udeb4"
  },
  {
    "from": ":evergreen_tree:",
    "to": "\ud83c\udf32"
  },
  {
    "from": ":deciduous_tree:",
    "to": "\ud83c\udf33"
  },
  {
    "from": ":palm_tree:",
    "to": "\ud83c\udf34"
  },
  {
    "from": ":cactus:",
    "to": "\ud83c\udf35"
  },
  {
    "from": ":ear_of_rice:",
    "to": "\ud83c\udf3e"
  },
  {
    "from": ":herb:",
    "to": "\ud83c\udf3f"
  },
  {
    "from": ":four_leaf_clover:",
    "to": "\ud83c\udf40"
  },
  {
    "from": ":maple_leaf:",
    "to": "\ud83c\udf41"
  },
  {
    "from": ":fallen_leaf:",
    "to": "\ud83c\udf42"
  },
  {
    "from": ":leaves:",
    "to": "\ud83c\udf43"
  },
  {
    "from": ":grapes:",
    "to": "\ud83c\udf47"
  },
  {
    "from": ":melon:",
    "to": "\ud83c\udf48"
  },
  {
    "from": ":watermelon:",
    "to": "\ud83c\udf49"
  },
  {
    "from": ":tangerine:",
    "to": "\ud83c\udf4a"
  },
  {
    "from": ":mandarin:",
    "to": "\ud83c\udf4a"
  },
  {
    "from": ":orange:",
    "to": "\ud83c\udf4a"
  },
  {
    "from": ":lemon:",
    "to": "\ud83c\udf4b"
  },
  {
    "from": ":banana:",
    "to": "\ud83c\udf4c"
  },
  {
    "from": ":pineapple:",
    "to": "\ud83c\udf4d"
  },
  {
    "from": ":mango:",
    "to": "\ud83e\udd6d"
  },
  {
    "from": ":apple:",
    "to": "\ud83c\udf4e"
  },
  {
    "from": ":green_apple:",
    "to": "\ud83c\udf4f"
  },
  {
    "from": ":pear:",
    "to": "\ud83c\udf50"
  },
  {
    "from": ":peach:",
    "to": "\ud83c\udf51"
  },
  {
    "from": ":cherries:",
    "to": "\ud83c\udf52"
  },
  {
    "from": ":strawberry:",
    "to": "\ud83c\udf53"
  },
  {
    "from": ":blueberries:",
    "to": "\ud83e\uded0"
  },
  {
    "from": ":kiwifruit:",
    "to": "\ud83e\udd5d"
  },
  {
    "from": ":kiwi_fruit:",
    "to": "\ud83e\udd5d"
  },
  {
    "from": ":tomato:",
    "to": "\ud83c\udf45"
  },
  {
    "from": ":olive:",
    "to": "\ud83e\uded2"
  },
  {
    "from": ":coconut:",
    "to": "\ud83e\udd65"
  },
  {
    "from": ":avocado:",
    "to": "\ud83e\udd51"
  },
  {
    "from": ":eggplant:",
    "to": "\ud83c\udf46"
  },
  {
    "from": ":potato:",
    "to": "\ud83e\udd54"
  },
  {
    "from": ":carrot:",
    "to": "\ud83e\udd55"
  },
  {
    "from": ":corn:",
    "to": "\ud83c\udf3d"
  },
  {
    "from": ":bell_pepper:",
    "to": "\ud83e\uded1"
  },
  {
    "from": ":cucumber:",
    "to": "\ud83e\udd52"
  },
  {
    "from": ":leafy_green:",
    "to": "\ud83e\udd6c"
  },
  {
    "from": ":broccoli:",
    "to": "\ud83e\udd66"
  },
  {
    "from": ":garlic:",
    "to": "\ud83e\uddc4"
  },
  {
    "from": ":onion:",
    "to": "\ud83e\uddc5"
  },
  {
    "from": ":mushroom:",
    "to": "\ud83c\udf44"
  },
  {
    "from": ":peanuts:",
    "to": "\ud83e\udd5c"
  },
  {
    "from": ":chestnut:",
    "to": "\ud83c\udf30"
  },
  {
    "from": ":bread:",
    "to": "\ud83c\udf5e"
  },
  {
    "from": ":croissant:",
    "to": "\ud83e\udd50"
  },
  {
    "from": ":baguette_bread:",
    "to": "\ud83e\udd56"
  },
  {
    "from": ":flatbread:",
    "to": "\ud83e\uded3"
  },
  {
    "from": ":pretzel:",
    "to": "\ud83e\udd68"
  },
  {
    "from": ":bagel:",
    "to": "\ud83e\udd6f"
  },
  {
    "from": ":pancakes:",
    "to": "\ud83e\udd5e"
  },
  {
    "from": ":waffle:",
    "to": "\ud83e\uddc7"
  },
  {
    "from": ":cheese_wedge:",
    "to": "\ud83e\uddc0"
  },
  {
    "from": ":cheese:",
    "to": "\ud83e\uddc0"
  },
  {
    "from": ":meat_on_bone:",
    "to": "\ud83c\udf56"
  },
  {
    "from": ":poultry_leg:",
    "to": "\ud83c\udf57"
  },
  {
    "from": ":cut_of_meat:",
    "to": "\ud83e\udd69"
  },
  {
    "from": ":bacon:",
    "to": "\ud83e\udd53"
  },
  {
    "from": ":hamburger:",
    "to": "\ud83c\udf54"
  },
  {
    "from": ":fries:",
    "to": "\ud83c\udf5f"
  },
  {
    "from": ":pizza:",
    "to": "\ud83c\udf55"
  },
  {
    "from": ":hotdog:",
    "to": "\ud83c\udf2d"
  },
  {
    "from": ":sandwich:",
    "to": "\ud83e\udd6a"
  },
  {
    "from": ":taco:",
    "to": "\ud83c\udf2e"
  },
  {
    "from": ":burrito:",
    "to": "\ud83c\udf2f"
  },
  {
    "from": ":tamale:",
    "to": "\ud83e\uded4"
  },
  {
    "from": ":stuffed_flatbread:",
    "to": "\ud83e\udd59"
  },
  {
    "from": ":falafel:",
    "to": "\ud83e\uddc6"
  },
  {
    "from": ":egg:",
    "to": "\ud83e\udd5a"
  },
  {
    "from": ":fried_egg:",
    "to": "\ud83c\udf73"
  },
  {
    "from": ":cooking:",
    "to": "\ud83c\udf73"
  },
  {
    "from": ":shallow_pan_of_food:",
    "to": "\ud83e\udd58"
  },
  {
    "from": ":stew:",
    "to": "\ud83c\udf72"
  },
  {
    "from": ":fondue:",
    "to": "\ud83e\uded5"
  },
  {
    "from": ":bowl_with_spoon:",
    "to": "\ud83e\udd63"
  },
  {
    "from": ":green_salad:",
    "to": "\ud83e\udd57"
  },
  {
    "from": ":popcorn:",
    "to": "\ud83c\udf7f"
  },
  {
    "from": ":butter:",
    "to": "\ud83e\uddc8"
  },
  {
    "from": ":salt:",
    "to": "\ud83e\uddc2"
  },
  {
    "from": ":canned_food:",
    "to": "\ud83e\udd6b"
  },
  {
    "from": ":bento:",
    "to": "\ud83c\udf71"
  },
  {
    "from": ":rice_cracker:",
    "to": "\ud83c\udf58"
  },
  {
    "from": ":rice_ball:",
    "to": "\ud83c\udf59"
  },
  {
    "from": ":rice:",
    "to": "\ud83c\udf5a"
  },
  {
    "from": ":curry:",
    "to": "\ud83c\udf5b"
  },
  {
    "from": ":ramen:",
    "to": "\ud83c\udf5c"
  },
  {
    "from": ":spaghetti:",
    "to": "\ud83c\udf5d"
  },
  {
    "from": ":sweet_potato:",
    "to": "\ud83c\udf60"
  },
  {
    "from": ":oden:",
    "to": "\ud83c\udf62"
  },
  {
    "from": ":sushi:",
    "to": "\ud83c\udf63"
  },
  {
    "from": ":fried_shrimp:",
    "to": "\ud83c\udf64"
  },
  {
    "from": ":fish_cake:",
    "to": "\ud83c\udf65"
  },
  {
    "from": ":moon_cake:",
    "to": "\ud83e\udd6e"
  },
  {
    "from": ":dango:",
    "to": "\ud83c\udf61"
  },
  {
    "from": ":dumpling:",
    "to": "\ud83e\udd5f"
  },
  {
    "from": ":fortune_cookie:",
    "to": "\ud83e\udd60"
  },
  {
    "from": ":takeout_box:",
    "to": "\ud83e\udd61"
  },
  {
    "from": ":crab:",
    "to": "\ud83e\udd80"
  },
  {
    "from": ":lobster:",
    "to": "\ud83e\udd9e"
  },
  {
    "from": ":shrimp:",
    "to": "\ud83e\udd90"
  },
  {
    "from": ":squid:",
    "to": "\ud83e\udd91"
  },
  {
    "from": ":oyster:",
    "to": "\ud83e\uddaa"
  },
  {
    "from": ":icecream:",
    "to": "\ud83c\udf66"
  },
  {
    "from": ":shaved_ice:",
    "to": "\ud83c\udf67"
  },
  {
    "from": ":ice_cream:",
    "to": "\ud83c\udf68"
  },
  {
    "from": ":doughnut:",
    "to": "\ud83c\udf69"
  },
  {
    "from": ":cookie:",
    "to": "\ud83c\udf6a"
  },
  {
    "from": ":birthday:",
    "to": "\ud83c\udf82"
  },
  {
    "from": ":cake:",
    "to": "\ud83c\udf70"
  },
  {
    "from": ":cupcake:",
    "to": "\ud83e\uddc1"
  },
  {
    "from": ":pie:",
    "to": "\ud83e\udd67"
  },
  {
    "from": ":chocolate_bar:",
    "to": "\ud83c\udf6b"
  },
  {
    "from": ":candy:",
    "to": "\ud83c\udf6c"
  },
  {
    "from": ":lollipop:",
    "to": "\ud83c\udf6d"
  },
  {
    "from": ":custard:",
    "to": "\ud83c\udf6e"
  },
  {
    "from": ":honey_pot:",
    "to": "\ud83c\udf6f"
  },
  {
    "from": ":baby_bottle:",
    "to": "\ud83c\udf7c"
  },
  {
    "from": ":glass_of_milk:",
    "to": "\ud83e\udd5b"
  },
  {
    "from": ":milk_glass:",
    "to": "\ud83e\udd5b"
  },
  {
    "from": ":coffee:",
    "to": "\u2615"
  },
  {
    "from": ":teapot:",
    "to": "\ud83e\uded6"
  },
  {
    "from": ":tea:",
    "to": "\ud83c\udf75"
  },
  {
    "from": ":sake:",
    "to": "\ud83c\udf76"
  },
  {
    "from": ":champagne:",
    "to": "\ud83c\udf7e"
  },
  {
    "from": ":wine_glass:",
    "to": "\ud83c\udf77"
  },
  {
    "from": ":cocktail:",
    "to": "\ud83c\udf78"
  },
  {
    "from": ":tropical_drink:",
    "to": "\ud83c\udf79"
  },
  {
    "from": ":beer:",
    "to": "\ud83c\udf7a"
  },
  {
    "from": ":beers:",
    "to": "\ud83c\udf7b"
  },
  {
    "from": ":clinking_glasses:",
    "to": "\ud83e\udd42"
  },
  {
    "from": ":tumbler_glass:",
    "to": "\ud83e\udd43"
  },
  {
    "from": ":cup_with_straw:",
    "to": "\ud83e\udd64"
  },
  {
    "from": ":bubble_tea:",
    "to": "\ud83e\uddcb"
  },
  {
    "from": ":beverage_box:",
    "to": "\ud83e\uddc3"
  },
  {
    "from": ":mate_drink:",
    "to": "\ud83e\uddc9"
  },
  {
    "from": ":ice_cube:",
    "to": "\ud83e\uddca"
  },
  {
    "from": ":chopsticks:",
    "to": "\ud83e\udd62"
  },
  {
    "from": ":fork_and_knife:",
    "to": "\ud83c\udf74"
  },
  {
    "from": ":spoon:",
    "to": "\ud83e\udd44"
  },
  {
    "from": ":hocho:",
    "to": "\ud83d\udd2a"
  },
  {
    "from": ":knife:",
    "to": "\ud83d\udd2a"
  },
  {
    "from": ":amphora:",
    "to": "\ud83c\udffa"
  },
  {
    "from": ":earth_africa:",
    "to": "\ud83c\udf0d"
  },
  {
    "from": ":earth_americas:",
    "to": "\ud83c\udf0e"
  },
  {
    "from": ":earth_asia:",
    "to": "\ud83c\udf0f"
  },
  {
    "from": ":globe_with_meridians:",
    "to": "\ud83c\udf10"
  },
  {
    "from": ":japan:",
    "to": "\ud83d\uddfe"
  },
  {
    "from": ":compass:",
    "to": "\ud83e\udded"
  },
  {
    "from": ":volcano:",
    "to": "\ud83c\udf0b"
  },
  {
    "from": ":mount_fuji:",
    "to": "\ud83d\uddfb"
  },
  {
    "from": ":bricks:",
    "to": "\ud83e\uddf1"
  },
  {
    "from": ":rock:",
    "to": "\ud83e\udea8"
  },
  {
    "from": ":wood:",
    "to": "\ud83e\udeb5"
  },
  {
    "from": ":hut:",
    "to": "\ud83d\uded6"
  },
  {
    "from": ":house:",
    "to": "\ud83c\udfe0"
  },
  {
    "from": ":house_with_garden:",
    "to": "\ud83c\udfe1"
  },
  {
    "from": ":office:",
    "to": "\ud83c\udfe2"
  },
  {
    "from": ":post_office:",
    "to": "\ud83c\udfe3"
  },
  {
    "from": ":european_post_office:",
    "to": "\ud83c\udfe4"
  },
  {
    "from": ":hospital:",
    "to": "\ud83c\udfe5"
  },
  {
    "from": ":bank:",
    "to": "\ud83c\udfe6"
  },
  {
    "from": ":hotel:",
    "to": "\ud83c\udfe8"
  },
  {
    "from": ":love_hotel:",
    "to": "\ud83c\udfe9"
  },
  {
    "from": ":convenience_store:",
    "to": "\ud83c\udfea"
  },
  {
    "from": ":school:",
    "to": "\ud83c\udfeb"
  },
  {
    "from": ":department_store:",
    "to": "\ud83c\udfec"
  },
  {
    "from": ":factory:",
    "to": "\ud83c\udfed"
  },
  {
    "from": ":japanese_castle:",
    "to": "\ud83c\udfef"
  },
  {
    "from": ":european_castle:",
    "to": "\ud83c\udff0"
  },
  {
    "from": ":wedding:",
    "to": "\ud83d\udc92"
  },
  {
    "from": ":tokyo_tower:",
    "to": "\ud83d\uddfc"
  },
  {
    "from": ":statue_of_liberty:",
    "to": "\ud83d\uddfd"
  },
  {
    "from": ":church:",
    "to": "\u26ea"
  },
  {
    "from": ":mosque:",
    "to": "\ud83d\udd4c"
  },
  {
    "from": ":hindu_temple:",
    "to": "\ud83d\uded5"
  },
  {
    "from": ":synagogue:",
    "to": "\ud83d\udd4d"
  },
  {
    "from": ":kaaba:",
    "to": "\ud83d\udd4b"
  },
  {
    "from": ":fountain:",
    "to": "\u26f2"
  },
  {
    "from": ":tent:",
    "to": "\u26fa"
  },
  {
    "from": ":foggy:",
    "to": "\ud83c\udf01"
  },
  {
    "from": ":night_with_stars:",
    "to": "\ud83c\udf03"
  },
  {
    "from": ":sunrise_over_mountains:",
    "to": "\ud83c\udf04"
  },
  {
    "from": ":sunrise:",
    "to": "\ud83c\udf05"
  },
  {
    "from": ":city_sunset:",
    "to": "\ud83c\udf06"
  },
  {
    "from": ":city_sunrise:",
    "to": "\ud83c\udf07"
  },
  {
    "from": ":bridge_at_night:",
    "to": "\ud83c\udf09"
  },
  {
    "from": ":carousel_horse:",
    "to": "\ud83c\udfa0"
  },
  {
    "from": ":ferris_wheel:",
    "to": "\ud83c\udfa1"
  },
  {
    "from": ":roller_coaster:",
    "to": "\ud83c\udfa2"
  },
  {
    "from": ":barber:",
    "to": "\ud83d\udc88"
  },
  {
    "from": ":circus_tent:",
    "to": "\ud83c\udfaa"
  },
  {
    "from": ":steam_locomotive:",
    "to": "\ud83d\ude82"
  },
  {
    "from": ":railway_car:",
    "to": "\ud83d\ude83"
  },
  {
    "from": ":bullettrain_side:",
    "to": "\ud83d\ude84"
  },
  {
    "from": ":bullettrain_front:",
    "to": "\ud83d\ude85"
  },
  {
    "from": ":train2:",
    "to": "\ud83d\ude86"
  },
  {
    "from": ":metro:",
    "to": "\ud83d\ude87"
  },
  {
    "from": ":light_rail:",
    "to": "\ud83d\ude88"
  },
  {
    "from": ":station:",
    "to": "\ud83d\ude89"
  },
  {
    "from": ":tram:",
    "to": "\ud83d\ude8a"
  },
  {
    "from": ":monorail:",
    "to": "\ud83d\ude9d"
  },
  {
    "from": ":mountain_railway:",
    "to": "\ud83d\ude9e"
  },
  {
    "from": ":train:",
    "to": "\ud83d\ude8b"
  },
  {
    "from": ":bus:",
    "to": "\ud83d\ude8c"
  },
  {
    "from": ":oncoming_bus:",
    "to": "\ud83d\ude8d"
  },
  {
    "from": ":trolleybus:",
    "to": "\ud83d\ude8e"
  },
  {
    "from": ":minibus:",
    "to": "\ud83d\ude90"
  },
  {
    "from": ":ambulance:",
    "to": "\ud83d\ude91"
  },
  {
    "from": ":fire_engine:",
    "to": "\ud83d\ude92"
  },
  {
    "from": ":police_car:",
    "to": "\ud83d\ude93"
  },
  {
    "from": ":oncoming_police_car:",
    "to": "\ud83d\ude94"
  },
  {
    "from": ":taxi:",
    "to": "\ud83d\ude95"
  },
  {
    "from": ":oncoming_taxi:",
    "to": "\ud83d\ude96"
  },
  {
    "from": ":car:",
    "to": "\ud83d\ude97"
  },
  {
    "from": ":red_car:",
    "to": "\ud83d\ude97"
  },
  {
    "from": ":oncoming_automobile:",
    "to": "\ud83d\ude98"
  },
  {
    "from": ":blue_car:",
    "to": "\ud83d\ude99"
  },
  {
    "from": ":pickup_truck:",
    "to": "\ud83d\udefb"
  },
  {
    "from": ":truck:",
    "to": "\ud83d\ude9a"
  },
  {
    "from": ":articulated_lorry:",
    "to": "\ud83d\ude9b"
  },
  {
    "from": ":tractor:",
    "to": "\ud83d\ude9c"
  },
  {
    "from": ":motor_scooter:",
    "to": "\ud83d\udef5"
  },
  {
    "from": ":manual_wheelchair:",
    "to": "\ud83e\uddbd"
  },
  {
    "from": ":motorized_wheelchair:",
    "to": "\ud83e\uddbc"
  },
  {
    "from": ":auto_rickshaw:",
    "to": "\ud83d\udefa"
  },
  {
    "from": ":bike:",
    "to": "\ud83d\udeb2"
  },
  {
    "from": ":scooter:",
    "to": "\ud83d\udef4"
  },
  {
    "from": ":kick_scooter:",
    "to": "\ud83d\udef4"
  },
  {
    "from": ":skateboard:",
    "to": "\ud83d\udef9"
  },
  {
    "from": ":roller_skate:",
    "to": "\ud83d\udefc"
  },
  {
    "from": ":busstop:",
    "to": "\ud83d\ude8f"
  },
  {
    "from": ":fuelpump:",
    "to": "\u26fd"
  },
  {
    "from": ":rotating_light:",
    "to": "\ud83d\udea8"
  },
  {
    "from": ":traffic_light:",
    "to": "\ud83d\udea5"
  },
  {
    "from": ":vertical_traffic_light:",
    "to": "\ud83d\udea6"
  },
  {
    "from": ":octagonal_sign:",
    "to": "\ud83d\uded1"
  },
  {
    "from": ":stop_sign:",
    "to": "\ud83d\uded1"
  },
  {
    "from": ":construction:",
    "to": "\ud83d\udea7"
  },
  {
    "from": ":anchor:",
    "to": "\u2693"
  },
  {
    "from": ":boat:",
    "to": "\u26f5"
  },
  {
    "from": ":sailboat:",
    "to": "\u26f5"
  },
  {
    "from": ":canoe:",
    "to": "\ud83d\udef6"
  },
  {
    "from": ":speedboat:",
    "to": "\ud83d\udea4"
  },
  {
    "from": ":ship:",
    "to": "\ud83d\udea2"
  },
  {
    "from": ":airplane_departure:",
    "to": "\ud83d\udeeb"
  },
  {
    "from": ":flight_departure:",
    "to": "\ud83d\udeeb"
  },
  {
    "from": ":airplane_arriving:",
    "to": "\ud83d\udeec"
  },
  {
    "from": ":flight_arrival:",
    "to": "\ud83d\udeec"
  },
  {
    "from": ":parachute:",
    "to": "\ud83e\ude82"
  },
  {
    "from": ":seat:",
    "to": "\ud83d\udcba"
  },
  {
    "from": ":helicopter:",
    "to": "\ud83d\ude81"
  },
  {
    "from": ":suspension_railway:",
    "to": "\ud83d\ude9f"
  },
  {
    "from": ":mountain_cableway:",
    "to": "\ud83d\udea0"
  },
  {
    "from": ":aerial_tramway:",
    "to": "\ud83d\udea1"
  },
  {
    "from": ":rocket:",
    "to": "\ud83d\ude80"
  },
  {
    "from": ":flying_saucer:",
    "to": "\ud83d\udef8"
  },
  {
    "from": ":luggage:",
    "to": "\ud83e\uddf3"
  },
  {
    "from": ":hourglass:",
    "to": "\u231b"
  },
  {
    "from": ":hourglass_flowing_sand:",
    "to": "\u23f3"
  },
  {
    "from": ":watch:",
    "to": "\u231a"
  },
  {
    "from": ":alarm_clock:",
    "to": "\u23f0"
  },
  {
    "from": ":clock12:",
    "to": "\ud83d\udd5b"
  },
  {
    "from": ":clock1230:",
    "to": "\ud83d\udd67"
  },
  {
    "from": ":clock1:",
    "to": "\ud83d\udd50"
  },
  {
    "from": ":clock130:",
    "to": "\ud83d\udd5c"
  },
  {
    "from": ":clock2:",
    "to": "\ud83d\udd51"
  },
  {
    "from": ":clock230:",
    "to": "\ud83d\udd5d"
  },
  {
    "from": ":clock3:",
    "to": "\ud83d\udd52"
  },
  {
    "from": ":clock330:",
    "to": "\ud83d\udd5e"
  },
  {
    "from": ":clock4:",
    "to": "\ud83d\udd53"
  },
  {
    "from": ":clock430:",
    "to": "\ud83d\udd5f"
  },
  {
    "from": ":clock5:",
    "to": "\ud83d\udd54"
  },
  {
    "from": ":clock530:",
    "to": "\ud83d\udd60"
  },
  {
    "from": ":clock6:",
    "to": "\ud83d\udd55"
  },
  {
    "from": ":clock630:",
    "to": "\ud83d\udd61"
  },
  {
    "from": ":clock7:",
    "to": "\ud83d\udd56"
  },
  {
    "from": ":clock730:",
    "to": "\ud83d\udd62"
  },
  {
    "from": ":clock8:",
    "to": "\ud83d\udd57"
  },
  {
    "from": ":clock830:",
    "to": "\ud83d\udd63"
  },
  {
    "from": ":clock9:",
    "to": "\ud83d\udd58"
  },
  {
    "from": ":clock930:",
    "to": "\ud83d\udd64"
  },
  {
    "from": ":clock10:",
    "to": "\ud83d\udd59"
  },
  {
    "from": ":clock1030:",
    "to": "\ud83d\udd65"
  },
  {
    "from": ":clock11:",
    "to": "\ud83d\udd5a"
  },
  {
    "from": ":clock1130:",
    "to": "\ud83d\udd66"
  },
  {
    "from": ":new_moon:",
    "to": "\ud83c\udf11"
  },
  {
    "from": ":waxing_crescent_moon:",
    "to": "\ud83c\udf12"
  },
  {
    "from": ":first_quarter_moon:",
    "to": "\ud83c\udf13"
  },
  {
    "from": ":moon:",
    "to": "\ud83c\udf14"
  },
  {
    "from": ":waxing_gibbous_moon:",
    "to": "\ud83c\udf14"
  },
  {
    "from": ":full_moon:",
    "to": "\ud83c\udf15"
  },
  {
    "from": ":waning_gibbous_moon:",
    "to": "\ud83c\udf16"
  },
  {
    "from": ":last_quarter_moon:",
    "to": "\ud83c\udf17"
  },
  {
    "from": ":waning_crescent_moon:",
    "to": "\ud83c\udf18"
  },
  {
    "from": ":crescent_moon:",
    "to": "\ud83c\udf19"
  },
  {
    "from": ":new_moon_with_face:",
    "to": "\ud83c\udf1a"
  },
  {
    "from": ":first_quarter_moon_with_face:",
    "to": "\ud83c\udf1b"
  },
  {
    "from": ":last_quarter_moon_with_face:",
    "to": "\ud83c\udf1c"
  },
  {
    "from": ":full_moon_with_face:",
    "to": "\ud83c\udf1d"
  },
  {
    "from": ":sun_with_face:",
    "to": "\ud83c\udf1e"
  },
  {
    "from": ":ringed_planet:",
    "to": "\ud83e\ude90"
  },
  {
    "from": ":star:",
    "to": "\u2b50"
  },
  {
    "from": ":star2:",
    "to": "\ud83c\udf1f"
  },
  {
    "from": ":stars:",
    "to": "\ud83c\udf20"
  },
  {
    "from": ":milky_way:",
    "to": "\ud83c\udf0c"
  },
  {
    "from": ":partly_sunny:",
    "to": "\u26c5"
  },
  {
    "from": ":cyclone:",
    "to": "\ud83c\udf00"
  },
  {
    "from": ":rainbow:",
    "to": "\ud83c\udf08"
  },
  {
    "from": ":closed_umbrella:",
    "to": "\ud83c\udf02"
  },
  {
    "from": ":umbrella_with_rain_drops:",
    "to": "\u2614"
  },
  {
    "from": ":zap:",
    "to": "\u26a1"
  },
  {
    "from": ":snowman_without_snow:",
    "to": "\u26c4"
  },
  {
    "from": ":fire:",
    "to": "\ud83d\udd25"
  },
  {
    "from": ":droplet:",
    "to": "\ud83d\udca7"
  },
  {
    "from": ":ocean:",
    "to": "\ud83c\udf0a"
  },
  {
    "from": ":jack_o_lantern:",
    "to": "\ud83c\udf83"
  },
  {
    "from": ":christmas_tree:",
    "to": "\ud83c\udf84"
  },
  {
    "from": ":fireworks:",
    "to": "\ud83c\udf86"
  },
  {
    "from": ":sparkler:",
    "to": "\ud83c\udf87"
  },
  {
    "from": ":firecracker:",
    "to": "\ud83e\udde8"
  },
  {
    "from": ":sparkles:",
    "to": "\u2728"
  },
  {
    "from": ":balloon:",
    "to": "\ud83c\udf88"
  },
  {
    "from": ":tada:",
    "to": "\ud83c\udf89"
  },
  {
    "from": ":confetti_ball:",
    "to": "\ud83c\udf8a"
  },
  {
    "from": ":tanabata_tree:",
    "to": "\ud83c\udf8b"
  },
  {
    "from": ":bamboo:",
    "to": "\ud83c\udf8d"
  },
  {
    "from": ":dolls:",
    "to": "\ud83c\udf8e"
  },
  {
    "from": ":flags:",
    "to": "\ud83c\udf8f"
  },
  {
    "from": ":wind_chime:",
    "to": "\ud83c\udf90"
  },
  {
    "from": ":rice_scene:",
    "to": "\ud83c\udf91"
  },
  {
    "from": ":red_envelope:",
    "to": "\ud83e\udde7"
  },
  {
    "from": ":ribbon:",
    "to": "\ud83c\udf80"
  },
  {
    "from": ":gift:",
    "to": "\ud83c\udf81"
  },
  {
    "from": ":ticket:",
    "to": "\ud83c\udfab"
  },
  {
    "from": ":trophy:",
    "to": "\ud83c\udfc6"
  },
  {
    "from": ":sports_medal:",
    "to": "\ud83c\udfc5"
  },
  {
    "from": ":medal_sports:",
    "to": "\ud83c\udfc5"
  },
  {
    "from": ":first_place_medal:",
    "to": "\ud83e\udd47"
  },
  {
    "from": ":1st_place_medal:",
    "to": "\ud83e\udd47"
  },
  {
    "from": ":second_place_medal:",
    "to": "\ud83e\udd48"
  },
  {
    "from": ":2nd_place_medal:",
    "to": "\ud83e\udd48"
  },
  {
    "from": ":third_place_medal:",
    "to": "\ud83e\udd49"
  },
  {
    "from": ":3rd_place_medal:",
    "to": "\ud83e\udd49"
  },
  {
    "from": ":soccer:",
    "to": "\u26bd"
  },
  {
    "from": ":baseball:",
    "to": "\u26be"
  },
  {
    "from": ":softball:",
    "to": "\ud83e\udd4e"
  },
  {
    "from": ":basketball:",
    "to": "\ud83c\udfc0"
  },
  {
    "from": ":volleyball:",
    "to": "\ud83c\udfd0"
  },
  {
    "from": ":football:",
    "to": "\ud83c\udfc8"
  },
  {
    "from": ":rugby_football:",
    "to": "\ud83c\udfc9"
  },
  {
    "from": ":tennis:",
    "to": "\ud83c\udfbe"
  },
  {
    "from": ":flying_disc:",
    "to": "\ud83e\udd4f"
  },
  {
    "from": ":bowling:",
    "to": "\ud83c\udfb3"
  },
  {
    "from": ":cricket_bat_and_ball:",
    "to": "\ud83c\udfcf"
  },
  {
    "from": ":field_hockey_stick_and_ball:",
    "to": "\ud83c\udfd1"
  },
  {
    "from": ":field_hockey:",
    "to": "\ud83c\udfd1"
  },
  {
    "from": ":ice_hockey_stick_and_puck:",
    "to": "\ud83c\udfd2"
  },
  {
    "from": ":ice_hockey:",
    "to": "\ud83c\udfd2"
  },
  {
    "from": ":lacrosse:",
    "to": "\ud83e\udd4d"
  },
  {
    "from": ":table_tennis_paddle_and_ball:",
    "to": "\ud83c\udfd3"
  },
  {
    "from": ":ping_pong:",
    "to": "\ud83c\udfd3"
  },
  {
    "from": ":badminton_racquet_and_shuttlecock:",
    "to": "\ud83c\udff8"
  },
  {
    "from": ":badminton:",
    "to": "\ud83c\udff8"
  },
  {
    "from": ":boxing_glove:",
    "to": "\ud83e\udd4a"
  },
  {
    "from": ":martial_arts_uniform:",
    "to": "\ud83e\udd4b"
  },
  {
    "from": ":goal_net:",
    "to": "\ud83e\udd45"
  },
  {
    "from": ":golf:",
    "to": "\u26f3"
  },
  {
    "from": ":fishing_pole_and_fish:",
    "to": "\ud83c\udfa3"
  },
  {
    "from": ":diving_mask:",
    "to": "\ud83e\udd3f"
  },
  {
    "from": ":running_shirt_with_sash:",
    "to": "\ud83c\udfbd"
  },
  {
    "from": ":ski:",
    "to": "\ud83c\udfbf"
  },
  {
    "from": ":sled:",
    "to": "\ud83d\udef7"
  },
  {
    "from": ":curling_stone:",
    "to": "\ud83e\udd4c"
  },
  {
    "from": ":dart:",
    "to": "\ud83c\udfaf"
  },
  {
    "from": ":yo-yo:",
    "to": "\ud83e\ude80"
  },
  {
    "from": ":kite:",
    "to": "\ud83e\ude81"
  },
  {
    "from": ":8ball:",
    "to": "\ud83c\udfb1"
  },
  {
    "from": ":crystal_ball:",
    "to": "\ud83d\udd2e"
  },
  {
    "from": ":magic_wand:",
    "to": "\ud83e\ude84"
  },
  {
    "from": ":nazar_amulet:",
    "to": "\ud83e\uddff"
  },
  {
    "from": ":video_game:",
    "to": "\ud83c\udfae"
  },
  {
    "from": ":slot_machine:",
    "to": "\ud83c\udfb0"
  },
  {
    "from": ":game_die:",
    "to": "\ud83c\udfb2"
  },
  {
    "from": ":jigsaw:",
    "to": "\ud83e\udde9"
  },
  {
    "from": ":teddy_bear:",
    "to": "\ud83e\uddf8"
  },
  {
    "from": ":pinata:",
    "to": "\ud83e\ude85"
  },
  {
    "from": ":nesting_dolls:",
    "to": "\ud83e\ude86"
  },
  {
    "from": ":black_joker:",
    "to": "\ud83c\udccf"
  },
  {
    "from": ":mahjong:",
    "to": "\ud83c\udc04"
  },
  {
    "from": ":flower_playing_cards:",
    "to": "\ud83c\udfb4"
  },
  {
    "from": ":performing_arts:",
    "to": "\ud83c\udfad"
  },
  {
    "from": ":art:",
    "to": "\ud83c\udfa8"
  },
  {
    "from": ":thread:",
    "to": "\ud83e\uddf5"
  },
  {
    "from": ":sewing_needle:",
    "to": "\ud83e\udea1"
  },
  {
    "from": ":yarn:",
    "to": "\ud83e\uddf6"
  },
  {
    "from": ":knot:",
    "to": "\ud83e\udea2"
  },
  {
    "from": ":eyeglasses:",
    "to": "\ud83d\udc53"
  },
  {
    "from": ":goggles:",
    "to": "\ud83e\udd7d"
  },
  {
    "from": ":lab_coat:",
    "to": "\ud83e\udd7c"
  },
  {
    "from": ":safety_vest:",
    "to": "\ud83e\uddba"
  },
  {
    "from": ":necktie:",
    "to": "\ud83d\udc54"
  },
  {
    "from": ":shirt:",
    "to": "\ud83d\udc55"
  },
  {
    "from": ":tshirt:",
    "to": "\ud83d\udc55"
  },
  {
    "from": ":jeans:",
    "to": "\ud83d\udc56"
  },
  {
    "from": ":scarf:",
    "to": "\ud83e\udde3"
  },
  {
    "from": ":gloves:",
    "to": "\ud83e\udde4"
  },
  {
    "from": ":coat:",
    "to": "\ud83e\udde5"
  },
  {
    "from": ":socks:",
    "to": "\ud83e\udde6"
  },
  {
    "from": ":dress:",
    "to": "\ud83d\udc57"
  },
  {
    "from": ":kimono:",
    "to": "\ud83d\udc58"
  },
  {
    "from": ":sari:",
    "to": "\ud83e\udd7b"
  },
  {
    "from": ":one-piece_swimsuit:",
    "to": "\ud83e\ude71"
  },
  {
    "from": ":briefs:",
    "to": "\ud83e\ude72"
  },
  {
    "from": ":shorts:",
    "to": "\ud83e\ude73"
  },
  {
    "from": ":bikini:",
    "to": "\ud83d\udc59"
  },
  {
    "from": ":womans_clothes:",
    "to": "\ud83d\udc5a"
  },
  {
    "from": ":purse:",
    "to": "\ud83d\udc5b"
  },
  {
    "from": ":handbag:",
    "to": "\ud83d\udc5c"
  },
  {
    "from": ":pouch:",
    "to": "\ud83d\udc5d"
  },
  {
    "from": ":school_satchel:",
    "to": "\ud83c\udf92"
  },
  {
    "from": ":thong_sandal:",
    "to": "\ud83e\ude74"
  },
  {
    "from": ":mans_shoe:",
    "to": "\ud83d\udc5e"
  },
  {
    "from": ":shoe:",
    "to": "\ud83d\udc5e"
  },
  {
    "from": ":athletic_shoe:",
    "to": "\ud83d\udc5f"
  },
  {
    "from": ":hiking_boot:",
    "to": "\ud83e\udd7e"
  },
  {
    "from": ":womans_flat_shoe:",
    "to": "\ud83e\udd7f"
  },
  {
    "from": ":high_heel:",
    "to": "\ud83d\udc60"
  },
  {
    "from": ":sandal:",
    "to": "\ud83d\udc61"
  },
  {
    "from": ":ballet_shoes:",
    "to": "\ud83e\ude70"
  },
  {
    "from": ":boot:",
    "to": "\ud83d\udc62"
  },
  {
    "from": ":crown:",
    "to": "\ud83d\udc51"
  },
  {
    "from": ":womans_hat:",
    "to": "\ud83d\udc52"
  },
  {
    "from": ":tophat:",
    "to": "\ud83c\udfa9"
  },
  {
    "from": ":mortar_board:",
    "to": "\ud83c\udf93"
  },
  {
    "from": ":billed_cap:",
    "to": "\ud83e\udde2"
  },
  {
    "from": ":military_helmet:",
    "to": "\ud83e\ude96"
  },
  {
    "from": ":prayer_beads:",
    "to": "\ud83d\udcff"
  },
  {
    "from": ":lipstick:",
    "to": "\ud83d\udc84"
  },
  {
    "from": ":ring:",
    "to": "\ud83d\udc8d"
  },
  {
    "from": ":gem:",
    "to": "\ud83d\udc8e"
  },
  {
    "from": ":mute:",
    "to": "\ud83d\udd07"
  },
  {
    "from": ":speaker:",
    "to": "\ud83d\udd08"
  },
  {
    "from": ":sound:",
    "to": "\ud83d\udd09"
  },
  {
    "from": ":loud_sound:",
    "to": "\ud83d\udd0a"
  },
  {
    "from": ":loudspeaker:",
    "to": "\ud83d\udce2"
  },
  {
    "from": ":mega:",
    "to": "\ud83d\udce3"
  },
  {
    "from": ":postal_horn:",
    "to": "\ud83d\udcef"
  },
  {
    "from": ":bell:",
    "to": "\ud83d\udd14"
  },
  {
    "from": ":no_bell:",
    "to": "\ud83d\udd15"
  },
  {
    "from": ":musical_score:",
    "to": "\ud83c\udfbc"
  },
  {
    "from": ":musical_note:",
    "to": "\ud83c\udfb5"
  },
  {
    "from": ":notes:",
    "to": "\ud83c\udfb6"
  },
  {
    "from": ":microphone:",
    "to": "\ud83c\udfa4"
  },
  {
    "from": ":headphones:",
    "to": "\ud83c\udfa7"
  },
  {
    "from": ":radio:",
    "to": "\ud83d\udcfb"
  },
  {
    "from": ":saxophone:",
    "to": "\ud83c\udfb7"
  },
  {
    "from": ":accordion:",
    "to": "\ud83e\ude97"
  },
  {
    "from": ":guitar:",
    "to": "\ud83c\udfb8"
  },
  {
    "from": ":musical_keyboard:",
    "to": "\ud83c\udfb9"
  },
  {
    "from": ":trumpet:",
    "to": "\ud83c\udfba"
  },
  {
    "from": ":violin:",
    "to": "\ud83c\udfbb"
  },
  {
    "from": ":banjo:",
    "to": "\ud83e\ude95"
  },
  {
    "from": ":drum_with_drumsticks:",
    "to": "\ud83e\udd41"
  },
  {
    "from": ":drum:",
    "to": "\ud83e\udd41"
  },
  {
    "from": ":long_drum:",
    "to": "\ud83e\ude98"
  },
  {
    "from": ":iphone:",
    "to": "\ud83d\udcf1"
  },
  {
    "from": ":calling:",
    "to": "\ud83d\udcf2"
  },
  {
    "from": ":telephone_receiver:",
    "to": "\ud83d\udcde"
  },
  {
    "from": ":pager:",
    "to": "\ud83d\udcdf"
  },
  {
    "from": ":fax:",
    "to": "\ud83d\udce0"
  },
  {
    "from": ":battery:",
    "to": "\ud83d\udd0b"
  },
  {
    "from": ":electric_plug:",
    "to": "\ud83d\udd0c"
  },
  {
    "from": ":computer:",
    "to": "\ud83d\udcbb"
  },
  {
    "from": ":minidisc:",
    "to": "\ud83d\udcbd"
  },
  {
    "from": ":floppy_disk:",
    "to": "\ud83d\udcbe"
  },
  {
    "from": ":cd:",
    "to": "\ud83d\udcbf"
  },
  {
    "from": ":dvd:",
    "to": "\ud83d\udcc0"
  },
  {
    "from": ":abacus:",
    "to": "\ud83e\uddee"
  },
  {
    "from": ":movie_camera:",
    "to": "\ud83c\udfa5"
  },
  {
    "from": ":clapper:",
    "to": "\ud83c\udfac"
  },
  {
    "from": ":tv:",
    "to": "\ud83d\udcfa"
  },
  {
    "from": ":camera:",
    "to": "\ud83d\udcf7"
  },
  {
    "from": ":camera_with_flash:",
    "to": "\ud83d\udcf8"
  },
  {
    "from": ":camera_flash:",
    "to": "\ud83d\udcf8"
  },
  {
    "from": ":video_camera:",
    "to": "\ud83d\udcf9"
  },
  {
    "from": ":vhs:",
    "to": "\ud83d\udcfc"
  },
  {
    "from": ":mag:",
    "to": "\ud83d\udd0d"
  },
  {
    "from": ":mag_right:",
    "to": "\ud83d\udd0e"
  },
  {
    "from": ":bulb:",
    "to": "\ud83d\udca1"
  },
  {
    "from": ":flashlight:",
    "to": "\ud83d\udd26"
  },
  {
    "from": ":izakaya_lantern:",
    "to": "\ud83c\udfee"
  },
  {
    "from": ":lantern:",
    "to": "\ud83c\udfee"
  },
  {
    "from": ":diya_lamp:",
    "to": "\ud83e\ude94"
  },
  {
    "from": ":notebook_with_decorative_cover:",
    "to": "\ud83d\udcd4"
  },
  {
    "from": ":closed_book:",
    "to": "\ud83d\udcd5"
  },
  {
    "from": ":book:",
    "to": "\ud83d\udcd6"
  },
  {
    "from": ":open_book:",
    "to": "\ud83d\udcd6"
  },
  {
    "from": ":green_book:",
    "to": "\ud83d\udcd7"
  },
  {
    "from": ":blue_book:",
    "to": "\ud83d\udcd8"
  },
  {
    "from": ":orange_book:",
    "to": "\ud83d\udcd9"
  },
  {
    "from": ":books:",
    "to": "\ud83d\udcda"
  },
  {
    "from": ":notebook:",
    "to": "\ud83d\udcd3"
  },
  {
    "from": ":ledger:",
    "to": "\ud83d\udcd2"
  },
  {
    "from": ":page_with_curl:",
    "to": "\ud83d\udcc3"
  },
  {
    "from": ":scroll:",
    "to": "\ud83d\udcdc"
  },
  {
    "from": ":page_facing_up:",
    "to": "\ud83d\udcc4"
  },
  {
    "from": ":newspaper:",
    "to": "\ud83d\udcf0"
  },
  {
    "from": ":bookmark_tabs:",
    "to": "\ud83d\udcd1"
  },
  {
    "from": ":bookmark:",
    "to": "\ud83d\udd16"
  },
  {
    "from": ":moneybag:",
    "to": "\ud83d\udcb0"
  },
  {
    "from": ":coin:",
    "to": "\ud83e\ude99"
  },
  {
    "from": ":yen:",
    "to": "\ud83d\udcb4"
  },
  {
    "from": ":dollar:",
    "to": "\ud83d\udcb5"
  },
  {
    "from": ":euro:",
    "to": "\ud83d\udcb6"
  },
  {
    "from": ":pound:",
    "to": "\ud83d\udcb7"
  },
  {
    "from": ":money_with_wings:",
    "to": "\ud83d\udcb8"
  },
  {
    "from": ":credit_card:",
    "to": "\ud83d\udcb3"
  },
  {
    "from": ":receipt:",
    "to": "\ud83e\uddfe"
  },
  {
    "from": ":chart:",
    "to": "\ud83d\udcb9"
  },
  {
    "from": ":e-mail:",
    "to": "\ud83d\udce7"
  },
  {
    "from": ":incoming_envelope:",
    "to": "\ud83d\udce8"
  },
  {
    "from": ":envelope_with_arrow:",
    "to": "\ud83d\udce9"
  },
  {
    "from": ":outbox_tray:",
    "to": "\ud83d\udce4"
  },
  {
    "from": ":inbox_tray:",
    "to": "\ud83d\udce5"
  },
  {
    "from": ":package:",
    "to": "\ud83d\udce6"
  },
  {
    "from": ":mailbox:",
    "to": "\ud83d\udceb"
  },
  {
    "from": ":mailbox_closed:",
    "to": "\ud83d\udcea"
  },
  {
    "from": ":mailbox_with_mail:",
    "to": "\ud83d\udcec"
  },
  {
    "from": ":mailbox_with_no_mail:",
    "to": "\ud83d\udced"
  },
  {
    "from": ":postbox:",
    "to": "\ud83d\udcee"
  },
  {
    "from": ":memo:",
    "to": "\ud83d\udcdd"
  },
  {
    "from": ":pencil:",
    "to": "\ud83d\udcdd"
  },
  {
    "from": ":briefcase:",
    "to": "\ud83d\udcbc"
  },
  {
    "from": ":file_folder:",
    "to": "\ud83d\udcc1"
  },
  {
    "from": ":open_file_folder:",
    "to": "\ud83d\udcc2"
  },
  {
    "from": ":date:",
    "to": "\ud83d\udcc5"
  },
  {
    "from": ":calendar:",
    "to": "\ud83d\udcc6"
  },
  {
    "from": ":card_index:",
    "to": "\ud83d\udcc7"
  },
  {
    "from": ":chart_with_upwards_trend:",
    "to": "\ud83d\udcc8"
  },
  {
    "from": ":chart_with_downwards_trend:",
    "to": "\ud83d\udcc9"
  },
  {
    "from": ":bar_chart:",
    "to": "\ud83d\udcca"
  },
  {
    "from": ":clipboard:",
    "to": "\ud83d\udccb"
  },
  {
    "from": ":pushpin:",
    "to": "\ud83d\udccc"
  },
  {
    "from": ":round_pushpin:",
    "to": "\ud83d\udccd"
  },
  {
    "from": ":paperclip:",
    "to": "\ud83d\udcce"
  },
  {
    "from": ":straight_ruler:",
    "to": "\ud83d\udccf"
  },
  {
    "from": ":triangular_ruler:",
    "to": "\ud83d\udcd0"
  },
  {
    "from": ":lock:",
    "to": "\ud83d\udd12"
  },
  {
    "from": ":unlock:",
    "to": "\ud83d\udd13"
  },
  {
    "from": ":lock_with_ink_pen:",
    "to": "\ud83d\udd0f"
  },
  {
    "from": ":closed_lock_with_key:",
    "to": "\ud83d\udd10"
  },
  {
    "from": ":key:",
    "to": "\ud83d\udd11"
  },
  {
    "from": ":hammer:",
    "to": "\ud83d\udd28"
  },
  {
    "from": ":axe:",
    "to": "\ud83e\ude93"
  },
  {
    "from": ":gun:",
    "to": "\ud83d\udd2b"
  },
  {
    "from": ":boomerang:",
    "to": "\ud83e\ude83"
  },
  {
    "from": ":bow_and_arrow:",
    "to": "\ud83c\udff9"
  },
  {
    "from": ":carpentry_saw:",
    "to": "\ud83e\ude9a"
  },
  {
    "from": ":wrench:",
    "to": "\ud83d\udd27"
  },
  {
    "from": ":screwdriver:",
    "to": "\ud83e\ude9b"
  },
  {
    "from": ":nut_and_bolt:",
    "to": "\ud83d\udd29"
  },
  {
    "from": ":probing_cane:",
    "to": "\ud83e\uddaf"
  },
  {
    "from": ":link:",
    "to": "\ud83d\udd17"
  },
  {
    "from": ":hook:",
    "to": "\ud83e\ude9d"
  },
  {
    "from": ":toolbox:",
    "to": "\ud83e\uddf0"
  },
  {
    "from": ":magnet:",
    "to": "\ud83e\uddf2"
  },
  {
    "from": ":ladder:",
    "to": "\ud83e\ude9c"
  },
  {
    "from": ":test_tube:",
    "to": "\ud83e\uddea"
  },
  {
    "from": ":petri_dish:",
    "to": "\ud83e\uddeb"
  },
  {
    "from": ":dna:",
    "to": "\ud83e\uddec"
  },
  {
    "from": ":microscope:",
    "to": "\ud83d\udd2c"
  },
  {
    "from": ":telescope:",
    "to": "\ud83d\udd2d"
  },
  {
    "from": ":satellite_antenna:",
    "to": "\ud83d\udce1"
  },
  {
    "from": ":syringe:",
    "to": "\ud83d\udc89"
  },
  {
    "from": ":drop_of_blood:",
    "to": "\ud83e\ude78"
  },
  {
    "from": ":pill:",
    "to": "\ud83d\udc8a"
  },
  {
    "from": ":adhesive_bandage:",
    "to": "\ud83e\ude79"
  },
  {
    "from": ":stethoscope:",
    "to": "\ud83e\ude7a"
  },
  {
    "from": ":door:",
    "to": "\ud83d\udeaa"
  },
  {
    "from": ":elevator:",
    "to": "\ud83d\uded7"
  },
  {
    "from": ":mirror:",
    "to": "\ud83e\ude9e"
  },
  {
    "from": ":window:",
    "to": "\ud83e\ude9f"
  },
  {
    "from": ":chair:",
    "to": "\ud83e\ude91"
  },
  {
    "from": ":toilet:",
    "to": "\ud83d\udebd"
  },
  {
    "from": ":plunger:",
    "to": "\ud83e\udea0"
  },
  {
    "from": ":shower:",
    "to": "\ud83d\udebf"
  },
  {
    "from": ":bathtub:",
    "to": "\ud83d\udec1"
  },
  {
    "from": ":mouse_trap:",
    "to": "\ud83e\udea4"
  },
  {
    "from": ":razor:",
    "to": "\ud83e\ude92"
  },
  {
    "from": ":lotion_bottle:",
    "to": "\ud83e\uddf4"
  },
  {
    "from": ":safety_pin:",
    "to": "\ud83e\uddf7"
  },
  {
    "from": ":broom:",
    "to": "\ud83e\uddf9"
  },
  {
    "from": ":basket:",
    "to": "\ud83e\uddfa"
  },
  {
    "from": ":roll_of_paper:",
    "to": "\ud83e\uddfb"
  },
  {
    "from": ":bucket:",
    "to": "\ud83e\udea3"
  },
  {
    "from": ":soap:",
    "to": "\ud83e\uddfc"
  },
  {
    "from": ":toothbrush:",
    "to": "\ud83e\udea5"
  },
  {
    "from": ":sponge:",
    "to": "\ud83e\uddfd"
  },
  {
    "from": ":fire_extinguisher:",
    "to": "\ud83e\uddef"
  },
  {
    "from": ":shopping_trolley:",
    "to": "\ud83d\uded2"
  },
  {
    "from": ":shopping_cart:",
    "to": "\ud83d\uded2"
  },
  {
    "from": ":smoking:",
    "to": "\ud83d\udeac"
  },
  {
    "from": ":headstone:",
    "to": "\ud83e\udea6"
  },
  {
    "from": ":moyai:",
    "to": "\ud83d\uddff"
  },
  {
    "from": ":placard:",
    "to": "\ud83e\udea7"
  },
  {
    "from": ":atm:",
    "to": "\ud83c\udfe7"
  },
  {
    "from": ":put_litter_in_its_place:",
    "to": "\ud83d\udeae"
  },
  {
    "from": ":potable_water:",
    "to": "\ud83d\udeb0"
  },
  {
    "from": ":wheelchair:",
    "to": "\u267f"
  },
  {
    "from": ":mens:",
    "to": "\ud83d\udeb9"
  },
  {
    "from": ":womens:",
    "to": "\ud83d\udeba"
  },
  {
    "from": ":restroom:",
    "to": "\ud83d\udebb"
  },
  {
    "from": ":baby_symbol:",
    "to": "\ud83d\udebc"
  },
  {
    "from": ":wc:",
    "to": "\ud83d\udebe"
  },
  {
    "from": ":passport_control:",
    "to": "\ud83d\udec2"
  },
  {
    "from": ":customs:",
    "to": "\ud83d\udec3"
  },
  {
    "from": ":baggage_claim:",
    "to": "\ud83d\udec4"
  },
  {
    "from": ":left_luggage:",
    "to": "\ud83d\udec5"
  },
  {
    "from": ":children_crossing:",
    "to": "\ud83d\udeb8"
  },
  {
    "from": ":no_entry:",
    "to": "\u26d4"
  },
  {
    "from": ":no_entry_sign:",
    "to": "\ud83d\udeab"
  },
  {
    "from": ":no_bicycles:",
    "to": "\ud83d\udeb3"
  },
  {
    "from": ":no_smoking:",
    "to": "\ud83d\udead"
  },
  {
    "from": ":do_not_litter:",
    "to": "\ud83d\udeaf"
  },
  {
    "from": ":non-potable_water:",
    "to": "\ud83d\udeb1"
  },
  {
    "from": ":no_pedestrians:",
    "to": "\ud83d\udeb7"
  },
  {
    "from": ":no_mobile_phones:",
    "to": "\ud83d\udcf5"
  },
  {
    "from": ":underage:",
    "to": "\ud83d\udd1e"
  },
  {
    "from": ":arrows_clockwise:",
    "to": "\ud83d\udd03"
  },
  {
    "from": ":arrows_counterclockwise:",
    "to": "\ud83d\udd04"
  },
  {
    "from": ":back:",
    "to": "\ud83d\udd19"
  },
  {
    "from": ":end:",
    "to": "\ud83d\udd1a"
  },
  {
    "from": ":on:",
    "to": "\ud83d\udd1b"
  },
  {
    "from": ":soon:",
    "to": "\ud83d\udd1c"
  },
  {
    "from": ":top:",
    "to": "\ud83d\udd1d"
  },
  {
    "from": ":place_of_worship:",
    "to": "\ud83d\uded0"
  },
  {
    "from": ":menorah_with_nine_branches:",
    "to": "\ud83d\udd4e"
  },
  {
    "from": ":menorah:",
    "to": "\ud83d\udd4e"
  },
  {
    "from": ":six_pointed_star:",
    "to": "\ud83d\udd2f"
  },
  {
    "from": ":aries:",
    "to": "\u2648"
  },
  {
    "from": ":taurus:",
    "to": "\u2649"
  },
  {
    "from": ":gemini:",
    "to": "\u264a"
  },
  {
    "from": ":cancer:",
    "to": "\u264b"
  },
  {
    "from": ":leo:",
    "to": "\u264c"
  },
  {
    "from": ":virgo:",
    "to": "\u264d"
  },
  {
    "from": ":libra:",
    "to": "\u264e"
  },
  {
    "from": ":scorpius:",
    "to": "\u264f"
  },
  {
    "from": ":sagittarius:",
    "to": "\u2650"
  },
  {
    "from": ":capricorn:",
    "to": "\u2651"
  },
  {
    "from": ":aquarius:",
    "to": "\u2652"
  },
  {
    "from": ":pisces:",
    "to": "\u2653"
  },
  {
    "from": ":ophiuchus:",
    "to": "\u26ce"
  },
  {
    "from": ":twisted_rightwards_arrows:",
    "to": "\ud83d\udd00"
  },
  {
    "from": ":repeat:",
    "to": "\ud83d\udd01"
  },
  {
    "from": ":repeat_one:",
    "to": "\ud83d\udd02"
  },
  {
    "from": ":fast_forward:",
    "to": "\u23e9"
  },
  {
    "from": ":rewind:",
    "to": "\u23ea"
  },
  {
    "from": ":arrow_up_small:",
    "to": "\ud83d\udd3c"
  },
  {
    "from": ":arrow_double_up:",
    "to": "\u23eb"
  },
  {
    "from": ":arrow_down_small:",
    "to": "\ud83d\udd3d"
  },
  {
    "from": ":arrow_double_down:",
    "to": "\u23ec"
  },
  {
    "from": ":cinema:",
    "to": "\ud83c\udfa6"
  },
  {
    "from": ":low_brightness:",
    "to": "\ud83d\udd05"
  },
  {
    "from": ":high_brightness:",
    "to": "\ud83d\udd06"
  },
  {
    "from": ":signal_strength:",
    "to": "\ud83d\udcf6"
  },
  {
    "from": ":vibration_mode:",
    "to": "\ud83d\udcf3"
  },
  {
    "from": ":mobile_phone_off:",
    "to": "\ud83d\udcf4"
  },
  {
    "from": ":heavy_plus_sign:",
    "to": "\u2795"
  },
  {
    "from": ":heavy_minus_sign:",
    "to": "\u2796"
  },
  {
    "from": ":heavy_division_sign:",
    "to": "\u2797"
  },
  {
    "from": ":question:",
    "to": "\u2753"
  },
  {
    "from": ":grey_question:",
    "to": "\u2754"
  },
  {
    "from": ":grey_exclamation:",
    "to": "\u2755"
  },
  {
    "from": ":exclamation:",
    "to": "\u2757"
  },
  {
    "from": ":heavy_exclamation_mark:",
    "to": "\u2757"
  },
  {
    "from": ":currency_exchange:",
    "to": "\ud83d\udcb1"
  },
  {
    "from": ":heavy_dollar_sign:",
    "to": "\ud83d\udcb2"
  },
  {
    "from": ":trident:",
    "to": "\ud83d\udd31"
  },
  {
    "from": ":name_badge:",
    "to": "\ud83d\udcdb"
  },
  {
    "from": ":beginner:",
    "to": "\ud83d\udd30"
  },
  {
    "from": ":o:",
    "to": "\u2b55"
  },
  {
    "from": ":white_check_mark:",
    "to": "\u2705"
  },
  {
    "from": ":x:",
    "to": "\u274c"
  },
  {
    "from": ":negative_squared_cross_mark:",
    "to": "\u274e"
  },
  {
    "from": ":curly_loop:",
    "to": "\u27b0"
  },
  {
    "from": ":loop:",
    "to": "\u27bf"
  },
  {
    "from": ":keycap_ten:",
    "to": "\ud83d\udd1f"
  },
  {
    "from": ":capital_abcd:",
    "to": "\ud83d\udd20"
  },
  {
    "from": ":abcd:",
    "to": "\ud83d\udd21"
  },
  {
    "from": ":1234:",
    "to": "\ud83d\udd22"
  },
  {
    "from": ":symbols:",
    "to": "\ud83d\udd23"
  },
  {
    "from": ":abc:",
    "to": "\ud83d\udd24"
  },
  {
    "from": ":ab:",
    "to": "\ud83c\udd8e"
  },
  {
    "from": ":cl:",
    "to": "\ud83c\udd91"
  },
  {
    "from": ":cool:",
    "to": "\ud83c\udd92"
  },
  {
    "from": ":free:",
    "to": "\ud83c\udd93"
  },
  {
    "from": ":id:",
    "to": "\ud83c\udd94"
  },
  {
    "from": ":new:",
    "to": "\ud83c\udd95"
  },
  {
    "from": ":ng:",
    "to": "\ud83c\udd96"
  },
  {
    "from": ":ok:",
    "to": "\ud83c\udd97"
  },
  {
    "from": ":sos:",
    "to": "\ud83c\udd98"
  },
  {
    "from": ":up:",
    "to": "\ud83c\udd99"
  },
  {
    "from": ":vs:",
    "to": "\ud83c\udd9a"
  },
  {
    "from": ":koko:",
    "to": "\ud83c\ude01"
  },
  {
    "from": ":u6709:",
    "to": "\ud83c\ude36"
  },
  {
    "from": ":u6307:",
    "to": "\ud83c\ude2f"
  },
  {
    "from": ":ideograph_advantage:",
    "to": "\ud83c\ude50"
  },
  {
    "from": ":u5272:",
    "to": "\ud83c\ude39"
  },
  {
    "from": ":u7121:",
    "to": "\ud83c\ude1a"
  },
  {
    "from": ":u7981:",
    "to": "\ud83c\ude32"
  },
  {
    "from": ":accept:",
    "to": "\ud83c\ude51"
  },
  {
    "from": ":u7533:",
    "to": "\ud83c\ude38"
  },
  {
    "from": ":u5408:",
    "to": "\ud83c\ude34"
  },
  {
    "from": ":u7a7a:",
    "to": "\ud83c\ude33"
  },
  {
    "from": ":u55b6:",
    "to": "\ud83c\ude3a"
  },
  {
    "from": ":u6e80:",
    "to": "\ud83c\ude35"
  },
  {
    "from": ":red_circle:",
    "to": "\ud83d\udd34"
  },
  {
    "from": ":large_orange_circle:",
    "to": "\ud83d\udfe0"
  },
  {
    "from": ":large_yellow_circle:",
    "to": "\ud83d\udfe1"
  },
  {
    "from": ":large_green_circle:",
    "to": "\ud83d\udfe2"
  },
  {
    "from": ":large_blue_circle:",
    "to": "\ud83d\udd35"
  },
  {
    "from": ":large_purple_circle:",
    "to": "\ud83d\udfe3"
  },
  {
    "from": ":large_brown_circle:",
    "to": "\ud83d\udfe4"
  },
  {
    "from": ":black_circle:",
    "to": "\u26ab"
  },
  {
    "from": ":white_circle:",
    "to": "\u26aa"
  },
  {
    "from": ":large_red_square:",
    "to": "\ud83d\udfe5"
  },
  {
    "from": ":large_orange_square:",
    "to": "\ud83d\udfe7"
  },
  {
    "from": ":large_yellow_square:",
    "to": "\ud83d\udfe8"
  },
  {
    "from": ":large_green_square:",
    "to": "\ud83d\udfe9"
  },
  {
    "from": ":large_blue_square:",
    "to": "\ud83d\udfe6"
  },
  {
    "from": ":large_purple_square:",
    "to": "\ud83d\udfea"
  },
  {
    "from": ":large_brown_square:",
    "to": "\ud83d\udfeb"
  },
  {
    "from": ":black_large_square:",
    "to": "\u2b1b"
  },
  {
    "from": ":white_large_square:",
    "to": "\u2b1c"
  },
  {
    "from": ":black_medium_small_square:",
    "to": "\u25fe"
  },
  {
    "from": ":white_medium_small_square:",
    "to": "\u25fd"
  },
  {
    "from": ":large_orange_diamond:",
    "to": "\ud83d\udd36"
  },
  {
    "from": ":large_blue_diamond:",
    "to": "\ud83d\udd37"
  },
  {
    "from": ":small_orange_diamond:",
    "to": "\ud83d\udd38"
  },
  {
    "from": ":small_blue_diamond:",
    "to": "\ud83d\udd39"
  },
  {
    "from": ":small_red_triangle:",
    "to": "\ud83d\udd3a"
  },
  {
    "from": ":small_red_triangle_down:",
    "to": "\ud83d\udd3b"
  },
  {
    "from": ":diamond_shape_with_a_dot_inside:",
    "to": "\ud83d\udca0"
  },
  {
    "from": ":radio_button:",
    "to": "\ud83d\udd18"
  },
  {
    "from": ":white_square_button:",
    "to": "\ud83d\udd33"
  },
  {
    "from": ":black_square_button:",
    "to": "\ud83d\udd32"
  },
  {
    "from": ":checkered_flag:",
    "to": "\ud83c\udfc1"
  },
  {
    "from": ":triangular_flag_on_post:",
    "to": "\ud83d\udea9"
  },
  {
    "from": ":crossed_flags:",
    "to": "\ud83c\udf8c"
  },
  {
    "from": ":waving_black_flag:",
    "to": "\ud83c\udff4"
  },
  {
    "from": ":black_flag:",
    "to": "\ud83c\udff4"
  }
]

export default emojies;
