# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from cms.app_base import CMSAppConfig
from djangocms_versioning.datastructures import VersionableItem

from academic_community import models, views


class AcademicCommunityAppConfig(CMSAppConfig):
    cms_enabled = True
    cms_toolbar_enabled_models = [
        (models.ObjectPageContent, views.render_object_page_content)
    ]

    djangocms_versioning_enabled = True

    versioning = [
        VersionableItem(
            content_model=models.ObjectPageContent,
            grouper_field_name="page",
            copy_function=models.copy_object_page_content,
        ),
    ]
