# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
# SPDX-License-Identifier: EUPL-1.2

"""
Websocket routing configuration for the django-academic-community app.

It exposes the `workers` dictionary, a mapping from worker name
consumer to be used by channels `ChannelNameRouter`.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/worker.html#receiving-and-consumers
"""
from typing import Any, Dict

from academic_community.notifications.consumers import NotificationWorker

workers: Dict[str, Any] = {
    "notification-worker": NotificationWorker.as_asgi(),
}
