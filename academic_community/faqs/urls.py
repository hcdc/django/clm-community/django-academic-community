"""Urls of the :mod:`faqs` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import path

from academic_community.faqs import views

app_name = "faqs"

urlpatterns = [
    path(
        "",
        views.QuestionCategoryListView.as_view(),
        name="questioncategory-list",
    ),
    path(
        "<int:pk>/",
        views.FAQDetailView.as_view(),
        name="faq-detail",
    ),
    path(
        "<int:object_id>/edit/",
        views.FAQUpdateView.as_view(),
        name="edit-faq",
    ),
    path(
        "<slug>/",
        views.QuestionCategoryDetailView.as_view(),
        name="questioncategory-detail",
    ),
    path(
        "<slug>/edit/",
        views.QuestionCategoryUpdateView.as_view(),
        name="edit-questioncategory",
    ),
]
