"""Urls of the :mod:`contact` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import path

from academic_community.contact import views

app_name = "contact"

urlpatterns = [
    path(
        "<int:pk>/",
        views.ContactFormView.as_view(),
        name="contact",
    ),
]
