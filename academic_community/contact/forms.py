"""Forms for the community contact app"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from captcha.fields import CaptchaField
from django import forms

from academic_community.notifications.models import OutgoingNotification
from academic_community.utils import PermissionCheckFormMixin


class ContactForm(PermissionCheckFormMixin, forms.ModelForm):
    """A form to create outgoing notifications that are sent to the managers."""

    class Meta:
        model = OutgoingNotification

        exclude = ["recipients", "encryption_key", "sender"]

    reply_to = forms.EmailField(
        required=True,
        help_text="Your email such that the recipient can get back to you.",
        label="Your email",
        max_length=200,
    )

    captcha = CaptchaField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["plain_text_body"].label = "Body"

    def update_from_anonymous(self):
        # only allow plain text body for anonymous users
        self.remove_field("body")
        self.fields["plain_text_body"].required = True

    def update_from_registered_user(self, user):
        # autogenerate the plain text body from the HTML
        self.remove_field("plain_text_body")
        self.remove_field("captcha")
        self.disable_field("reply_to")
        self.fields["body"].required = True
