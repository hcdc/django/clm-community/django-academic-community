# SPDX-FileCopyrightText: 2020-2021 Helmholtz-Zentrum Geesthacht
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
from __future__ import annotations

from typing import TYPE_CHECKING

from cms.models import CMSPlugin
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from academic_community import utils

if TYPE_CHECKING:
    from django.contrib.auth.models import User


User = get_user_model()  # type: ignore # noqa: F811


class ContactFormBasePluginModel(CMSPlugin):
    """A plugin for a contact form."""

    class Meta:
        abstract = True
        permissions = (
            (
                "use_contact_form",
                "Can use this contact form",
            ),
        )

    user_use_permission = models.ManyToManyField(  # type: ignore[var-annotated]
        settings.AUTH_USER_MODEL,
        help_text=(
            "Explicit users that are allowed to create notifications via "
            "the contact form."
        ),
        blank=True,
    )

    group_use_permission = models.ManyToManyField(  # type: ignore[var-annotated]
        Group,
        help_text=(
            "Explicit groups that are allowed to create notifications via "
            "the contact form."
        ),
        blank=True,
    )

    recipients = models.ManyToManyField(  # type: ignore[var-annotated]
        settings.AUTH_USER_MODEL,
        help_text=(
            "Explicit recipients that should be reached through this form."
        ),
        blank=True,
        related_name="contact_form_recipient",
    )

    recipient_groups = models.ManyToManyField(  # type: ignore[var-annotated]
        Group,
        help_text=(
            "Groups whose members should be contacted through this form."
        ),
        blank=True,
        related_name="contact_form_recipient",
    )

    show_banner = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=(
            "Show a banner with some information text when the user does not "
            "have the permissions to use the contact form. If this is not "
            "enabled, the form will just be ommited."
        ),
    )

    def copy_relations(self, oldinstance):
        self.user_use_permission.clear()
        self.user_use_permission.add(*oldinstance.user_use_permission.all())
        self.group_use_permission.clear()
        self.group_use_permission.add(*oldinstance.group_use_permission.all())
        self.recipients.clear()
        self.recipients.add(*oldinstance.recipients.all())
        self.recipient_groups.clear()
        self.recipient_groups.add(*oldinstance.recipient_groups.all())


class ContactFormPluginModel(ContactFormBasePluginModel):  # type: ignore[django-manager-missing]
    pass


@receiver(
    m2m_changed, sender=ContactFormPluginModel.user_use_permission.through
)
def update_user_use_permission(
    instance: ContactFormBasePluginModel,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove view permission for users."""

    if action not in ["post_add", "post_remove", "post_clear"]:
        return

    codename = "use_contact_form"
    utils.bulk_update_user_permissions(instance, action, codename, pk_set)


@receiver(
    m2m_changed, sender=ContactFormPluginModel.group_use_permission.through
)
def update_group_use_permission(
    instance: ContactFormBasePluginModel,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove view permission for users."""

    if action not in ["post_add", "post_remove", "post_clear"]:
        return

    codename = "use_contact_form"
    utils.bulk_update_group_permissions(instance, action, codename, pk_set)
