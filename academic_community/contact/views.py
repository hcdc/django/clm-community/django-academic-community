# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.contrib.auth import get_user_model
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.utils.functional import cached_property
from django.views import generic

from academic_community.contact import forms, models
from academic_community.mixins import PermissionCheckViewMixin
from academic_community.notifications.models import OutgoingNotification
from academic_community.utils import PermissionRequiredMixin

User = get_user_model()


class ContactFormView(
    SuccessMessageMixin,
    PermissionRequiredMixin,
    PermissionCheckViewMixin,
    generic.CreateView,
):
    model = OutgoingNotification

    form_class = forms.ContactForm

    permission_required = "contact.use_contact_form"

    success_message = (
        "Thank you for getting in touch! Your message has been sent."
    )

    template_name = "contact/contact_form.html"

    def get_initial(self):
        initial = super().get_initial()
        if hasattr(self.request.user, "email"):
            initial["reply_to"] = self.request.user.email
        return initial

    def get_success_url(self):
        plugin = self.permission_object
        if plugin.placeholder and plugin.placeholder.page:
            return plugin.placeholder.page.get_absolute_url()
        else:
            return self.request.path

    def get_prefix(self):
        """Return the prefix to use for forms."""
        # we need to set the prefix here because because it is set by the
        # plugin
        return "contact-form-%i" % self.kwargs["pk"]

    def form_valid(self, form):
        if form.instance.plain_text_body:
            form.instance.create_html_body()
        else:
            form.instance.create_plain_text_body()

        if self.request.user.is_anonymous:
            # use the anonymous user from django-guardian
            sender = get_user_model().get_anonymous()  # type: ignore
        else:
            sender = self.request.user
        form.instance.sender = sender

        plugin = self.permission_object

        recipients = User.objects.filter(
            Q(pk__in=plugin.recipients.values_list("pk", flat=True))
            | Q(
                groups__pk__in=plugin.recipient_groups.values_list(
                    "pk", flat=True
                )
            )
        )

        response = super().form_valid(form)
        form.instance.recipients.set(recipients)

        form.instance.create_notifications()
        form.instance.send_mails_now()
        return response

    @cached_property
    def permission_object(self) -> models.ContactFormBasePluginModel:
        """Get the plugin model."""
        return models.ContactFormPluginModel.objects.get(pk=self.kwargs["pk"])
