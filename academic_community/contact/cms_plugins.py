# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib.auth import get_user_model
from django.shortcuts import resolve_url
from django.utils.translation import gettext as _

from academic_community import utils
from academic_community.contact import forms, models

User = get_user_model()


@plugin_pool.register_plugin
class ContactFormPlugin(CMSPluginBase):
    """A plugin to display content to certain user or groups only."""

    model = models.ContactFormPluginModel
    name = _("Contact form")
    module = _("Contact")
    render_template = "contact/components/contact_form_plugin.html"
    allow_children = True

    filter_horizontal = [
        "user_use_permission",
        "group_use_permission",
        "recipients",
        "recipient_groups",
    ]

    text_enabled = True

    cache = False

    contact_form_class = forms.ContactForm

    def render(
        self,
        context,
        instance: models.ContactFormPluginModel,
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        context["instance"] = instance
        user = context["request"].user
        if utils.has_perm(user, "contact.use_contact_form", instance):
            initial = self.get_contact_form_initial(
                context, instance, placeholder
            )
            form = self.contact_form_class(
                initial=initial, prefix="contact-form-%i" % instance.pk
            )
            form.update_from_user(user)
            context["form"] = form
            context["target_url"] = resolve_url("contact:contact", instance.pk)
        return context

    def get_contact_form_initial(
        self,
        context,
        instance: models.ContactFormPluginModel,
        placeholder,
    ):
        """Get the initial data for the contact form."""
        user = context["request"].user
        ret = {}
        if not user.is_anonymous:
            if (
                hasattr(user, "communitymember")
                and user.communitymember.contact_email
            ):
                ret["reply_to"] = user.communitymember.contact_email.email
            else:
                ret["reply_to"] = user.email
        return ret

    def get_changeform_initial_data(self, request):
        """Get the intial data for the change form.

        This is the same as for the standard EventCreateView.
        """
        initial = super().get_changeform_initial_data(request)
        groups = utils.get_groups("DEFAULT", "ANONYMOUS")
        initial["group_use_permission"] = groups
        initial["recipients"] = User.objects.filter(pk=request.user.pk)
        return initial
