"""clm_tools_test_site URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import include, path
from django.views.i18n import JavaScriptCatalog

from academic_community import views

admin.autodiscover()

urlpatterns = [
    path("sitemap.xml", sitemap, {"sitemaps": {"cmspages": CMSSitemap}}),
    path("e2ee/", views.E2EEStatusView.as_view(), name="e2ee-status"),
    path(
        "e2ee/delete/<uuid:uuid>/",
        views.MasterKeySecretDeleteView.as_view(),
        name="masterkeysecret-delete",
    ),
    path("e2ee/rest/", include("django_e2ee.urls")),
    path("notifications/", include("academic_community.notifications.urls")),
    path("rest/", include("academic_community.rest.urls")),
    path("channels/", include("academic_community.channels.urls")),
    path("search/", include("academic_community.search.urls")),
    path("history/", include("academic_community.history.urls")),
    path("contact/", include("academic_community.contact.urls")),
    path("faqs/", include("academic_community.faqs.urls")),
    path("captcha/", include("captcha.urls")),
    path("mailinglists/", include("academic_community.mailinglists.urls")),
    path("uploads/", include("academic_community.uploaded_material.urls")),
    path("select2/", include("django_select2.urls")),
    # include the Django Admins Javascript Catalog (necessary for the
    # forms.FilteredSelectMultiple widget)
    path(
        "jsi18n/",
        JavaScriptCatalog.as_view(packages=["academic_community"]),
        name="jsi18n",
    ),
    path(
        "manifest.webmanifest",
        views.ManifestView.as_view(),
        name="webmanifest",
    ),
    path(
        "serviceworker.js",
        views.ServiceWorkerView.as_view(),
        name="serviceworker",
    ),
    path("accounts/", include("academic_community.members.urls")),
    path("", include("cms.urls")),
]

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
