# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.21 on 2023-10-09 16:50

import djangocms_frontend.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("activities", "0036_auto_20231009_1104"),
    ]

    operations = [
        migrations.AddField(
            model_name="activitylistpluginmodel",
            name="enable_filters",
            field=models.BooleanField(
                default=False,
                help_text="Enable the option to filter the resulting list.",
            ),
        ),
        migrations.AddField(
            model_name="activitylistpluginmodel",
            name="filter_button_attributes",
            field=djangocms_frontend.fields.AttributesField(
                blank=True,
                default=dict,
                help_text="Additional attributes for the filter button",
                verbose_name="Attributes",
            ),
        ),
        migrations.AddField(
            model_name="activitylistpluginmodel",
            name="filter_button_text",
            field=models.CharField(
                blank=True,
                help_text="The text to show on the filter button.",
                max_length=50,
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="activitylistpluginmodel",
            name="filter_query_prefix",
            field=models.SlugField(
                blank=True,
                help_text="The suffix for the filter parameter in the GET request. If you have multiple plugins with filter options on a page, make sure, to give them unique suffixes.",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="activitylistpluginmodel",
            name="show_active_filters",
            field=models.BooleanField(
                default=True,
                help_text="Show the active filters (when filters are enabled)",
            ),
        ),
        migrations.AddField(
            model_name="activitylistpluginmodel",
            name="show_filter_button",
            field=models.BooleanField(
                default=True,
                help_text="Show the button to open the filtering menu (when filters are enabled)",
            ),
        ),
        migrations.AddField(
            model_name="activitylistpluginmodel",
            name="show_filters_in_card",
            field=models.BooleanField(
                default=True, help_text="Render a card around the badges."
            ),
        ),
        migrations.AddField(
            model_name="activitylistpluginmodel",
            name="strict_filters",
            field=models.BooleanField(
                default=True,
                help_text="Return no data at all if invalid filter parameters are provided in the URL.",
            ),
        ),
    ]
