# Generated by Django 3.2.7 on 2021-11-18 11:17

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("activities", "0008_activity_groups"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="activity",
            name="mailing_list",
        ),
    ]
