# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.22 on 2023-11-29 13:08

import aldryn_apphooks_config.fields
import django.db.models.deletion
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("events", "0017_eventlistpluginmodel_app_config"),
        (
            "activities",
            "0043_alter_activityacademicorganizationlistpluginmodel_show_all_child_organizations",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="activityeventlistpluginmodel",
            name="app_config",
            field=aldryn_apphooks_config.fields.AppHookConfigField(
                blank=True,
                help_text="The namespace whose events to display.",
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="events.eventsconfig",
                verbose_name="Event Namespace",
            ),
        ),
    ]
