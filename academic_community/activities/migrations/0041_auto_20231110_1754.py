# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.22 on 2023-11-10 16:54

import aldryn_apphooks_config.fields
import django.db.models.deletion
import djangocms_frontend.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("topics", "0024_auto_20231110_1754"),
        ("activities", "0040_auto_20231027_1804"),
    ]

    operations = [
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="app_config",
            field=aldryn_apphooks_config.fields.AppHookConfigField(
                blank=True,
                help_text="The namespace whose topics to display.",
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="topics.topicsconfig",
                verbose_name="Topics Namespace",
            ),
        ),
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="enable_filters",
            field=models.BooleanField(
                default=False,
                help_text="Enable the option to filter the resulting list.",
            ),
        ),
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="filter_button_attributes",
            field=djangocms_frontend.fields.AttributesField(
                blank=True,
                default=dict,
                help_text="Additional attributes for the filter button",
                verbose_name="Attributes",
            ),
        ),
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="filter_button_text",
            field=models.CharField(
                blank=True,
                help_text="The text to show on the filter button.",
                max_length=50,
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="filter_query_prefix",
            field=models.SlugField(
                blank=True,
                help_text="The suffix for the filter parameter in the GET request. If you have multiple plugins with filter options on a page, make sure, to give them unique suffixes.",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="show_active_filters",
            field=models.BooleanField(
                default=True,
                help_text="Show the active filters (when filters are enabled)",
            ),
        ),
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="show_filter_button",
            field=models.BooleanField(
                default=True,
                help_text="Show the button to open the filtering menu (when filters are enabled)",
            ),
        ),
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="show_filters_in_card",
            field=models.BooleanField(
                default=True, help_text="Render a card around the badges."
            ),
        ),
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="strict_filters",
            field=models.BooleanField(
                default=True,
                help_text="Return no data at all if invalid filter parameters are provided in the URL.",
            ),
        ),
    ]
