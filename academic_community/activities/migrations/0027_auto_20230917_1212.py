# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2.21 on 2023-09-17 10:12

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("activities", "0026_activitychannellistpluginmodel"),
    ]

    operations = [
        migrations.AddField(
            model_name="activitymateriallistpluginmodel",
            name="from_url",
            field=models.BooleanField(
                default=False,
                help_text="Get the activity from the URL rather than from the activity field. Note that this only works for activity detail pages.",
            ),
        ),
        migrations.AddField(
            model_name="addactivitymaterialbuttonpluginmodel",
            name="from_url",
            field=models.BooleanField(
                default=False,
                help_text="Get the activity from the URL rather than from the activity field. Note that this only works for activity detail pages.",
            ),
        ),
        migrations.AlterField(
            model_name="activitymateriallistpluginmodel",
            name="activity",
            field=models.ForeignKey(
                blank=True,
                help_text="The activity that you want to display the material for.",
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="activities.activity",
            ),
        ),
        migrations.AlterField(
            model_name="addactivitymaterialbuttonpluginmodel",
            name="activity",
            field=models.ForeignKey(
                blank=True,
                help_text="The activity that you want to display the material for.",
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="activities.activity",
            ),
        ),
    ]
