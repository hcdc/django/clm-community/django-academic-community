# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

# Generated by Django 3.2 on 2024-04-29 11:21

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("activities", "0050_auto_20240610_1404"),
    ]

    operations = [
        migrations.AddField(
            model_name="activitytopiclistpluginmodel",
            name="app_config_from_request",
            field=models.BooleanField(
                default=True,
                help_text="This option allows to infer the namespace for the %(topics)s from the URL, so you do not have to specify it manually.",
                verbose_name="Infer namespace from URL location",
            ),
        ),
    ]
