# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Optional,
    Sequence,
    Set,
    Tuple,
)

import reversion
from aldryn_apphooks_config.fields import AppHookConfigField
from aldryn_apphooks_config.managers.base import AppHookConfigQuerySet
from cms.models import CMSPlugin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import IntegrityError, models
from django.db.models.signals import (
    m2m_changed,
    post_delete,
    post_save,
    pre_save,
)
from django.dispatch import receiver
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from djangocms_versioning.constants import DRAFT
from djangocms_versioning.models import Version
from guardian.shortcuts import (
    assign_perm,
    get_anonymous_user,
    get_objects_for_user,
    remove_perm,
)

from academic_community import utils
from academic_community.activities import app_settings
from academic_community.activities.cms_appconfig import ActivitiesConfig
from academic_community.channels.models import (
    AbstractChannelListPluginModel,
    Channel,
    ChannelGroup,
    ChannelRelation,
    ChannelSubscription,
    GroupMentionLink,
    MentionLink,
)
from academic_community.ckeditor5.fields import CKEditor5Field
from academic_community.history.models import RevisionMixin
from academic_community.institutions.models import (
    AbstractAcademicOrganizationListPluginModel,
    AcademicMembership,
    AcademicOrganization,
)
from academic_community.members.models import (
    AbstractCommunityMemberListPluginModel,
    CommunityMember,
)
from academic_community.models import NamedModel, ObjectPage, ObjectPageContent
from academic_community.models.cms import (
    AbstractPaginationPluginModelMixin,
    FilterPluginModelMixin,
)
from academic_community.notifications.models import SystemNotification
from academic_community.uploaded_material.models import (
    AddMaterialButtonBasePluginModel,
    MaterialListPluginBaseModel,
    MaterialRelation,
)

if TYPE_CHECKING:
    from django.contrib.auth.models import User

    from academic_community.events.models import Event
    from academic_community.events.programme.models import Contribution
    from academic_community.topics.models import Topic


User = get_user_model()  # type: ignore  # noqa: F811


class Category(NamedModel):  # type: ignore[django-manager-missing]
    """A category of a :model:`topics.Activity`."""

    name = models.CharField(
        max_length=255, help_text="The name of the category."
    )

    abbreviation = models.CharField(
        max_length=10,
        help_text="Abbreviation of this category",
        blank=True,
        null=True,
    )

    short_name_format = models.CharField(
        max_length=300,
        help_text=(
            "Format for the short name to display the activity. Possible keys "
            "are: abbreviation, name, category_name and category_abbreviation"
        ),
        default="{category_abbreviation} {abbreviation}",
    )

    name_format = models.CharField(
        max_length=300,
        help_text=(
            "Format for the short name to display the activity. Possible keys "
            "are: abbreviation, name, category_name and category_abbreviation"
        ),
        default="{category_abbreviation} {name} ({abbreviation})",
    )

    card_class = models.CharField(
        max_length=300,
        help_text=(
            "Additional classes that are added to the activity card for "
            "formatting"
        ),
        null=True,
        blank=True,
    )

    card_template = models.CharField(
        verbose_name="Card template",
        help_text=_(
            "The template to use for rendering the cards of the corresponding %(working_groups)s"
        )
        % {"working_groups": _("working groups")},
        choices=app_settings.ACTIVITY_CARD_TEMPLATES,
        default=app_settings.ACTIVITY_CARD_TEMPLATES[0][0],
        max_length=255,
    )


class ActivityQueryset(AppHookConfigQuerySet):
    """A queryset with extra methods for querying members."""

    def all_active(self) -> models.QuerySet[Activity]:
        return self.filter(end_date__isnull=True)

    def all_finished(self) -> models.QuerySet[Activity]:
        return self.filter(end_date__isnull=False)


class ActivityManager(
    models.Manager.from_queryset(ActivityQueryset)  # type: ignore # noqa: E501
):
    """Database manager for Activities."""


def get_first_activitiesconfig() -> Optional[ActivitiesConfig]:
    return ActivitiesConfig.objects.first()


@reversion.register
class Activity(RevisionMixin, models.Model):  # type: ignore[django-manager-missing]
    """An activity within the community."""

    objects = ActivityManager()

    event_set: models.QuerySet[Event]

    contribution_set: models.manager.RelatedManager[Contribution]

    activitychannelrelation_set: models.manager.RelatedManager[
        ActivityChannelRelation
    ]

    activitymaterialrelation_set: models.manager.RelatedManager[
        ActivityMaterialRelation
    ]

    activitygroup: ActivityGroup

    class Meta:
        verbose_name = _("Working Group")
        verbose_name_plural = _("Working Groups")
        ordering = [
            models.F("end_date").asc(nulls_first=True),
        ]
        permissions = (
            ("change_activity_lead", "Can change activity leader"),
            ("add_material_relation", "Can add an activity material relation"),
            ("add_channel_relation", "Can add an activity channel relation"),
        )

    def get_absolute_url(self) -> str:
        """Get the url to the detailed view of this working group."""
        if self.app_config is None:
            return ""
        else:
            return reverse(
                self.app_config.namespace + ":activity-detail",
                args=[str(self.abbreviation)],
            )

    def get_edit_url(self) -> str:
        """Get the url to edit the working group."""
        if self.app_config is None:
            return ""
        else:
            return reverse(
                self.app_config.namespace + ":edit-activity",
                args=[str(self.abbreviation)],
            )

    def has_placeholder_change_permission(self, user: User):
        """Method for django-cms to check permissions.

        Django-CMS unfortunately does not support object-level-permissions
        at the moment (see
        https://github.com/django-cms/django-cms/issues/6665), which is why we
        implement this workaround."""
        perm = utils.get_model_perm(self, "change")
        return utils.has_perm(user, perm, self)

    @property
    def active_topics(self) -> models.QuerySet[Topic]:
        """Get the active topics within this activity."""
        return Topic.objects.filter(
            metadata__activities__p_keys__contains=self.pk,
            end_date__isnull=True,
        )

    @property
    def finished_topics(self) -> models.QuerySet[Topic]:
        """Get the finished topics within this activity."""
        return Topic.objects.filter(
            metadata__activities__p_keys__contains=self.pk,
            end_date__isnull=False,
        )

    app_config = AppHookConfigField(
        ActivitiesConfig,
        null=True,
        on_delete=models.SET_NULL,
        default=get_first_activitiesconfig,
    )

    name = models.CharField(
        max_length=255,
        help_text=_("The name of the %(working_group)s.")
        % {"working_group": _("working group")},
    )

    abbreviation = models.CharField(
        max_length=20,
        unique=True,
        help_text=_("The abbreviated name of the %(working_group)s.")
        % {"working_group": _("working group")},
    )

    logo = models.ImageField(
        upload_to="static/images/activity-logos/",
        help_text=_("Logo of the %(working_group)s.")
        % {"working_group": _("working group")},
        null=True,
        blank=True,
    )

    abstract = CKEditor5Field(
        max_length=4000,
        help_text=_("An abstract of the %(working_group)s.")
        % {"working_group": _("working group")},
        null=True,
        blank=True,
    )

    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        null=True,
        help_text=_("The category of this %(working_group)s.")
        % {"working_group": _("working group")},
    )

    invitation_only = models.BooleanField(
        default=False,
        help_text=_(
            "If set, only the %(working_group)s leaders are able to add new "
            "members. Otherwise, community members can freely join and leave "
            "through their profile page or on the %(working_group)s detail page."
        )
        % {"working_group": _("working group")},
    )

    leaders = models.ManyToManyField(
        CommunityMember,
        related_name="activity_leader",
        help_text=_("The community members leading this %(working_group)s.")
        % {"working_group": _("working group")},
    )

    start_date = models.DateField(
        auto_now_add=True,
        help_text=_("The date when this %(working_group)s started.")
        % {"working_group": _("working group")},
    )

    end_date = models.DateField(
        null=True,
        blank=True,
        help_text=_("The date when this %(working_group)s stopped.")
        % {"working_group": _("working group")},
    )

    members = models.ManyToManyField(
        CommunityMember,
        help_text=_(
            "Community members contributing to this %(working_group)s."
        )
        % {"working_group": _("working group")},
        blank=True,
        through=CommunityMember.activities.through,
    )

    former_members = models.ManyToManyField(
        CommunityMember,
        help_text=_(
            "Community members formerly contributing to this %(working_group)s."
        )
        % {"working_group": _("working group")},
        blank=True,
        related_name="former_activity_set",
    )

    user_view_permission = models.ManyToManyField(
        User,
        help_text=_(
            "Select explicit users that are allowed view this "
            "%(working_group)s."
        )
        % {"working_group": _("working group")},
        blank=True,
        related_name="activity_view_activity",
    )

    group_view_permission = models.ManyToManyField(
        Group,
        help_text=_(
            "Select explicit groups that are allowed to view this "
            "%(working_group)s."
        )
        % {"working_group": _("working group")},
        blank=True,
        related_name="activity_view_activity",
    )

    user_add_material_relation_permission = models.ManyToManyField(
        User,
        help_text=(
            "Users that can upload and relate material to this "
            "%(working_group)s."
        )
        % {"working_group": _("working group")},
        verbose_name="Users with material upload permission",
        blank=True,
        related_name="activity_add_material_relation",
    )

    group_add_material_relation_permission = models.ManyToManyField(
        Group,
        help_text=(
            "Groups that can upload and relate material to this "
            "%(working_group)s."
        )
        % {"working_group": _("working group")},
        verbose_name="Groups with material upload permission",
        blank=True,
        related_name="activity_add_material_relation",
    )

    user_add_channel_relation_permission = models.ManyToManyField(
        User,
        help_text=(
            "Users that can create channels and relate them to this "
            "%(working_group)s."
        )
        % {"working_group": _("working group")},
        verbose_name="Users with channel creation permission",
        blank=True,
        related_name="activity_add_channel_relation",
    )

    group_add_channel_relation_permission = models.ManyToManyField(
        Group,
        help_text=(
            "Groups that can create channels and relate them to this "
            "%(working_group)s."
        )
        % {"working_group": _("working group")},
        verbose_name="Groups with channel creation permission",
        blank=True,
        related_name="activity_add_channel_relation",
    )

    pages = GenericRelation(ObjectPage)

    @cached_property
    def page(self) -> ObjectPageContent:
        return self.pages.first()

    def update_permissions(self, member: CommunityMember):
        """Update the permissions of the leader and regular member."""
        if not member.user:
            return
        user: User = member.user
        if self.leaders.filter(pk=member.pk):
            assign_perm("change_activity_lead", user, self)
            assign_perm("change_activity", user, self)
            assign_perm("view_activity", member.user, self)
        else:
            remove_perm("change_activity_lead", user, self)
            remove_perm("change_activity", user, self)
            if not self.user_view_permission.filter(
                pk=member.user.pk
            ).exists():
                remove_perm("view_activity", member.user, self)
        for channel_relation in self.activitychannelrelation_set.all_valid():  # type: ignore[attr-defined]
            channel_relation.channel.update_user_permissions(user)
        for material_relation in self.activitymaterialrelation_set.all():
            material_relation.material.update_user_permissions(user)

    def remove_all_permissions(self, user: User):
        """Remove all permissions for the given user."""
        permissions = [key for key, label in self.__class__._meta.permissions]
        for perm in ["view_activity", "change_activity"] + permissions:
            remove_perm(perm, user, self)
        for channel_relation in self.activitychannelrelation_set.all_valid():  # type: ignore[attr-defined]
            channel_relation.channel.update_user_permissions(user)
        for material_relation in self.activitymaterialrelation_set.all():
            material_relation.material.update_user_permissions(user)

    @property
    def is_finished(self) -> bool:
        """True if this activity is finished."""
        return bool(self.end_date)

    @property
    def real_members(self) -> models.QuerySet[CommunityMember]:
        if self.is_finished:
            return self.former_members.all()
        else:
            return self.members.all()

    def synchronize_group(
        self, members: Sequence[CommunityMember], add: bool = True
    ) -> None:
        """Add or remove members from the associated user groups.

        Parameters
        ----------
        members: list of communitymembers
            The members that have been added to the list of :attr:`members`.
        """
        if not hasattr(self, "activitygroup"):
            return
        group: ActivityGroup = self.activitygroup
        for member in members:
            if member.user:
                if add:
                    member.user.groups.add(group)
                else:
                    member.user.groups.remove(group)

    def inform_leader(
        self, members: Sequence[CommunityMember], add: bool = True
    ) -> None:
        """Add or remove members from the associated user groups.

        Parameters
        ----------
        members: list of communitymembers
            The members that have been added to the list of :attr:`members`.
        """
        users: List[User] = []
        for leader in self.leaders.all():
            if leader.user:
                users.append(leader.user)
        if add:
            subject = f"New members have been added to {self}"
        else:
            subject = f"Members have been removed from {self}"
        SystemNotification.create_notifications(
            users,
            subject,
            "activities/post_activity_memberchange_email.html",
            {"activity": self, "communitymember_list": members, "added": add},
        )

    @property
    def short_name(self) -> str:
        if self.category:
            try:
                return self.category.short_name_format.format(
                    **self.formatters
                ).strip()
            except (KeyError, TypeError):
                return self.category.short_name_format
        else:
            return self.abbreviation

    @property
    def formatters(self) -> Dict[str, str]:
        """A dictionary with possible formatters."""
        ret = dict(name=self.name, abbreviation=self.abbreviation)
        category = self.category
        if category:
            ret.update(
                dict(
                    category_name=category.name,
                    category_abbreviation=category.abbreviation or "",
                )
            )
        return ret

    @cached_property
    def organizations(self) -> models.QuerySet[AcademicOrganization]:
        """Get the organizations whose members participate here."""
        if not self.end_date:
            kws = {"member__activity__pk": self.pk}
        else:
            kws = {"member__former_activity_set__pk": self.pk}
        memberships = AcademicMembership.objects.filter(
            end_date__isnull=True, **kws
        )
        return AcademicOrganization.objects.filter(
            pk__in=memberships.values_list("organization__pk", flat=True)
        )

    def get_template(self):
        """Get the template for the CMS placeholders."""
        return "activities/activity_detail_cms.html"

    def __str__(self):
        if self.category:
            try:
                return self.category.name_format.format(
                    **self.formatters
                ).strip()
            except (KeyError, TypeError):
                return self.category.short_name_format
        else:
            return self.name


class ActivityGroup(Group):  # type: ignore[django-manager-missing]
    """A group for an activity."""

    activity = models.OneToOneField(
        Activity,
        help_text=_("The associated %(working_group)s for the group")
        % {"working_group": _("working group")},
        on_delete=models.CASCADE,
    )

    @classmethod
    def create_for_activity(cls, activity: Activity) -> ActivityGroup:
        group = None
        try:
            group = cls.objects.create(  # type: ignore
                name=f"Members of {activity}", activity=activity
            )
        except IntegrityError:
            for i in range(10):
                try:
                    group = cls.objects.create(  # type: ignore
                        name=f"Members of {activity} - {i}", activity=activity
                    )
                except IntegrityError:
                    pass
                else:
                    break
            if group is None:
                raise
        for members in [activity.members.all(), activity.former_members.all()]:
            group.user_set.add(*members.values_list("user__pk", flat=True))
        return group  # type: ignore


MentionLink.registry.register_autocreation(ActivityGroup)(GroupMentionLink)


@MaterialRelation.registry.register_model_name(
    _("%(working_group)s Material") % {"working_group": _("Working Group")}
)
@MaterialRelation.registry.register_relation
class ActivityMaterialRelation(MaterialRelation):
    """Activity related material."""

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="unique_activity_relation_for_material",
                fields=("activity", "material"),
            )
        ]

    related_permission_field = "activity"

    related_object_url_field = "abbreviation"

    related_add_permissions = ["activities.add_material_relation", "change"]

    activity = models.ForeignKey(
        Activity,
        on_delete=models.CASCADE,
        verbose_name=_("Working Group"),
        help_text=_(
            "The related %(working_group)s in the community that this material belongs "
            "to."
        )
        % {"working_group": _("working group")},
    )

    def get_group_permissions(self, group: Group, *args, **kwargs) -> Set[str]:
        ret = super().get_group_permissions(group, *args, **kwargs)
        if (
            group.pk == self.activity.activitygroup.pk
            and not self.symbolic_relation
        ):
            ret |= {"view_material"}
        return ret

    def get_user_permissions(self, user: User, *args, **kwargs) -> Set[str]:
        ret = super().get_user_permissions(user, *args, **kwargs)
        if self.activity.leaders.filter(user__pk=user.pk):
            ret |= {
                "change_material",
                "delete_material",
                "view_material",
            }
        return ret

    @property
    def url_kws(self) -> Tuple[str, List[Any]]:
        fieldname = self.related_object_url_field or "pk"
        related_object: Activity = self.related_permission_object  # type: ignore[assignment]
        if related_object.app_config:
            return (
                related_object.app_config.namespace,
                [getattr(related_object, fieldname)],
            )
        else:
            return super().url_kws

    @classmethod
    def get_url_kws_from_kwargs(cls, **kwargs) -> Tuple[str, List[Any]]:
        """Get the url keywords from kwargs.

        This is the same as :attr:`url_kws`, but for a classmethod instead of
        a property.
        """
        fieldname = cls.related_object_url_field or "pk"
        related_object = cls.get_related_permission_object_from_kws(**kwargs)

        if related_object.app_config:
            return (
                related_object.app_config.namespace,
                [getattr(related_object, fieldname)],
            )
        else:
            return super().get_url_kws_from_kwargs(**kwargs)


@ChannelRelation.registry.register_model_name(
    _("%(working_group)s Channel") % {"working_group": _("Working Group")}
)
@ChannelRelation.registry.register_relation
class ActivityChannelRelation(ChannelRelation):
    """Activity related channel."""

    related_permission_field = "activity"

    related_object_url_field = "abbreviation"

    related_add_permissions = ["activities.add_channel_relation", "change"]

    activity = models.ForeignKey(
        Activity,
        on_delete=models.CASCADE,
        verbose_name=_("Working Group"),
        help_text=_(
            "The related %(working_group)s in the community that this channel belongs "
            "to."
        )
        % {"working_group": _("working group")},
    )

    subscribe_members = models.BooleanField(
        default=True,
        help_text=(
            "If enabled, all current and future group members are "
            "subscribed to the channel."
        ),
        verbose_name="Automatically subscribe members to the channel",
    )

    def remove_subscription_and_notify(
        self, user: User, notify=True
    ) -> Optional[ChannelSubscription]:
        """Maybe remove the channel subscription of a user."""
        subscription = self.remove_subscription(user)
        if notify and subscription:
            subscription.create_subscription_removal_notification(
                reason=(
                    f"the relation to {self.activity.short_name} has been "
                    "removed"
                )
            )
        return subscription

    def get_mentionlink(self, user: User) -> MentionLink:
        return self.activity.activitymentionlink

    def get_group_permissions(self, group: Group, *args, **kwargs) -> Set[str]:
        ret = super().get_group_permissions(group, *args, **kwargs)
        if (
            group.pk == self.activity.activitygroup.pk
            and not self.symbolic_relation
        ):
            ret |= {"view_channel", "start_thread", "post_comment"}
        return ret

    def get_user_permissions(self, user: User, *args, **kwargs) -> Set[str]:
        ret = super().get_user_permissions(user, *args, **kwargs)
        if self.activity.leaders.filter(user__pk=user.pk):
            ret |= {
                "change_channel",
                "delete_channel",
                "view_channel",
                "start_thread",
                "post_comment",
            }
        return ret


class ActivityPluginModelMixin(models.Model):
    """Abstract base model for activity related plugins."""

    class Meta:
        abstract = True

    activity = models.ForeignKey(
        Activity,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text=_(
            "The %(working_group)s that you want to display the content for."
        )
        % {"working_group": _("working group")},
    )

    from_url = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=_(
            "Get the %(working_group)s from the URL rather than from the %(working_group)s "
            "field. Note that this only works for %(working_group)s detail pages."
        )
        % {"working_group": _("working group")},
    )


class ActivityMaterialListPluginModel(  # type: ignore
    ActivityPluginModelMixin, MaterialListPluginBaseModel
):
    """A plugin to display material related to a specific activity."""

    members_only_material = models.BooleanField(
        null=True,
        blank=True,
        help_text=(
            "If yes, only material for "
            + _("working group")
            + " members is shown, if No, only "
            "material that is explicitly not only for members is shown."
        ),
    )


class AddActivityMaterialButtonPluginModel(  # type: ignore
    ActivityPluginModelMixin, AddMaterialButtonBasePluginModel
):
    """A plugin to generate a button to add community material."""

    members_only = models.BooleanField(
        default=False,
        help_text=(
            "If yes, the option making material to members only is "
            "preselected."
        ),
    )


@ChannelGroup.registry.register
class ActivityChannelGroup(ChannelGroup):
    """A channel group for a specific activity."""

    activity = models.ForeignKey(
        Activity,
        on_delete=models.CASCADE,
        help_text=_(
            "Select a %(working_group)s whose channels you want to " "display."
        )
        % {"working_group": _("working group")},
    )

    @property
    def channels(self):
        return get_objects_for_user(
            self.user,
            "view_channel",
            Channel.objects.filter(
                activitychannelrelation__activity=self.activity
            ),
        )


@MentionLink.registry.register_autocreation("activities.Activity")
@MentionLink.registry.register_for_query
class ActivityMentionLink(MentionLink):
    """A mention of an activity."""

    related_model: Activity = models.OneToOneField(  # type: ignore
        Activity, on_delete=models.CASCADE
    )

    badge_classes = [
        "badge",
        "activity-badge",
        "rounded-pill",
        "text-decoration-none",
        "bg-secondary",
        "text-black",
    ]

    @property
    def subscribers(self) -> models.QuerySet[User]:
        activity = self.related_model
        return activity.activitygroup.user_set.all()

    @property
    def name(self) -> str:
        activity = self.related_model
        abbrev = activity.abbreviation
        if activity.category and activity.category.abbreviation:
            return f"{activity.category.abbreviation} {abbrev}"
        return abbrev

    @property
    def related_model_verbose_name(self) -> str:
        return "Activity"

    @property
    def subtitle(self) -> str:
        return self.related_model.name

    @classmethod
    def query_for_autocomplete(
        cls,
        queryset: models.QuerySet[MentionLink],
        query: str,
        user: User,
        prefix: str = "",
        query_name: str = "default",
    ) -> Tuple[models.QuerySet[MentionLink], Optional[models.Q]]:
        queryset = queryset.annotate(
            activity_full_name=models.functions.Concat(  # type: ignore
                models.F(prefix + "related_model__category__abbreviation"),
                models.F(prefix + "related_model__abbreviation"),
            ),
        )
        return (queryset, models.Q(activity_full_name__istartswith=query))


class ActivityAbstractPluginModel(ActivityPluginModelMixin, CMSPlugin):  # type: ignore[django-manager-missing]
    """A plugin model for activity abstracts."""

    truncate = models.PositiveIntegerField(  # type: ignore[var-annotated]
        null=True,
        blank=True,
        help_text="Truncate the abstract to a maximum amount of characters.",
    )


class ActivitySidebarPluginModel(ActivityPluginModelMixin, CMSPlugin):  # type: ignore[django-manager-missing]
    """A plugin model to display the sidebar of an activity."""

    pass

    # from_url cannot be used here as this plugin should not be used on the
    # activity detail page itself
    from_url = None  # type: ignore


class ActivityBadgesPluginModel(ActivityPluginModelMixin, CMSPlugin):  # type: ignore[django-manager-missing]
    """A plugin model for activity abstracts."""

    show_members = models.BooleanField(  # type: ignore[var-annotated]
        default=True, help_text="Show a badge with the number of members."
    )

    show_topics = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=_("Show a badge with the number of active %(topics)s.")
        % {"topics": _("topics")},
    )

    show_start_date = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=_(
            "Show a badge with the start date of the %(working_group)s."
        )
        % {"working_group": _("working group")},
    )

    show_end_date = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=_(
            "Show a badge with the end date of the %(working_group)s "
            "(if it ended at all)."
        )
        % {"working_group": _("working group")},
    )

    collapsed = models.BooleanField(  # type: ignore[var-annotated]
        default=True, help_text="Show the card collapsed by default."
    )

    card_class = models.CharField(  # type: ignore[var-annotated]
        null=True,
        blank=True,
        help_text="Extra classes to the card.",
        max_length=200,
    )

    truncate_abstract = models.PositiveIntegerField(  # type: ignore[var-annotated]
        default=200,
        null=True,
        blank=True,
        help_text="Truncate the abstract of the cards to a maximum amount of characters. Set this to 0 to hide the abstracts.",
    )


class ActivityCardPluginModelMixin(models.Model):
    """An abstract base model to display activity cards."""

    class Meta:
        abstract = True

    collapsed = models.BooleanField(  # type: ignore[var-annotated]
        default=True, help_text="Show the card collapsed by default."
    )

    card_class = models.CharField(  # type: ignore[var-annotated]
        null=True,
        blank=True,
        help_text="Extra classes to the card.",
        max_length=200,
    )

    truncate_abstract = models.PositiveIntegerField(  # type: ignore[var-annotated]
        default=200,
        null=True,
        blank=True,
        help_text="Truncate the abstract of the card to a maximum amount of characters. Set this to 0 to hide the abstract.",
    )

    card_template = models.CharField(
        verbose_name="Card template",
        help_text=_(
            "The template to use for rendering the cards of the corresponding %(working_groups)s"
        )
        % {"working_groups": _("working groups")},
        choices=app_settings.ACTIVITY_CARD_TEMPLATES,
        null=True,
        blank=True,
        max_length=255,
    )


class ActivityCardPluginModel(  # type: ignore[django-manager-missing]
    ActivityPluginModelMixin, ActivityCardPluginModelMixin, CMSPlugin
):
    """A plugin to display a card of an activity."""


class AbstractActivityListPluginModel(  # type: ignore[django-manager-missing]
    ActivityCardPluginModelMixin,
    FilterPluginModelMixin,
    AbstractPaginationPluginModelMixin,
    CMSPlugin,
):
    """An abstract base model to display an activity list."""

    class Meta:
        abstract = True

    check_permissions = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text=(
            "Only display the members that the website visitor can view the "
            "profile of."
        ),
    )

    show_active = models.BooleanField(  # type: ignore[var-annotated]
        default=True,
        help_text="Display active " + _("working groups") + ".",
    )

    show_finished = models.BooleanField(  # type: ignore[var-annotated]
        default=False, help_text="Show finished " + _("working groups") + "."
    )

    categories = models.ManyToManyField(  # type: ignore[var-annotated]
        Category,
        blank=True,
        help_text="Only display "
        + _("working groups")
        + " of specific categories.",
    )

    display_badges = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=(
            "Rather than displaying cards, show badges with the working "
            "group abbreviation."
        ),
    )

    show_count = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text="Display the total number of items that are available",
    )

    app_config = AppHookConfigField(
        ActivitiesConfig,
        verbose_name=_("%(working_group)s Namespace")
        % {"working_group": _("Working Group")},
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("The namespace whose working groups to display."),
    )


class ActivityListPluginModel(AbstractActivityListPluginModel):  # type: ignore[django-manager-missing]
    """A plugin model to display community members."""

    activities = models.ManyToManyField(  # type: ignore[var-annotated]
        Activity,
        blank=True,
        verbose_name=_("Working Group"),
        help_text="The working groups you'd like to display.",
    )

    show_all = models.BooleanField(  # type: ignore[var-annotated]
        default=False,
        help_text=_(
            "Show all %(working_groups)s, no matter what you selected "
            "above."
        )
        % {"working_groups": _("working groups")},
    )

    def copy_relations(self, oldinstance):
        self.activities.clear()
        self.activities.add(*oldinstance.activities.all())


class ActivityMembersListPluginModel(  # type: ignore
    ActivityPluginModelMixin, AbstractCommunityMemberListPluginModel
):
    """A plugin model to display the members of a working/project group."""

    leaders_only = models.BooleanField(  # type: ignore[var-annotated]
        default=False, help_text="Only show the leaders of the working group."
    )


class ActivityAcademicOrganizationListPluginModel(  # type: ignore
    ActivityPluginModelMixin, AbstractAcademicOrganizationListPluginModel
):
    """A plugin model to list institutions in an activity."""


# isort: off

# to avoid circular imports, we need to import the topic model here
from academic_community.topics.models import (  # noqa: F811
    AbstractTopicListPluginModel,
    Topic,
)

# isort: on


class ActivityTopicListPluginModel(  # type: ignore
    ActivityPluginModelMixin, AbstractTopicListPluginModel
):
    """A plugin model for working group topics."""


class ActivityChannelListPluginModel(  # type: ignore
    ActivityPluginModelMixin, AbstractChannelListPluginModel
):
    """A plugin model to display channels of a working/project group"""


# isort: off

# to avoid circular imports, we need to import the event model here
from academic_community.events.models import (  # noqa: F811
    AbstractEventListPluginModel,
    Event,
)

# isort: on


class ActivityEventListPluginModel(  # type: ignore
    ActivityPluginModelMixin, AbstractEventListPluginModel
):
    """A plugin model to display community members."""


@receiver(pre_save, sender=Activity)
def remove_abstract_styles(instance: Activity, **kwargs):
    """Remove the font-style elements of the abstract, etc."""
    instance.abstract = utils.remove_style(instance.abstract)


@receiver(m2m_changed, sender=Activity.leaders.through)
def update_leader_permissions(
    instance: Activity, action: str, pk_set: List[int], **kwargs
):
    if action not in ["post_remove", "post_add", "post_clear"]:
        return
    members = CommunityMember.objects.filter(pk__in=pk_set)

    for member in members:
        instance.update_permissions(member)


@receiver(post_save, sender=Activity)
def assign_user_permissions(sender, instance: Activity, **kwargs):
    if instance.end_date and instance.members.count():
        members = list(instance.members.all())
        instance.members.clear()
        instance.former_members.add(*members)

    if hasattr(instance, "activitygroup"):
        instance.activitygroup.name = f"Members of {instance}"
        instance.activitygroup.save()
    else:
        ActivityGroup.create_for_activity(instance)


@receiver(post_save, sender=Activity)
def create_object_page(instance: Activity, created: bool, **kwargs):
    if created:
        page = ObjectPage.objects.create(content_object=instance)
        content = ObjectPageContent.objects.create(page=page)

        ct_content = ContentType.objects.get_for_model(ObjectPageContent)
        Version.objects.create(
            created_by=get_anonymous_user(),
            state=DRAFT,
            number=1,
            object_id=content.pk,
            content_type=ct_content,
        )


@receiver(post_delete, sender=Activity)
def remove_topic_relations(sender, instance: Activity, **kwargs):
    """Remove the activity from related topics."""
    for topic in Topic.objects.filter(
        metadata__activities__p_keys__contains=instance.pk
    ):
        keys = topic.metadata["activities"]["p_keys"]
        topic.metadata["activities"]["p_keys"] = [
            key for key in keys if key != instance.pk
        ]
        topic.save()


@receiver(m2m_changed, sender=Activity.former_members.through)
@receiver(m2m_changed, sender=Activity.members.through)
def syncronize_activity_group(sender, **kwargs):
    sender = kwargs["instance"]

    pk_set = kwargs["pk_set"]

    action = kwargs["action"]

    if action not in ["post_remove", "post_add", "post_clear"]:
        return

    activities: Sequence[Activity] = []
    members: Sequence[CommunityMember] = []

    if isinstance(sender, Activity):
        activities = [sender]
        if pk_set:
            members = CommunityMember.objects.filter(pk__in=pk_set)
            for member in members:
                # save the member to trigger a new version
                member.save()

    elif isinstance(sender, CommunityMember):
        if pk_set:
            activities = Activity.objects.filter(pk__in=pk_set)
            members = [sender]
            for activity in activities:
                # save the activity to trigger a new version
                activity.save()

    else:
        return

    # check for groups
    for activity in activities:
        activity.synchronize_group(members, add=(action == "post_add"))
        activity.inform_leader(members, add=(action == "post_add"))

    users: List[User] = [member.user for member in members if member.user]

    if action == "post_add":
        new_subscriptions: List[ChannelSubscription] = []
        for activity in activities:
            relations = activity.activitychannelrelation_set.filter(
                subscribe_members=True, symbolic_relation=False
            )
            for user in users:
                try:
                    ActivityChannelGroup.objects.get_or_create(
                        activity=activity,
                        user=user,
                        name=activity.short_name,
                        slug=activity.abbreviation,
                    )
                except IntegrityError:
                    pass
            for relation in relations:
                new_subscriptions.extend(relation.create_subscriptions(users))

        if new_subscriptions:
            activity_names = ", ".join(a.short_name for a in activities)
            ChannelSubscription.create_aggregated_subscription_notifications(
                new_subscriptions,
                "New channel subscriptions for " + activity_names,
                reason="you joined " + activity_names,
            )
    else:
        removed_subscriptions: List[ChannelSubscription] = []
        for activity in activities:
            relations = activity.activitychannelrelation_set.filter(
                subscribe_members=True, symbolic_relation=False
            )
            for relation in relations:
                removed_subscriptions.extend(
                    relation.remove_subscriptions(users)
                )
        if removed_subscriptions:
            activity_names = ", ".join(a.short_name for a in activities)
            ChannelSubscription.create_aggregated_subscription_removal_notifications(
                removed_subscriptions,
                "Channel subscriptions removed for " + activity_names,
                reason="you left " + activity_names,
            )


@receiver(post_save, sender=ActivityChannelRelation)
def create_channel_subscriptions(
    instance: ActivityChannelRelation, created: bool, **kwargs
):
    """Create the channel subscriptions for the activity members."""
    channel = instance.channel
    activity = instance.activity
    for leader in activity.leaders.filter(user__isnull=False):
        channel.update_user_permissions(leader.user)  # type: ignore
    perms = channel.update_group_permissions(activity.activitygroup)
    if "view_channel" in perms:
        if instance.subscribe_members:
            users: List[User] = [
                member.user  # type: ignore
                for member in activity.real_members.filter(user__isnull=False)
            ]
            subscriptions = instance.create_subscriptions(users)
            for subscription in subscriptions:
                subscription.create_subscription_notification(
                    reason="you are a member of " + activity.short_name
                )
    else:
        for member in activity.real_members.filter(user__isnull=False):
            user: User = member.user  # type: ignore
            instance.remove_subscription_and_notify(user)


@receiver(post_delete, sender=ActivityChannelRelation)
def remove_channel_view_permission(
    instance: ActivityChannelRelation, **kwargs
):
    """Remove the permission to view a channel"""
    activity = instance.activity
    channel = instance.channel

    try:
        group = activity.activitygroup
    except Activity.activitygroup.RelatedObjectDoesNotExist:  # type: ignore[attr-defined]
        perms = {"view_channel"}
    else:
        perms = channel.update_group_permissions(group)
    for leader in activity.leaders.filter(user__isnull=False):
        channel.update_user_permissions(leader.user)  # type: ignore
    if "view_channel" not in perms:
        for member in activity.real_members.filter(user__isnull=False):
            user: User = member.user  # type: ignore
            instance.remove_subscription_and_notify(user)


@receiver(post_delete, sender=Activity)
def update_activity_material_relations(instance: Activity, **kwargs):
    """Update all related material permissions.

    This updates the permissions on the related material after the activity has
    been deleted. We cannot do this in a post_delete relation to
    :class:`ActivityMaterialRelation` because the corresponding groups have
    been deleted already.
    """
    leaders = list(instance.leaders.filter(user__isnull=True))
    for material_relation in instance.activitymaterialrelation_set.all():
        for leader in leaders:
            print(
                "removing permission of {} in {}".format(
                    leader, material_relation.material
                )
            )
            material_relation.material.update_user_permissions(leader.user)  # type: ignore
        material_relation.material.update_group_permissions(
            instance.activitygroup
        )

    for channel_relation in instance.activitychannelrelation_set.all():
        for leader in leaders:
            channel_relation.channel.update_user_permissions(leader.user)  # type: ignore
        channel_relation.channel.update_group_permissions(
            instance.activitygroup
        )


@receiver(post_delete, sender=ActivityMaterialRelation)
@receiver(post_save, sender=ActivityMaterialRelation)
def update_material_group_permissions_on_relation(
    instance: ActivityMaterialRelation, **kwargs
):
    """Create the channel subscriptions for the activity members."""
    material = instance.material
    activity = instance.activity
    for leader in activity.leaders.filter(user__isnull=False):
        material.update_user_permissions(leader.user)  # type: ignore
    try:
        group = activity.activitygroup
    except Activity.activitygroup.RelatedObjectDoesNotExist:  # type: ignore[attr-defined]
        pass
    else:
        material.update_group_permissions(group)


@receiver(
    m2m_changed, sender=Activity.user_add_material_relation_permission.through
)
def update_user_add_material_relation_permission(
    instance: Activity,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove add_material_relation permission for users."""

    if action in ["post_add", "post_remove", "post_clear"]:
        utils.bulk_update_user_permissions(
            instance, action, "add_material_relation", pk_set
        )


@receiver(
    m2m_changed, sender=Activity.group_add_material_relation_permission.through
)
def update_group_add_material_relation_permission(
    instance: Activity,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove add_material_relation permission for groups."""

    if action in ["post_add", "post_remove", "post_clear"]:
        utils.bulk_update_group_permissions(
            instance, action, "add_material_relation", pk_set
        )


@receiver(
    m2m_changed, sender=Activity.user_add_channel_relation_permission.through
)
def update_user_add_channel_relation_permission(
    instance: Activity,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove add_channel_relation permission for users."""

    if action in ["post_add", "post_remove", "post_clear"]:
        utils.bulk_update_user_permissions(
            instance, action, "add_channel_relation", pk_set
        )


@receiver(
    m2m_changed, sender=Activity.group_add_channel_relation_permission.through
)
def update_group_add_channel_relation_permission(
    instance: Activity,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove add_channel_relation permission for groups."""

    if action in ["post_add", "post_remove", "post_clear"]:
        utils.bulk_update_group_permissions(
            instance, action, "add_channel_relation", pk_set
        )


@receiver(m2m_changed, sender=Activity.user_view_permission.through)
def update_user_view_activity_permission(
    instance: Activity,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove add_channel_relation permission for users."""

    if action in ["post_add", "post_remove", "post_clear"]:
        if action == "post_remove":
            # make sure the leaders still have access
            leader_pks = instance.leaders.values_list("user__pk", flat=True)
            pk_set = [pk for pk in pk_set if pk not in leader_pks]

        utils.bulk_update_user_permissions(
            instance, action, "add_channel_relation", pk_set
        )

        if action == "post_clear":
            # assign view permissions for the leaders
            for leader in instance.leaders.filter(user__isnull=False):
                assign_perm("view_activity", leader.user, instance)  # type: ignore


@receiver(m2m_changed, sender=Activity.group_view_permission.through)
def update_group_view_activity_permission(
    instance: Activity,
    action: str,
    pk_set: list[int],
    **kwargs,
):
    """Add or remove add_channel_relation permission for groups."""

    if action in ["post_add", "post_remove", "post_clear"]:
        if action == "post_remove":
            # make sure the members still have access
            pk_set = [pk for pk in pk_set if pk != instance.activitygroup.pk]

        utils.bulk_update_group_permissions(
            instance, action, "add_channel_relation", pk_set
        )

        if action == "post_clear":
            # make sure that the the activity group still has access
            assign_perm("view_activity", instance.activitygroup, instance)
