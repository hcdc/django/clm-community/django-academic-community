# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from aldryn_apphooks_config.app_base import CMSConfigApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import gettext as _

from academic_community.activities.cms_appconfig import ActivitiesConfig


@apphook_pool.register
class ActivitiesApphook(CMSConfigApp):
    app_name = "activities"
    name = _("Working groups")
    app_config = ActivitiesConfig

    def get_urls(self, page=None, language=None, **kwargs):
        return ["academic_community.activities.urls"]
