# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from aldryn_apphooks_config.admin import BaseAppHookConfig, ModelAppHookConfig
from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin
from django.db.models import Count
from djangocms_versioning.models import Version
from guardian.admin import GuardedModelAdmin
from reversion_compare.admin import CompareVersionAdmin

from academic_community import utils
from academic_community.activities import models
from academic_community.admin import ManagerAdminMixin
from academic_community.channels.admin import RelatedMentionLinkAdmin


@admin.register(models.Category)
class CategoryAdmin(ManagerAdminMixin, admin.ModelAdmin):
    pass


@admin.register(models.ActivitiesConfig)
class ActivitiesConfigAdmin(BaseAppHookConfig, admin.ModelAdmin):
    pass


@admin.register(models.Activity)
class ActivityAdmin(
    ManagerAdminMixin,
    GuardedModelAdmin,
    ModelAppHookConfig,
    CompareVersionAdmin,
):
    """An admin for the Activity model."""

    list_filter = ["category", "start_date", "end_date"]

    list_display = [
        "name",
        "abbreviation",
        "category",
        "start_date",
        "end_date",
    ]

    filter_horizontal = [
        "leaders",
        "members",
        "former_members",
        "user_add_material_relation_permission",
        "group_add_material_relation_permission",
        "user_add_channel_relation_permission",
        "group_add_channel_relation_permission",
        "user_view_permission",
        "group_view_permission",
    ]

    search_fields = [
        "name",
        "abbreviation",
        "leaders__first_name",
        "leaders__last_name",
        "leaders__email__email",
    ]

    def get_changeform_initial_data(self, request):
        ret = super().get_changeform_initial_data(request)
        if "group_view_permission" not in ret:
            ret["group_view_permission"] = utils.get_groups("MEMBERS")
        return ret

    def can_change_content(
        self, request, content_obj: models.ActivitiesConfig
    ) -> bool:
        """Returns True if user can change content_obj"""
        version = Version.objects.get_for_content(content_obj)
        return version.check_modify.as_bool(request.user)


admin.register(models.ActivityGroup)(GroupAdmin)


@admin.register(models.ActivityMaterialRelation)
class ActivityMaterialRelationAdmin(ManagerAdminMixin, admin.ModelAdmin):
    """An admin for :model:`activities.ActivityMaterialRelation`"""

    search_fields = [
        "material__name",
        "activity__name",
        "activity__abbreviation",
    ]

    list_display = ["material", "activity"]

    list_filter = [
        "material__date_created",
        "material__last_modification_date",
    ]


@admin.register(models.ActivityChannelRelation)
class ActivityChannelRelationAdmin(ManagerAdminMixin, admin.ModelAdmin):
    """An admin for :model:`activities.ActivityChannelRelation`"""

    search_fields = [
        "channel__name",
        "channel__body",
        "activity__name",
        "activity__abbreviation",
    ]

    list_display = [
        "channel",
        "activity",
    ]

    list_filter = [
        "channel__date_created",
        "channel__last_modification_date",
        "channel__last_comment_modification_date",
    ]


@admin.register(models.ActivityChannelGroup)
class ActivityChannelGroupAdmin(admin.ModelAdmin):
    """An admin for a channel group."""

    search_fields = [
        "name",
        "slug",
        "activity__name",
        "activity__abbreviation",
        "user__username",
        "user__first_name",
        "user__last_name",
    ]

    list_display = ["name", "activity", "user", "get_channel_count"]

    @admin.display(description="Channels")
    def get_channel_count(self, obj: models.ChannelGroup):
        return str(obj.channels.count())


@admin.register(models.ActivityMentionLink)
class ActivityMentionLinkAdmin(RelatedMentionLinkAdmin):
    """An admin for an activity mention link."""

    search_fields = [
        "related_model__abbreviation",
        "related_model__name",
    ]

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .annotate(
                subscriber_count=Count("related_model__activitygroup__user")
            )
        )

    @admin.display(ordering="subscriber_count")
    def subscribers(self, obj: models.ActivityMentionLink):  # type: ignore
        return obj.subscriber_count  # type: ignore
