"""Urls of the :mod:`institutions` app."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from django.urls import include, path

from academic_community.activities import views

app_name = "activities"

urlpatterns = [
    path(
        "<slug>/",
        views.ActivityDetail.as_view(),
        name="activity-detail",
    ),
    path(
        "<slug>/uploads/",
        include(views.ActivityMaterialRelationViewSet().urls),
    ),
    path(
        "<slug>/channels/",
        include(views.ActivityChannelRelationViewSet().urls),
    ),
    path("<slug>/edit/", views.ActivityUpdate.as_view(), name="edit-activity"),
    path(
        "<slug>/join/", views.JoinActivityView.as_view(), name="join-activity"
    ),
    path(
        "<slug>/leave/",
        views.LeaveActivityView.as_view(),
        name="leave-activity",
    ),
]
