"""Plugins for activities"""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2


from typing import Any, Dict, List, Optional

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib.auth.models import Group
from django.db.models import Exists, OuterRef, Q, QuerySet
from django.urls import reverse
from django.utils.translation import gettext as _
from guardian.shortcuts import get_anonymous_user, get_objects_for_user

from academic_community import utils
from academic_community.activities import filters, models
from academic_community.channels.cms_plugins import ChannelListPublisherBase
from academic_community.channels.models import Channel
from academic_community.cms_plugins import (
    ActiveFiltersPublisher,
    FilterButtonPublisher,
    FilterListPublisher,
    PaginationPublisherMixin,
)
from academic_community.events.cms_plugins import EventListPublisherBase
from academic_community.events.models import Event
from academic_community.institutions.cms_plugins import (
    AcademicOrganizationListPublisherBase,
)
from academic_community.institutions.models import AcademicOrganization
from academic_community.members.cms_plugins import (
    CommunityMembersListPublisherBase,
)
from academic_community.members.models import CommunityMember
from academic_community.topics.cms_plugins import TopicListPublisherBase
from academic_community.topics.models import Topic
from academic_community.uploaded_material.cms_plugins import (
    AddMaterialButtonPublisher,
    MaterialListPublisher,
)
from academic_community.uploaded_material.models import Material


class ActivityPluginModelMixin:
    """Abstract base model for activity related plugins."""

    def get_activity(
        self, context, instance: models.ActivityPluginModelMixin
    ) -> Optional[models.Activity]:
        if instance.activity:  # type: ignore
            return instance.activity  # type: ignore
        elif instance.from_url:  # type: ignore
            try:
                view = context["view"]
                slug = view.kwargs["slug"]
            except KeyError:
                return None
            try:
                return models.Activity.objects.get(abbreviation=slug)
            except models.Activity.DoesNotExist:
                return None
        else:
            return None


@plugin_pool.register_plugin
class ActivityActiveFiltersPublisher(ActiveFiltersPublisher):
    """A plugin to display the applied filters for activities."""

    module = _("Working groups")
    cache = False

    name = _("Display active filters for %(working_groups)s") % {
        "working_groups": _("Working Groups")
    }

    filterset_class = filters.ActivityFilterSet

    list_model = models.Activity


@plugin_pool.register_plugin
class ActivityFilterButtonPublisher(FilterButtonPublisher):
    """A plugin to display the applied filters for activities."""

    module = _("Working groups")
    cache = False

    name = _("Filter button for %(working_groups)s") % {
        "working_groups": _("Working Groups")
    }

    filterset_class = filters.ActivityFilterSet

    list_model = models.Activity


class ActivityListPublisherBase(PaginationPublisherMixin, FilterListPublisher):
    """A plugin to display material to the user."""

    module = _("Working groups")
    render_template = "activities/components/activity_listplugin.html"
    cache = False

    filterset_class = filters.ActivityFilterSet

    list_model = models.Activity

    context_object_name = "activities"

    def get_query(
        self, instance: models.AbstractActivityListPluginModel
    ) -> Optional[Q]:
        """Get the query for the members."""
        query = None

        queries: List[Q] = []

        if instance.show_active and not instance.show_finished:
            queries.append(Q(end_date__isnull=True))
        elif instance.show_finished and not instance.show_active:
            queries.append(Q(end_date__isnull=False))
        categories = instance.categories.all()
        if categories:
            queries.append(
                Q(category__pk__in=categories.values_list("pk", flat=True))
            )
        if instance.app_config:
            queries.append(Q(app_config=instance.app_config))
        for q in queries:
            query = q if query is None else (query & q)  # type: ignore[operator]
        return query

    def filter_list_queryset(
        self,
        context,
        instance: models.AbstractActivityListPluginModel,  # type: ignore[override]
        queryset: QuerySet[models.Activity],
    ) -> QuerySet[models.Activity]:
        queryset = super().filter_list_queryset(context, instance, queryset)
        query = self.get_query(instance)
        if query is not None:
            queryset = queryset.filter(query)
        if instance.check_permissions:
            user = context["request"].user
            if user.is_anonymous:
                user = get_anonymous_user()
            queryset = get_objects_for_user(user, "view_activity", queryset)
        return queryset

    def render(
        self,
        context,
        instance: models.AbstractActivityListPluginModel,  # type: ignore[override]
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        context["show_badges"] = instance.display_badges
        context["base_id"] = f"activitylist-plugin-{instance.pk}-"
        if not instance.display_badges:
            context["truncate"] = instance.truncate_abstract
            context["card_class"] = instance.card_class
            context["show"] = not instance.collapsed
            if instance.card_template:
                context["activity_card_template"] = instance.card_template
        return context


@plugin_pool.register_plugin
class ActivityListPublisher(ActivityListPublisherBase):
    """A plugin to display selected activities"""

    name = _("Selected %(working_groups)s") % {
        "working_groups": _("Working Groups")
    }
    model = models.ActivityListPluginModel

    filter_horizontal = ["activities", "categories"]

    def get_full_queryset(
        self, context, instance: models.ActivityListPluginModel
    ):
        """Get the activities to display"""
        if instance.show_all:
            return super().get_full_queryset(context, instance)
        else:
            return instance.activities.all()


@plugin_pool.register_plugin
class ActivityMembersListPublisher(
    ActivityPluginModelMixin, CommunityMembersListPublisherBase
):
    """A publisher to display the members of a working group."""

    model = models.ActivityMembersListPluginModel
    module = _("Working groups")
    name = _("Members of a %(working_group)s") % {
        "working_group": _("working group")
    }

    autocomplete_fields = ["activity"]

    def get_full_queryset(
        self, context, instance: models.ActivityMembersListPluginModel
    ) -> QuerySet[CommunityMember]:
        activity = self.get_activity(context, instance)
        if activity is None:
            return CommunityMember.objects.none()
        if instance.leaders_only:
            return activity.leaders.all()
        else:
            return activity.real_members


@plugin_pool.register_plugin
class ActivityAbstractPublisher(ActivityPluginModelMixin, CMSPluginBase):
    model = models.ActivityAbstractPluginModel
    module = _("Working groups")
    render_template = "activities/components/activity_abstract_plugin.html"
    name = _("Abstract of a %(working_group)s") % {
        "working_group": _("working group")
    }
    cache = True

    text_enabled = True

    autocomplete_fields = ["activity"]

    def render(
        self,
        context,
        instance: models.ActivityAbstractPluginModel,
        placeholder,
    ):
        context["activity"] = self.get_activity(context, instance)
        context["truncate"] = instance.truncate
        return context


@plugin_pool.register_plugin
class ActivitySidebarPublisher(ActivityPluginModelMixin, CMSPluginBase):
    model = models.ActivitySidebarPluginModel
    module = _("Working groups")
    render_template = "activities/components/activity_sidebar.html"
    name = _("Sidebar of a %(working_group)s detail page") % {
        "working_group": _("working group")
    }

    autocomplete_fields = ["activity"]

    def render(
        self,
        context,
        instance: models.ActivitySidebarPluginModel,
        placeholder,
    ):
        context["activity"] = self.get_activity(context, instance)
        return context


@plugin_pool.register_plugin
class ActivityCardPublisher(ActivityPluginModelMixin, CMSPluginBase):
    model = models.ActivityCardPluginModel
    module = _("Working groups")
    render_template = "activities/components/activity_card.html"
    name = _("%(working_group)s card") % {"working_group": _("Working group")}
    cache = True

    autocomplete_fields = ["activity"]

    def render(
        self,
        context,
        instance: models.ActivityCardPluginModel,
        placeholder,
    ):
        context["activity"] = self.get_activity(context, instance)
        context["truncate"] = instance.truncate_abstract
        context["card_class"] = instance.card_class
        context["show"] = not instance.collapsed
        if instance.card_template:
            context["activity_card_template"] = instance.card_template
        return context


@plugin_pool.register_plugin
class ActivityBadgesPublisher(ActivityPluginModelMixin, CMSPluginBase):
    model = models.ActivityBadgesPluginModel
    module = _("Working groups")
    render_template = "activities/components/activity_badges_plugin.html"
    name = _("Stats (Badges) of a %(working_group)s") % {
        "working_group": _("working group")
    }
    cache = True

    autocomplete_fields = ["activity"]

    def render(
        self,
        context,
        instance: models.ActivityBadgesPluginModel,
        placeholder,
    ):
        context["activity"] = self.get_activity(context, instance)
        context["instance"] = instance
        return context


@plugin_pool.register_plugin
class ActivityEventListPublisher(
    ActivityPluginModelMixin, EventListPublisherBase
):
    """A publisher to display the members of a working group."""

    model = models.ActivityEventListPluginModel
    module = _("Working groups")
    name = _("Events of a %(working_group)s") % {
        "working_group": _("working group")
    }

    autocomplete_fields = ["activity"]

    def get_full_queryset(
        self, context, instance: models.ActivityEventListPluginModel
    ):
        """Get the activities to display"""
        activity = self.get_activity(context, instance)
        if activity is None:
            return Event.objects.none()
        return activity.event_set.all()  # type: ignore


@plugin_pool.register_plugin
class ActivityMaterialListPublisher(
    ActivityPluginModelMixin, MaterialListPublisher
):
    """A plugin to display activity material to the user."""

    model = models.ActivityMaterialListPluginModel
    module = _("Working groups")
    name = _("%(working_group)s related material") % {
        "working_group": _("Working group")
    }

    autocomplete_fields = ["activity"]

    def get_full_queryset(
        self, context, instance: models.ActivityMaterialListPluginModel
    ) -> QuerySet[Material]:
        user = context["request"].user
        activity = self.get_activity(context, instance)
        if activity is None:
            return Material.objects.none()
        if not utils.has_perm(
            user, "activities.view_activity", instance.activity
        ):
            return Material.objects.none()
        else:
            return Material.objects.all()

    def filter_list_queryset(
        self,
        context,
        instance: models.MaterialListPluginBaseModel,
        queryset: QuerySet[Material],
    ) -> QuerySet[Material]:
        queryset = super().filter_list_queryset(context, instance, queryset)
        members_group = utils.get_members_group()
        activity = self.get_activity(context, instance)
        if activity is None:
            return Material.objects.none()
        kws: Dict[str, Any] = dict(activitymaterialrelation__activity=activity)
        if instance.members_only_material is not None:
            query = Group.objects.filter(
                material_read__pk=OuterRef("pk"), pk=members_group.pk
            )
            queryset = queryset.annotate(available_to_members=Exists(query))
            kws["available_to_members"] = not instance.members_only_material
        return queryset.filter(**kws)

    def render(
        self,
        context,
        instance: models.ActivityMaterialListPluginModel,  # type: ignore[override]
        placeholder,
    ):
        context = super().render(context, instance, placeholder)
        activity = self.get_activity(context, instance)
        if activity:
            context["material_list_url"] = reverse(
                activity.app_config.namespace
                + ":activitymaterialrelation-list",
                args=(activity.abbreviation,),
            )
        context["material_relation_model"] = models.ActivityMaterialRelation
        return context


@plugin_pool.register_plugin
class AddActivityMaterialButtonPublisher(
    ActivityPluginModelMixin, AddMaterialButtonPublisher
):
    """A plugin to display activity material to the user."""

    model = models.AddActivityMaterialButtonPluginModel
    module = _("Working groups")
    name = _("%(working_group)s related material button") % {
        "working_group": _("Working group")
    }

    autocomplete_fields = ["activity"]

    def can_render_button(
        self, context, instance: models.AddActivityMaterialButtonPluginModel
    ) -> bool:
        activity = self.get_activity(context, instance)
        return (
            activity is not None
            and models.ActivityMaterialRelation.has_add_permission(
                context["request"].user,
                slug=activity.abbreviation,
            )
        )

    def get_create_url(
        self, context, instance: models.AddActivityMaterialButtonPluginModel
    ) -> str:
        activity = self.get_activity(context, instance)
        if activity is not None:
            return models.ActivityMaterialRelation.get_create_url_from_kwargs(
                slug=activity.abbreviation
            )
        else:
            return ""

    def get_url_params(
        self, context, instance: models.AddActivityMaterialButtonPluginModel
    ) -> Dict[str, Any]:
        ret = super().get_url_params(context, instance)
        ret["members_only"] = instance.members_only
        return ret


@plugin_pool.register_plugin
class ActivityTopicListPublisher(
    ActivityPluginModelMixin, TopicListPublisherBase
):
    """A plugin to display community members of a working group."""

    name = _("%(topics)s of a %(working_group)s") % {
        "working_group": _("working group"),
        "topics": _("Topics"),
    }
    module = _("Working groups")
    model = models.ActivityTopicListPluginModel

    autocomplete_fields = ["activity"]

    def get_full_queryset(
        self, context, instance: models.ActivityTopicListPluginModel
    ) -> QuerySet[Topic]:
        activity = self.get_activity(context, instance)
        if activity is None:
            return Topic.objects.none()
        return Topic.objects.filter(
            metadata__activities__p_keys__contains=activity.pk
        )


@plugin_pool.register_plugin
class ActivityAcademicOrganizationListPublisher(
    ActivityPluginModelMixin, AcademicOrganizationListPublisherBase
):
    """A plugin to display organizations involved in a working group."""

    name = _("Organizations of a %(working_group)s") % {
        "working_group": _("working group")
    }
    module = _("Working groups")
    model = models.ActivityAcademicOrganizationListPluginModel

    autocomplete_fields = ["activity"]

    def get_full_queryset(
        self,
        context,
        instance: models.ActivityAcademicOrganizationListPluginModel,
    ) -> QuerySet[AcademicOrganization]:
        activity = self.get_activity(context, instance)
        if activity is None:
            return AcademicOrganization.objects.none()
        return activity.organizations


@plugin_pool.register_plugin
class ActivityChannelListPublisher(
    ActivityPluginModelMixin, ChannelListPublisherBase
):
    """A plugin to display channels or a working group"""

    name = _("Channels of a %(working_group)s") % {
        "working_group": _("working group")
    }
    module = _("Working groups")
    model = models.ActivityChannelListPluginModel

    channel_relation_model = models.ActivityChannelRelation

    autocomplete_fields = ["activity"]

    def get_url_kws(
        self, context, instance: models.ActivityChannelListPluginModel
    ) -> Dict[str, str]:
        ret = super().get_url_kws(context, instance)
        if instance.activity:
            ret["slug"] = instance.activity.abbreviation
        elif instance.from_url:
            try:
                ret["slug"] = context["view"].kwargs["slug"]
            except (KeyError, AttributeError):
                pass
        return ret

    def get_full_queryset(
        self, context, instance: models.ActivityChannelListPluginModel
    ) -> QuerySet[Channel]:
        activity = self.get_activity(context, instance)
        if activity is None:
            return Channel.objects.none()

        query_name = (
            models.ActivityChannelRelation.channel.field.related_query_name()
        )
        related_permission_field = (
            models.ActivityChannelRelation.related_permission_field
        )

        attr = "%s__%s__pk" % (
            query_name,
            related_permission_field,
        )

        return Channel.objects.filter(**{attr: activity.pk})

    def render(
        self,
        context,
        instance: models.ActivityChannelListPluginModel,  # type: ignore[override]
        placeholder,
    ):
        ret = super().render(context, instance, placeholder)
        ret[
            models.ActivityChannelRelation.related_permission_field
        ] = self.get_activity(context, instance)
        ret["channel_relation_model"] = self.channel_relation_model
        return ret
