"""Filter sets for the community member views."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

import django_filters
from django.contrib.postgres.search import SearchVector

from academic_community.activities import models
from academic_community.filters import ActiveFilterSet


class ActivityFilterSet(ActiveFilterSet):
    """A filterset for topics."""

    internal_fields = ["start_date", "end_date", "members", "organizations"]

    class Meta:
        model = models.Activity
        fields = {
            "name": ["search"],
            "abbreviation": ["istartswith"],
            "abstract": ["search"],
            "start_date": ["range", "lte", "gte"],
            "end_date": ["range", "lte", "gte", "isnull"],
            "category": ["exact"],
        }

    start_date = django_filters.DateRangeFilter()
    end_date = django_filters.DateRangeFilter()

    o = django_filters.OrderingFilter(
        label="Order by",
        fields=(
            ("name", "name"),
            ("abbreviation", "abbreviation"),
            ("start_date", "start_date"),
            ("end_date", "end_date"),
        ),
        field_labels={
            "name": "Name",
            "abbreviation": "Abbreviation",
            "start_date": "Start date",
            "end_date": "End date",
        },
    )

    members = django_filters.CharFilter(
        method="filter_members",
        label="Activities with the following members",
        distinct=True,
    )

    organizations = django_filters.CharFilter(
        method="filter_organizations",
        field_name="members",
        label="Activities with these organizations",
        distinct=True,
    )

    @property
    def form(self):
        form = super().form
        form.template_name = "activities/components/activity_filter_form.html"
        return form

    def filter_members(self, queryset, name, value):
        return (
            queryset.annotate(
                search=SearchVector(
                    "members__first_name", "members__last_name"
                )
            )
            .filter(search=value)
            .order_by("pk")
            .distinct("pk")
        )

    def filter_organizations(self, queryset, name, value):
        return (
            queryset.annotate(
                search=SearchVector(
                    "members__membership__name",
                    "members__membership__unit__parent_department__name",
                    "members__membership__unit__parent_department__abbreviation",  # noqa: E501
                    "members__membership__unit__parent_department__parent_institution__name",  # noqa: E501
                    "members__membership__unit__parent_department__parent_institution__abbreviation",  # noqa: E501
                    "members__membership__department__parent_institution__name",  # noqa: E501
                    "members__membership__department__parent_institution__abbreviation",  # noqa: E501
                    "members__membership__institution__abbreviation",
                    "members__membership__department__abbreviation",
                    "members__membership__unit__abbreviation",
                )
            )
            .filter(search=value)
            .order_by("pk")
            .distinct("pk")
        )
