"""Permission tests."""

# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING, Callable

import reversion

if TYPE_CHECKING:
    from django.contrib.auth.models import User

    from academic_community.activities.models import Activity
    from academic_community.members.models import CommunityMember


def test_change_activity_lead(
    user_member_factory: Callable[[], CommunityMember],
    activity: Activity,
    member: CommunityMember,
):
    """Test changing the activity lead"""

    new_member = user_member_factory()

    user: User = member.user  # type: ignore
    new_user: User = new_member.user  # type: ignore

    for perm in ["change_activity_lead", "change_activity"]:
        assert user.has_perm(perm, activity)
        assert not new_user.has_perm(perm, activity)

    with reversion.create_revision():
        activity.leaders.remove(member)
        activity.leaders.add(new_member)

    for perm in ["change_activity_lead", "change_activity"]:
        assert not user.has_perm(perm, activity)
        assert new_user.has_perm(perm, activity)
