# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2
# SPDX-License-Identifier: EUPL-1.2

"""App settings
------------

This module defines the settings options for the
``django-academic-community`` app.
"""


from __future__ import annotations

from django.conf import settings  # noqa: F401
from django.utils.translation import gettext as _

#: Templates for activity cards
ACTIVITY_CARD_TEMPLATES = [
    ("activity_card", _("Default")),
    ("activity_logo_card", _("Simple card with logo")),
    ("activity_logo_title_card", _("Simple card with logo and title")),
] + getattr(
    settings,
    "ACTIVITY_CARD_TEMPLATES",
    [],
)
