# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from __future__ import annotations

from typing import TYPE_CHECKING, Any, Dict, List, Tuple

from django.contrib.auth.mixins import UserPassesTestMixin
from django.views import generic

from academic_community.activities import forms, models
from academic_community.channels.models.private_conversation import (
    PrivateConversation,
)
from academic_community.channels.views import (
    ChannelRelationInline,
    ChannelRelationViewSet,
)
from academic_community.faqs.views import FAQContextMixin
from academic_community.history.views import RevisionMixin
from academic_community.mixins import AppConfigMixin, PermissionCheckViewMixin
from academic_community.uploaded_material.views import (
    MaterialRelationInline,
    MaterialRelationViewSet,
)
from academic_community.utils import PermissionRequiredMixin
from academic_community.views import register_object_page_content_renderer

if TYPE_CHECKING:
    from academic_community.channels.forms import ChannelRelationForm


class ActivityAppConfigMixin(AppConfigMixin):
    """An appconfig mixin for a generic view on the Activity model."""

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.namespace(self.namespace)


class ActivityDetail(
    ActivityAppConfigMixin, PermissionRequiredMixin, generic.DetailView
):
    """A view of a specific activity."""

    model = models.Activity

    slug_field = "abbreviation"

    permission_required = "activities.view_activity"

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["content"] = self.get_object().page.get_content()
        return context


def render_activity_content(
    request, objectpagecontent: models.ObjectPageContent
):
    activity: models.Activity = objectpagecontent.page.content_object  # type: ignore
    return ActivityDetailFrontendEditing.as_view()(
        request,
        activity=activity,
        slug=activity.abbreviation,
        content=objectpagecontent.pk,
    )


register_object_page_content_renderer(models.Activity, render_activity_content)


class ActivityDetailFrontendEditing(ActivityDetail):
    """A detail view dedicated to the frontend editing."""

    template_name_suffix = "_detail_cms"

    def get_object(self, queryset=None):
        if "activity" in self.kwargs:
            ret = self.kwargs["activity"]
            if ret.app_config:
                self.request.current_app = ret.app_config.namespace
        else:
            ret = super().get_object(queryset)
        self.request.toolbar.set_object(ret)
        return ret

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        if "content" in self.kwargs:
            context["content"] = self.kwargs["content"]
        else:
            context["content"] = self.get_object().page.get_content()
        return context


class ActivityUpdate(
    ActivityAppConfigMixin,
    RevisionMixin,
    FAQContextMixin,
    PermissionCheckViewMixin,
    PermissionRequiredMixin,
    generic.edit.UpdateView,
):
    permission_required = "activities.change_activity"

    model = models.Activity

    form_class = forms.ActivityForm

    slug_field = "abbreviation"


class JoinActivityView(
    ActivityAppConfigMixin,
    UserPassesTestMixin,
    RevisionMixin,
    generic.edit.UpdateView,
):
    """View to enter an activity."""

    model = models.Activity

    slug_field = "abbreviation"

    fields: list[str] = []

    template_name = "activities/activity_join_form.html"

    def test_func(self):
        """Test if the activity is invitation_only."""
        activity: models.Activity = self.get_object()
        return (
            hasattr(self.request.user, "communitymember")
            and self.request.user.communitymember.is_member
            and not activity.is_finished
            and not activity.invitation_only
        )

    def form_valid(self, form):
        self.object.members.add(self.request.user.communitymember)  # type: ignore
        return super().form_valid(form)


class LeaveActivityView(
    ActivityAppConfigMixin,
    UserPassesTestMixin,
    RevisionMixin,
    generic.edit.UpdateView,
):
    """View to enter an activity."""

    model = models.Activity

    slug_field = "abbreviation"

    fields: list[str] = []

    template_name = "activities/activity_leave_form.html"

    def test_func(self):
        """Test if the activity is invitation_only."""
        if not hasattr(self.request.user, "communitymember"):
            return False
        member = self.request.user.communitymember
        return (
            member.activities.filter(abbreviation=self.kwargs["slug"]).exists()
            or member.former_activity_set.filter(
                abbreviation=self.kwargs["slug"]
            ).exists()
        )

    def form_valid(self, form):
        self.object.members.remove(self.request.user.communitymember)  # type: ignore
        self.object.former_members.remove(self.request.user.communitymember)  # type: ignore
        return super().form_valid(form)


@models.ActivityMaterialRelation.registry.register_relation_inline
class ActivityMaterialRelationInline(MaterialRelationInline):
    model = models.ActivityMaterialRelation


class ActivityMaterialRelationViewSet(AppConfigMixin, MaterialRelationViewSet):
    """A viewset for activity material."""

    relation_model = models.ActivityMaterialRelation

    def get_breadcrumbs(self, request, **kwargs) -> List[Tuple[str, str]]:
        model = self.relation_model
        related_object: models.Activity = (
            model.get_related_permission_object_from_kws(**kwargs)
        )
        return [
            (
                related_object.abbreviation,
                related_object.get_absolute_url(),
            ),
        ]

    def get_context_data(self, request, **kwargs):
        model = self.relation_model
        context = super().get_context_data(request, **kwargs)
        related_object: models.Activity = (
            model.get_related_permission_object_from_kws(**kwargs)
        )
        if related_object.app_config:
            context["namespace"] = related_object.app_config.namespace
        return context


@ChannelRelationInline.registry.register_relation_inline
class ActivityChannelRelationInline(ChannelRelationInline):
    model = models.ActivityChannelRelation


@ChannelRelationInline.registry.register_relation_hook(
    PrivateConversation, models.ActivityChannelRelation
)
def disable_subscribe_members(form: ChannelRelationForm):
    form.fields["subscribe_members"].initial = False
    form.disable_field(
        "subscribe_members",
        " <i>This option is not available for private conversations.</i>",
    )


class ActivityChannelRelationViewSet(AppConfigMixin, ChannelRelationViewSet):
    """A viewset for activity channels."""

    relation_model = models.ActivityChannelRelation

    def get_breadcrumbs(self, request, **kwargs) -> List[Tuple[str, str]]:
        model = self.relation_model
        related_object: models.Activity = (
            model.get_related_permission_object_from_kws(**kwargs)
        )
        return [
            (
                related_object.abbreviation,
                related_object.get_absolute_url(),
            ),
        ]

    def get_context_data(self, request, **kwargs):
        model = self.relation_model
        context = super().get_context_data(request, **kwargs)
        related_object: models.Activity = (
            model.get_related_permission_object_from_kws(**kwargs)
        )
        if related_object.app_config:
            context["namespace"] = related_object.app_config.namespace
        return context
