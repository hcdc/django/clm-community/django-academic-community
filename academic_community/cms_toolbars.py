# Copyright (C) 2020-2021 Helmholtz-Zentrum Geesthacht
# Copyright (C) 2021 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from cms.extensions.toolbar import ExtensionToolbar
from cms.toolbar_pool import toolbar_pool
from django.utils.translation import gettext_lazy as _

from academic_community import models


@toolbar_pool.register
class PageStylesExtensionToolbar(ExtensionToolbar):
    # defines the model for the current toolbar
    model = models.PageStylesExtension

    def populate(self):
        # setup the extension toolbar with permissions and sanity checks
        if self.request.current_page:
            current_page_menu = self._setup_extension_toolbar()
        else:
            current_page_menu = None

        # if it's all ok
        if current_page_menu and self.toolbar.edit_mode_active:
            # retrieves the instance of the current extension (if any) and the toolbar item URL
            url = self.get_page_extension_admin()[1]
            if url:
                sub_menu = self._get_sub_menu(
                    current_page_menu,
                    "further_page_settings",
                    "Further page settings",
                    position=0,
                )
                # adds a toolbar item in position 0 (at the top of the menu)
                sub_menu.add_modal_item(
                    _("Styles"),
                    url=url,
                    disabled=not self.request.user.has_perm(
                        "academic_community.change_pagestylesextension"
                    ),
                    position=0,
                )


@toolbar_pool.register
class PageMenuExtensionToolbar(ExtensionToolbar):
    # defines the model for the current toolbar
    model = models.PageMenuExtension

    def populate(self):
        # setup the extension toolbar with permissions and sanity checks
        if self.request.current_page:
            current_page_menu = self._setup_extension_toolbar()
        else:
            current_page_menu = None

        # if it's all ok
        if current_page_menu and self.toolbar.edit_mode_active:
            # retrieves the instance of the current extension (if any) and the toolbar item URL
            url = self.get_page_extension_admin()[1]
            if url:
                sub_menu = self._get_sub_menu(
                    current_page_menu,
                    "further_page_settings",
                    "Further page settings",
                    position=0,
                )
                # adds a toolbar item in position 0 (at the top of the menu)
                sub_menu.add_modal_item(
                    _("Menu options"),
                    url=url,
                    disabled=not self.request.user.has_perm(
                        "academic_community.change_pagemenuextension"
                    ),
                    position=0,
                )
