# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

.PHONY: clean clean-build clean-pyc clean-test clean-database coverage dist docs help install lint lint/flake8 lint/black database
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys

from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test clean-npm clean-venv clean-database ## remove all build, test, coverage and Python artifacts

clean-database: ## remove the database
	rm -rf db.sqlite3

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-npm: ## remove node_modules
	rm -fr node_modules/

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

clean-venv:  # remove the virtual environment
	rm -rf venv

lint/isort: ## check style with flake8
	isort --skip migrations --check academic_community tests
lint/flake8: ## check style with flake8
	flake8 --exclude migrations academic_community tests
lint/black: ## check style with black
	black --exclude migrations --check academic_community tests
	blackdoc --exclude migrations --check academic_community tests
lint/reuse:  ## check licenses
	reuse lint

lint: lint/isort lint/black lint/flake8 lint/reuse  ## check style

formatting:
	isort --skip migrations academic_community tests
	black --exclude migrations academic_community tests
	blackdoc --exclude migrations academic_community tests

quick-test: ## run tests quickly with the default Python
	python -m pytest

test: ## run tox
	tox

test-all: test test-docs ## run tests and test the docs

coverage: ## check code coverage quickly with the default Python
	python -m pytest --cov academic_community --cov-report=html
	$(BROWSER) htmlcov/index.html

docs: ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

test-docs: ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs linkcheck

servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

release: dist ## package and upload a release
	twine upload dist/*

dist: clean-build ## builds source and wheel package
	django-admin compilemessages
	python -m build
	ls -l dist

SCSS_FOLDER=$(shell find academic_community -name scss)

css:  ## build the css file from the scss files
	sass -I node_modules/ $(foreach folder,$(SCSS_FOLDER),-I $(folder)/..) academic_community/static/css/main.scss academic_community/static/css/main.css
	sass -I node_modules/ $(foreach folder,$(SCSS_FOLDER),-I $(folder)/..) academic_community/static/css/filtered-multiple-select-overrides.scss academic_community/static/css/filtered-multiple-select-overrides.css

js:  ## build the JS bundles
	npm run build
	npm run build-emoji
	npm run build-popover

install: clean-pyc clean-test ## install the package to the active Python's site-packages
	python -m pip install .

database: clean-database ## create a new database
	python manage.py migrate

dev-install: clean-pyc clean-test
	python -m pip install -r docs/requirements.txt
	python -m pip install -e .[dev]
	-npm install
	pre-commit install

venv-install: clean
	python -m venv venv
	venv/bin/python -m pip install -r docs/requirements.txt
	venv/bin/python -m pip install -e .[dev]
	venv/bin/pre-commit install
