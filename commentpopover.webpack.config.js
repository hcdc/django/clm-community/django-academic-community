// SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: CC0-1.0

'use strict';



const webpack = require('webpack');
const path = require('path');

const config = {
  entry: path.resolve( __dirname, 'academic_community/channels/static/js/src/commentpopover.js' ),
  output: {
    path: path.resolve( __dirname, 'academic_community/channels/static/commentpopover/dist' ),
    filename: 'bundle.js',
	  libraryTarget: 'umd',
	  libraryExport: 'default'
  },
  externals: {
    $: "jquery",
    jQuery: "jquery"
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
  }
};

module.exports = config;
