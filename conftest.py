# SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import datetime as dt
import os
from itertools import count
from typing import Any, Callable, Dict, Optional, Tuple, Union, cast

import pytest
import reversion
from cms.utils.permissions import set_current_user
from django.contrib.auth.models import Group, User
from django.core.cache import cache
from django.utils import timezone
from menus.menu_pool import menu_pool
from psycopg2.extras import DateTimeTZRange

from academic_community import utils
from academic_community.activities.models import Activity, Category
from academic_community.institutions.models import (
    AcademicMembership,
    AcademicOrganization,
    Unit,
)

# import fixtures from academic_community.institutions.tests.test_models
from academic_community.institutions.tests.test_models import (  # noqa: F401
    city,
    country,
    department,
    institution,
    unit,
)
from academic_community.members.models import CommunityMember
from academic_community.tests import (  # noqa: F401
    LazyFixture,
    _resolve_lazy_fixture,
    is_lazy_fixture,
    lazy_fixture,
)
from academic_community.topics.models import Keyword, Topic, TopicMembership


@pytest.fixture(autouse=True)
def use_test_urls(request, settings, db, create_default_cms_pages):
    settings.ROOT_URLCONF = "testproject._test_urls"


@pytest.fixture(scope="session")
def create_default_cms_pages(django_db_blocker, django_db_setup):
    """A fixture to create the default django CMS pages."""
    from cms.utils.apphook_reload import reload_urlconf
    from django.core.management import call_command

    with django_db_blocker.unblock():
        call_command("default_content", "-a")
        reload_urlconf()


def pytest_make_parametrize_id(
    config: pytest.Config,
    val: object,
    argname: str,
) -> Optional[str]:
    """Inject lazy fixture parametrized id.

    Args:
        config (pytest.Config): pytest configuration.
        value (object): fixture value.
        argname (str): automatic parameter name.

    Returns:
        str: new parameter id.
    """
    if is_lazy_fixture(val):
        return cast(LazyFixture, val).name
    return None


@pytest.hookimpl(tryfirst=True)
def pytest_fixture_setup(
    fixturedef: pytest.FixtureDef,
    request: pytest.FixtureRequest,
) -> Optional[Any]:
    """Lazy fixture setup hook.

    This hook will never take over a fixture setup but just simply will
    try to resolve recursively any lazy fixture found in request.param.

    Reference:
    - https://bit.ly/3SyvsXJ

    Args:
        fixturedef (pytest.FixtureDef): fixture definition object.
        request (pytest.FixtureRequest): fixture request object.

    Returns:
        object | None: fixture value or None otherwise.
    """
    if hasattr(request, "param") and request.param:
        request.param = _resolve_lazy_fixture(request.param, request)
    return None


@pytest.fixture
def member_factory(db) -> Callable[[], CommunityMember]:
    """A factory for creating community members."""

    counter = count()

    def factory():
        i = next(counter)
        with reversion.create_revision():
            return CommunityMember.objects.create(
                first_name=f"Max{i}",
                last_name=f"Mustermann{i}",
                is_member=True,
            )

    return factory


@pytest.fixture
def dummy_password() -> str:
    return "some_password"


@pytest.fixture(autouse=True)
def teardown_cms(db):
    """Fixture to teardown the CMS stuff.

    In a standard django test case setting, this would be done in the
    :attr:`cms.test_utils.testcase.BaseCMSTestCase._post_teardown` method, see
    https://github.com/django-cms/django-cms/blob/73cbbdb00ea0af9ecfc102fd8394102d6a01c899/cms/test_utils/testcases.py#L119
    """
    yield
    menu_pool.clear()
    cache.clear()
    set_current_user(None)


@pytest.fixture
def user_member_factory(
    db, member_factory, django_user_model, dummy_password
) -> Callable[[], CommunityMember]:
    def factory():
        with reversion.create_revision():
            member = member_factory()
            user = django_user_model.objects.create(
                username=f"{member.first_name}_{member.last_name}",
                password=dummy_password,
            )
            user.set_password(dummy_password)
            member.user = user
            member.save()
        return member

    return factory


@pytest.fixture
def member(db, user_member_factory) -> CommunityMember:
    """Create a member in the community."""
    return user_member_factory()


@pytest.fixture
def authenticated_client(member: CommunityMember, dummy_password: str):
    """Authenticate a client."""
    from django.test.client import Client

    client = Client()
    user: User = member.user  # type: ignore
    client.force_login(user)
    yield client
    client.logout()


@pytest.fixture(
    params=[
        lazy_fixture("institution"),
        lazy_fixture("department"),
        lazy_fixture("unit"),
    ]
)
def organization(request) -> AcademicOrganization:
    return request.param


@pytest.fixture
def membership(
    member: CommunityMember, organization: AcademicOrganization
) -> AcademicMembership:
    """Create a membership of a member within the community."""
    with reversion.create_revision():
        return AcademicMembership.objects.create(
            member=member, organization=organization
        )


@pytest.fixture
def category(db) -> Category:
    """Create a test category."""
    return Category.objects.create(name="Test Category")


@pytest.fixture
def activity(category: Category, member: CommunityMember) -> Activity:
    """Create a test activity."""
    with reversion.create_revision():
        ret = Activity.objects.create(
            name="Test activity",
            abbreviation="Test",
            category=category,
            start_date=dt.date.today(),
        )
        ret.leaders.add(member)
    return ret


@pytest.fixture
def default_group(db) -> Group:
    """Create the default Group for registered users."""
    return utils.get_default_group()


@pytest.fixture
def members_group(db) -> Group:
    """Create the default group for community members."""
    return utils.get_members_group()


@pytest.fixture
def keyword(db) -> Keyword:
    """Create a topic keyword."""
    return Keyword.objects.create(name="Test keyword")


@pytest.fixture
def topic(
    member: CommunityMember,
    unit: Unit,  # noqa: F811
) -> Topic:
    """Create a test topic."""
    with reversion.create_revision():
        return Topic.objects.create(
            name="Test topic",
            leader=member,
            lead_organization=unit,
            id_name="HZG-999",
        )


@pytest.fixture
def topic_membership(topic: Topic, member: CommunityMember) -> TopicMembership:
    """Register a member to a test topic."""
    with reversion.create_revision():
        return TopicMembership.objects.create(
            topic=topic,
            member=member,
            approved=True,
            start_date=dt.datetime.now() - dt.timedelta(days=1),
        )


@pytest.fixture
def topic_membership_factory(
    topic: Topic, user_member_factory: Callable[[], CommunityMember]
) -> Callable[[], TopicMembership]:
    """Register a member to a test topic."""

    def factory():
        with reversion.create_revision():
            return TopicMembership.objects.create(
                topic=topic,
                member=user_member_factory(),
                approved=True,
                start_date=dt.datetime.now() - dt.timedelta(days=1),
            )

    return factory


@pytest.fixture
def current_range() -> DateTimeTZRange:
    return DateTimeTZRange(
        timezone.now() - datetime.timedelta(hours=1),
        timezone.now() + datetime.timedelta(hours=1),
    )


@pytest.fixture
def past_range() -> DateTimeTZRange:
    return DateTimeTZRange(
        timezone.now() - datetime.timedelta(hours=2),
        timezone.now() - datetime.timedelta(hours=1),
    )


@pytest.fixture
def upcoming_range() -> DateTimeTZRange:
    return DateTimeTZRange(
        timezone.now() + datetime.timedelta(hours=1),
        timezone.now() + datetime.timedelta(hours=2),
    )


@pytest.fixture
def future_time() -> dt.datetime:
    return timezone.now() + datetime.timedelta(hours=1)


@pytest.fixture
def past_time() -> dt.datetime:
    return timezone.now() - datetime.timedelta(hours=1)


if (
    os.getenv("RUNNING_TESTS")
    and os.getenv("POSTGRES_DB") == "ACADEMIC_COMMUNITY_TEST_DB"
):

    @pytest.fixture(scope="session")
    def django_db_setup():
        pass


@pytest.fixture
def url_kwargs(request) -> Dict[str, Union[str, int]]:
    """Get the kwargs for a URL"""
    ret: Dict[str, Union[str, int]] = {}
    fixtures: Dict[str, Union[str, Tuple[str, str]]] = request.param
    for key, params in fixtures.items():
        fixture: str
        attr: str
        if isinstance(params, str):
            fixture, attr = params, "pk"
        else:
            fixture, attr = params
        value = request.getfixturevalue(fixture)
        for subattr in attr.split("."):
            value = getattr(value, subattr)
        ret[key] = value
    return ret
