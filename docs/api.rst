.. SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _api:

API Reference
=============

.. toctree::

    api/academic_community
