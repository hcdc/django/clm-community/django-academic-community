.. SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _installation:

Installation
============

To install the `django-academic-community` package for your Django
project, you need to follow two steps:

1. :ref:`Install the package <install-package>`
2. :ref:`Add the app to your Django project <install-django-app>`

Note that this app requires a postgres database!


.. _install-package:

Installation from PyPi
----------------------
The recommended way to install this package is via pip and PyPi via::

    pip install django-academic-community

or, if you are using `pipenv`, via::

    pipenv install django-academic-community

Or install it directly from `the source code repository on Gitlab`_ via::

    pip install git+https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community.git

The latter should however only be done if you want to access the development
versions (see :ref:`install-develop`).


.. _install-django-app:

Install the Django App for your project
---------------------------------------
To use the `django-academic-community` package in your Django project,
you need to add the app to your ``INSTALLED_APPS``, configure your ``urls.py``, run
the migration, add a login button in your templates. Here are the step-by-step
instructions:

1. Extend your ``INSTALLED_APPS`` of your ``settings.py`` with the
   :attr:`academic_community.INSTALLED_APPS`

   .. code-block:: python

       # in settings.py
       import academic_community

       INSTALLED_APPS = academic_community.INSTALLED_APPS + [
           # whatever you want to add
       ]
2. in your projects urlconf (see :setting:`ROOT_URLCONF`), add include
   :mod:`academic_community.urls` via::

       from django.urls import include, path

       urlpatterns += [
           path("", include("academic_community.urls")),
        ]
3. Run ``python manage.py migrate`` to add models to your database
4. Run ``python manage.py set_default_permissions`` to set the default
   permissions for community managers, members and cms editors.
5. Configure the app to your needs (see :ref:`configuration`) (you can also
   have a look into the ``testproject`` folder in the `source code repository`_
   for a possible configuration)

   Please make sure that you have an ``asgi.py`` file as in the
   ``testproject`` and set the ``ASGI_APPLICATION`` variable in your
   ``settings.py``.
   See https://channels.readthedocs.io/en/stable/installation.html
6. Now you should create the default CMS-Pages for simplicity via
   ``python manage.py default_content -a``


If you need a deployment-ready django setup for an app like this, please
have a look at the following template:
https://codebase.helmholtz.cloud/hcdc/software-templates/djac-docker-template


That's it!

.. _the source code repository on Gitlab: https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community


.. _install-develop:

Installation for development
----------------------------
Please head over to our :ref:`contributing guide <contributing>` for
installation instruction for development.
