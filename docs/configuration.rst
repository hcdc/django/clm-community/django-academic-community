.. SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0
.. SPDX-License-Identifier: CC-BY-4.0

.. _configuration:

Configuration options
=====================

Configuration settings
----------------------

The following settings have an effect on the app.

.. automodulesumm:: academic_community.app_settings
    :autosummary-no-titles:
