.. SPDX-FileCopyrightText: 2020-2023 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. django-academic-community documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-academic-community's documentation!
=====================================================

|CI|
|Code coverage| |Docs|
|Latest Release|
|PyPI version|
|Code style: black|
|Imports: isort|
|PEP8|
|Checked with mypy|
|REUSE status|

.. rubric:: A Django app to manage members, topics, events, chats and more in an academic community

.. warning::

    This project is subject to active development. We apologize for the lack in
    documentation and hope to improve on this in the near future.

    Stay tuned for updates and discuss with us at
    https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   api
   contributing
   todo


How to cite this software
-------------------------

.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff


License information
-------------------
Copyright © 2020-2023 Helmholtz-Zentrum hereon GmbH

The source code of django-academic-community is licensed under
EUPL-1.2.

If not stated otherwise, the contents of this documentation is licensed under
CC-BY-4.0.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |CI| image:: https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community/badges/master/pipeline.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community/-/pipelines?page=1&scope=all&ref=master
.. |Code coverage| image:: https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community/badges/master/coverage.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community/-/graphs/master/charts
.. |Docs| image:: https://readthedocs.org/projects/django-academic-community/badge/?version=latest
   :target: https://django-academic-community.readthedocs.io/en/latest/
.. |Latest Release| image:: https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community/-/badges/release.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community
.. .. TODO: uncomment the following line when the package is published at https://pypi.org
.. .. |PyPI version| image:: https://img.shields.io/pypi/v/django-academic-community.svg
..    :target: https://pypi.python.org/pypi/django-academic-community/
.. |Code style: black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. |Imports: isort| image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. |PEP8| image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. |Checked with mypy| image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
.. |REUSE status| image:: https://api.reuse.software/badge/codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community
   :target: https://api.reuse.software/info/codebase.helmholtz.cloud/hcdc/django/clm-community/django-academic-community
